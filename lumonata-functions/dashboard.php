<?php

function get_dashboard()
{
    global $db;
    
    if( !is_user_logged() )
    {
        header( 'location:' . get_admin_url() );
    }

    set_template( TEMPLATE_PATH . '/template/dashboard.html', 'dashboard' );

    add_block( 'dashboard-block', 'd-block', 'dashboard' );

    add_actions( 'dashboard_content', 'default_dashboard_content', true );

    add_variable( 'title', 'Dashboard' );
    add_variable( 'more_link', get_state_url( 'ticket&sub=booking' ) );
    add_variable( 'dashboard_content', attemp_actions( 'dashboard_content' ) );
    
    add_actions( 'section_title', 'Dashboard' );

    parse_template( 'dashboard-block', 'd-block', false );

    return return_template( 'dashboard' );
}

function default_dashboard_content( $activate = true )
{
    if( $activate )
    {

    }
}

?> 