<?php

add_actions( 'menus_admin_page', 'menus_ajax' );

function run_menus_action()
{
    if( isset( $_POST['create_menu_set'] ) )
    {
        if( !empty( $_POST['setname'] ) )
        {
            add_menu_set();
        }
    }

    if( isset( $_POST['add_selected'] ) )
    {
        $json_menu_set = get_meta_data( 'menu_set', 'menus' );
            
        if( !empty( $json_menu_set ) )
        {
            $menu_set      = json_decode( $json_menu_set, true );
            $menu_set_keys = array_keys( $menu_set );
            $active_tab    = '';
            
            if( isset( $_POST['selected_menu'] ) )
            {
                if( empty( $_GET['tab'] ) )
                {
                    if( count( $menu_set_keys ) > 0 )
                    {
                        $active_tab = $menu_set_keys[0];
                    }
                }
                else
                {
                    $active_tab = $_GET['tab'];
                }
                
                if( add_menu_set_items( $active_tab ) )
                {
                    if( empty( $active_tab ) )
                    {
                        header( 'location:' . get_state_url( $_GET[ 'state' ] ) );
                    }
                    else
                    {
                        header( 'location:' . get_state_url( $_GET[ 'state' ] . '&tab=' . $active_tab ) );
                    }

                    exit;
                }
            }
        }
    }
}

function set_menus()
{
    run_menus_action();

    set_template( TEMPLATE_PATH . '/template/menus.html', 'menus' );

    add_block( 'menus-set-block', 'ms-block', 'menus' );
    add_block( 'menus-block', 'm-block', 'menus' );

    $json_menu_set = get_meta_data( 'menu_set', 'menus' );
    $menu_set      = json_decode( $json_menu_set, true );

    if( empty( $json_menu_set ) || count( $menu_set ) < 1 )
    {
        $the_sets = '
        <p>Please create the Menu Set first before you add the Application Items. To add new Menu Set, please fill the Set Name above and then click Create Menu Set.</p>
        <p>After you create the Menu Set, then choose the Application on the left side to add to the Menu that you created. Click Add To Menu Set to add the items to this Menu</p>
        <p>When you finish adding the application items, you can sorting the Menu Items by dragg and dropping it.</p>';
    }
    else
    {
        $menu_set_keys = array_keys( $menu_set );
                    
        $i = 0;
        
        if( empty( $_GET['tab'] ) )
        {
            if( count( $menu_set_keys ) > 0 )
            {
                $active_tab = $menu_set_keys[0];
            }
        }
        else
        {
            $active_tab = $_GET['tab'];
        }
                    
        foreach( $menu_set as $key => $val )
        {
            if( $i < 4 )
            {
                if( $key == $active_tab )
                {
                    add_variable( 'menu-class', 'active' );
                }
                else
                {
                    add_variable( 'menu-class', '' );
                }
            }
            elseif( $key == $active_tab )
            {
                add_variable( 'menu-class', 'active' );
            }

            add_variable( 'menu-index', $i );
            add_variable( 'menu-key', $key );
            add_variable( 'menu-name', $val );
            add_variable( 'menu-url', get_tab_url( $key ) );

            add_variable( 'ajax-url', get_menus_ajax_url() );
            add_variable( 'reload-url', get_state_url( 'menus' ) );

            parse_template( 'menus-set-block', 'ms-block', true );
            
            $i++;
        }
                    
        $i = 0;
        
        $view_more_tabs = false;
        $menu_set_2 = '';

        foreach( $menu_set as $key => $val )
        {
            if( $i == 4 )
            {
                $menu_set_2 = '
                <li class="view_more_tabs">
                    <a href="javascript:;" >
                        <img src="' . TEMPLATE_URL . '/images/ico-arrow.png" border="0" />
                    </a>
                    <ul id="sub_tab">';

                    if( $key != $active_tab )
                    {
                        $menu_set_2 .= '<li><a href="' . get_tab_url( $key ) . '">' . $val . '</a></li>';

                        $view_more_tabs = true;
                    }
            }
            elseif( $i > 4 and $key != $active_tab )
            {
                $menu_set_2 .= '<li><a href="' . get_tab_url( $key ) . '">' . $val . '</a></li>';

                $view_more_tabs = true;
            }
            
            $i++;
        }
                    
        if( $view_more_tabs )
        {
            $menu_set_2 .= '</ul></li>';
        }

        add_variable( 'menu_set_2', $menu_set_2 );

        $the_sets = '
        <div class="process-alert" id="procces_alert">Saving...</div>
        <input type="hidden" value="' . $active_tab . '" id="activetab" />
        <div class="wrap">';

            $menu_name = isset( $menu_set[ $active_tab ]) ? $menu_set[ $active_tab ] : '';

            $the_sets .= '
            <div class="edit-name-field">
                <span>Menu Name</span>
                <input id="menu-label-name" type="text" value="' . $menu_name . '" name="menu_name" class="medium_textbox" autocomplete="off">
                <input id="menu-label-key" type="hidden" value="' . $active_tab . '" name="menu_key" class="medium_textbox" autocomplete="off">
                <a class="edit-menu-name">Edit</a>
            </div>';

            $menu_items=json_decode( get_meta_data('menu_items_'. $active_tab, 'menus'), true );            
            $menu_order=json_decode( get_meta_data('menu_order_'. $active_tab, 'menus'), true );

            $the_sets .= '
            <ul id="theorder" class="page-list">
                ' . get_menu_items( $menu_items, $menu_order, 'theorder' ) . '
            </ul>';

            if( !empty( $menu_items ) )
            {        
                foreach( $menu_items as $key => $val )
                {            
                    $the_sets .= '
                    <script type="text/javascript">
                        jQuery(document).ready(function(){
                            jQuery("#view_' . $val['id'] . '").click(function(){
                                jQuery("#details_' . $val['id'] . '").slideToggle(100);
                                return false;
                            });

                            jQuery("#edit_items_' . $val['id'] . '").click(function(){
                                var prm = new Object;
                                    prm.ajax_key  = "edit-menu-item";
                                    prm.id        = "' . $val['id'] . '";
                                    prm.tab       = jQuery("#activetab").val();
                                    prm.link      = jQuery("#link_' . $val['id'] . '").val();
                                    prm.label     = jQuery("#label_' . $val['id'] . '").val();
                                    prm.target    = jQuery("#target_' . $val['id'] . '").val();
                                    prm.permalink = jQuery("#permalink_' . $val['id'] . '").val();
                                    prm.menu_type = jQuery("#menu_type_' . $val['id'] . '").val();

                                jQuery.post("' . get_menus_ajax_url() . '", prm, function( e ){
                                    jQuery("#procces_alert").show();

                                    if( e.result == "success" )
                                    {
                                        window.location.reload( true );
                                    }
                                }, "json");

                            });

                            jQuery("#remove_items_' . $val['id'] . '").click(function(){
                                var prm = new Object;
                                    prm.ajax_key  = "remove-menu-item";
                                    prm.id        = "' . $val['id'] . '";
                                    prm.tab       = jQuery("#activetab").val();

                                jQuery.post("' . get_menus_ajax_url() . '", prm, function( e ){
                                    jQuery("#procces_alert").show();

                                    if( e.result == "success" )
                                    {
                                        window.location.reload( true );
                                    }
                                }, "json");
                            });
                        });
                    </script>';
                }        
            }

            $the_sets .= '
            <script type="text/javascript">
                jQuery("input.urlNum1").keyup(function(){
                    var val = jQuery(this).val();

                    jQuery(this).parent().find("input.urlNum2").val( val );
                });

                jQuery("#theorder").nestedSortable({
                    opacity: .8,
                    maxLevels: 0,
                    handle: "div",
                    listType: "ul",
                    items: ".page-item1",
                    tolerance: "pointer",
                    stop: function(eve, ui){
                        var prm = new Object;
                            prm.ajax_key   = "reorder-menu";
                            prm.active_tab = "' . $active_tab . '";
                            prm.theorder   = jQuery("#theorder").nestedSortable("toHierarchy");

                        jQuery.ajax({
                            data: prm,
                            url: "' . get_menus_ajax_url() . '",
                            type: "POST"
                        }); 
                    }
                });

                jQuery(".edit-menu-name").click( function(){
                    var prm = new Object;
                        prm.ajax_key  = "edit-label-menu";
                        prm.menu_key  = jQuery("#menu-label-key").val();
                        prm.menu_name = jQuery("#menu-label-name").val();

                    jQuery.post("' . get_menus_ajax_url() . '", prm, function( e ){
                        jQuery("#procces_alert").show();

                        if( e.result == "success" )
                        {
                            window.location.reload( true );
                        }
                    }, "json");

                    return false;
                });
            </script>
        </div>';
        
        add_actions( 'css_elements', 'get_custom_css', TEMPLATE_URL . '/css/menu-list.css' );
        add_actions( 'js_elements', 'get_javascript', 'jquery.mjs.nestedSortable' );
    }

    $apps = run_actions( 'plugin_menu_post_type' );
    $type = '';

    if( !empty( $apps ) )
    {
        $type .= 'obj.plug_post_type.push("blogs");';

        foreach( $apps as $key => $val )
        {
            $type .= 'obj.plug_post_type.push("' . $key . '");';
        }
    }
    
    add_variable( 'plugin_menu_set', attemp_actions( 'plugin_menu_set' ) );
    add_variable( 'active_tab', isset( $_GET['tab'] ) ? $_GET['tab'] : '' );
    add_variable( 'published_pages', get_published_pages() );
    add_variable( 'ajax_url', get_menus_ajax_url() );
    add_variable( 'attemp_plug_post_type', $type );
    add_variable( 'current_page', current_page_url() );
    add_variable( 'img_url', get_theme_img() );
    add_variable( 'the_sets', $the_sets );

    add_actions( 'section_title', 'Menus' );

    parse_template( 'menus-block', 'm-block', 'menus' );

    return return_template( 'menus' );
}

function get_menus_ajax_url()
{    
    return get_state_url( 'ajax&apps=menus' );
}

function update_menu_order( $menu_set, $new_order = array() )
{
    return update_meta_data( 'menu_order_' . $menu_set, json_encode( $new_order ), 'menus' );
}

function edit_menu_items( $id, $menuset )
{
    if( empty( $menuset ) )
    {
        $json_menu_set = get_meta_data( 'menu_set', 'menus' );
        $menu_set      = json_decode( $json_menu_set, true );
        $menu_set_keys = array_keys( $menu_set );
        
        if( count( $menu_set_keys ) > 0 )
        {
            $menuset = $menu_set_keys[0];
        }
        else
        {
            return false;
        }
    }
    
    $menu_items = get_meta_data( 'menu_items_' . $menuset, 'menus' );
    $menu_items = json_decode( $menu_items, true );
    
    foreach( $menu_items as $key => $val )
    {
        if( is_array( $val ) )
        {
            if( $val['id'] == $id )
            {
                $new_menu_items[] = array(
                    'id'        => $val['id'],
                    'link'      => $_POST['link'],
                    'label'     => $_POST['label'],
                    'target'    => $_POST['target'],
                    'menu_type' => $_POST['menu_type'],
                    'permalink' => $_POST['permalink'] 
                );
            }
            else
            {
                $new_menu_items[] = array(
                    'id'        => $val['id'],
                    'link'      => $val['link'],
                    'label'     => $val['label'],
                    'target'    => $val['target'],
                    'menu_type' => $val['menu_type'],
                    'permalink' => $val['permalink'] 
                );
            }
        }
    }
    
    $edited_menu_items = json_encode( $new_menu_items );
    
    return update_meta_data( 'menu_items_' . $menuset, $edited_menu_items, 'menus' );
}

function remove_menu_items( $id, $menuset )
{
    if( empty( $menuset ) )
    {
        $json_menu_set = get_meta_data( 'menu_set', 'menus' );
        $menu_set      = json_decode( $json_menu_set, true );
        $menu_set_keys = array_keys( $menu_set );
        
        if( count( $menu_set_keys ) > 0 )
        {
            $menuset = $menu_set_keys[0];
        }
        else
        {
            return false;
        }
    }
    
    //-- Remove Menu Items
    $menu_items = get_meta_data( 'menu_items_' . $menuset, 'menus' );
    $menu_items = json_decode( $menu_items, true );
    $menu_order = get_meta_data( 'menu_order_' . $menuset, 'menus' );
    $menu_order = json_decode( $menu_order, true );
    
    foreach( $menu_items as $key => $val )
    {
        if( is_array( $val ) )
        {
            if( in_array( $val['id'], get_child_items( $id, $menu_order ) ) )
            {
                unset( $menu_items[$key] );
            }
        }
    }
    
    $menu_items = json_encode( $menu_items );
    
    if( update_meta_data( 'menu_items_' . $menuset, $menu_items, 'menus' ) )
    {
        
        //-- Remove Menu Order
        $new_order = remove_menu_orders( $id, $menuset, $menu_order );
        $new_order = json_encode( $new_order );
        
        return update_meta_data( 'menu_order_' . $menuset, $new_order, 'menus' );
    }
}

function get_child_items( $root_id, $menu_order = array() )
{
    $results = array();
    
    foreach( $menu_order as $key => $val )
    {
        if( find_is_match_array( $root_id, $menu_order[$key] ) )
        {
            $results = array(
                 $root_id 
            );
            
            if( isset( $val['children'] ) && is_array( $val['children'] ) )
            {
                foreach( $val['children'] as $keyc => $valc )
                {
                    $results = array_merge( $results, get_child_items( $valc['id'], $val['children'] ) );
                }
            }
        }
    }
    
    return $results;
}

function remove_menu_orders( $id, $menuset, &$menu_order = array() )
{
    
    if( empty( $menuset ) || $id == '' || empty( $menu_order ) )
    {
        return;
    }
    
    foreach( $menu_order as $key => &$val )
    {
        if( find_is_match_array( $id, $menu_order[$key] ) )
        {
            unset( $menu_order[$key] );
        }
        else
        {
            if( isset( $val['children'] ) && is_array( $val['children'] ) )
            {
                remove_menu_orders( $id, $menuset, $val['children'] );
            }
        }
    }
    
    return filter_order( $menu_order );
}

function filter_order( &$array = array() )
{
    foreach( $array as $key => &$val )
    {
        
        if( empty( $val['children'] ) )
        {
            unset( $val['children'] );
        }
        
        if( isset( $val['children'] ) && is_array( $val['children'] ) )
        {
            filter_order( $val['children'] );
        }
    }
    
    return $array;
}

function find_is_match_array( $id, $array = array() )
{
    if( $id == $array['id'] )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function add_menu_set_items( $active_tab, $is_single = false )
{
    if( empty( $active_tab ) )
    {
        return false;
    }
    
    if( !empty( $active_tab ) )
    {
        
        $menu_set_items        = get_meta_data( 'menu_items_' . $active_tab, 'menus' );
        $decode_menu_set_items = json_decode( $menu_set_items, true );
        
        if( count( $decode_menu_set_items ) == 0 )
        {
            $id = 0;
        }
        else
        {
            $tmp_arr = array();
            
            foreach( $decode_menu_set_items as $key_1 => $val_1 )
            {
                $tmp_arr[] = $val_1['id'];
            }
            
            $id = max( $tmp_arr ) + 1;
        }
        
        if( $is_single )
        {
            $_POST['id'] = $id;

            $the_menu_items[] = array_diff_key( $_POST, array_flip( array( 'tab', 'ajax_key' ) ) );
            $the_menu_order[] = array( 'id' => $id );
        }
        else
        {
            foreach( $_POST['selected_menu'] as $key => $value )
            {
                $post_data = array_diff_key( $_POST, array_flip( array( 'activetab', 'add_selected', 'curpage', 'selected_menu' ) ) );
                $item_data = array( 'id' => $id );

                foreach( $post_data as $idx => $obj )
                {
                    $item_data[ $idx ] = $obj[ $value ];
                }

                $the_menu_items[] = $item_data;
                $the_menu_order[] = array( 'id' => $id );

                $id++;
            }
        }
        
        $menu_items = get_meta_data( 'menu_items_' . $active_tab, 'menus' );
        $menu_order = get_meta_data( 'menu_order_' . $active_tab, 'menus' );
        
        if( empty( $menu_items ) )
        {
            set_meta_data( 'menu_items_' . $active_tab, json_encode( $the_menu_items ), 'menus' );
        }
        else
        {
            $the_menu_items = array_merge( json_decode( $menu_items, true ), $the_menu_items );

            update_meta_data( 'menu_items_' . $active_tab, json_encode( $the_menu_items ), 'menus' );
        }
        
        if( empty( $menu_order ) )
        {
            set_meta_data( 'menu_order_' . $active_tab, json_encode( $the_menu_order ), 'menus' );
        }
        else
        {
            $the_menu_order = array_merge( json_decode( $menu_order, true ), $the_menu_order );

            update_meta_data( 'menu_order_' . $active_tab, json_encode( $the_menu_order ), 'menus' );
        }
        
        return true;
    }
}

function add_menu_set()
{
    $current_menu_set = get_meta_data( 'menu_set', 'menus' );
    $menu_key         = generateSefUrl( rem_slashes( $_POST['setname'] ) );
    $menu_set         = array( $menu_key => $_POST['setname'] );
    
    if( empty( $current_menu_set ) )
    {
        $menu_set = json_encode( $menu_set );

        set_meta_data( 'menu_set', $menu_set, 'menus' );
    }
    else
    {
        $ob_menu_set     = json_decode( $current_menu_set, true );
        $menu_set        = array_merge( $ob_menu_set, $menu_set );
        $encode_menu_set = json_encode( $menu_set );
        
        update_meta_data( 'menu_set', $encode_menu_set, 'menus' );
    }
    
    header( 'location:' . get_tab_url( $menu_key ) );

    exit;
}

function remove_menu_set( $menu_set )
{
    //-- Remove menu set
    $json_menu_set = get_meta_data( 'menu_set', 'menus' );
    $ob_menu_set   = json_decode( $json_menu_set, true );
    
    foreach( $ob_menu_set as $key => $val )
    {
        if( $menu_set == $key )
        {
            unset( $ob_menu_set[$key] );

            break;
        }
    }
    
    $encode_menu_set = json_encode( $ob_menu_set );
    
    //-- Remove the menu items
    delete_meta_data( 'menu_order_' . $menu_set, 'menus' );
    delete_meta_data( 'menu_items_' . $menu_set, 'menus' );
    
    return update_meta_data( 'menu_set', $encode_menu_set, 'menus' );
}

function get_menu_items( $menu_items = array(), $menu_order = array(), $active_tab )
{
    $target = array( '_self' => 'Same Window', '_blank' => 'New Window' );
    $html   = '';
    
    if( is_array( $menu_order ) )
    {
        foreach( $menu_order as $key => $val )
        {
            $items_val = array_match( $menu_items, 'id', $val['id'] );

            $menu_type  = isset( $items_val[0]['menu_type'] ) ? $items_val[0]['menu_type'] : '';
            $permalink  = isset( $items_val[0]['permalink'] ) ? $items_val[0]['permalink'] : '';
            $target_val = isset( $items_val[0]['target'] ) ? $items_val[0]['target'] : '';
            $label_val  = isset( $items_val[0]['label'] ) ? $items_val[0]['label'] : '';
            $class_val  = isset( $items_val[0]['class'] ) ? $items_val[0]['class'] : '';
            $type_val   = isset( $items_val[0]['type'] ) ? $items_val[0]['type'] : '';
            $link_val   = isset( $items_val[0]['link'] ) ? $items_val[0]['link'] : '';
            
            $html .= '
			<li id="ele-' . $val['id'] . '" class="clear-element page-item1">
				<div class="sort-handle">
					<div class="apps_item">
						<div class="apps_item_text">' . $label_val . '</div>
						<div class="view" id="view_' . $val['id'] . '"></div>
					</div>
				</div>
				<div class="menuset_details_item" id="details_' . $val['id'] . '" style="display:none;">
					<p class="label">
						<label>Label :</label>
						<input type="text" value="' . $label_val . '" id="label_' . $val['id'] . '" name="title[' . $val['id'] . ']" class="medium_textbox" />
						<span class="clear-element"></span>
					</p>
                    <p class="label">
                        <label>CSS Class :</label>
                        <input type="text" value="' . $class_val . '" id="class_' . $val['id'] . '" name="class[' . $val['id'] . ']" class="medium_textbox" />
                        <span class="clear-element"></span>
                    </p>
					<p class="target">
						<label>Target :</label>
						<select id="target_' . $val['id'] . '" name="target[' . $val['id'] . ']">';
            
				            foreach( $target as $key_items => $val_items )
				            {
				                if( $key_items == $target_val )
				                {
				                    $html .= '<option value="' . $key_items . '" selected="selected">' . $val_items . '</option>';
				                }
				                else
				                {
				                    $html .= '<option value="' . $key_items . '">' . $val_items . '</option>';
				                }
				            }
				            
				            $html .= '
						</select>
						<span class="clear-element"></span>
					</p>';

                    $html .= '
                    <p class="label" ' . ( $menu_type == 'custom_url' ? '' : 'style="display:none;"' ) . '>
                        <label>URL :</label>
                        <input type="text" value="' . $link_val . '"  id="link_' . $val['id'] . '" name="link[' . $val['id'] . ']" class="medium_textbox urlNum1" />
                        <input type="hidden" value="' . $permalink . '" id="permalink_' . $val['id'] . '" name="permalink[' . $val['id'] . ']" class="urlNum2" />
                        <span class="clear-element"></span>
                    </p>

                    <input type="hidden" value="' . $menu_type . '"  id="menu_type_' . $val['id'] . '" name="menu_type[' . $val['id'] . ']" class="medium_textbox" />
                    <input type="hidden" value="' . $type_val . '"  id="type_' . $val['id'] . '" name="type[' . $val['id'] . ']" class="medium_textbox" />

					<div class="act">
						<input type="button" value="Remove" id="remove_items_' . $val['id'] . '" name="remove_menu[' . $val['id'] . ']" class="button" />
						<input type="button" value="Save" id="edit_items_' . $val['id'] . '" name="save_menu[' . $val['id'] . ']" class="button" />
					</div>
				</div>';
            
	            if( isset( $val['children'] ) )
	            {
	                if( is_array( $val['children'] ) )
	                {
	                    $html .= '
						<ul class="page-list">
							' . get_menu_items( $menu_items, $val['children'], $active_tab ) . '
						</ul>';
	                }
	            }
	            
	            $html .= '
			</li>';
        }
    }
    
    return $html;
}

function array_match( $array, $key, $value )
{
    $results = array();

    if( is_array( $array ) )
    {        
        if( isset( $array[$key] ) )
        {
            if( $array[$key] == $value )
            {
                $results[] = $array;
            }
        }
        
        foreach( $array as $subarray )
        {
            $results = array_merge( $results, array_match( $subarray, $key, $value ) );
        }
        
    }
    
    return $results;    
}

function get_published_pages()
{
    global $db;
    
    $html = '';
    
    $pub_pages = the_published_pages();
    
    if( $db->num_rows( $pub_pages ) > 0 )
    {
        while( $d = $db->fetch_array( $pub_pages ) )
        {
            $link  = '/?page_id=' . $d['larticle_id'];
            $ptype = 'published_pages';
            $plink = $d['lsef'] . '.html';

            $html .= '
            <li class="draggable">
            	<div class="apps_item">
            		<div class="apps_item_text">' . $d['larticle_title'] . '</div>
            		<div class="add" id="add_' . $d['larticle_id'] . '">
            			<input type="checkbox" name="selected_menu[]" class="selected_menu" value="' . $d['larticle_id'] . '" />
            		</div>
            		<div class="view" id="view_' . $d['larticle_id'] . '"></div>
            	</div>
            	<div class="apps_details_item" id="details_set_' . $d['larticle_id'] . '" style="display:none;">
            		<p>
            			<label>Label:</label>
            			<input type="text" value="' . $d['larticle_title'] . '" id="label_' . $d['larticle_id'] . '" name="label[' . $d['larticle_id'] . ']" class="medium_textbox" />
            		</p>
            		<p>
            			<label>Target:</label>
            			<select id="target_' . $d['larticle_id'] . '" name="target[' . $d['larticle_id'] . ']">
            				<option value="_self">Same Window</option>
            				<option value="_blank">New Window</option>
            			</select>
            		</p>
            		<div style="text-align:right;">
            			<input type="button" id="add_to_menu_set_' . $d['larticle_id'] . '" value="Add to menu set" name="add[' . $d['larticle_id'] . ']" class="button" />
            		</div>
            		<input type="hidden" value="' . $link . '" id="link_' . $d['larticle_id'] . '" name="link[' . $d['larticle_id'] . ']" />
            		<input type="hidden" value="' . $plink . '" id="permalink_' . $d['larticle_id'] . '" name="permalink[' . $d['larticle_id'] . ']" />
                    <input type="hidden" value="' . $ptype . '" id="menu_type_' . $d['larticle_id'] . '" name="menu_type[' . $d['larticle_id'] . ']" />
                    <input type="hidden" value="' . $d['larticle_type'] . '" id="type_' . $d['larticle_id'] . '" name="type[' . $d['larticle_id'] . ']" />
                    <input type="hidden" value="' . $d['larticle_id'] . '" id="page_id_' . $d['larticle_id'] . '" name="page_id[' . $d['larticle_id'] . ']" />
            	</div>
            </li>
            <script type="text/javascript">
            	jQuery(document).ready(function(){
    				jQuery("#view_' . $d['larticle_id'] . '").click(function(){
    					jQuery("#details_set_' . $d['larticle_id'] . '").slideToggle(100);
    					return false;
    				});

    				jQuery("#add_to_menu_set_' . $d['larticle_id'] . '").click(function(){
    					jQuery("#procces_alert").show();

                        var prm = new Object;
                            prm.ajax_key  = "add-single-selected-menu";
                            prm.tab       = jQuery("#activetab").val();
                            prm.link      = jQuery("#link_' . $d['larticle_id'] . '").val();
                            prm.type      = jQuery("#type_' . $d['larticle_id'] . '").val();
                            prm.label     = jQuery("#label_' . $d['larticle_id'] . '").val();
                            prm.target    = jQuery("#target_' . $d['larticle_id'] . '").val();
                            prm.page_id   = jQuery("#page_id_' . $d['larticle_id'] . '").val();
                            prm.permalink = jQuery("#permalink_' . $d['larticle_id'] . '").val();
                            prm.menu_type = jQuery("#menu_type_' . $d['larticle_id'] . '").val();

                        jQuery.post("' . get_menus_ajax_url() . '", prm, function( e ){
                            if( e.result == "success")
                            {
                                window.location.reload( true );
                            }
                        }, "json");
    				});
    			});
    		</script>';        
        }
    }
    else
    {
        $html .= '
        <li class="draggable">
            <span>No data was found</span>
        </li>';
    }
    
    return $html;  
}

function get_published_post( $app_name = '' )
{
    global $db;
    
    $html = '';
    
    $pub_post = the_published_post( $app_name );
    
    if( $db->num_rows( $pub_post ) > 0 )
    {
        while( $d = $db->fetch_array( $pub_post ) )
        {
            $link  = '/?page_id=' . $d['larticle_id'];
            $ptype = 'published_post';
            $plink = $d['larticle_type'] . '/' . $d['lsef'] . '.html';

            $html .= '
            <li class="draggable">
            	<div class="apps_item">
            		<div class="apps_item_text">' . $d['larticle_title'] . '</div>
            		<div class="add" id="add_' . $d['larticle_id'] . '">
            			<input type="checkbox" name="selected_menu[]" class="selected_menu" value="' . $d['larticle_id'] . '" />
            		</div>
            		<div class="view" id="view_' . $d['larticle_id'] . '"></div>
            	</div>
            	<div class="apps_details_item" id="details_set_' . $d['larticle_id'] . '" style="display:none;">
            		<p>
            			<label>Label:</label>
            			<input type="text" value="' . $d['larticle_title'] . '" id="label_' . $d['larticle_id'] . '" name="label[' . $d['larticle_id'] . ']" class="medium_textbox" />
            		</p>
            		<p>
            			<label>Target:</label>
            			<select id="target_' . $d['larticle_id'] . '" name="target[' . $d['larticle_id'] . ']">
            				<option value="_self">Same Window</option>
            				<option value="_blank">New Window</option>
            			</select>
            		</p>
            		<div style="text-align:right;">
            			<input type="button" id="add_to_menu_set_' . $d['larticle_id'] . '" value="Add to menu set" name="add[' . $d['larticle_id'] . ']" class="button" />
            		</div>
            		<input type="hidden" value="' . $link . '" id="link_' . $d['larticle_id'] . '" name="link[' . $d['larticle_id'] . ']" />
            		<input type="hidden" value="' . $plink . '" id="permalink_' . $d['larticle_id'] . '" name="permalink[' . $d['larticle_id'] . ']" />
                    <input type="hidden" value="' . $ptype . '" id="menu_type_' . $d['larticle_id'] . '" name="menu_type[' . $d['larticle_id'] . ']" />
                    <input type="hidden" value="' . $d['larticle_type'] . '" id="type_' . $d['larticle_id'] . '" name="type[' . $d['larticle_id'] . ']" />
                    <input type="hidden" value="' . $d['larticle_id'] . '" id="page_id_' . $d['larticle_id'] . '" name="page_id[' . $d['larticle_id'] . ']" />
            	</div>
            </li>
            <script type="text/javascript">
            	jQuery(document).ready(function(){
    				jQuery("#view_' . $d['larticle_id'] . '").click(function(){
    					jQuery("#details_set_' . $d['larticle_id'] . '").slideToggle(100);
    					return false;
    				});

    				jQuery("#add_to_menu_set_' . $d['larticle_id'] . '").click(function(){
    					jQuery("#procces_alert").show();

                        var prm = new Object;
                            prm.ajax_key  = "add-single-selected-menu";
                            prm.tab       = jQuery("#activetab").val();
                            prm.link      = jQuery("#link_' . $d['larticle_id'] . '").val();
                            prm.type      = jQuery("#type_' . $d['larticle_id'] . '").val();
                            prm.label     = jQuery("#label_' . $d['larticle_id'] . '").val();
                            prm.target    = jQuery("#target_' . $d['larticle_id'] . '").val();
                            prm.page_id   = jQuery("#page_id_' . $d['larticle_id'] . '").val();
                            prm.permalink = jQuery("#permalink_' . $d['larticle_id'] . '").val();
                            prm.menu_type = jQuery("#menu_type_' . $d['larticle_id'] . '").val();;

    					jQuery.post("' . get_menus_ajax_url() . '", prm, function( e ){
    						if( e.result == "success")
                            {
                                window.location.reload( true );
    						}
    					}, "json");
    				});
    			});
    		</script>';        
        }
    }
    else
    {
        $html .= '
        <li class="draggable">
            <span>No data was found</span>
        </li>';
    }
    
    return $html;
}

function get_published_apps( $app_name = '' )
{
    global $db;
    
    $html = '';
    
    $pub_apps = the_published_apps( $app_name );
    
    if( $db->num_rows( $pub_apps ) > 0 )
    {
        while( $d = $db->fetch_array( $pub_apps ) )
        {
            $link  = '/?app_name=' . $d['lgroup'] . '&amp;cat_id=' . $d['lrule_id'];
            $plink = $d['lgroup'] . '/' . $d['lsef'] . '.html';
            $ptype = 'published_apps';
            
            $html .= '
            <li class="draggable">
            	<div class="apps_item">
            		<div class="apps_item_text">' . $d['lname'] . '</div>
            		<div class="add" id="add_' . $d['lrule_id'] . '">
            			<input type="checkbox" name="selected_menu[]"  class="selected_menu" value="' . $d['lrule_id'] . '" />
            		</div>
            		<div class="view" id="view_' . $d['lrule_id'] . '"></div>
            	</div>
            	<div class="apps_details_item" id="details_set_' . $d['lrule_id'] . '" style="display:none;">
            		<p>
            			<label>Label:</label>
            			<input type="text" id="label_' . $d['lrule_id'] . '" value="' . $d['lname'] . '" name="label[' . $d['lrule_id'] . ']" class="medium_textbox" />
            		</p>
            		<p>
            			<label>Target:</label>
        				<select id="target_' . $d['lrule_id'] . '" name="target[' . $d['lrule_id'] . ']">
        					<option value="_self">Same Window</option>
        					<option value="_blank">New Window</option>
        				</select>
        			</p>
        			<div style="text-align:right;">
        				<input id="add_to_menu_set_' . $d['lrule_id'] . '" type="button" value="Add to menu set" name="add[' . $d['lrule_id'] . ']" class="button" />
        			</div>
        			<input type="hidden" id="link_' . $d['lrule_id'] . '" value="' . $link . '" name="link[' . $d['lrule_id'] . ']" />
        			<input type="hidden" id="permalink_' . $d['lrule_id'] . '" value="' . $plink . '" name="permalink[' . $d['lrule_id'] . ']" />
                    <input type="hidden" id="type_' . $d['lrule_id'] . '" value="' . $d['lgroup'] . '" name="type[' . $d['lrule_id'] . ']" />
                    <input type="hidden" id="menu_type_' . $d['lrule_id'] . '" value="' . $ptype . '" name="menu_type[' . $d['lrule_id'] . ']" />
                    <input type="hidden" id="rule_id_' . $d['lrule_id'] . '" value="' . $d['lrule_id'] . '" name="rule_id[' . $d['lrule_id'] . ']" />
        		</div>';
            
            	$active_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : '';
            
            	$html .= '
            	<script type="text/javascript">
            		jQuery(document).ready(function(){
    					jQuery("#view_' . $d['lrule_id'] . '").click(function(){
    						jQuery("#details_set_' . $d['lrule_id'] . '").slideToggle(100);
    						return false;
    					});

    					jQuery("#add_to_menu_set_' . $d['lrule_id'] . '").click(function(){
    						jQuery("#procces_alert").show();

                            var prm = new Object;
                                prm.ajax_key  = "add-single-selected-menu";
                                prm.tab       = jQuery("#activetab").val();
                                prm.link      = jQuery("#link_' . $d['lrule_id'] . '").val();
                                prm.type      = jQuery("#type_' . $d['lrule_id'] . '").val();
                                prm.label     = jQuery("#label_' . $d['lrule_id'] . '").val();
                                prm.target    = jQuery("#target_' . $d['lrule_id'] . '").val();
                                prm.rule_id   = jQuery("#rule_id_' . $d['lrule_id'] . '").val();
                                prm.permalink = jQuery("#permalink_' . $d['lrule_id'] . '").val();
                                prm.menu_type = jQuery("#menu_type_' . $d['lrule_id'] . '").val();;

    						jQuery.post("' . get_menus_ajax_url() . '", prm, function( e ){
    							if( e.result = "success" )
                                {
    								window.location.reload( true );
    							}
    						}, "json");
    					});
    				});
    			</script>
    		</li>';        
        }
    }
    else
    {
        $html .= '
        <li class="draggable">
            <span>No data was found</span>
        </li>';
    }
    
    return $html;    
}

function get_published_post_archive( $apps = array() )
{
    global $db;
    
    $html    = '';
    $archive = the_post_archive();
    
    if( $db->num_rows( $archive ) > 0 )
    {
        $no = 0;

        while( $d = $db->fetch_array( $archive ) )
        {
            $name  = ( array_key_exists( $d['larticle_type'], $apps ) ? $apps[ $d['larticle_type'] ] : ucfirst( $d['larticle_type'] ) );
            $link  = $d['larticle_type'] . '/';
            $plink = $d['larticle_type'] . '/';
            $ptype = 'post_archive';

            $html .= '
            <li class="draggable">
            	<div class="apps_item">
            		<div class="apps_item_text">' . $name . '</div>
            		<div class="add" id="add_' . $no . '">
            			<input type="checkbox" name="selected_menu[]" class="selected_menu" value="' . $no . '" />
            		</div>
            		<div class="view" id="view_' . $no . '"></div>
            	</div>
            	<div class="apps_details_item" id="details_set_' . $no . '" style="display:none;">
            		<p>
            			<label>Label:</label>
            			<input type="text" value="' . $name . '" id="label_' . $no . '" name="label[' . $no . ']" class="medium_textbox" />
            		</p>
            		<p>
            			<label>Target:</label>
            			<select id="target_' . $no . '" name="target[' . $no . ']">
            				<option value="_self">Same Window</option>
            				<option value="_blank">New Window</option>
            			</select>
            		</p>
            		<div style="text-align:right;">
            			<input type="button" id="add_to_menu_set_' . $no . '" value="Add to menu set" name="add[' . $no . ']" class="button" />
            		</div>
            		<input type="hidden" value="' . $link . '" id="link_' . $no . '" name="link[' . $no . ']" />
            		<input type="hidden" value="' . $plink . '" id="permalink_' . $no . '" name="permalink[' . $no . ']" />
                    <input type="hidden" value="' . $d['larticle_type'] . '" id="type_' . $no . '" name="type[' . $no . ']" />
                    <input type="hidden" value="' . $ptype . '" id="menu_type_' . $no . '" name="menu_type[' . $no . ']" />
            	</div>
            </li>
            <script type="text/javascript">
            	jQuery(document).ready(function(){
    				jQuery("#view_' . $no . '").click(function(){
    					jQuery("#details_set_' . $no . '").slideToggle(100);
    					return false;
    				});

    				jQuery("#add_to_menu_set_' . $no . '").click(function(){
    					jQuery("#procces_alert").show();

                        var prm  = new Object;
                            prm.ajax_key  = "add-single-selected-menu";
                            prm.tab       = jQuery("#activetab").val(),
                            prm.link      = jQuery("#link_' . $no . '").val();
                            prm.type      = jQuery("#type_' . $no . '").val();
                            prm.label     = jQuery("#label_' . $no . '").val();
                            prm.target    = jQuery("#target_' . $no . '").val();
                            prm.permalink = jQuery("#permalink_' . $no . '").val();
                            prm.menu_type = jQuery("#menu_type_' . $no . '").val();;

    					jQuery.post("' . get_menus_ajax_url() . '", prm, function( e ){
    						if( e.result == "success" )
                            {
    							window.location.reload( true );
    						}
    					}, "json");
    				});
    			});
    		</script>';

    		$no++; 
        }
    }
    else
    {
        $html .= '
        <li class="draggable">
            <span>No data was found</span>
        </li>';
    }

    return $html;
}

function get_custome_url()
{
    $html = '
    <li class="draggable">
        <div class="apps_details_item" >
            <p>
                <label>Label:</label>
                <input type="text" value="" id="label" name="label" class="medium_textbox" />
            </p>
            <p>
                <label>URL:</label>
                <input type="text" value="" id="link" name="link" class="medium_textbox" />
                <i>( include the http:// )</i>
            </p>
            <p>
                <label>Target:</label>
                <select name="target" id="target" style="width:100%;">
                    <option value="_self">Same Window</option>
                    <option value="_blank">New Window</option>
                </select>
            </p>
            <input type="hidden" value="" id="type" name="type" />
            <input type="hidden" value="custom_url" id="menu_type" name="menu_type" />
            <div style="text-align:right;">
                <input type="button" value="Add to menu set" id="add_to_menu_set" name="add" class="button" />
            </div>
        </div>
    </li>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery("#add_to_menu_set").click(function(){
                var url = "' . get_menus_ajax_url() . '";
                var prm = new Object();
                    prm.ajax_key  = "add-single-selected-menu";
                    prm.tab       = jQuery("#activetab").val(),
                    prm.link      = jQuery("#link").val();
                    prm.type      = jQuery("#type").val();
                    prm.permalink = jQuery("#link").val();
                    prm.label     = jQuery("#label").val();
                    prm.target    = jQuery("#target").val();
                    prm.menu_type = jQuery("#menu_type").val();

                jQuery("#procces_alert").show();

                jQuery.post( url, prm ,function( e ){
                    if( e.result == "success" )
                    {
                        window.location.reload( true );
                    }
                }, "json");
            });
        });
    </script>';
    
    return $html;    
}

function the_menus( $args = '' )
{
    if( empty( $args ) )
    {
        return;
    }
    
    $var['menuset']    = ' ';
    $var['style']      = 'li';
    $var['show_title'] = "true";
    $var['addClass']   = 'theMenu';
    $var['addID']      = 'navMenu-' . rand( 10, 10000 );
    
    if( !empty( $args ) )
    {
        $args = explode( '&', $args );
        
        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );
            
            if( $variable == 'menuset' || $variable == 'style' || $variable == 'show_title' || $variable == 'addClass' || $variable == 'addID' )
            {
                $var[$variable] = $value;
            }
        }
    }
    
    $menuset = get_meta_data( 'menu_set', 'menus' );
    $menuset = json_decode( $menuset, TRUE );
    
    if( !is_array( $menuset ) )
    {
        return;
    }
    
    foreach( $menuset as $key => $val )
    {
        if( strtolower( $val ) == strtolower( $var['menuset'] ) )
        {
            $menuset_key = $key;
            $set_name    = $val;
            break;
        }
    }
    
    if( !isset( $menuset_key ) )
    {
        return;
    }
    
    $menu_items = get_meta_data( 'menu_items_' . $menuset_key, 'menus' );
    $menu_items = json_decode( $menu_items, TRUE );
    
    $menu_order = get_meta_data( 'menu_order_' . $menuset_key, 'menus' );
    $menu_order = json_decode( $menu_order, TRUE );
    
    $return = '';
    
    if( $var['show_title'] == 'true' )
    {
        if( !empty( $var['set_name'] ) )
        {
            $return = '<h2>' . $var['set_name'] . '</h2>';
        }
        else
        {
            $return = '<h2>' . $set_name . '</h2>';
        }
    }
    
    $return .= fetch_menu_set_items( $menu_items, $menu_order, $var['style'], $var['addClass'], $var['addID'] );
    
    return $return;
}

function the_page_menu( $home = true )
{    
    global $db;
    
    $query  = $db->prepare_query( "SELECT * FROM lumonata_articles WHERE larticle_type='pages' AND larticle_status='publish' AND lshare_to=0 ORDER BY lorder" );    
    $result = $db->do_query( $query );
    
    $menu = "
    <ul>";
    
	    if( $home )
	    {
	        $menu .= "<li><a href=\"" . site_url() . "\">Home</a></li>";
	    }
	    
	    while( $data = $db->fetch_array( $result ) )
	    {
	        if( is_permalink() )
	        {
	            $menu .= "<li><a href=\"" . site_url( $data['lsef'] ) . "\">" . $data['larticle_title'] . "</a></li>";
	        }	        
	        else
	        {
	            $menu .= "<li><a href=\"" . site_url( '?page_id=' . $data['larticle_id'] ) . "\">" . $data['larticle_title'] . "</a></li>";
	        }	        
	    }
    
    	$menu .= "
    </ul>";
    
    return $menu;    
}

function fetch_menu_set_items( $menu_items = array(), $menu_order = array(), $style, $addClass = '', $addID = '' )
{
    $return = '';
    $i      = 0;
    
    if( is_array( $menu_order ) )
    {
        $liOpen  = '';
        $liClose = '';
        $ulOpen  = '';
        $ulClose = '';
        
        if( $style == 'li' )
        {
            $return .= '' . $ulOpen . '<ul class="' . $addClass . '" id="' . $addID . '" >';
        }

        foreach( $menu_order as $key => $val )
        {
            $items_val = array_match( $menu_items, 'id', $val['id'] );

            if( empty( $items_val ) )
            {
                continue;
            }

            if( is_permalink() )
            {
                if( substr( $items_val[0]['permalink'], 0, 4 ) == "http" )
                {
                    $link = $items_val[0]['permalink'];
                }
                else
                {
                    $link = site_url( $items_val[0]['permalink'] );
                    
                    $active_plugin = get_active_plugin( 'plugins' );

                    if( isset( $active_plugin['lumonata-destination-application'] ) )
                    {
                        $id = explode( '=', $items_val[0]['link'] );

                        if( isset( $id[1] ) )
                        {
                            $dest = get_additional_field_relationship( $id[1], 'destination' );
                            
                            if( !empty( $dest ) )
                            {
                                $link = site_url( $dest . '/' . $items_val[0]['permalink'] );
                            }
                        }
                    }
                }
            }
            else
            {
                if( substr( $items_val[0]['permalink'], 0, 4 ) == "http" )
                {
                    $link = $items_val[0]['link'];
                }
                else
                {
                    $link = site_url( $items_val[0]['link'] );
                    
                    $active_plugin = get_active_plugin( 'plugins' );
                    
                    if( isset( $active_plugin['lumonata-destination-application'] ) )
                    {
                        $id = explode( '=', $items_val[0]['link'] );
                        if( isset( $id[1] ) )
                        {
                            $dest = get_additional_field_relationship( $id[1], 'destination' );
                            
                            if( !empty( $dest ) )
                            {
                                $link = site_url( $dest . '/' . $items_val[0]['permalink'] );
                            }
                        }
                    }
                }
            }
            
            // SET SELECTED MENU --
            $selected = ( is_active_menu( $link ) ? 'active' : '' );
            
            if( $style == 'li' )
            {
                $cls = generateSefUrl( $items_val[0]['label'] );
                
                if( isset( $val['children'] ) && is_array( $val['children'] ) )
                {
                    $return .= "<li class='" . $selected . ' ' . $cls . "'>" . $liOpen . "<a href=\"" . $link . "\">" . $items_val[0]['label'] . "</a>";
                }
                else
                {
                    $return .= "<li class='" . $selected . ' ' . $cls . "'><a href=\"" . $link . "\">" . $items_val[0]['label'] . "</a>";
                }
            }
            else
            {
                $return .= "<div><a class='" . $addClass . "' href=\"" . $link . "\">" . $items_val[0]['label'] . "</a>";
            }
            
            if( isset( $val['children'] ) && is_array( $val['children'] ) )
            {
                if( $style == 'li' )
                {
                    $return .= fetch_menu_set_items( $menu_items, $val['children'], $style ) . "" . $liClose . "</li>";
                }
                else
                {
                    $return .= fetch_menu_set_items( $menu_items, $val['children'], $style ) . "</div>";
                }
            }
            else
            {
                if( $style == 'li' )
                {
                    $return .= "</li>";
                }
                else
                {
                    $return .= "</div>";
                }
            }
            
            $i++;
        }
        
        if( $style == 'li' )
        {
            $return .= "</ul>" . $ulClose;
        }
    }
    
    return $return;
}


function get_the_categories( $args = '' )
{    
    global $db;
    
    $var['order']         = 'ASC';
    $var['type']          = 'li';
    $var['app_name']      = ''; 
    $var['category_name'] = '';
    $var['sef']           = '';
    $var['parent_id']     = 0;

    if( !empty( $args ) )
    {        
        $args = explode( '&', $args );
        
        foreach( $args as $val )
        {            
            list( $variable, $value ) = explode( '=', $val );
            
            if( $variable == 'app_name' || $variable == 'parent_id' || $variable == 'type' || $variable == 'order' || $variable == 'category_name' || $variable == 'sef' )
            {                
                $var[$variable] = $value;
            }            
        }        
    }
    
    if( !empty( $var['category_name'] ) && !empty( $var['app_name'] ) )
    {        
        $query  = $db->prepare_query( "SELECT a.* FROM lumonata_rules a WHERE a.lname=%s AND a.lrule='categories' AND a.lgroup=%s AND", $var['category_name'], $var['app_name'] );
        $result = $db->do_query( $query );        
        $data   = $db->fetch_array( $result );
        
        $var['parent_id'] = $data['lrule_id'];        
    }
    elseif( !empty( $var['sef'] ) && !empty( $var['app_name'] ) )
    {
        $query  = $db->prepare_query( "SELECT a.* FROM lumonata_rules a WHERE lsef=%s AND lrule='categories' AND lgroup=%s AND", $var['sef'], $var['app_name'] );
        $result = $db->do_query( $query );        
        $data  = $db->fetch_array( $result );
        
        $var['parent_id'] = $data['lrule_id'];
    }
    
    return recursive_taxonomy( 0, 'categories', $var['app_name'], $var['type'], array(), $var['order'], $var['parent_id'], 0, true );
}

function menus_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'add-single-selected-menu' )
    {
        $json_menu_set = get_meta_data( 'menu_set', 'menus' );
            
        if( !empty( $json_menu_set ) )
        {
            $menu_set      = json_decode( $json_menu_set, true );
            $menu_set_keys = array_keys( $menu_set );
            $active_tab    = '';
            
            if( empty( $_POST['tab'] ) )
            {
                if( count( $menu_set_keys ) > 0 )
                {
                    $active_tab = $menu_set_keys[0];
                }
            }
            else
            {
                $active_tab = $_POST['tab'];
            }
            
            if( add_menu_set_items( $active_tab, true ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'reorder-menu' )
    {
        update_menu_order( $_POST['active_tab'], $_POST['theorder'] );
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'edit-menu-item' )
    {
        if( edit_menu_items( $_POST['id'], $_POST['tab'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'remove-menu-item' )
    {
        if( remove_menu_items( $_POST['id'], $_POST['tab'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'edit-label-menu' )
    {
        $menu_set = json_decode( get_meta_data( 'menu_set', 'menus' ), true );

        if( isset( $menu_set[ $_POST['menu_key'] ] ) && $_POST['menu_name'] != '' )
        {
            $menu_set[ $_POST['menu_key'] ] = $_POST['menu_name'];

            if( update_meta_data( 'menu_set', json_encode( $menu_set ), 'menus' ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
        }
    }

    if( isset( $_POST['app_set_name'] ) )
    {
        if( $_POST['app_set_name'] == 'pages' )
        {
            echo get_published_pages();
        }
        elseif( $_POST['app_set_name'] == 'archive' )
        {
            echo get_published_post_archive( $_POST['plug_post_type'] );
        }
        elseif( $_POST['app_set_name'] == 'url' )
        {
            echo get_custome_url();
        }
        elseif( in_array( $_POST['app_set_name'], $_POST['plug_post_type'] ) )
        {
            echo get_published_post( $_POST['app_set_name'] );
        }
        else
        {
            echo get_published_apps( $_POST['app_set_name'] );
        }
    }

    if( isset( $_POST['removed_menu_set'] ) )
    {
        if( remove_menu_set( $_POST['removed_menu_set'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    exit;
}

?>