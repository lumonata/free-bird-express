<?php

add_actions( 'media_admin_page', 'media_ajax' );

function get_upload_media()
{
    extract( $_GET );

    set_template( TEMPLATE_PATH . '/template/media.html', 'media' );

    add_block( 'image-media-url-block', 'i-md-block','media' );
    add_block( 'other-media-url-block', 'o-md-block','media' );
    add_block( 'gallery-media-block', 'g-md-block', 'media' );
    add_block( 'upload-media-block', 'up-md-block', 'media' );
    add_block( 'media-block', 'md-block', 'media' );

    if( isset( $_POST['insert'] ) && !is_array( $_POST['insert'] ) )
    {
        edit_attachment( $_POST['attachment_id'], $_POST['title'], $_POST['order'], $_POST['alt_text'], $_POST['caption'] );
    }

    if( isset( $tab ) && $tab == 'from-computer' )
   	{
        add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/dropzone@5.9.3/dist/min/dropzone.min.css' );
        add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/dropzone@5.9.3/dist/min/dropzone.min.js' );

        parse_template( 'upload-media-block', 'up-md-block' ); 
   	}
   	elseif( isset( $tab ) && $tab == 'from-url' )
   	{
        add_variable( 'insert_button', button( 'button=insert&type=button' ) );

        if( $type == 'image' )
        {
            add_variable( 'type-label', 'Image' );

            parse_template( 'image-media-url-block', 'i-md-block' );
        }
        else
        {
            if( $type == 'flash' )
            {
                add_variable( 'type-label', 'Flash' );
            }
            elseif( $type == 'video')
            {
                add_variable( 'type-label', 'Video' );
            }
            elseif( $type == 'music' )
            {
                add_variable( 'type-label', 'Music' );
            }
            elseif( $type == 'pdf' )
            {
                add_variable( 'type-label', 'PDF' );
            }
            elseif( $type == 'doc' )
            {
                add_variable( 'type-label', 'Document' );
            }

            parse_template( 'other-media-url-block', 'o-md-block' ); 
        }
   	}
   	elseif( isset( $tab ) && in_array( $tab, array( 'gallery', 'library' ) ) )
   	{
        if( is_delete_all_media() )
        {
            add_variable( 'attachment', delete_attachment_form() );
        }
        else
        {
            if( is_confirm_delete_media() )
            {
                if( isset( $_POST['id'] ) && is_array( $_POST['id'] ) )
                {
                    foreach( $_POST['id'] as $key => $val )
                    {
                        delete_attachment( $val );
                    }
                }
                elseif( isset( $_POST['delete_id'] ) )
                {
                    delete_attachment( $_POST['delete_id'] );
                }
            }

            if( is_save_all_changes_media() )
            {
                foreach( $_POST['attachment_id'] as $i => $attachment_id )
                {
                    edit_attachment( $attachment_id, $_POST['title'][ $i ], $_POST['order'][ $i ], $_POST['alt_text'][ $i ], $_POST['caption'][ $i ] );
                }

                add_variable( 'response', '<div class="alert_green">Update process successfully.</div>' );
            }

            if( $tab == 'gallery' )
            {
                add_variable( 'attachment', get_attachment( $post_id ) );
            }
            else
            {
                add_variable( 'attachment', get_attachment() );
            }
        }
        
        parse_template( 'gallery-media-block', 'g-md-block' );
   	}
    
    add_variable( 'tab', $tab );
    add_variable( 'type', $type );
    add_variable( 'post-id', $post_id );
    add_variable( 'textarea-id', $textarea_id );

    add_variable( 'tabs', set_media_tab() );
    add_variable( 'admin-url', get_admin_url() );
    add_variable( 'ajax-url', get_media_ajax_url() );
    add_variable( 'template-url', template_url( null, true ) );
    
    parse_template( 'media-block', 'md-block' ); 
    print_template( 'media' );
} 

/*
| -----------------------------------------------------------------------------
| Media - Set Tabs
| -----------------------------------------------------------------------------
*/
function set_media_tab()
{
    extract( $_GET );

    $tab_array = array(
        'from-computer' => 'From Computer',
        'from-url'      => 'From External URL',
        'gallery'       => 'Gallery (' . count_attachment( $post_id ) . ')',
        'library'       => 'Library (' . count_attachment() . ')'
    );

    $tab_media  = '';
    $tab_active = isset( $tab ) ? $tab : 'from-computer';

    foreach( $tab_array as $key => $name )
    {
        $tab_media .= '
        <li class="' . ( $tab_active == $key ? 'active' : '' ) . '">
            <a href="' . site_url( 'lumonata-admin/?state=media&tab=' . $key . '&post_id=' . $post_id . '&apps=' . $apps . '&type=' . $type . '&textarea_id=' . $textarea_id ) . '">
                ' . $name . '
            </a>
        </li>';
    }

    return $tab_media;
}

function add_media_attachment()
{
    global $db;

    $data = array();

    if( isset( $_FILES[ 'file' ]['error'] ) && $_FILES[ 'file' ]['error'] == 0 )
    {
        //-- Create destination folder if folder is not exist yet
        if( is_dir( FILES_PATH .'/'. upload_folder_name() ) === FALSE )
        {
            if( create_dir( FILES_PATH .'/'. upload_folder_name() ) === FALSE )
            {
                return $data;
            }
        }

        $file_name   = $_FILES[ 'file' ][ 'name' ];
        $file_size   = $_FILES[ 'file' ][ 'size' ];
        $file_type   = $_FILES[ 'file' ][ 'type' ];
        $file_source = $_FILES[ 'file' ][ 'tmp_name' ];
        
        extract( $_POST );

        if( is_allow_file_type( $file_type, $type ) )
        {
            if( is_allow_file_size( $file_size ) )
            {
                //-- If file type is Image
                if( $type == 'image' )
                {
                    if( upload_image_attachment( $file_source, $file_type, $file_name, $post_id ) )
                    {
                        $attach_id   = $db->insert_id();
                        $title_file  = file_name_filter( $file_name );
                        $upload_date = date( get_date_format(), time() );

                        $data[] = get_attachment_by_id( $attach_id );
                    }                            
                }
                else
                {
                    if( upload_media_attachment( $file_source, $file_type, $file_name, $post_id ) )
                    {
                        $attach_id   = $db->insert_id();
                        $title_file  = file_name_filter( $file_name );
                        $upload_date = date( get_date_format(), time() );

                        $data[] = get_attachment_by_id( $attach_id );
                    }
                }
            }
        }
    }

    return $data;
}

/*
| -----------------------------------------------------------------------------
| Media - Attachment Tab URL
| -----------------------------------------------------------------------------
*/
function get_attachment_tab_url( $tab )
{
    extract( $_GET );

    $textarea_id = isset( $textarea_id ) ? '&textarea_id=' . $textarea_id : '';
    $post_id     = isset( $post_id ) ? '&post_id=' . $post_id : '';
    $apps        = isset( $apps ) ? '&apps=' . $apps : '';
    $type        = isset( $type ) ? '&type=' . $type : '';

    return get_state_url( 'media&tab=' . $tab . $post_id . $apps . $type . $textarea_id );
}

/*
| -----------------------------------------------------------------------------
| Media - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_media_ajax_url()
{
    return get_state_url( 'ajax&apps=media' );
}

/*
| -----------------------------------------------------------------------------
| Media - Save Change All Media?
| -----------------------------------------------------------------------------
*/
function is_save_all_changes_media()
{
    if( isset( $_POST['save_all_changes'] ) && $_POST['save_all_changes'] == 'Save All Changes' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -----------------------------------------------------------------------------
| Media - Delete All Media?
| -----------------------------------------------------------------------------
*/
function is_delete_all_media()
{
    if( isset( $_POST['delete'] ) && $_POST['delete'] == 'Delete' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -----------------------------------------------------------------------------
| Media - Confirm Delete Media?
| -----------------------------------------------------------------------------
*/
function is_confirm_delete_media()
{
    if( isset( $_POST['confirm_delete'] ) && strtolower( $_POST['confirm_delete'] ) == 'yes' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -----------------------------------------------------------------------------
| Media - Ajax Function
| -----------------------------------------------------------------------------
*/
function media_ajax()
{    
    global $db;

    extract( $_POST );

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $pkey ) && $pkey == 'add-media-attachment' )
    {
        $data = add_media_attachment();

        if( empty( $data ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            $content = '';

            foreach( $data as $i => $d )
            {
                $content .= attachment_details( $d, $i, $textarea_id, $tab );
            }

            echo json_encode( array( 'result' => 'success', 'content' => $content ) );
        }
    }

    if( isset( $pkey ) && $pkey == 'edit-media-attachment' )
    {
        if( edit_attachment( $attachment_id, $title, null, $alt_text, $caption ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    exit;
}