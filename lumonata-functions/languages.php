<?php

add_actions( 'language_admin_page', 'language_ajax' );

/*
| -------------------------------------------------------------------------------------
| Get Language Setting Content
| -------------------------------------------------------------------------------------
*/
function multi_language_setting( $the_tab, $tabs )
{
    global $flash;

	$tabs = set_tabs( array_merge( $tabs, $the_tab ), 'multi_language' );

    if( is_add_new() )
    {
        return add_new_language( $tabs );
    }
    elseif( is_edit() )
    {
    	if( is_num_language( 'id=' . $_GET['id'] ) > 0 )
        {
            return edit_mlanguage( $tabs, $_GET['id'] );
        }
    }
    elseif( is_delete_all() )
    {
    	return delete_batch_language( $tabs );
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key => $val )
        {
            delete_language( $val );
        }

		$flash->add( array( 'type'=> 'success', 'content' => array( 'Delete batch language successfully.' ) ) );

        header( 'location:' . get_state_url( 'global_settings&tab=multi_language' ) );

        exit;
    }

    if( is_num_language() == 0 && isset( $_GET['prc'] ) === FALSE )
    {
        header( 'location:' . get_state_url( 'global_settings&tab=multi_language&prc=add_new' ) );

        exit;
    }
    elseif( is_num_language() > 0 )
    {
        return get_language_list();
    }
}

/*
| -------------------------------------------------------------------------------------
| Get Language List Count
| -------------------------------------------------------------------------------------
*/
function is_num_language()
{
    global $db;

    $s = 'SELECT * FROM lumonata_language';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    
    return $db->num_rows( $r );
}

/*
| -----------------------------------------------------------------------------
| Language Ajax URL
| -----------------------------------------------------------------------------
*/
function get_language_ajax_url()
{
    return get_state_url( 'ajax&apps=language' );
}

/*
| -------------------------------------------------------------------------------------
| Set variable for language list
| -------------------------------------------------------------------------------------
*/
function get_language_list()
{
    set_template( TEMPLATE_PATH . '/template/language-list.html', 'language' );

    add_block( 'language-setting', 'lg-block', 'language' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'alert', message_block() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_language_ajax_url() );
    add_variable( 'button', get_language_admin_button( get_state_url( 'global_settings&tab=multi_language' ) ) );

    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );
    
    parse_template( 'language-setting', 'lg-block' );

    return return_template( 'language' );
}

/*
| -------------------------------------------------------------------------------------
| Set variable for language new data
| -------------------------------------------------------------------------------------
*/
function add_new_language( $tabs )
{
    save_language();

    $data = get_language();

    extract( $data );

    set_template( TEMPLATE_PATH . '/template/language-form.html', 'language' );

    add_block( 'language-form', 'lg-block', 'language' );

    add_variable( 'tabs', $tabs );
    add_variable( 'lang_id', $llang_id );
    add_variable( 'language_name', $llanguage );
    add_variable( 'language_txt', 'No Image' );
    add_variable( 'language_code', language_flags_list( $llanguage_code ) );
    add_variable( 'language_flag', language_flags_image( $llanguage_code ) );
    add_variable( 'button', get_language_admin_button( get_state_url( 'global_settings&tab=multi_language' ), true ) );

    add_variable( 'status_active', $lstatus == 0 ? '' : 'checked' );
    add_variable( 'status_inactive', $lstatus == 0 ? 'checked' : '' );
    
    add_variable( 'default_no', $ldefault == 0 ? 'checked' : '' );
    add_variable( 'default_yes', $ldefault == 0 ? '' : 'checked' );

    add_variable( 'alert', message_block() );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'app_title', 'Add New Language' ); 

    parse_template( 'language-form', 'lg-block', false );

    return return_template( 'language' );    
}

/*
| -------------------------------------------------------------------------------------
| Set variable for language exists data
| -------------------------------------------------------------------------------------
*/
function edit_mlanguage( $tabs, $id = '' )
{
    update_mlanguage();

    $data = get_language( $id );

    extract( $data );

    set_template( TEMPLATE_PATH . '/template/language-form.html', 'language' );

    add_block( 'language-form', 'lg-block', 'language' );

    add_variable( 'tabs', $tabs );
    add_variable( 'lang_id', $llang_id );
    add_variable( 'language_name', $llanguage );
    add_variable( 'language_txt', 'No Image' );
    add_variable( 'language_code', language_flags_list( $llanguage_code ) );
    add_variable( 'language_flag', language_flags_image( $llanguage_code ) );
    add_variable( 'button', get_language_admin_button( get_state_url( 'global_settings&tab=multi_language' ), true ) );

    add_variable( 'status_active', $lstatus == 0 ? '' : 'checked' );
    add_variable( 'status_inactive', $lstatus == 0 ? 'checked' : '' );
    
    add_variable( 'default_no', $ldefault == 0 ? 'checked' : '' );
    add_variable( 'default_yes', $ldefault == 0 ? '' : 'checked' );

    add_variable( 'alert', message_block() );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'app_title', 'Add New Language' ); 

    parse_template( 'language-form', 'lg-block', false );

    return return_template( 'language' );  
}

/*
| -------------------------------------------------------------------------------------
| Set list of deleted language setting
| -------------------------------------------------------------------------------------
*/
function delete_batch_language( $tabs )
{
    global $db;

	set_template( TEMPLATE_PATH . '/template/language-batch-delete.html', 'language' );

	add_block( 'delete-loop-block', 'dl-block', 'language' );
    add_block( 'delete-block', 'd-block', 'language' );

	foreach( $_POST['select'] as $key=>$val )
	{
		$s = 'SELECT * FROM lumonata_language WHERE llang_id=%d';
		$q = $db->prepare_query( $s, $val );
        $r = $db->do_query( $q );
        $d = $db->fetch_array( $r );

		add_variable( 'lang_name', $d['llanguage'] );
		add_variable( 'lang_id', $d['llang_id'] );

		parse_template( 'delete-loop-block', 'dl-block', true );
	}

    add_variable( 'tabs', $tabs );
    add_variable( 'title', 'Delete Languages' );
	add_variable( 'backlink', get_state_url( 'global_settings&tab=multi_language' ) );
	add_variable( 'message', 'Are you sure want to delete '. ( count( $_POST['select']) == 1 ? 'this' : 'these' ) . ' language :' );

	parse_template( 'delete-block', 'd-block', false );

    return return_template( 'language' );
}

function get_language_content_css()
{
    $multi_language = get_meta_data( 'multi_language' );
    
    if( $multi_language == 1 )
    {
        $def = get_default_language();
    }
}

function get_default_language()
{
    global $db;

    $s = 'SELECT * FROM lumonata_language WHERE lstatus = 1 AND ldefault = 1';
    $q = $db->prepare_query( $s );
    $r = $db->do_query($q);

    return $db->fetch_array($r);
}

/*
| -------------------------------------------------------------------------------------
| Get All Data Language
| -------------------------------------------------------------------------------------
*/
function get_all_language( $exclude_default = false )
{
    global $db;
    
    $data = array();

    $s = 'SELECT * FROM lumonata_language WHERE lstatus = 1 ORDER BY ldefault DESC';
    $q = $db->prepare_query( $s );
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    if( $n > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            if( $exclude_default && $d['ldefault'] == 1 )
            {
                continue;
            }

            $data[] = array( 
                'lstatus'        => $d['lstatus'],
                'ldefault'       => $d['ldefault'],
                'llang_id'       => $d['llang_id'],
                'llanguage'      => $d['llanguage'],
                'llanguage_code' => $d['llanguage_code'],
            );
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Get Language By ID
| -------------------------------------------------------------------------------------
*/
function get_language( $id = '', $field = '' )
{
    global $db;

    $data = array( 
        'lstatus'        => ( isset( $_POST['lstatus'] ) ? $_POST['lstatus'] : 1 ),
        'ldefault'       => ( isset( $_POST['ldefault'] ) ? $_POST['ldefault'] : 0 ),
        'llang_id'       => ( isset( $_POST['llang_id'] ) ? $_POST['llang_id'] : null ), 
        'llanguage'      => ( isset( $_POST['llanguage'] ) ? $_POST['llanguage'] : '' ), 
        'llanguage_code' => ( isset( $_POST['llanguage_code'] ) ? $_POST['llanguage_code'] : '' )
    );

    $s = 'SELECT * FROM lumonata_language WHERE llang_id = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data = array( 
                'lstatus'        => $d['lstatus'],
                'ldefault'       => $d['ldefault'],
                'llang_id'       => $d['llang_id'],
                'llanguage'      => $d['llanguage'],
                'llanguage_code' => $d['llanguage_code'],
            );
        }
    }

    if( !empty( $field ) && isset( $data[$field] ) )
    {
        return $data[$field];
    }
    else
    {
        return $data;
    }
}

/*
| -------------------------------------------------------------------------------------
| Save new data language
| -------------------------------------------------------------------------------------
*/
function save_language()
{
    global $db, $flash;

    if( is_save_draft() || is_publish() || is_save_changes() )
    {
    	$error = validate_language_data();

        if( empty( $error ) )
        {
	    	extract( $_POST );

	        $s = 'INSERT INTO lumonata_language(
	    			llanguage, 
	    			llanguage_code, 
	    			lstatus, 
	    			lorder, 
	    			lpost_by, 
	    			lpost_date, 
	    			lupdated_by, 
	    			ldlu,
	    			ldefault) 
	    		  VALUES( %s, %s, %d, %d, %d, %s, %d, %s, %d )';
	        $q = $db->prepare_query( $s, $llanguage, $llanguage_code, $lstatus, 1, $_COOKIE['user_id'], date( 'Y-m-d H:i:s' ), $_COOKIE['user_id'], date( 'Y-m-d H:i:s' ), $ldefault );
	        $r = $db->do_query( $q );

	        if( is_array( $r ) )
	        {
	        	$flash->add( array( 'type'=> 'success', 'content' => array( 'Saving new language failed.' ) ) );
	        }
	        else
	        {
	            if( $ldefault == 1 )
	            {
	                $s2 = 'UPDATE lumonata_language SET ldefault=%d WHERE llang_id<>%d';
	                $q2 = $db->prepare_query( $s2, 0, $db->insert_id() );
	                $r2 = $db->do_query( $q2 );
	            }

		        $flash->add( array( 'type'=> 'success', 'content' => array( 'Saving new language successfully.' ) ) );

		        header( 'location:' . get_state_url( 'global_settings&tab=multi_language&prc=add_new' ) );

		        exit;
	        }
	    }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Update data language
| -------------------------------------------------------------------------------------
*/
function update_mlanguage()
{
    global $db, $flash;

    if( is_save_draft() || is_publish() || is_save_changes() )
    {
    	$error = validate_language_data();

        if( empty( $error ) )
        {
	    	extract( $_POST );

	        $s = 'UPDATE lumonata_language SET
					llanguage_code=%s,
					llanguage=%s,
					lstatus=%d,
					lupdated_by=%d,
					ldlu=%s,
					ldefault=%d
	              WHERE llang_id=%d';
	        $q = $db->prepare_query( $s, $llanguage_code, $llanguage, $lstatus, $_COOKIE['user_id'], date( 'Y-m-d H:i:s' ), $ldefault, $llang_id );
	        $r = $db->do_query( $q );

	        if( is_array( $r ) )
	        {
	        	$flash->add( array( 'type'=> 'success', 'content' => array( 'Update language failed.' ) ) );
	        }
	        else
	        {
	            if( $ldefault == 1 )
	            {
	                $s2 = 'UPDATE lumonata_language SET ldefault=%d WHERE llang_id<>%d';
	                $q2 = $db->prepare_query( $s2, 0, $llang_id );
	                $r2 = $db->do_query( $q2 );
	            }

		        $flash->add( array( 'type'=> 'success', 'content' => array( 'Update language successfully.' ) ) );

		        header( 'location:' . get_state_url( 'global_settings&tab=multi_language&prc=edit&id=' . $llang_id ) );

		        exit;
	        }
	    }
        else
        {
            $flash->add( array( 'type'=> 'error', 'content' => $error ) );
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Delete language
| -------------------------------------------------------------------------------------
*/
function delete_language( $lang_id )
{
    global $db;

    $s = 'DELETE FROM lumonata_language WHERE llang_id=%d';
    $q = $db->prepare_query( $s, $lang_id );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
    	return false;
    }
    else
    {
    	return true;
    }
}

/*
| -------------------------------------------------------------------------------------
| Validate data language
| -------------------------------------------------------------------------------------
*/
function validate_language_data()
{
    $error = array();

    if( isset( $_POST['llanguage'] ) && empty( $_POST['llanguage'] ) )
    {
        $error[] = 'Language name can\'t be empty';
    }

    if( isset( $_POST['llanguage_code'] ) && $_POST['llanguage_code'] == '' )
    {
        $error[] = 'Country code can\'t be empty';
    }

    return $error;
}

/*
| -----------------------------------------------------------------------------
| Language Action Button
| -----------------------------------------------------------------------------
*/
function get_language_admin_button( $url = '', $is_form = false )
{
	if( $is_form )
	{
		if( is_contributor() )
	    {
	        return '
	        <li>' . button( 'button=add_new', $url . '&prc=add_new' ) . '</li>
	        <li>' . button( 'button=save_draft&label=Save' ) . '</li>
	        <li>' . button( 'button=cancel', $url ) . '</li>';
	    }
	    else
	    {
	        return '
	        <li>' . button( 'button=publish' ) . '</li>
	        <li>' . button( 'button=save_draft' ) . '</li>            
	        <li>' . button( 'button=cancel', $url ) . '</li>
	        <li>' . button( 'button=add_new', $url . '&prc=add_new' ) . '</li>';
	    }
	}
	else
	{
	    if( is_contributor() )
	    {
	        return '
	        <li>' . button( 'button=add_new', $url . '&prc=add_new' ) . '</li>
	        <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
	    }
	    else
	    {
	        return '
	        <li>' . button( 'button=add_new', $url . '&prc=add_new' ) . '</li>
	        <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
	    }
	}
}

/*
| -------------------------------------------------------------------------------------
| Set option of all language
| -------------------------------------------------------------------------------------
*/
function language_flags_list( $lcode = '' )
{
    $codes = array(
        'en' => 'English' , 
        'ar' => 'Arabic' ,
        'cs' => 'Czech' ,
        'da' => 'Danish' , 
        'de' => 'German' , 
        'el' => 'Greek' , 
        'es' => 'Spanish' ,
        'fr' => 'French' , 
        'hi' => 'Hindi' ,
        'in' => 'Indonesian' , 
        'it' => 'Italian' , 
        'iw' => 'Hebrew' , 
        'jp' => 'Japanese' , 
        'ko' => 'Korean' ,
        'ms' => 'Malay' , 
        'nl' => 'Dutch' , 
        'ru' => 'Russian' , 
        'tl' => 'Tagalog' , 
        'vi' => 'Vietnamese' , 
        'zh' => 'Chinese' , 
    );

    $options = '
    <select name="llanguage_code" class="select-opt" autocomplete="off">
        <option value="">Select Language Code</option>';

        foreach( $codes as $key => $obj )
        {
            $options .= '
            <option value="' . $key . '" ' . ( $lcode == $key ? 'selected="selected"' : '' ) . '>
            	' . $obj . '
            </option>';
        }

        $options .= '
    </select>';

    return $options;
}

function language_flags_image( $lcode = '' )
{
	if( !empty( $lcode ) )
	{
		return TEMPLATE_URL . '/flags/'. $lcode . '.svg';
	}
}

/*
| -----------------------------------------------------------------------------
| Language Table Query
| -----------------------------------------------------------------------------
*/
function get_language_list_query()
{    
    global $db;

    extract( $_POST );

    $rdata = $_REQUEST;
    $cols  = array( 
        2 => 'a.llanguage',
        3 => 'a.llanguage_code',
        4 => 'a.ldefault', 
        5 => 'a.lstatus'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            if( isset( $cols[ $rdata['order'][$i]['column'] ] ) )
            {
                $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
            }
        }

        $order = empty( $o ) ? '' : 'ORDER BY ' . implode( ', ', $o );
    }
    else
    {
        $order = 'ORDER BY a.lorder ASC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $q = 'SELECT * FROM lumonata_language AS a ' . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $q = 'SELECT * FROM lumonata_language AS a WHERE ( ' . implode( ' OR ', $search ) . ' ) ' . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }
    
    $data = array();

    if( $n2 > 0 )
    {
        $url = get_state_url( 'global_settings&tab=multi_language' );

        while( $d2 = $db->fetch_array( $r2 ) )
        {   
            $order     = $d2[ 'lorder' ];
            $status    = $d2[ 'lstatus' ];
            $id        = $d2[ 'llang_id' ];
            $default   = $d2[ 'ldefault' ];
            $title     = $d2[ 'llanguage' ];
            $code      = $d2[ 'llanguage_code' ];
            $flag      = TEMPLATE_URL . '/flags/' . $d2['llanguage_code'] .'.svg';

            $data[] = array(
                'id'        => $id,
                'flag'      => $flag,
                'code'      => $code,
                'order'     => $order,
                'title'     => $title,
                'status'    => $status,
                'default'   => $default,
                'ajax_link' => get_language_ajax_url(),
                'edit_link' => $url . '&prc=edit&id=' . $id
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'data'            => $data,
        'recordsTotal'    => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'draw'            => intval( $rdata['draw'] )
    );

    return $result;
}

/*
| -----------------------------------------------------------------------------
| Language Ajax Function
| -----------------------------------------------------------------------------
*/
function language_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( is_user_logged() === FALSE )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-language-data' )
    {
        $data = get_language_list_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-language' )
    {
        if( delete_language( $_POST['id'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    exit;
}

?>