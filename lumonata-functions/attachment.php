<?php

add_actions( 'attachment_admin_page', 'attachment_ajax' );

function insert_attachment( $article_id = 0, $rule_id = 0, $title, $mime_type, $attach_loc, $attach_loc_thumb = '', $attach_loc_med = '', $attach_large = '', $alt_text = '', $caption = '', $content = '', $app_name = '', $reset_order = true )
{
    global $db;

    $upload_date      = date( 'Y-m-d H:i:s', time() );
    $date_last_update = date( 'Y-m-d H:i:s', time() );
    $title            = rem_slashes( $title );
    $alt_text         = rem_slashes( $alt_text );
    $caption          = rem_slashes( $caption );

    if( $reset_order )
    {
        $s  = 'INSERT INTO lumonata_attachment(
                larticle_id,
                lrule_id,
                lattach_loc,
                lattach_loc_thumb,
                lattach_loc_medium,
                lattach_loc_large,
                lapp_name,
                ltitle,
                lcontent,
                lalt_text,
                lcaption,
                upload_date,
                date_last_update,
                mime_type,
                lorder)
            VALUES(%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d)';
        $q  = $db->prepare_query( $s, 
                $article_id, 
                $rule_id,
                $attach_loc, 
                $attach_loc_thumb, 
                $attach_loc_med, 
                $attach_large, 
                $app_name,
                $title, 
                $content,
                $alt_text, 
                $caption, 
                $upload_date, 
                $date_last_update, 
                $mime_type, 
                1 );

        if( reset_order_id( 'lumonata_attachment' ) )
        {
            $r = $db->do_query( $q );

            if( is_array( $r ) )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        if( empty( $article_id ) )
        {
            $s = 'SELECT COUNT( a.lattach_id ) + 1 AS num 
                  FROM lumonata_attachment AS a 
                  WHERE a.larticle_id = %d AND a.lapp_name = %s';
            $q = $db->prepare_query( $s, $article_id, $app_name );
            $r = $db->do_query( $q );
        }
        else
        {
            $s = 'SELECT COUNT( a.lattach_id ) + 1 AS num 
                  FROM lumonata_attachment AS a 
                  WHERE a.lrule_id = %d AND a.lapp_name = %s';
            $q = $db->prepare_query( $s, $rule_id, $app_name );
            $r = $db->do_query( $q );
        }

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            $d = $db->fetch_array( $r );

            $s = 'INSERT INTO lumonata_attachment(
                    larticle_id,
                    lrule_id,
                    lattach_loc,
                    lattach_loc_thumb,
                    lattach_loc_medium,
                    lattach_loc_large,
                    lapp_name,
                    ltitle,
                    lcontent,
                    lalt_text,
                    lcaption,
                    upload_date,
                    date_last_update,
                    mime_type,
                    lorder)
                  VALUES(%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d)';
            $q = $db->prepare_query( $s, 
                    $article_id, 
                    $rule_id,
                    $attach_loc, 
                    $attach_loc_thumb, 
                    $attach_loc_med, 
                    $attach_large, 
                    $app_name,
                    $title, 
                    $content,
                    $alt_text, 
                    $caption, 
                    $upload_date, 
                    $date_last_update, 
                    $mime_type, 
                    $d['num'] );
            
            $r = $db->do_query( $q );

            if( is_array( $r ) )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}

function update_attachment( $attach_id, $article_id = 0, $rule_id = 0, $title = '', $mime_type = '', $attach_loc = '', $attach_loc_thumb = '', $attach_loc_med = '', $attach_large = '', $alt_text = '', $caption = '', $content = '', $app_name = '' )
{
    global $db;

    $upload_date      = date( 'Y-m-d H:i:s', time() );
    $date_last_update = date( 'Y-m-d H:i:s', time() );
    $title            = rem_slashes( $title );
    $alt_text         = rem_slashes( $alt_text );
    $caption          = rem_slashes( $caption );

    $s  = 'UPDATE lumonata_attachment SET
            larticle_id=%d,
            lrule_id=%d,
            lattach_loc=%s,
            lattach_loc_thumb=%s,
            lattach_loc_medium=%s,
            lattach_loc_large=%s,
            lapp_name=%s,
            ltitle=%s,
            lcontent=%s,
            lalt_text=%s,
            lcaption=%s,
            upload_date=%s,
            date_last_update=%s,
            mime_type=%s
        WHERE lattach_id=%d';
    $q  = $db->prepare_query( $s, 
            $article_id,
            $rule_id,
            $attach_loc, 
            $attach_loc_thumb, 
            $attach_loc_med, 
            $attach_large, 
            $app_name,
            $title,
            $content,
            $alt_text, 
            $caption, 
            $upload_date, 
            $date_last_update, 
            $mime_type, 
            $attach_id);

    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

function edit_attachment( $attach_id, $title, $order = null, $alt_text = '', $caption = '', $content = '' )
{
    global $db;

    $title            = rem_slashes( $title );
    $alt_text         = rem_slashes( $alt_text );
    $caption          = rem_slashes( $caption );
    $date_last_update = date( 'Y-m-d H:i:s', time() );

    if( $order == null || $order == '' )
    {
        $sql = 'UPDATE lumonata_attachment SET
                    ltitle=%s,
                    lalt_text=%s,
                    lcaption=%s,
                    lcontent=%s,
                    date_last_update=%s
                WHERE lattach_id=%d';
        $query = $db->prepare_query( $sql, $title, $alt_text, $caption, $content, $date_last_update, $attach_id );
    }
    else
    {
        $sql = 'UPDATE lumonata_attachment SET
                    ltitle=%s,
                    lalt_text=%s,
                    lcaption=%s,
                    lcontent=%s,
                    date_last_update=%s,
                    lorder=%d
                WHERE lattach_id=%d';
        $query = $db->prepare_query( $sql, $title, $alt_text, $caption, $content, $date_last_update, $order, $attach_id );
    }

    if( $db->do_query( $query ) )
    {
        return true;
    }

    return false;
}

function delete_attachment_form()
{
    $content = '
    <form action="" method="post">';

        $content .= '
        <div class="alert_red_form">
            <strong>
                Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this media' : 'these media' ) . ':
            </strong>
            <ol>';

                foreach( $_POST['select'] as $key => $val )
                {
                    $content .= '<li>' . attachment_value( 'ltitle', $val ) . '<input type="hidden" name="id[]" value="' . $val . '"></li>';
                }

                $content .= '
            </ol>
        </div>
        <div class="action_box">
            <input type="submit" name="confirm_delete" value="Yes" class="button" />
            <input type="button" name="confirm_delete" value="No" class="button" onclick="location=\'' . get_attachment_tab_url( $_GET['tab'] ) . '\'" />
        </div>
    </form>';

    return $content;
}

function delete_attachment( $attach_id )
{
    global $db;

    //-- original file
    $loc = attachment_value( 'lattach_loc', $attach_id );

    if( !empty( $loc ) )
    {
        $file = ROOT_PATH . $loc;

        if( file_exists( $file ) )
        {
            unlink( $file );
        }
    }

    //-- thumbanil
    $loc = attachment_value( 'lattach_loc_thumb', $attach_id );

    if( !empty( $loc ) )
    {
        $file = ROOT_PATH . $loc;

        if( file_exists( $file ) )
        {
            unlink( $file );
        }
    }

    //-- Medium
    $loc = attachment_value( 'lattach_loc_medium', $attach_id );

    if( !empty( $loc ) )
    {
        $file = ROOT_PATH . $loc;

        if( file_exists( $file ) )
        {
            unlink( $file );
        }
    }

    //-- Large
    $loc = attachment_value( 'lattach_loc_large', $attach_id );

    if( !empty( $loc ) )
    {
        $file = ROOT_PATH . $loc;

        if( file_exists( $file ) )
        {
            unlink( $file );
        }
    }

    $query = $db->prepare_query( 'DELETE FROM lumonata_attachment WHERE lattach_id=%d', $attach_id );

    if( $db->do_query( $query ) )
    {
        return true;
    }

    return false;

}

function attachment_value( $val, $attach_id )
{
    global $db;

    $q = $db->prepare_query( "SELECT $val FROM lumonata_attachment WHERE lattach_id=%d", $attach_id );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d[$val];
}

function upload_image_attachment( $source, $file_type, $file_name, $post_id )
{
    global $db;

    $folder_name = upload_folder_name();

    if( !defined( 'FILES_LOCATION' ) )
    {
        define( 'FILES_LOCATION', '/lumonata-content/files' );
    }

    if( !is_dir( FILES_PATH . '/' . upload_folder_name() ) )
    {
        create_dir( FILES_PATH . '/' . upload_folder_name() );
    }

    $default_title   = file_name_filter( $file_name );
    $file_name       = character_filter( $file_name );
    $file_name       = file_name_filter( $file_name ) . '-' . time() . file_name_filter( $file_name, true );

    //-- Thumbnail Image
    $file_name_t     = '';
    $file_location_t = '';
    $file_name_t     = file_name_filter( $file_name ) . '-thumbnail' . file_name_filter( $file_name, true );
    $file_location_t = FILES_LOCATION . '/' . $folder_name . '/' . $file_name_t;
    $destination     = FILES_PATH . '/' . $folder_name . '/' . $file_name_t;

    add_actions( 'thumbnail_file_location', $file_location_t );

    //-- Create thumbnail image here
    if( upload_resize( $source, $destination, $file_type, thumbnail_image_width(), thumbnail_image_height() ) )
    {
        //-- Medium Image
        $file_name_m     = '';
        $file_location_m = '';
        $file_name_m     = file_name_filter( $file_name ) . '-medium' . file_name_filter( $file_name, true );
        $file_location_m = FILES_LOCATION . '/' . $folder_name . '/' . $file_name_m;
        $destination = FILES_PATH . '/' . $folder_name . '/' . $file_name_m;

        add_actions( 'medium_file_location', $file_location_m );

        //-- Create medium size image here
        if( upload_resize( $source, $destination, $file_type, medium_image_width(), medium_image_height() ) )
        {
            //-- Large Image
            $file_name_l     = '';
            $file_location_l = '';
            $file_name_l     = file_name_filter( $file_name ) . '-large' . file_name_filter( $file_name, true );
            $file_location_l = FILES_LOCATION . '/' . $folder_name . '/' . $file_name_l;
            $destination_l   = FILES_PATH . '/' . $folder_name . '/' . $file_name_l;

            add_actions( 'large_file_location', $file_location_l );

            //-- Create Large size image here
            if( upload_resize( $source, $destination_l, $file_type, large_image_width(), large_image_height() ) )
            {
                //-- Original Image
                $destination   = FILES_PATH . '/' . $folder_name . '/' . $file_name;
                $file_location = FILES_LOCATION . '/' . $folder_name . '/' . $file_name;

                add_actions( 'original_file_location', FILES_LOCATION . '/' . $folder_name . '/' . $file_name );

                //-- Upload the original image
                if( upload( $source, $destination ) )
                {
                    return insert_attachment( $post_id, 0, $default_title, $file_type, $file_location, $file_location_t, $file_location_m, $file_location_l );
                }
            }
        }
    }

    return false;
}

function upload_media_attachment( $source, $file_type, $file_name, $post_id )
{
    $folder_name = upload_folder_name();

    if( !defined( 'FILES_LOCATION' ) )
    {
        define( 'FILES_LOCATION', '/lumonata-content/files' );
    }

    $default_title = file_name_filter( $file_name );
    $file_name     = character_filter( $file_name );
    $destination   = FILES_PATH . '/' . $folder_name . '/' . $file_name;
    $file_location = FILES_LOCATION . '/' . $folder_name . '/' . $file_name;

    if( upload( $source, $destination ) )
    {
        add_actions( 'original_file_location', $file_location );

        return insert_attachment( $post_id, 0, $default_title, $file_type, $file_location );
    }
}

function get_attachment_by_app_name( $article_id, $app_name = '', $return_query = true )
{
    global $db;

    $s = 'SELECT * FROM lumonata_attachment WHERE larticle_id = %s AND lapp_name = %s ORDER BY lorder ASC';
    $q = $db->prepare_query( $s, $article_id, $app_name );
    $r = $db->do_query( $q );

    if( $return_query )
    {
        return $r;
    }
    else
    {
        return $db->fetch_array( $r );
    }
}

function get_rule_attachment_by_app_name( $rule_id, $app_name = '', $return_query = true )
{
    global $db;

    $s = 'SELECT * FROM lumonata_attachment WHERE lrule_id = %s AND lapp_name = %s ORDER BY lorder ASC';
    $q = $db->prepare_query( $s, $rule_id, $app_name );
    $r = $db->do_query( $q );

    if( $return_query )
    {
        return $r;
    }
    else
    {
        return $db->fetch_array( $r );
    }
}

function get_attachment_by_id( $attach_id )
{
    global $db;

    $s = 'SELECT * FROM lumonata_attachment WHERE lattach_id = %d';
    $q = $db->prepare_query( $s, $attach_id );
    $r = $db->do_query( $q );

    return $db->fetch_array( $r );
}

function get_attachment( $post_id = 0 )
{
    global $db;

    $url    = get_attachment_tab_url( $_GET[ 'tab' ] );
    $ajxurl = get_attachment_ajax_url();
    $order  = 'ASC';
    $attch  = '';
    $js     = '';

    if( isset( $_GET[ 'sort_order' ] ) )
    {
        if( $_GET[ 'sort_order' ] == 'desc' )
        {
            $order = 'DESC';
        }
    }

    if( $post_id == 0 )
    {
        $q = $db->prepare_query( 'SELECT * FROM lumonata_attachment ORDER BY lorder ' . $order );
    }
    else
    {
        $q = $db->prepare_query( 'SELECT * FROM lumonata_attachment WHERE larticle_id = %d ORDER BY lorder ' . $order, $post_id );
    }

    //-- Setup paging system
    $num_rows = count_rows( $q );
    $viewed   = list_viewed();

    if( isset( $_GET[ 'page' ] ) )
    {
        $page = $_GET[ 'page' ];
    }
    else
    {
        $page = 1;
    }

    $limit = $viewed * ( $page - 1 );

    if( $post_id == 0 )
    {
        $q = $db->prepare_query( 'SELECT * FROM lumonata_attachment WHERE ltitle != %s ORDER BY lorder ' . $order . ' LIMIT %d, %d', 'thumb_pdf', $limit, $viewed );
    }
    else
    {
        $q = $db->prepare_query( 'SELECT * FROM lumonata_attachment WHERE larticle_id = %d AND ltitle != %s ORDER BY lorder ' . $order . ' LIMIT %d, %d', $post_id, 'thumb_pdf', $limit, $viewed );
    }

    $r = $db->do_query( $q );
    $i = $viewed * ( $page - 1 ) + 1; 
    $y = $i + count_rows( $q ) - 1;

    $attch  = '
    <form method="post" action="">
        <input type="hidden" name="the_tab" value="' . $_GET[ 'tab' ] . '" />
        <input type="hidden" name="start_order" value="' . $i . '" />
        <input type="hidden" name="site_url" value="' . site_url() . '" />

        <div class="media_navigation clearfix">
            <div class="search_wraper">
                ' . search_box( $ajxurl, 'media_gallery', 'textarea_id=' . $_GET[ 'textarea_id' ] . '&tab=' . $_GET[ 'tab' ] . '&article_id=' . $post_id . '&pkey=search-attachment' ) . '
            </div>

            <div class="sort_order">
                Sort Order: 
                <a href="' . get_attachment_tab_url( $_GET[ 'tab' ] ) . '&sort_order=asc">Ascending</a> |
                <a href="' . get_attachment_tab_url( $_GET[ 'tab' ] ) . '&sort_order=desc">Descending </a>
            </div>
        </div>

        <div id="response">{response}</div>

        <div class="media_title clearfix">
            <input type="checkbox" class="title_checkbox" name="select_all" style="margin:10px 0px 10px 1px;">
            <div class="media_description">Media</div>
            <div class="media_action" style="width: 45%;">Sort / Order</div>
        </div>';

        $attch .= '
        <div id="media_gallery" style="border: medium none;">';

            if( isset( $_POST[ 's' ] ) && $_POST[ 's' ] != 'Search' )
            {
                $attch .= search_attachment_results( $_POST[ 's' ], $_GET[ 'tab' ], $post_id, $_GET[ 'textarea_id' ] );
            }
            else
            {
                $attch .= gallery_items( $r, $_GET[ 'tab' ], $_GET[ 'textarea_id' ], $i );
            }

            $attch .= '
        </div>';

        $attch .= '
        <div class="media_navigation clearfix">
            <div class="attach-list-action-box">
                ' . button( 'button=save_changes&label=Save All Changes&name=save_all_changes' ) . ' &nbsp;' . button( 'button=delete&type=submit&enable=false' ) . '
            </div>
            <div class="paging_media">' . paging( $url . '&page=', $num_rows, $page, $viewed, 5 ) . '</div>
        </div>';

        $attch .= '
    </form>';

    return $attch;
}

function search_attachment_results( $s = '', $tab = 'gallery', $article_id = '', $textarea_id = 0 )
{
    global $db;

    if( empty( $s ) )
    {
        if( $tab == 'gallery' )
        {
            $sql = $db->prepare_query( 'SELECT * FROM lumonata_attachment WHERE larticle_id = %d ORDER BY lorder', $article_id );
        }
        else
        {
            $sql = $db->prepare_query( 'SELECT * FROM lumonata_attachment ORDER BY lorder' );
        }
    }
    else
    {
        if( $tab == 'gallery' )
        {
            $sql = $db->prepare_query( 'SELECT * FROM lumonata_attachment WHERE larticle_id=%d AND ( ltitle LIKE %s OR lalt_text LIKE %s OR lcaption LIKE %s ) ORDER BY lorder', $article_id, '%' . $s . '%', '%' . $s . '%', '%' . $s . '%' );
        }
        else
        {
            $sql = $db->prepare_query( 'SELECT * FROM lumonata_attachment WHERE ltitle LIKE %s OR lalt_text LIKE %s OR lcaption LIKE %s ORDER BY lorder', '%' . $s . '%', '%' . $s . '%', '%' . $s . '%' );
        }
    }

    $result = $db->do_query( $sql );

    if( $db->num_rows( $result ) == 0 )
    {
        return "<div class=\"alert_yellow\">No results found for <em>$s</em>. Check your spelling or try another term.</div><br />";
    }

    return gallery_items( $result, $tab, $textarea_id );
}

function gallery_items( $result, $tab, $textarea_id, $i = 1 )
{
    global $db;

    $attch = '';

    while( $d = $db->fetch_array( $result ) )
    {
        $attch .= '
        <div class="media_gallery_item clearfix" id="attachment_' . $d['lattach_id'] . '">
            <div class="attach-loader"></div>
            <div class="clearfix">
                <input type="checkbox" value="' . $d['lattach_id'] . '" class="title_checkbox select" name="select[]"  style="margin-top: 0px;">
                <div class="media_description">
                    <div class="media_image_small">' . attachment_icon( $d['mime_type'], '60:60', $d['lattach_loc_thumb'] ) . '</div>
                    <div class="media_item_title">' . stripslashes( $d['ltitle'] ) . '</div>
                </div>
                <div class="media_action">
                    <div class="media_item_action">
                        <input type="text" value="' . $d['lorder'] . '" id="order_' . $d['lattach_id'] . '" class="small_textbox" name="order[' . $i . ']">&nbsp;
                        <a href="#" id="show_attach_' . $d['lattach_id'] . '">Show</a> | <a href="#" rel="delete_' . $d['lattach_id'] . '">Delete</a>
                    </div>
                </div>
            </div>
            <div class="details_item clearfix" style="display:none" id="detail_attach_' . $d['lattach_id'] . '">
                ' . attachment_details( $d, $i, $textarea_id, $tab ) . '
            </div>
            <script type="text/javascript" language="javascript">
                jQuery(function(){
                    jQuery("#show_attach_' . $d['lattach_id'] . '").click(function(){
                        jQuery("#detail_attach_' . $d['lattach_id'] . '").slideToggle(100);
                        jQuery(this).text( jQuery(this).text() == "Show" ? "Hide" : "Show" );

                        return false;
                    });

                    jQuery("#detail_attach_' . $d['lattach_id'] . '").css("display", "none");
                });
            </script>
        </div>';

        $attch .= delete_confirmation_box( $d['lattach_id'], 'Are you sure want to delete <code>' . $d['ltitle'] . '</code> from the gallery?', get_attachment_tab_url( $tab ), 'attachment_' . $d['lattach_id'] );
        
        $i++;
    }

    return $attch;
}

function count_attachment( $article_id = 0 )
{
    global $db;

    if( $article_id != 0 )
    {
        $sql = $db->prepare_query( 'SELECT * FROM lumonata_attachment where larticle_id=%d', $article_id );
    }
    else
    {
        $sql = $db->prepare_query( 'SELECT * FROM lumonata_attachment' );
    }

    $r = $db->do_query( $sql );

    return $db->num_rows( $r );
}

function upload_folder_name()
{
    return date( 'Y', time() ) . date( 'm', time() );
}

function attachment_icon( $file_type, $file_size = '', $file_location = '' )
{
    $width  = '';
    $height = '';
    $img    = '';

    if( !empty( $file_size ) )
    {
        list( $width, $height ) = explode( ':', $file_size );

        $width  = 'width="' . $width . 'px"';
        $height = 'width="' . $height . 'px"';
    }

    switch( $file_type )
    {
        case 'image/jpg':
        case 'image/jpeg':
        case 'image/pjpeg':
        case 'image/gif':
        case 'image/png':
            $img = '<img src="' . site_url( $file_location ) . '" ' . $width . ' ' . $height . ' />';
            break;
        case 'application/x-shockwave-flash':
            $img = '<img src="' . template_url( 'media/default.png', true ) . '" ' . $width . ' ' . $height . ' />';
            break;
        case 'application/octet-stream':
            $img = '<img src="' . template_url( 'media/video.png', true ) . '" ' . $width . ' ' . $height . ' />';
            break;
        case 'audio/m4a':
        case 'audio/x-ms-wma':
        case 'audio/mpeg':
            $img = '<img src="' . template_url( 'media/audio.png', true ) . '" ' . $width . ' ' . $height . ' />';
            break;
        case 'application/octet':
        case 'application/pdf':
            $img = '<img src="' . template_url( 'media/pdf.png', true ) . '" ' . $width . ' ' . $height . ' />';
            break;
        case 'application/msword':
        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
            $img = '<img src="' . template_url( 'media/document.png', true ) . '" ' . $width . ' ' . $height . ' />';
            break;
        case 'text/plain':
            $img = '<img src="' . template_url( 'media/text.png', true ) . '" ' . $width . ' ' . $height . ' />';
            break;
    }
    return $img;
}

function update_attachment_order( $order, $start )
{
    global $db;

    foreach( $order as $key => $val )
    {
        $s = 'UPDATE lumonata_attachment SET lorder=%d WHERE lattach_id=%d';
        $q = $db->prepare_query( $s, $key + $start, $val );
        $db->do_query( $q );
    }
}

function attachment_details( $d, $index, $textarea_id, $tab )
{
    $mime_type = array(
        'image/jpg',
        'image/jpeg',
        'image/pjpeg',
        'image/gif',
        'image/png' 
    );

    $detail = '
    <div class="details_attachment_wrapper">';

        if( in_array( $d['mime_type'], $mime_type ) )
        {
            $detail .= '        
            <div id="response_' . $d['lattach_id'] . '"></div>
            <div class="details_attachment_set clearfix">
                <div class="media_field_left_image">';
                    $detail .= attachment_icon( $d['mime_type'], '100:100', $d['lattach_loc_medium'] );
                    $detail .= '
                </div>
                <div class="media_field_right_image">
                    <strong>File Type : </strong>' . $d['mime_type'] . '<br /><br />
                    <strong>Upload Date : </strong>' . date( get_date_format(), strtotime( $d['upload_date'] ) ) . '<br /><br />
                </div>
            </div>
            <div class="details_attachment_set clearfix">
                <div class="media_field_left">
                    <strong>Title: </strong>
                </div>
                <div class="media_field_right">
                    <input type="text" class="textbox title_' . $d['lattach_id'] . '" name="title[' . $index . ']" value="' . stripslashes( $d['ltitle'] ) . '" />
                </div>
            </div>
            <div class="details_attachment_set clearfix">
                <div class="media_field_left">
                    <strong>Alternate Text: </strong>
                </div>
                <div class="media_field_right">
                    <input type="text" class="textbox alt_text_' . $d['lattach_id'] . '" name="alt_text[' . $index . ']" value="' . stripslashes( $d['lalt_text'] ) . '" />
                </div>
            </div>
            <div class="details_attachment_set clearfix">
                <div class="media_field_left">
                    <strong>Caption: </strong>
                </div>
                <div class="media_field_right">
                    <input type="text" class="textbox caption_' . $d['lattach_id'] . '" name="caption[' . $index . ']" value="' . stripslashes( $d['lcaption'] ) . '" />
                </div>
            </div>
            <div class="details_attachment_set clearfix">
                <div class="media_field_left">
                    <strong>Alignment: </strong>
                </div>
                <div class="media_field_right">
                    <ul id="alignment">
                        <li id="alignment_none" ><input type="radio" class="alignment_' . $d['lattach_id'] . '" name="alignment[' . $index . ']" value="none" checked="checked" />None</li>
                        <li id="alignment_left"><input type="radio" class="alignment_' . $d['lattach_id'] . '" name="alignment[' . $index . ']" value="left"   />Left</li>
                        <li id="alignment_middle"> <input type="radio" class="alignment_' . $d['lattach_id'] . '" name="alignment[' . $index . ']" value="center"  />Center</li>
                        <li id="alignment_right"><input type="radio" class="alignment_' . $d['lattach_id'] . '" name="alignment[' . $index . ']" value="right"  />Right</li>
                    </ul>
                </div>
            </div>
            <div class="details_attachment_set clearfix">
                <div class="media_field_left">
                    <strong>Image Size: </strong>
                </div>
                <div class="media_field_right">
                    <input type="radio" class="image_size_' . $d['lattach_id'] . '" name="image_size[' . $index . ']" value="thumbnail" />Thumbnail (' . thumbnail_image_width() . ' x ' . thumbnail_image_height() . ')
                    <input type="radio" class="image_size_' . $d['lattach_id'] . '" name="image_size[' . $index . ']" value="medium" checked="checked" />Medium (' . medium_image_width() . ' x ' . medium_image_height() . ')
                    <input type="radio" class="image_size_' . $d['lattach_id'] . '" name="image_size[' . $index . ']" value="large"  />Large (' . large_image_width() . ' x ' . large_image_height() . ')
                    <input type="radio" class="image_size_' . $d['lattach_id'] . '" name="image_size[' . $index . ']" value="original"  />Original
                    
                    <input type="hidden" class="thumbnail_' . $d['lattach_id'] . '" name="thumbnail[' . $index . ']" value="' . site_url( $d['lattach_loc_thumb'] ) . '" />
                    <input type="hidden" class="medium_' . $d['lattach_id'] . '" name="medium[' . $index . ']" value="' . site_url( $d['lattach_loc_medium'] ) . '" />
                    <input type="hidden" class="large_' . $d['lattach_id'] . '" name="large[' . $index . ']" value="' . site_url( $d['lattach_loc_large'] ) . '" />
                    <input type="hidden" class="original_' . $d['lattach_id'] . '" name="original[' . $index . ']" value="' . site_url( $d['lattach_loc'] ) . '" />
                </div>
            </div>
            <div class="details_attachment_set clearfix">
                <div class="media_field_left">
                    <strong>Link Image to: </strong>
                </div>
                <div class="media_field_right">
                    <input id="link_to_file_' . $d['lattach_id'] . '" type="hidden" class="link_to_file_' . $d['lattach_id'] . '" value="' . site_url( $d['lattach_loc'] ) . '" />
                    <input id="link_to_' . $d['lattach_id'] . '" type="text" class="link_to_' . $d['lattach_id'] . ' textbox" name="link_to[' . $index . ']" value="' . site_url( $d['lattach_loc'] ) . '" />
                    
                    <button id="link_none_' . $d['lattach_id'] . '" type="button" class="link_none_' . $d['lattach_id'] . ' button" value="">None</button>
                    <button id="link_to_image_' . $d['lattach_id'] . '" type="button" class="link_to_image_' . $d['lattach_id'] . ' button" value="' . site_url( $d['lattach_loc'] ) . '">Link to Image</button>
                    
                    <p>Enter a link URL or click above for presets.</p>
                </div>
            </div>
            <div class="details_attachment_set clearfix">
                <ul class="button_navigation" style="margin:20px 0 20px 0px;text-align:right;">
                    <li>' . button( 'button=insert&id=insert_' . $d['lattach_id'] . '&index=' . $index . '&name=insert[' . $index . ']' ) . '&nbsp</li>
                    <li>' . button( 'button=save_changes&id=save_changes_' . $d['lattach_id'] . '&index=' . $index . '&name=save_changes[' . $index . ']' ) . '</li>
                </ul>

                <input type="hidden" class="type_' . $d['lattach_id'] . '" name="type[' . $index . ']" value="' . $d['mime_type'] . '" />
                <input type="hidden" class="textarea_id_' . $d['lattach_id'] . '" name="textarea_id[' . $index . ']" value="' . $textarea_id . '" />
                <input type="hidden" class="attachment_id_' . $d['lattach_id'] . '" name="attachment_id[' . $index . ']" value="' . $d['lattach_id'] . '" />
            </div>';
        }
        else
        {
            $detail .= '
            <div id="response_' . $d['lattach_id'] . '"></div>
            <div class="details_attachment_set clearfix">
                <div class="media_field_left_image">';
                    $detail .= attachment_icon( $d['mime_type'] );
                    $detail .= '
                </div>
                <div class="media_field_right_image">
                    <strong>File Type : </strong>' . $d['mime_type'] . '<br /><br />
                    <strong>Upload Date : </strong>' . date( get_date_format(), strtotime( $d['upload_date'] ) ) . '<br /><br />
                </div>
            </div>
            <div class="details_attachment_set clearfix">
                <div class="media_field_left">
                    <strong>Title: </strong>
                </div>
                <div class="media_field_right">
                    <input type="text" class="textbox title_' . $d['lattach_id'] . '" name="title[' . $index . ']" value="' . $d['ltitle'] . '" />
                </div>
            </div>
            <div class="details_attachment_set clearfix">
                <div class="media_field_left">
                    <strong>Caption: </strong>
                </div>
                <div class="media_field_right">
                    <input type="text" class="textbox caption_' . $d['lattach_id'] . '" name="caption[' . $index . ']" value="' . $d['lcaption'] . '" />
                </div>
            </div>
            <div class="details_attachment_set clearfix">
                <div class="media_field_left">
                    <strong>Description: </strong>
                </div>
                <div class="media_field_right">
                    <textarea class="alt_text_' . $d['lattach_id'] . '" name="alt_text[' . $index . ']" rows="5" />' . $d['lalt_text'] . '</textarea>
                </div>
            </div>
            <div class="details_attachment_set clearfix">
                <div class="media_field_left">
                    <strong>Link File to: </strong>
                </div>
                <div class="media_field_right">
                    <input id="link_to_file_' . $d['lattach_id'] . '" type="hidden" class="link_to_file_' . $d['lattach_id'] . '" value="' . site_url( $d['lattach_loc'] ) . '" />
                    <input id="link_to_' . $d['lattach_id'] . '" type="text" class="link_to_' . $d['lattach_id'] . ' textbox" name="link_to[' . $index . ']" value="' . site_url( $d['lattach_loc'] ) . '" />
                    
                    <button id="link_none_' . $d['lattach_id'] . '" type="button" class="link_none_' . $d['lattach_id'] . ' button" value="">None</button>
                    <button id="link_to_image_' . $d['lattach_id'] . '" type="button" class="link_to_image_' . $d['lattach_id'] . ' button" value="' . site_url( $d['lattach_loc'] ) . '">Link to Image</button>
                    
                    <p>Enter a link URL or click above for presets.</p>
                </div>
            </div>';

            $detail .= '
            <div class="details_attachment_set clearfix">
                <ul class="button_navigation" style="margin:20px 0 20px 0px;text-align:right;">
                    <li>' . button( 'button=insert&type=button&id=insert_' . $d['lattach_id'] . '&index=' . $index . '&name=insert[' . $index . ']' ) . '&nbsp </li>
                    <li>' . button( 'button=save_changes&id=save_changes_' . $d['lattach_id'] . '&index=' . $index . '&name=save_changes[' . $index . ']' ) . '</li>
                </ul>

                <input type="hidden" class="type_' . $d['lattach_id'] . '" name="type[' . $index . ']" value="' . $d['mime_type'] . '" />
                <input type="hidden" class="textarea_id_' . $d['lattach_id'] . '" name="textarea_id[' . $index . ']" value="' . $textarea_id . '" />
                <input type="hidden" class="attachment_id_' . $d['lattach_id'] . '" name="attachment_id[' . $index . ']" value="' . $d['lattach_id'] . '" />
            </div>';
        }

        $detail .= '
        <script type="text/javascript">
            jQuery(function(){
                upload_media_action( ' . $d['lattach_id'] . ' );
            });
        </script>
    </div>';

    return $detail;
}

function get_load_more_js_media()
{
    $type = $_GET['tab'];
    $url  = get_attachment_tab_url( $_GET['tab'] );    
    $js   = "
    <script type=\"text/javascript\">
        jQuery(function(){
            jQuery('.loadMore').live(\"click\",function(){
                var PAGE = jQuery(this).attr(\"id\");
                var TYPE = \"$type\";

                if( PAGE )
                {
                    jQuery(\"#loadMore\"+PAGE).html('<img src=\"" . template_url( 'images/loading.gif', true ) . "\" border=\"0\">');        

                    jQuery.ajax({
                        type: \"POST\",
                        url: \"" . $url . "\",
                        data: \"page=\"+ PAGE + \"&loadMore=\" + TYPE,
                        cache: false,
                        success: function(html){
                            jQuery(\"#media_gallery\").append(html);
                            jQuery(\"#loadMore\"+PAGE).remove(); // removing old more button
                        }
                    });
                }
                else
                {
                    jQuery(\".morebox\").html('The End');// no results
                }

                return false;
            });
        });
    </script>";

    return $js;
}

function get_load_more_link_media( $pageID = '' )
{
    $link = '
    <div id="loadMore' . $pageID . '" class="morebox" style="width: 98.8% !important;">
        <a href="#" class="loadMore" id="' . $pageID . '" rel="1">Load more...</a>
    </div>';

    return $link;
}

/*
| -----------------------------------------------------------------------------
| Media - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_attachment_ajax_url()
{
    return get_state_url( 'ajax&apps=attachment' );
}

function attachment_ajax()
{
    global $db;

    extract( $_POST );

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $pkey ) && $pkey == 'search-attachment' )
    {
        echo search_attachment_results( $s, $tab, $article_id, $textarea_id );
    }

    exit;
}

?>