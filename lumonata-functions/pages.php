<?php

add_actions( 'pages_admin_page', 'page_ajax' );

/*
| -----------------------------------------------------------------------------
| Admin Page
| -----------------------------------------------------------------------------
*/
function get_admin_page()
{
    $post_id = 0;

    run_page_actions();
    
    //-- Display add new form
    if( is_add_new() )
    {
        return add_new_page( $post_id );
    }
    elseif( is_edit() )
    {
        if( is_contributor() || is_author() )
        {
            if( is_num_articles( 'id=' . $_GET[ 'id' ] ) > 0 )
            {
                return edit_page( $_GET[ 'id' ] );
            }
            else
            {
                return '
                <div class="alert_red_form">
                    You don\'t have an authorization to access this page
                </div>';
            }
        }
        else
        {
            return edit_page( $_GET[ 'id' ] );
        }
    }
    elseif( is_delete_all() )
    {
        return delete_batch_page( 0, 'pages', 'Page|Pages' );
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST[ 'id' ] as $key => $val )
        {
            delete_article( $val, 'pages' );
        }
    }
    
    //-- Automatic to display add new when there is no records on database
    if( is_num_articles() == 0 )
    {
        header( 'location:' . get_state_url( 'pages&prc=add_new' ) );
    }
    elseif( is_num_articles() > 0 )
    {
        return get_pages_list( 'pages', 'Page|Pages' );
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Page Actions
| -----------------------------------------------------------------------------
*/
function run_page_actions()
{
    global $db;

    //-- Publish, Unpublish & Save Draft 
    //-- Actions From List Page
    if( isset( $_POST[ 'select' ] ) )
    {
        $status = ( is_save_draft() ? 'draft' : ( is_unpublish() ? 'unpublish' : 'publish' ) );
        
        foreach( $_POST[ 'select' ] as $key => $val )
        {
            update_articles_status( $val, $status );
        }
    }
    else
    {
        if( is_save_draft() || is_publish() )
        {
            $content   = $_POST['post'][0];
            $title     = $_POST['title'][0];
            $id        = $_POST['post_id'][0];
            $status    = is_save_draft() ? 'draft' : 'publish';
            $comments  = isset( $_POST['allow_comments'][0] ) ? 'allowed' : 'not_allowed';
            $share_opt = isset( $_POST['share_option'][0] ) ? $_POST['share_option'][0] : '';
            $sefbox    = isset( $_POST['sef_box'][0] ) ? $_POST['sef_box'][0] : generateSefUrl( $title );
            $post_date = isset( $_POST['post_date'][0] ) ? date('Y-m-d H:i:s', strtotime( $_POST['post_date'][0] ) ) : date('Y-m-d H:i:s');

            if( is_add_new() )
            {
                save_article( $title, $content, $status, 'pages', $post_date, $comments, $sefbox, $share_opt );
            
                $post_id = $db->insert_id();

                attachment_sync( $id, $post_id );
                additional_field_sync( $id, $post_id );
            }
            elseif( is_edit() )
            {
                update_article( $id, $title, $content, $status, 'pages', $post_date, $comments, $share_opt );
            
                $post_id = $id;
            }

            if( isset( $_POST['additional_fields'] ) )
            {
                foreach( $_POST['additional_fields'] as $key => $val )
                {
                    if( $key == 'custom_field' )
                    {
                        $field = array();

                        foreach( $val as $subkey => $subval )
                        {
                            foreach( $subval as $name => $obj )
                            {
                                foreach( $obj as $label => $data )
                                {
                                    foreach( $data as $types => $d )
                                    {
                                        $field[$name] = array( 'type' => $types, 'label' => $label, 'value' => $d );
                                    }
                                }
                            }
                        }
                        
                        edit_additional_field( $post_id, $key, json_encode( $field, JSON_HEX_QUOT | JSON_HEX_TAG ), 'pages' );
                    }
                    else
                    {
                        foreach( $val as $subkey => $subval )
                        {
                            $subval = is_array( $subval ) ? json_encode( $subval ) : $subval;

                            edit_additional_field( $post_id, $key, $subval, 'pages' );
                        }
                    }
                }
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Page - List
| -----------------------------------------------------------------------------
*/
function get_pages_list( $type = 'pages', $title )
{
    $title  = explode( '|', $title );
    $button = get_page_admin_button( get_state_url( $type ) );

    set_template( TEMPLATE_PATH . '/template/pages-list.html', 'pages' );

    add_block( 'list-block', 'l-block', 'pages' );

    add_variable( 'type', $type );
    add_variable( 'button', $button );
    add_variable( 'title', $title[1] );
    add_variable( 'limit', post_viewed() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_page_ajax_url() );
    add_variable( 'view-option', get_page_view_option() );
    
    add_actions( 'section_title', 'Pages' );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );

    parse_template( 'list-block', 'l-block', 'pages' );

    return return_template( 'pages' );
}

/*
| -----------------------------------------------------------------------------
| Admin Page - Add New
| -----------------------------------------------------------------------------
*/
function add_new_page( $post_id = 0 )
{
    $ajax_url = get_page_ajax_url();
    $url      = get_state_url( 'pages' );
    $button   = get_page_admin_button( $url, true );

    set_template( TEMPLATE_PATH . '/template/pages-form.html', 'pages' );

    add_block( 'form-language-btn-block', 'flb-block', 'pages' );
    add_block( 'form-language-loop-block', 'fll-block', 'pages' );
    add_block( 'form-block', 'f-block', 'pages' );

    add_variable( 'title', '' );
    add_variable( 'textarea', textarea( 'post[0]', 0 ) );    
    
    add_variable( 'i', 0 );
    add_variable( 'button', $button );
    add_variable( 'ajax_url', $ajax_url );
    add_variable( 'site_url', site_url() );
    add_variable( 'imgs_url', get_theme_img() );
    add_variable( 'page_title', 'Add New Page' );
    add_variable( 'sef_scheme', get_sef_scheme( 'pages', $ajax_url ) );
    add_variable( 'additional_data', attemp_actions( 'pages_additional_field' ) );
    
    page_language_field( $post_id, 'pages', 'Pages' );

    add_actions( 'section_title', 'Pages - Add New' );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/css/select2.min.css' );

    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/js/select2.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/tinymce@4.9.11/tinymce.min.js' );
    add_actions( 'js_elements', 'get_javascript', 'tinymce' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'pages' );
}

/*
| -----------------------------------------------------------------------------
| Admin Page - Edit
| -----------------------------------------------------------------------------
*/
function edit_page( $post_id = 0 )
{
    global $thepost;
    
    $thepost->post_id    = $post_id;
    $thepost->post_index = 0;

    $ajax_url = get_page_ajax_url();
    $url      = get_state_url( 'pages' );
    $data     = fetch_articles( 'id=' . $post_id );
    $button   = get_page_admin_button( $url, true );

    set_template( TEMPLATE_PATH . '/template/pages-form.html', 'pages' );

    add_block( 'form-language-btn-block', 'flb-block', 'pages' );
    add_block( 'form-language-loop-block', 'fll-block', 'pages' );
    add_block( 'form-block', 'f-block', 'pages' );

    add_variable( 'title', rem_slashes( $data[ 'larticle_title' ] ) );
    add_variable( 'textarea', textarea( 'post[0]', 0, rem_slashes( $data[ 'larticle_content' ] ), $post_id ) );
    
    add_variable( 'i', 0 );
    add_variable( 'button', $button );
    add_variable( 'ajax_url', $ajax_url );
    add_variable( 'site_url', site_url() );
    add_variable( 'imgs_url', get_theme_img() );
    add_variable( 'page_title', 'Edit Page' );
    add_variable( 'sef_scheme', get_sef_scheme( 'pages', $ajax_url, 0, $post_id, $data, true ) );
    add_variable( 'additional_data', attemp_actions( 'pages_additional_field' ) );
    
    page_language_field( $post_id, 'pages', 'Pages' );
    
    add_actions( 'section_title', 'Pages - Edit' );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/css/select2.min.css' );

    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/js/select2.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/tinymce@4.9.11/tinymce.min.js' );
    add_actions( 'js_elements', 'get_javascript', 'tinymce' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'pages' );
}

/*
| -----------------------------------------------------------------------------
| Admin Page - Language Field
| -----------------------------------------------------------------------------
*/
function page_language_field( $post_id = 0, $type, $title, $index = 0 )
{    
    //-- Set Language Form
    $use_multi_lang = get_meta_data( 'multi_language' );
    $lang_list_data = get_all_language();
    
    if( $use_multi_lang == 1 && empty( $lang_list_data ) === FALSE )
    {
        $current_lang = '';

        foreach( $lang_list_data as $d )
        {
            extract( $d );

            $larticle_title   = get_additional_field( $post_id, 'larticle_title_' . $llanguage_code, $type );
            $larticle_content = get_additional_field( $post_id, 'larticle_content_' . $llanguage_code, $type );

            add_variable( 'llanguage', $llanguage );
            add_variable( 'llanguage_index', $index );
            add_variable( 'llanguage_code', $llanguage_code );
            add_variable( 'llanguage_btn_cls', $ldefault == 1 ? 'active' : '' );
            add_variable( 'llanguage_content_css', 'content-lang ' . $llanguage_code );
            add_variable( 'llanguage_additional_data', attemp_actions( $type . '_additional_field_' . $llanguage_code ) );

            add_variable( 'llanguage_larticle_title', $larticle_title );
            add_variable( 'llanguage_textarea', textarea( 'additional_fields[larticle_content_' . $llanguage_code . '][0]', 0, $larticle_content, $post_id, true, false ) );

            add_variable( 'template_url', TEMPLATE_URL );
                
            parse_template( 'form-language-btn-block', 'flb-block', true );

            if( $ldefault == 1 )
            {
                $current_lang = $llanguage_code;

                continue;
            }

            parse_template( 'form-language-loop-block', 'fll-block', true );
        }

        add_variable( 'conten_css', 'content-lang ' . $current_lang );
    }
    else
    {
        add_variable( 'btn_cls', 'hidden' );
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Page - Batch Delete
| -----------------------------------------------------------------------------
*/
function delete_batch_page( $idx, $type, $title )
{
    $title    = explode( '|', $title );
    $backlink = get_state_url( $type );

    set_template( TEMPLATE_PATH . '/template/page-batch-delete.html', 'page-batch-delete' );

    add_block( 'delete-loop-block', 'dl-block', 'page-batch-delete' );
    add_block( 'delete-block', 'd-block', 'page-batch-delete' );

    foreach( $_POST[ 'select' ] as $key => $val )
    {
        $d = fetch_articles( 'id=' . $val . '&type=' . $type );

        add_variable( 'page_id', $d[ 'larticle_id' ] );
        add_variable( 'page_title', $d[ 'larticle_title' ] );

        parse_template( 'delete-loop-block', 'dl-block', true );
    }

    add_variable( 'type', $type );
    add_variable( 'title', $title[ 1 ] );
    add_variable( 'backlink', $backlink );
    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST[ 'select' ] ) == 1 ? 'this ' . $title[ 0 ] : 'these ' . $title[ 1 ] ) );
    
    add_actions( 'section_title', $title[ 1 ] );

    parse_template( 'delete-block', 'd-block', 'page-batch-delete' );

    return return_template( 'page-batch-delete' );
}

/*
| -----------------------------------------------------------------------------
| Admin Page - Table Query
| -----------------------------------------------------------------------------
*/
function get_page_list_query()
{    
    global $db;

    extract( $_POST );

    $rdata = $_REQUEST;
    $cols  = array( 
        1 => 'a.larticle_title',
        3 => 'a.lpost_by',
        4 => 'a.lpost_date', 
        5 => 'a.larticle_status'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            if( isset( $cols[ $rdata['order'][$i]['column'] ] ) )
            {
                $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
            }
        }

        $order = !empty( $o ) ? 'ORDER BY ' . implode( ', ', $o ) : '';
    }
    else
    {
        $order = 'ORDER BY a.lorder ASC';
    }

    if( isset( $show ) && !empty( $show ) && $show != 'all' )
    {
        $where = $db->prepare_query( ' AND larticle_status = %s', $show );
    }
    else
    {
        $where = '';
    }

    if( empty( $rdata['search']['value']) )
    {
        $q = 'SELECT * FROM lumonata_articles AS a WHERE a.larticle_type = "' . $type . '" ' . $where . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $q = 'SELECT * FROM lumonata_articles AS a WHERE a.larticle_type = "' . $type . '" ' . $where . ' AND ( ' . implode( ' OR ', $search ) . ' ) ' . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $url = get_state_url( $type );

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $order     = $d2[ 'lorder' ];
            $id        = $d2[ 'larticle_id' ];
            $user      = fetch_user( $d2[ 'lpost_by' ] );
            $user_link = user_url( $user[ 'luser_id' ] );
            $avatar    = get_avatar( $user[ 'luser_id' ], 3 );
            $category  = get_article_category_list( $d2['larticle_id'], $type );
            $date      = date( get_date_format(), strtotime( $d2[ 'lpost_date' ] ) );
            $title     = isset( $d2[ 'larticle_title_lang' ] ) ? $d2[ 'larticle_title_lang' ] : $d2[ 'larticle_title' ];
            $status    = '<strong style="' . ( $d2[ 'larticle_status' ] != 'publish' ? 'color:red;' : '' ) . '">' . ucfirst( $d2[ 'larticle_status' ] ) . '</strong>';

            $data[] = array(
                'id'        => $id,
                'order'     => $order,
                'date'      => $date,
                'title'     => $title,
                'status'    => $status,
                'avatar'    => $avatar,
                'category'  => $category,
                'user_link' => $user_link,
                'ajax_link' => get_page_ajax_url(),
                'author'    => $user[ 'ldisplay_name' ],
                'edit_link' => $url . '&prc=edit&id=' . $id
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Action Button
| -----------------------------------------------------------------------------
*/
function get_page_admin_button( $new_url = '', $is_form = false )
{
    if( $is_form )
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=save_draft' ) . '</li>
            <li>' . button( 'button=cancel', $new_url ) . '</li>
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=publish' ) . '</li>  
            <li>' . button( 'button=save_draft' ) . '</li>                    
            <li>' . button( 'button=cancel', $new_url ) . '</li>  
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
    }
    else
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>
            <li>' . button( 'button=publish&type=submit&enable=false' ) . '</li>
            <li>' . button( 'button=unpublish&type=submit&enable=false' ) . '</li>';
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Page - View Option
| -----------------------------------------------------------------------------
*/
function get_page_view_option()
{
    $opt_viewed   = '';    
    $show_data    = isset( $_POST[ 'data_to_show' ] ) ? $_POST[ 'data_to_show' ] : ( isset( $_GET[ 'data_to_show' ] ) ? $_GET[ 'data_to_show' ] : '' );
    $data_to_show = array( 'all' => 'All', 'publish' => 'Publish', 'unpublish' => 'Unpublish', 'draft' => 'Draft' );
    
    foreach( $data_to_show as $key => $val )
    {
        if( isset( $show_data ) )
        {
            if( $show_data == $key )
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" checked="checked" />
                <label>' . $val . '</label>';
            }
            else
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" />
                <label>' . $val . '</label>';
            }
        }
        elseif( $key == 'all' )
        {
            $opt_viewed .= '
            <input type="radio" name="data_to_show" value="' . $key . '" checked="checked" />
            <label>' . $val . '</label>';
        }
        else
        {
            $opt_viewed .= '
            <input type="radio" name="data_to_show" value="' . $key . '" />
            <label>' . $val . '</label>';
        }
    }

    return $opt_viewed;
}

/*
| -----------------------------------------------------------------------------
| Admin Page - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_page_ajax_url()
{
    return get_state_url( 'ajax&apps=pages' );
}

/*
| -----------------------------------------------------------------------------
| Admin Page - Ajax Function
| -----------------------------------------------------------------------------
*/
function page_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-page-data' )
    {
        $data = get_page_list_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'delete-pages' )
    {
        if( is_delete( $_POST['state'] ) )
        {
            run_actions( $_POST['state'] . '_additional_delete' );
            
            if( delete_article( $_POST['id'], $_POST['state'] ) )
            {
                $result = array( 'result' => 'success' );
            }
            else
            {
                $result = array( 'result' => 'failed' );
            }
        }
        else
        {
            $result = array( 'result' => 'failed' );
        }

        echo json_encode( $result );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'reorder-data' )
    {
        $result = array( 'result' => 'failed' );

        if( !empty( $_POST['reorder'] ) )
        {
            foreach( $_POST['reorder'] as $id => $order )
            {
                if( update_page_order( $id, $order, $_POST['state'] ) )
                {
                    $result = array( 'result' => 'success' );
                }
            }
        }

        echo json_encode( $result );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'get-sef-scheme' )
    {
        echo get_sef_scheme_content();
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'update-sef-scheme' )
    {
        if( update_sef( $_POST['post_id'], $_POST['title'], $_POST['new_sef'], $_POST['type'] ) )
        {
            $result = array( 'result' => 'success' );
        }
        else
        {
            $result = array( 'result' => 'failed' );
        }

        echo json_encode( $result );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'upload-files' )
    {
        $data = upload_post_file();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-uploaded-files' )
    {
        $data = delete_post_file();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'upload-images' )
    {
        $data = upload_post_image();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-uploaded-images' )
    {
        $data = delete_post_image();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'get-images' )
    {
        $data = get_post_image();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'edit-image-info' )
    {
        $data = edit_post_image_info();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'reorder-images' )
    {
        $data = reorder_post_image();

        echo json_encode( $data );
    }

    exit;
}