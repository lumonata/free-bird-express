<?php

if( isset( $_GET['src'] ) && isset( $_GET['w'] ) && isset( $_GET['h'] ) )
{
    $mwidth  = $_GET['w'];
    $mheight = $_GET['h'];

    $image = @file_get_contents( $_GET['src'] );

    if( $image )
    {
        $im = new Imagick();

        $im->readImageBlob( $image );
        $im->setImageFormat( 'jpg' );

        $geo = $im->getImageGeometry();

        if( $geo[ 'width' ] > $geo[ 'height' ] )
        {
            $scale = ( $geo[ 'width' ] > $mwidth ) ? $mwidth / $geo[ 'width' ] : 1;
        }
        else
        {
            $scale = ( $geo[ 'height' ] > $mheight ) ? $mheight / $geo[ 'height' ] : 1;
        }

        $nwidth  = $scale * $geo[ 'width' ];
        $nheight = $scale * $geo[ 'height' ];

        $im->setImageCompressionQuality( 85 );
        $im->resizeImage( $nwidth, $nheight, Imagick::FILTER_LANCZOS, 1.1 );

        header( 'Content-type: image/jpg' );

        echo $im;

        $im->clear();
        $im->destroy();
    }
}

?>