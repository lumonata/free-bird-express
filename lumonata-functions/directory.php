<?php

function get_dir( $path )
{
    $path = opendir( $path );

    while( $dir = readdir( $path ) )
    {
        if( $dir != '..' && $dir != '.' && $dir != '' && $dir != 'Thumbs.db' )
        {
            $dirlist[] = $dir;
        }
    }

    closedir( $path );

    return $dirlist;
}

function remove_dir( $directory, $empty = false )
{
    if( substr( $directory, -1 ) == '/' )
    {
        $directory = substr( $directory, 0, -1 );
    }
    
    if( !file_exists( $directory ) || !is_dir( $directory ) )
    {
        return false;
    }
    elseif( !is_readable( $directory ) )
    {
        return false;
    }
    elseif( $directory == CLASSES_PATH || $directory == FUNCTIONS_PATH || $directory == ADMIN_PATH || $directory == CONTENT_PATH )
    {
        return false;
    }
    else
    {
        $handle = opendir( $directory );

        while( false !== ( $item = readdir( $handle ) ) )
        {
            if( $item != '.' && $item != '..' )
            {
                $path = $directory . '/' . $item;

                if( is_dir( $path ) )
                {
                    remove_dir( $path );
                }
                else
                {
                    unlink( $path );
                }
            }
        }

        closedir( $handle );
        
        if( $empty == false )
        {
            if( !rmdir( $directory ) )
            {
                return false;
            }
        }

        return true;
    }
}

function create_dir( $path )
{
    if( !is_dir( $path ) )
    {
        return mkdir( $path, 0755 );
    }

    return false;
}

function get_file_parameters( $file, $dir_type )
{    
    $default_parameters = array(
        'Name'        => 'Name',
        'URL'         => 'URL',
        'Version'     => 'Version',
        'Description' => 'Description',
        'Author'      => 'Author',
        'AuthorURL'   => 'Author URL',
        'Path'        => 'Path' 
    );
    
    $parameters = fetch_parameters( $file, $default_parameters, $dir_type );

    return $parameters;
}

function fetch_parameters( $file, $parameters, $dir_type = 'plugins' )
{
    $fp        = fopen( $file, 'r' );
    $file_data = fread( $fp, 1707 );
    
    fclose( $fp );
    
    $allowedtags = array(
        'a'       => array( 'href' => array(), 'title' => array () ),
        'abbr'    => array( 'title' => array () ),
        'acronym' => array( 'title' => array () ),
        'code'    => array(),
        'em'      => array(),
        'strong'  => array ()
    );

    foreach( $parameters as $field => $regex )
    {
        if( $field == 'Path' )
        {            
            $text_domain          = $dir_type == 'apps' ? APPS_PATH : PLUGINS_PATH;
            $the_params[ $field ] = str_replace( $text_domain, '', $file );

            continue;
        }

        preg_match( '/' . preg_quote( $regex, '/' ) . ':(.*)$/mi', $file_data, $match );

        if( empty( $match[ 1 ] ) )
        {
            $the_params[ $field ] = '';
        }
        else
        {
            $the_params[ $field ] = _cleanup_header_comment( kses( $match[ 1 ], $allowedtags ) );
        }
        
    }

    return $the_params;
}

function scan_dir( $dir_type = 'apps', $dir = '' )
{    
    $dirlist      = array();
    $plugin_files = array();
    
    if( $dir_type == 'apps' )
    {
        $root = APPS_PATH;
    }
    elseif( $dir_type == 'plugins' )
    {
        $root = PLUGINS_PATH;
    }
    elseif( $dir_type == 'themes' )
    {
        $root = FRONT_TEMPLATE_PATH . '/' . get_meta_data( 'front_theme', 'themes' );
    }

    if( !empty( $dir ) )
    {
        $root = $root . '/' . $dir;
    }

    if( $handle = opendir( $root ) )
    {
        while( $file = readdir( $handle ) )
        {
            if( substr( $file, 0, 1 ) != '.' )
            {
                $file = $root . '/' . $file;

                if( !is_dir( $file ) )
                {
                    if( $dir_type == 'themes' )
                    {                        
                        if( strtolower( substr( $file, -4 ) ) == '.jpg' || strtolower( substr( $file, -5 ) ) == '.jpeg' || strtolower( substr( $file, -4 ) ) == '.gif' || strtolower( substr( $file, -4 ) ) == '.png' || strtolower( substr( $file, -4 ) ) == '.swf' )
                        {
                            $the_files[] = $file;
                        }
                        
                    }
                    else
                    {
                        if( substr( $file, -4 ) == '.php' )
                        {
                            $the_files[] = $file;
                        }
                    }
                }
                else
                {
                    if( $subhandle = opendir( $file ) )
                    {
                        while( $subfile = readdir( $subhandle ) )
                        {
                            if( substr( $subfile, 0, 1 ) != '.' )
                            {
                                if( $dir_type == 'themes' )
                                {
                                    if( strtolower( substr( $file, -4 ) ) == '.jpg' || strtolower( substr( $file, -5 ) ) == '.jpeg' || strtolower( substr( $file, -4 ) ) == '.gif' || strtolower( substr( $file, -4 ) ) == '.png' || strtolower( substr( $file, -4 ) ) == '.swf' )
                                    {
                                        $the_files[] = $file;
                                    }
                                }
                                else
                                {
                                    if( substr( $subfile, -4 ) == '.php' )
                                    {
                                        $the_files[] = $file . '/' . $subfile;
                                    }
                                }
                            }
                        }

                        closedir( $subhandle );
                    }
                }
            }
        }

        closedir( $handle );
    }
    
    if( $dir_type == 'themes' )
    {
        foreach( $the_files as $key => $val )
        {
            $file = str_replace( $root . '/', '', $val );

            if( !preg_match( '#(.*)thumb(.*)#', $file ) )
            {                
                $tfile      = explode( '.', $file );
                $thumb_file = $tfile[ 0 ] . '-thumb.' . $tfile[ 1 ];

                $master_file[ 'origin' ][] = FRONT_TEMPLATE_URL . '/' . get_meta_data( 'front_theme', 'themes' ) . '/images/headers/' . $file;                
                $master_file[ 'thumb' ][]  = FRONT_TEMPLATE_URL . '/' . get_meta_data( 'front_theme', 'themes' ) . '/images/headers/' . $thumb_file;
            }
        }
    }
    else
    {
        foreach( $the_files as $the_file )
        {
            if( is_readable( $the_file ) )
            {
                $param = get_file_parameters( $the_file, $dir_type );

                if( !empty( $param[ 'Name' ] ) )
                {
                    $master_file[ generateSefUrl( $param[ 'Name' ] ) ] = $param;
                }
            }
        }
    }
    
    return $master_file;    
}

?>