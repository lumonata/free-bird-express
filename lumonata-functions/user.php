<?php

add_actions( 'user_admin_page', 'user_ajax' );

/**
* To check if the URI requested is call login form.
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return boolean
* 
*/
function is_login_form()
{
    if( empty( $_GET['state'] ) || $_GET['state'] == 'login' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
* To check if the URI requested is call registration form.
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return boolean
* 
*/
function is_register_form()
{
    if( isset( $_GET['state'] ) && $_GET['state'] == 'register' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
* To check if the URI requested is call thanks form after registration.
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return boolean
*  
*/
function is_thanks_page()
{
    if( isset( $_GET['state'] ) && $_GET['state'] == 'thanks' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
* To check if the URI requested is call email verification form.
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return boolean
*  
*/
function is_verify_account()
{
    if( isset( $_GET['state'] ) && $_GET['state'] == 'verify' && isset( $_GET['token'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
* To check if the URI requested is call forget password form.
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return boolean
*  
*/
function is_forget_password()
{
    if( !empty( $_GET['state'] ) && $_GET['state'] == 'forget_password' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
* To check if the URI contain redirection.
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return boolean 
* 
*/
function is_redirect()
{
    if( !empty( $_GET['redirect'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
* To check if the user already logi or not.
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return boolean 
* 
*/
function is_user_logged()
{
    if( isset( $_COOKIE['user_id'] ) && isset( $_COOKIE['password'] ) && isset( $_COOKIE['thecookie'] ) )
    {
        if( md5( $_COOKIE['password'] . $_COOKIE['user_id'] ) == $_COOKIE['thecookie'] )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

/**
* Login POST action and validate the input.
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return boolean 
* 
*/
function post_login()
{
    if( count( $_POST ) > 0 )
    {
        return validate_login();
    }
}

/**
* Validate the login input.
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return boolean 
* 
*/
function validate_login()
{
    if( empty( $_POST['username'] ) || empty( $_POST['password'] ) )
    {
        return '<div class="alert_red">Empty Username or Password.</div>';
    }
    else
    {
        if( is_exist_user( $_POST['username'] ) && is_match_password() )
        {
            if( isset( $_POST['remember_login'] ) )
            {
                //-- Remember username and password for 1 year
                setcookie( 'username', $_POST['username'], time() + 60 * 60 * 24 * 365, '/' );
                setcookie( 'password', $_POST['password'], time() + 60 * 60 * 24 * 365, '/' );

                $d = fetch_user( $_POST['username'] );

                setcookie( 'user_id', $d['luser_id'], time() + 60 * 60 * 24 * 365, '/' );
                setcookie( 'user_type', $d['luser_type'], time() + 60 * 60 * 24 * 365, '/' );
                setcookie( 'user_name', $d['ldisplay_name'], time() + 60 * 60 * 24 * 365, '/' );
                setcookie( 'thecookie', md5( $_POST['password'] . $d['luser_id'] ), time() + 60 * 60 * 24 * 365, '/' );
            }
            else
            {
                //-- Cookie expires when browser closes
                setcookie( 'username', $_POST['username'], false, '/' );
                setcookie( 'password', $_POST['password'], false, '/' );

                $d = fetch_user( $_POST['username'] );

                setcookie( 'user_id', $d['luser_id'], false, '/' );
                setcookie( 'user_type', $d['luser_type'], false, '/' );
                setcookie( 'user_name', $d['ldisplay_name'], false, '/' );
                setcookie( 'thecookie', md5( $_POST['password'] . $d['luser_id'] ), false, '/' );
            }

            if( is_redirect() )
            {
                header( 'location:' . base64_decode( $_GET['redirect'] ) );
            }
            else
            {
                header( 'location:' . get_state_url( 'dashboard' ) );
            }
        }
        else
        {
            return '<div class="alert_red">Wrong Username or Password.</div>';
        }
    }
}

/**
* Logout action to destroy the cookie
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return boolean 
* 
*/
function do_logout()
{
    setcookie( 'user_id', '', time() - 3600, '/' );
    setcookie( 'username', '', time() - 3600, '/' );
    setcookie( 'password', '', time() - 3600, '/' );
    setcookie( 'user_type', '', time() - 3600, '/' );
    setcookie( 'user_name', '', time() - 3600, '/' );
    setcookie( 'thecookie', '', time() - 3600, '/' );

    header( 'location:' . get_state_url( 'login' ) );
}

/**
* To check the if the user exist or not.
*
* @author Wahya Biantara
*
* @since alpha
* 
* @param string $username username
* 
* @return boolean 
* 
*/
function is_exist_user( $username )
{
    global $db;

    $s = 'SELECT * FROM lumonata_users WHERE lusername = %s';
    $q = $db->prepare_query( $s, $username );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
* To check if the email address is exist or not.
*
* @author Wahya Biantara
*
* @since alpha
* 
* @param string $email email address
* 
* @return boolean 
* 
*/
function is_exist_email( $email )
{
    global $db;

    $s = 'SELECT * FROM lumonata_users WHERE lemail = %s';
    $q = $db->prepare_query( $s, $email );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
* To count lumonata_user table on database.
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return Integer number of users
* 
*/
function is_num_users()
{
    global $db;

    $s = 'SELECT * FROM lumonata_users';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );

    return $db->num_rows( $r );
}

/**
* To check if the password that sent from the POST variable are match with the mention user name
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return boolean
* 
*/
function is_match_password()
{
    global $db;

    $s = 'SELECT * FROM lumonata_users WHERE lusername = %s AND lpassword = %s AND lstatus = %d';
    $q = $db->prepare_query( $s, $_POST['username'], md5( $_POST['password'] ), 1 );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
* Login Form Design
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return string login form html
* 
*/
function get_login_form()
{
    set_template( TEMPLATE_PATH . '/template/login.html' );

    add_block( 'mainBlock', 'mBlock' );

    if( isset( $_COOKIE['username'] ) )
    {
        add_variable( 'username', $_COOKIE['username'] );
    }

    if( isset( $_COOKIE['password'] ) )
    {
        add_variable( 'password', $_COOKIE['password'] );
    }

    add_variable( 'alert', post_login() );
    add_variable( 'web_title', web_title() );
    add_variable( 'style_sheet', get_css() );
    add_variable( 'login_action', current_page_url() );

    parse_template( 'mainBlock', 'mBlock' );
    print_template();
}

/**
* The Sign Up URL
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return string Sign up URL
* 
*/
function signup_url()
{
    return site_url( 'lumonata-admin/?state=register' );
}

/**
* The Sign In URL
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return string Sign In URL
* 
*/
function signin_url()
{
    return site_url( 'lumonata-admin/?state=login&redirect=' . current_page_url() );
}

/**
* URL of user profile when login
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return string User 
* 
*/
function user_url( $id )
{
    return site_url( 'lumonata-admin/?state=my-profile&amp;id=' . $id );
}

/**
* Call the Login Form Design
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return string Login HTML Design 
* 
*/
function sign_in_form()
{
    set_template( TEMPLATE_PATH . '/template/login.html', 'sign_in_form' );

    add_block( 'signin_form', 'mBlock', 'sign_in_form' );

    if( isset( $_SERVER['HTTP_REFERER'] ) )
    {
        add_variable( 'action', get_admin_url() . '/?redirect=' . $_SERVER['HTTP_REFERER'] );
    }
    else
    {
        add_variable( 'action', get_admin_url() . '/' );
    }

    add_variable( 'signup_url', signup_url() );

    parse_template( 'signin_form', 'mBlock' );

    return return_template( 'sign_in_form' );
}

/**
* Password reseter form. In this function the sending process also executed here 
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return string Resend HTML Design 
* 
*/
function resendPassword()
{
    if( !isset( $_GET['uep'] ) )
        header( "location:" . get_admin_url() . "/?state=login" );
    $uep = base64_decode( $_GET['uep'] );
    $uep = json_decode( $uep, true );
    if( !is_array( $uep ) )
        header( "location:" . get_admin_url() . "/?state=login" );
    //Resend
    if( isset( $_POST['resend'] ) )
    {
        //send activation email
        $token = $uep['key']['token'];
        send_register_notification( $uep['key']['username'], $uep['key']['email'], $uep['key']['password'], $token );
        add_variable( 'resendto', " Resent to: " . $uep['key']['email'] );
        //print the template
        parse_template( 'thanksPage', 'thxBlock' );
        print_template( 'signup_form' );
    }
    //set template
    set_template( TEMPLATE_PATH . "/template/resend-password.html", "resend" );
    //set block
    add_block( 'thanksPage', 'thxBlock', "resend" );
    add_variable( 'web_title', web_title() );
    add_variable( 'style_sheet', get_css() );
    add_variable( 'email', $uep['key']['email'] );
    add_variable( 'username', $uep['key']['username'] );
    add_variable( 'password', $uep['key']['password'] );
    //print the template
    parse_template( 'thanksPage', 'thxBlock' );
    print_template( 'resend' );
}

/**
* Function to verify email activation process
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return string The activation process status and the login form when active 
* 
*/
function verify_account()
{
    global $db;
    set_template( TEMPLATE_PATH . "/template/verify-account.html", "verify" );
    //set block
    add_block( 'verifyPage', 'vrfBlock', "verify" );
    if( !isset( $_GET['token'] ) )
        header( "location:" . get_admin_url() . "/?state=login" );
    $themail      = explode( ".", $_GET['token'] );
    $sql          = $db->prepare_query( "SELECT * FROM lumonata_users

                                 WHERE lactivation_key=%s AND lstatus=0", $themail[0] );
    $r            = $db->do_query( $sql );
    $invited_user = $db->fetch_array( $r );
    if( $db->num_rows( $r ) > 0 )
    {
        $query  = $db->prepare_query( "UPDATE lumonata_users

        								SET lstatus=1

        								WHERE lactivation_key=%s", $themail[0] );
        $result = $db->do_query( $query );
        if( $result )
        {
            // add_friend_list( $invited_user['luser_id'], "Work" );
            // add_friend_list( $invited_user['luser_id'], "School" );
            // add_friend_list( $invited_user['luser_id'], "Family" );
            // if( !empty( $themail[1] ) )
            // {
            //     $query                 = $db->prepare_query( "UPDATE lumonata_comments

        				// 						SET lcomment_status='approved' 

        				// 						WHERE lcomentator_email=%s", base64_decode( $themail[1] ) );
            //     $update_comment_status = $db->do_query( $query );
            // }
            $status = "<div class=\"alert_yellow\">

        						Activation process succeeded, thanks for doing the activation. 

        						Please sign in using the form below:

        				</div>";
            $status .= "<h2>Sign In</h2>";
            $status .= "<form method=\"post\" action=\"" . get_admin_url() . "/\">

	        				<table cellspacing=\"0\" >

								<tr>

									<td>Username:</td>

									<td><input type=\"text\" name=\"username\" class=\"inputtext\" style=\"width:300px;\"  /></td>

								</tr>

								<tr>

									<td>Password:</td>

									<td><input type=\"password\" name=\"password\" class=\"inputtext\" style=\"width:300px;\"  /></td>

								</tr>

								<tr>

									<td></td>

									<td><input type=\"submit\" value=\"Sign In\" name=\"login\" class=\"button\" /></td>

								</tr>

							</table>

						</form>";
            //add new user to administrator
            //if invite from friend
            if( isset( $_GET['iid'] ) && !empty( $_GET['iid'] ) )
            {
                $invitr_user = fetch_user( $_GET['iid'] );
                    
                request_approved_mail( $invitr_user['lemail'], $invitr_user['ldisplay_name'], $invited_user['luser_id'], $invited_user['lsex'] );
            }
        }
    }
    else
    {
        $status = "<p>You have done the activation process before.</p>";
    }
    add_variable( 'web_title', web_title() );
    add_variable( 'style_sheet', get_css() );
    add_variable( 'status', $status );
    //print the template
    parse_template( 'verifyPage', 'vrfBlock' );
    print_template( 'verify' );
}

/**
* Function to handle forgot password action 
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return string Action status 
* 
*/
function post_forget_password()
{
    if( count( $_POST ) > 0 )
    {
        return validate_forget_password();
    }
    else
    {
        return '
        <div class="alert_yellow">
            Please enter your username or e-mail address. You will receive a new password via e-mail.
        </div>';
    }
}

/**
* Function to check the email input, when request a new password
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return string Action status
* 
*/
function validate_forget_password()
{
    if( empty( $_POST['user_email'] ) )
    {
        return "<div class=\"alert_yellow\">Please enter your username or e-mail address. You will receive a new password via e-mail.</div>";
    }
    else
    {
        if( is_exist_email( $_POST['user_email'] ) )
        {
            $new_password = random_string();
            $user_id      = fetch_user_id_by_email( $_POST['user_email'] );
            $user         = fetch_user( $user_id );
            if( reset_user_password( $user_id, $new_password ) )
                $return = reset_password_email( $_POST['user_email'], $user['lusername'], $user['ldisplay_name'], $new_password );
            else
                return false;
        }
        elseif( is_exist_user( $_POST['user_email'] ) )
        {
            $new_password = random_string();
            $user_id      = fetch_user_id_by_username( $_POST['user_email'] );
            $user         = fetch_user( $user_id );
            if( reset_user_password( $user_id, $new_password ) )
            {
                $return = reset_password_email( $user['lemail'], $user['lusername'], $user['ldisplay_name'], $new_password );
            }
            else
                return false;
        }
        else
        {
            return "<div class=\"alert_yellow\">Please enter your valid username or e-mail address.</div>";
        }
        if( $return )
            return "<div class=\"alert_yellow\">Your password has been reseted. Please check your email.</div>";
    }
}

/**
* Call Forget Password HTML Design
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return string HTML Design  
* 
*/
function get_forget_password_form()
{
    set_template( TEMPLATE_PATH . '/template/forget-password.html' );

    add_block( 'mainBlock', 'mBlock' );

    add_variable( 'web_title', web_title() );
    add_variable( 'style_sheet', get_css() );
    add_variable( 'login_action', current_page_url() );
    add_variable( 'alert', post_forget_password() );

    parse_template( 'mainBlock', 'mBlock' );    
    print_template();
}

/**
* Is used in admin area, to get the list of registered user. Navigation button are configured here.
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return string list of registered user in HTML  
* 
*/
function get_users_list( $tabs = '' )
{
    $url = get_state_url( 'users' );

    set_template( TEMPLATE_PATH . '/template/users-list.html', 'users' );

    add_block( 'list-block', 'l-block', 'users' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_users_ajax_url() );
    add_variable( 'button', get_admin_button( $url ) );

    add_actions( 'section_title', 'Users' );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );

    parse_template( 'list-block', 'l-block', 'users' );

    return return_template( 'users' );
}

function get_users_ajax_url()
{
    return get_state_url( 'ajax&apps=user' );
}

/**
* Is used in admin area, to get the detail list of registered user. 
*
* @author Ngurah Rai
*
* @since alpha
* 
* @return array list of registered user in JSON 
* 
*/
function get_users_list_query()
{    
    global $db;

    extract( $_POST );

    $rdata = $_REQUEST;
    $cols  = array( 
        1 => 'a.lusername',
        2 => 'a.ldisplay_name',
        4 => 'a.luser_type'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            if( isset( $cols[ $rdata['order'][$i]['column'] ] ) )
            {
                $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
            }
        }

        $order = !empty( $o ) ? 'ORDER BY ' . implode( ', ', $o ) : '';
    }
    else
    {
        $order = 'ORDER BY a.luser_id DESC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $q = 'SELECT * FROM lumonata_users AS a ' . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $q = 'SELECT * FROM lumonata_users AS a WHERE ( ' . implode( ' OR ', $search ) . ' ) ' . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $url = get_state_url( 'users' );

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'email'     => $d2['lemail'],
                'id'        => $d2['luser_id'],
                'username'  => $d2['lusername'],
                'name'      => $d2['ldisplay_name'],
                'ajax_link' => get_users_ajax_url(),
                'user_link' => user_url( $d2['luser_id'] ),
                'usertype'  => ucfirst( $d2['luser_type'] ),
                'avatar'    => get_avatar( $d2['luser_id'], 3 ),
                'edit_link' => get_state_url( 'users&prc=edit&id=' . $d2[ 'luser_id' ] )
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/**
* Will return the user type in array 
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return array type of user
* 
*/
function user_type()
{
    global $db;

    $user_type = array(
        'author'        => 'Author',
        'editor'        => 'Editor',
        'standard'      => 'Standard',
        'contributor'   => 'Contributor',
        'administrator' => 'Administrator' 
    );

    $s = 'SELECT * FROM lumonata_meta_data WHERE lapp_name = %s';
    $q = $db->prepare_query( $s, 'user_privileges' );
    $r = $db->do_query($q);

    if( $db->num_rows($r) > 0 )
    {
        while( $d=$db->fetch_array($r) )
        {
            $value = json_decode( $d['lmeta_value'], true );

            $user_type[ $d['lmeta_name'] ] = $value['name'];
        }
    }

    return $user_type;
}

/**
* Save user to database
*
* @author Wahya Biantara
*
* @since alpha
* 
* @param string $username unique username
* @param string $password unique password
* @param string $email email address
* @param integer $sex 1=Male, 2=Female
* @param string $user_type Default user type are: standard, contributor, author, editor, administrator
* @param string $birthday The date format is Y-m-d H:i:s
* @param string $status the user status(0=pendding activation,1=active, 2=blocked)
* 
* @return boolean True if the insert process is success  
* 
*/
function save_user( $username, $password, $email, $sex, $user_type, $birthday, $status = 0, $display_name = '' )
{
    global $db;

    $regis_date   = date( 'Y-m-d H:i:s' );
    $activate_key = md5( $username . $email . $password );

    if( empty( $display_name ) )
    {
        $display_name = $username;
    }

    $s = 'INSERT INTO lumonata_users(
            lusername,
            ldisplay_name,
            lpassword,
            lemail,
            lsex,
            lregistration_date,
            luser_type,
            lactivation_key,
            lbirthday,
            lstatus,
            ldlu)
          VALUES( %s, %s, %s, %s, %d, %s, %s, %s, %s, %d, %s )';
    $q = $db->prepare_query( $s, $username, $display_name, md5( $password ), $email, $sex, $regis_date, $user_type, $activate_key, $birthday, $status, $regis_date );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/**
* Update user password to DB  
*
* @author Wahya Biantara
*
* @since alpha
* 
* @param integer $user_id User ID 
* @param string $new_password New Password
* 
* @return boolean True if the reset process is success 
* 
*/
function reset_user_password( $user_id, $new_password )
{
    global $db;

    if( empty( $user_id ) || empty( $new_password ) )
    {
        return;
    }

    $s = 'UPDATE lumonata_users SET lpassword = %s WHERE luser_id = %d';
    $q = $db->prepare_query( $s, md5( $new_password ), $user_id );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/**
* Used to edit user database   
*
* @author Wahya Biantara
*
* @since alpha
* 
* @param integer $id edited user ID
* @param string $display_name User display name
* @param string $password New password if any change
* @param string $email New email address if applicable
* @param integer $sex 1=Male, 2=Female
* @param string $user_type Default user type are: standard, contributor, author, editor, administrator
* @param string $birthday The date format is Y-m-d H:i:s
* @param string $status the user status(0=pendding activation,1=active, 2=blocked)
* 
* @return boolean True if the insert process is success  
* 
*/
function edit_user( $id, $display_name, $password, $email, $sex, $user_type, $birthday, $status = 0 )
{
    global $db;

    if( empty( $password ) )
    {
        $s = 'UPDATE lumonata_users SET 
                ldisplay_name = %s,
                lsex = %d,
                luser_type = %s,
                lbirthday = %s,
                lstatus = %d,
                ldlu = %s,
                lemail = %s
              WHERE luser_id=%d';
        $q = $db->prepare_query( $s, $display_name, $sex, $user_type, $birthday, $status, date( 'Y-m-d H:i:s' ), $email, $id );
    }
    else
    {
        $s = 'UPDATE lumonata_users SET 
                ldisplay_name = %s,
                lsex = %d,
                lpassword = %s,
                luser_type = %s,
                lbirthday = %s,
                lstatus = %d,
                ldlu = %s,
                lemail = %s
              WHERE luser_id = %d';
        $q = $db->prepare_query( $s, $display_name, $sex, md5( $password ), $user_type, $birthday, $status, date( 'Y-m-d H:i:s' ), $email, $id );
    }

    return $db->do_query( $q );
}

/**
* Delete user from database   
*
* @author Wahya Biantara
*
* @since alpha
* 
* @param integer $id deleted user ID
* 
* @return boolean True if the delete process is success 
* 
*/
function delete_user( $id )
{
    global $db;

    if( $id != 1 )
    {
        $q = $db->prepare_query( 'DELETE FROM lumonata_users WHERE luser_id = %d', $id );

        if( is_user_logged() )
        {
            if( $_COOKIE['user_id'] == $id )
            {
                return false;
            }
            else
            {
                $r = $db->do_query( $q );

                if( is_array( $r ) )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        else
        {
            $r = $db->do_query( $q );

            if( is_array( $r ) )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}

/**
* Validate the user input   
*
* @author Wahya Biantara
*
* @since alpha
* 
* @param string $username unique username
* @param string $password unique password
* @param string $re_password must have same value with password
* @param string $email Email address
* @param integer $sex 1=Male, 2=Female
* @param string $website User website address
* 
* @return string when data are good return OK, if not return the alert text  
* 
*/
function is_valid_user_input( $username, $first_name, $last_name, $password, $re_password, $email, $sex, $website )
{
    if( empty( $username ) )
    {
        return '<div class="alert_red_form">Please specifiy your username</div>';
    }

    if( strlen( $username ) < 5 )
    {
        return '<div class="alert_red_form">Username <em>$username</em> should be at least five characters long</div>';
    }

    if( is_exist_user( $username ) && ( is_add_new() || is_register_form() ) )
    {
        return '<div class="alert_red_form">Username <em>$username</em> is not available, please try another username</div>';
    }

    if( empty( $first_name ) )
    {
        return '<div class="alert_red_form">Please specifiy your first name</div>';
    }

    if( empty( $last_name ) )
    {
        return '<div class="alert_red_form">Please specifiy your last name</div>';
    }

    if( empty( $email ) )
    {
        return '<div class="alert_red_form">Please specifiy your email</div>';
    }

    if( is_exist_email( $email ) && ( is_add_new() || is_register_form() ) )
    {
        return '<div class="alert_red_form">This email is already taken. Please choose another email</div>';
    }

    if( empty( $sex ) )
    {
        return '<div class="alert_red_form">Please select your gender</div>';
    }

    if( !isEmailAddress( $email ) )
    {
        return '<div class="alert_red_form">Invalid email format(<em>$email</em>) </div>';
    }

    if( !empty( $website ) && $website != 'http://' )
    {
        if( !is_website_address( $website ) )
        {
            return '<div class="alert_red_form">Invalid website format (<em>$website</em>)</div>';
        }
    }

    if( is_add_new() || is_category( 'appname=register' ) || $_GET['state'] == 'register' )
    {
        if( empty( $password ) || strlen( $password ) < 7 )
        {
            return '<div class="alert_red_form">The password should be at least seven characters long</div>';
        }
        if( $password != $re_password )
        {
            return '<div class="alert_red_form">Password do not match</div>';
        }
    }
    elseif( is_edit() || is_edit_all() )
    {
        if( !empty( $password ) )
        {
            if( strlen( $password ) < 7 )
            {
                return '<div class="alert_red_form">' . $username . 's password should be at least seven characters long</div>';
            }
            elseif( $password != $re_password )
            {
                return '<div class="alert_red_form">' . $username . 's password do not match</div>';
            }
        }
    }

    return 'OK';
}

function get_admin_user()
{
    global $db;

    $alert = '';

    if( is_save_changes() )
    {
        extract( $_POST );

        $alert       = '';
        $thebirthday = '0000-00-00';
        
        if( is_add_new() )
        {
            $validation_rs = is_valid_user_input( $username, $first_name, $last_name, $password, $re_password, $email, $sex, $website );
            
            if( $validation_rs == 'OK' )
            {
                if( !empty( $birthday ) && !empty( $birthmonth ) && !empty( $birthyear ) )
                {
                    $thebirthday = $birthmonth . '/' . $birthday . '/' . $birthyear;
                    $thebirthday = date( "Y-m-d", strtotime( $thebirthday ) );
                }
                
                $display_name = $first_name . ' ' . $last_name;
                
                if( save_user( $username, $password, $email, $sex, $user_type, $thebirthday, $status, $display_name ) )
                {
                    $user_id = $db->insert_id();                    
                    
                    if( !empty( $first_name ) )
                    {
                        add_additional_field( $user_id, 'first_name', $first_name, 'user' );
                    }
                    
                    add_additional_field( $user_id, 'last_name', $last_name, 'user' );
                    
                    add_additional_field( $user_id, 'website', $website, 'user' );

                    //-- Send message is checked
                    if( isset( $send ) )
                    {
                        $token = md5( $username . $email . $password ) . '.';

                        send_register_notification( $username, $email, $password, $token );
                    }
                    
                    header( 'location:' . get_state_url( 'users&prc=add_new' ) );
                }
            }
            else
            {
                $alert = $validation_rs;
            }
        }
        elseif( is_edit() )
        {
            $validation_rs = is_valid_user_input( $username[ 0 ], $first_name[ 0 ], $last_name[ 0 ], $password[ 0 ], $re_password[ 0 ], $email[ 0 ], $sex[ 0 ], $website[ 0 ] );
            
            if( $validation_rs == 'OK' )
            {
                if( !empty( $birthday[ 0 ] ) && !empty( $birthmonth[ 0 ] ) && !empty( $birthyear[ 0 ] ) )
                {
                    $thebirthday = $birthmonth[ 0 ] . '/' . $birthday[ 0 ] . '/' . $birthyear[ 0 ];
                    $thebirthday = date( 'Y-m-d', strtotime( $thebirthday ) );
                }
                
                if( edit_user( $_GET[ 'id' ], $display_name[ 0 ], $password[ 0 ], $email[ 0 ], $sex[ 0 ], $user_type[ 0 ], $thebirthday, $status[ 0 ] ) )
                {
                    edit_additional_field( $_GET[ 'id' ], 'first_name', $first_name[ 0 ], 'user' );
                    edit_additional_field( $_GET[ 'id' ], 'last_name', $last_name[ 0 ], 'user' );
                    edit_additional_field( $_GET[ 'id' ], 'website', $website[ 0 ], 'user' );
                    
                    //-- Send message is checked
                    if( isset( $send[ 0 ] ) )
                    {
                        $token = md5( $username[ 0 ] . $email[ 0 ] ) . '#';

                        send_register_notification( $username[ 0 ], $email[ 0 ], $password[ 0 ], $token );
                    }
                    
                    header( 'location:' . get_state_url( 'users' ) );
                }
            }
            else
            {
                $alert = $validation_rs;
            }
        }
        elseif( is_edit_all() )
        {
            foreach( $select as $key => $val )
            {
                $validation_rs = is_valid_user_input( $username[ $key ], $first_name[ $key ], $last_name[ $key ], $password[ $key ], $re_password[ $key ], $email[ $key ], $sex[ $key ], $website[ $key ] );
                
                if( $validation_rs == 'OK' )
                {
                    if( !empty( $birthday[ $key ] ) && !empty( $birthmonth[ $key ] ) && !empty( $birthyear[ $key ] ) )
                    {
                        $thebirthday = $birthmonth[ $key ] . '/' . $birthday[ $key ] . '/' . $birthyear[ $key ];
                        $thebirthday = date( 'Y-m-d', strtotime( $thebirthday ) );
                    }
                    
                    if( edit_user( $val, $display_name[ $key ], $password[ $key ], $email[ $key ], $sex[ $key ], $user_type[ $key ], $thebirthday, $status[ $key ] ) )
                    {
                        edit_additional_field( $val, 'first_name', $first_name[ $key ], 'user' );
                        edit_additional_field( $val, 'last_name', $last_name[ $key ], 'user' );
                        edit_additional_field( $val, 'website', $website[ $key ], 'user' );
                    
                        //-- send message is checked
                        if( isset( $send[ $key ] ) )
                        {
                            $token = md5( $username[ $key ] . $email[ $key ] ) . '#';

                            send_register_notification( $username[ $key ], $email[ $key ], $password[ $key ], $token );
                        }
                    }
                }
                else
                {
                    $alert = $validation_rs;

                    break;
                }
            }
            
            if( $validation_rs == 'OK' )
            {
                header( 'location:' . get_state_url( 'users' ) );
            }
        }
        
        add_variable( 'alert', $alert );
    }
    
    //-- Is add new user    
    if( is_add_new() )
    {
        set_template( TEMPLATE_PATH . '/template/users.html', 'users' );
        
        add_block( 'usersAddNew', 'uAddNew', 'users' );
        add_block( 'usersEdit', 'uEdit', 'users' );
        add_block( 'profilePicture', 'pPicture', 'users' );
        
        add_actions( 'section_title', 'Add New User' );

        add_actions( 'js_elements', 'get_javascript', 'password_strength' );
        add_actions( 'js_elements', 'get_javascript', 'password' );
        
        add_variable( 'website', 'http://' );
        
        if( is_save_changes() )
        {
            add_variable( 'email', $_POST[ 'email' ] );
            add_variable( 'website', $_POST[ 'website' ] );
            add_variable( 'user_name', $_POST[ 'username' ] );
            add_variable( 'last_name', $_POST[ 'last_name' ] );
            add_variable( 'first_name', $_POST[ 'first_name' ] );
        }
        
        add_variable( 'prc', 'Add New User' );
        add_variable( 'save_user', button( 'button=save_changes&label=Save User' ) );
        add_variable( 'cancel', button( 'button=cancel', get_state_url( 'users' ) ) );
        
        //-- User Type
        $user_type = '
        <fieldset>
            <p><label>User Type:</label></p>
            <select name="user_type" class="user-type-option no-margin" autocomplete="off">';
            
                foreach( user_type() as $key => $val )
                {
                    if( is_save_changes() )
                    {
                        if( $key == $_POST[ 'user_type' ] )
                        {
                            $user_type .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                        }
                    }
                    
                    $user_type .= '<option value="' . $key . '">' . $val . '</option>';
                }
                
                $user_type .= '
            </select>

            ' . new_user_type_form() . '
        </fieldset>';
        
        add_variable( 'user_type', $user_type );
        
        //-- Gender
        $sexar = array( '1' => 'Male', '2' => 'Female' );    
        $sex   = '
        <select name="sex" class="no-margin">
            <option value="">Select Sex</option>';

                foreach( $sexar as $key => $val )
                {
                    if( is_save_changes() )
                    {
                        if( $key == $_POST[ 'sex' ] )
                        {
                            $sex .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                        }
                    }
                    
                    $sex .= '<option value="' . $key . '">' . $val . '</option>';
                }
        
            $sex .= '
        </select>';
        
        add_variable( 'sex', $sex );
        add_variable( 'send_checked', '' );
        
        //-- Birthday
        $birthday   = ( isset( $_POST[ 'birthday' ] ) ) ? $_POST[ 'birthday' ] : '';
        $birthyear  = ( isset( $_POST[ 'birthyear' ] ) ) ? $_POST[ 'birthyear' ] : '';
        $birthmonth = ( isset( $_POST[ 'birthmonth' ] ) ) ? $_POST[ 'birthmonth' ] : '';

        get_date_picker( 'js_elements', $birthday, $birthmonth, $birthyear );
        
        //-- User status
        $user_status = array( 0 => 'Waiting Email Validation', 1 => 'Active', 2 => 'Block' );
        $the_status  = '
        <fieldset>
            <p><label>Status:</label></p>
            <select name="status" class="no-margin">';
        
                foreach( $user_status as $key => $val )
                {
                    if( is_save_changes() )
                    {
                        if( $key == $_POST[ 'sex' ] )
                        {
                            $the_status .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                        }
                    }
                    elseif( $key == 1 )
                    {
                        $the_status .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                    }
                    else
                    {
                        $the_status .= '<option value="' . $key . '" >' . $val . '</option>';
                    }
                }
                
            $the_status .= '
            </select>
        </fieldset>';
        
        add_variable( 'user_status', $the_status );
        add_variable( 'ajax_url', get_users_ajax_url() );
        
        parse_template( 'usersAddNew', 'uAddNew' );

        return return_template( 'users' );
    }
    elseif( is_edit() )
    {
        //-- if edit single user
        set_template( TEMPLATE_PATH . '/template/users.html', 'users' );
        
        add_block( 'usersEdit', 'uEdit', 'users' );
        add_block( 'usersAddNew', 'uAddNew', 'users' );
        add_block( 'profilePicture', 'pPicture', 'users' );
        
        add_actions( 'section_title', 'Edit User' );
        
        add_actions( 'js_elements', 'get_javascript', 'password_strength' );
        
        add_variable( 'i', 0 );
        add_variable( 'website', 'http://' );
        
        if( is_save_changes() )
        {
            add_variable( 'email', $_POST[ 'email' ][ 0 ] );
            add_variable( 'website', $_POST[ 'website' ][ 0 ] );
            add_variable( 'username', $_POST[ 'username' ][ 0 ] );
            add_variable( 'last_name', $_POST[ 'last_name' ][ 0 ] );
            add_variable( 'first_name', $_POST[ 'first_name' ][ 0 ] );
            
            //-- find the user type
            $user_type = '
            <fieldset>
                <p><label>User Type:</label></p>
                <select name="user_type[0]" class="user-type-option no-margin">';
            
                    foreach( user_type() as $key => $val )
                    {
                        if( $key == $_POST[ 'user_type' ][ 0 ] )
                        {
                            $user_type .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                        }
                        else
                        {
                            $user_type .= '<option value="' . $key . '">' . $val . '</option>';
                        }
                    }
            
                    $user_type .= '
                </select>
                ' . new_user_type_form() . '
            </fieldset>';
            
            //-- Find user display name
            $display_name = '
            <select name="display_name[0]" class="no-margin">';
            
                foreach( opt_display_name( $_GET[ 'id' ] ) as $key => $val )
                {
                    if( $key == $_POST[ 'display_name' ][ 0 ] )
                    {
                        $display_name .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                    }
                    else
                    {
                        $display_name .= '<option value="' . $key . '">' . $val . '</option>';
                    }
                }
                
                $display_name .= '
            </select>';
            
            //-- Find Gender            
            $sexar = array( '1' => 'Male', '2' => 'Female' );
            $sex = '
            <select name="sex[0]" class="no-margin">
                <option value="">Select Sex</option>';
            
                foreach( $sexar as $key => $val )
                {
                    if( $key == $_POST[ 'sex' ][ 0 ] )
                    {
                        $sex .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                    }
                    else
                    {
                        $sex .= '<option value="' . $key . '">' . $val . '</option>';
                    }
                }
            
                $sex .= '
            </select>';
            
            add_variable( 'sex', $sex );
            
            //-- Birthday
            $birthday   = ( isset( $_POST[ 'birthday' ][ 0 ] ) ) ? $_POST[ 'birthday' ][ 0 ] : '';
            $birthyear  = ( isset( $_POST[ 'birthyear' ][ 0 ] ) ) ? $_POST[ 'birthyear' ][ 0 ] : '';
            $birthmonth = ( isset( $_POST[ 'birthmonth' ][ 0 ] ) ) ? $_POST[ 'birthmonth' ][ 0 ] : '';
            
            get_date_picker( 'js_elements', $birthday, $birthmonth, $birthyear, true, 0 );
            
            //-- User status
            $user_status = array( 0 => 'Waiting Email Validation', 1 => 'Active', 2 => 'Block' );
            $the_status  = '
            <fieldset>
                <p><label>Status:</label></p>
                <select name="status[0]" class="no-margin">';
            
                    foreach( $user_status as $key => $val )
                    {
                        if( $key == $_POST[ 'sex' ][ 0 ] )
                        {
                            $the_status .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                        }
                        else
                        {
                            $the_status .= '<option value="' . $key . '">' . $val . '</option>';
                        }
                    }
            
                    $the_status .= '
                </select>
            </fieldset>';
            
            add_variable( 'user_status', $the_status );
            add_variable( 'ajax_url', get_users_ajax_url() );
            
            if( is_administrator() )
            {
                if( isset( $_POST[ 'send' ][ 0 ] ) )
                {
                    $send_checked = 'checked="checked"';
                }
                else
                {
                    $send_checked = '';
                }

                $value = '
                <fieldset>
                    <p><label>Send Details?</label></p>
                    <input type="checkbox" name="send[0]" ' . $send_checked . ' /> Send all details to the new user via email.
                </fieldset>';
                
                add_variable( 'send_option', $value );
            }
        }
        else
        {
            $d = fetch_user( $_GET[ 'id' ] );
            
            add_variable( 'email', $d[ 'lemail' ] );
            add_variable( 'username', $d[ 'lusername' ] );
            add_variable( 'first_name', get_additional_field( $_GET[ 'id' ], 'first_name', 'user' ) );
            add_variable( 'last_name', get_additional_field( $_GET[ 'id' ], 'last_name', 'user' ) );
            
            $website = get_additional_field( $_GET[ 'id' ], 'website', 'user' );
            
            if( empty( $website ) )
            {
                add_variable( 'website', HTSERVER . '//' );
            }
            else
            {
                add_variable( 'website', get_additional_field( $_GET[ 'id' ], 'website', 'user' ) );
            }
            
            //-- Find the user type
            $user_type = '
            <fieldset>
                <p><label>User Type:</label></p>
                <select name="user_type[0]" class="user-type-option no-margin" autocomplete="off">';
            
                    foreach( user_type() as $key => $val )
                    {
                        if( $key == $d[ 'luser_type' ] )
                        {
                            $user_type .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                        }
                        else
                        {
                            $user_type .= '<option value="' . $key . '">' . $val . '</option>';
                        }
                    }
            
                    $user_type .= '
                </select>
                ' . new_user_type_form() . '
            </fieldset>';
            
            //-- Find user display name
            $display_name = '
            <select name="display_name[0]" class="no-margin">';
            
                foreach( opt_display_name( $d[ 'luser_id' ] ) as $key => $val )
                {
                    if( $key == $d[ 'ldisplay_name' ] )
                    {
                        $display_name .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                    }
                    else
                    {
                        $display_name .= '<option value="' . $key . '">' . $val . '</option>';
                    }
                }
                
                $display_name .= '
            </select>';
            
            //-- Find Gender
            $sexar = array( '1' => 'Male', '2' => 'Female' );
            $sex = '
            <select name="sex[0]" class="no-margin">
                <option value="">Select Sex</option>';
            
                    foreach( $sexar as $key => $val )
                    {
                        if( $key == $d[ 'lsex' ] )
                        {
                            $sex .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                        }
                        else
                        {
                            $sex .= '<option value="' . $key . '">' . $val . '</option>';
                        }
                    }
                    
                $sex .= '
            </select>';
            
            add_variable( 'sex', $sex );
            
            if( is_administrator() )
            {
                $value = '
                <fieldset>
                    <p><label>Send Details?</label></p>
                    <input type="checkbox" name="send[0]" /> Send all details to the new user via email.
                </fieldset>';
                
                add_variable( 'send_option', $value );
            }
            
            //-- birthday
            $birthday   = ( !empty( $d[ 'lbirthday' ] ) ) ? date( 'j', strtotime( $d[ 'lbirthday' ] ) ) : '';
            $birthmonth = ( !empty( $d[ 'lbirthday' ] ) ) ? date( 'n', strtotime( $d[ 'lbirthday' ] ) ) : '';
            $birthyear  = ( !empty( $d[ 'lbirthday' ] ) ) ? date( 'Y', strtotime( $d[ 'lbirthday' ] ) ) : '';
            
            get_date_picker( 'js_elements', $birthday, $birthmonth, $birthyear, true, 0 );
            
            //-- User status
            $user_status = array( 0 => 'Waiting Email Validation', 1 => 'Active', 2 => 'Block' );
            $the_status  = '
            <fieldset>
                <p><label>Status:</label></p>
                <select name="status[0]" class="no-margin">';
            
                    foreach( $user_status as $key => $val )
                    {
                        if( $key == $d[ 'lstatus' ] )
                        {
                            $the_status .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                        }
                        else
                        {
                            $the_status .= '<option value="' . $key . '" >' . $val . '</option>';
                        }
                    }
            
                    $the_status .= '
                </select>
            </fieldset>';

            add_variable( 'user_status', $the_status );
        }

        add_variable( 'prc', 'Edit User' );
        add_variable( 'user_type', $user_type );
        add_variable( 'display_name', $display_name );
        add_variable( 'save_user', button( 'button=save_changes&label=Save User' ) );
        add_variable( 'cancel', button( 'button=cancel', get_state_url( 'users' ) ) );
        
        parse_template( 'usersEdit', 'uEdit' );

        return return_template( 'users' );
    }
    elseif( is_delete_all() )
    {
        return delete_batch_users();
    }
    elseif( is_confirm_delete() )
    {        
        foreach( $_POST[ 'id' ] as $key => $val )
        {
            delete_user( $val );
        }
    }
    elseif( is_profile() )
    {
        return edit_profile();
    }
    elseif( is_profile_picture() )
    {
        return edit_profile_picture();
    }

    if( is_num_users() > 0 )
    {
        return get_users_list();
    }
}

/**
* Each user can edit his/her user profile when they login. This function will be called when they want to edit the profile.
* All process are happen here.    
*
* @author Wahya Biantara
*
* @since alpha
* 
* @return string The HTML design of edit profile  
* 
*/
function edit_profile()
{
    global $db;

    $alert = '';

    set_template( TEMPLATE_PATH . '/template/users.html', 'users' );

    add_block( 'usersEdit', 'uEdit', 'users' );
    add_block( 'usersAddNew', 'uAddNew', 'users' );
    add_block( 'profilePicture', 'pPicture', 'users' );

    add_variable( 'i', 0 );
    add_variable( 'website', 'http://' );

    if( is_save_changes() )
    {
        extract( $_POST );

        $validation_rs = is_valid_user_input( $username[0], $first_name[0], $last_name[0], $password[0], $re_password[0], $email[0], $sex[0], $website[0] );
        
        if( $validation_rs == 'OK' )
        {
            $thebirthday = '000-00-00';

            if( !empty( $birthday[0] ) && !empty( $birthmonth[0] ) && !empty( $birthyear[0] ) )
            {
                $thebirthday = $birthmonth[0] . '/' . $birthday[0] . '/' . $birthyear[0];
                $thebirthday = date( 'Y-m-d', strtotime( $thebirthday ) );
            }

            $duser = fetch_user( $_COOKIE['user_id'] );

            if( edit_user( $_COOKIE['user_id'], $display_name[0], $password[0], $email[0], $sex[0], $duser['luser_type'], $thebirthday, $duser['lstatus'] ) )
            {
                edit_additional_field( $_COOKIE['user_id'], 'first_name', $first_name[0], 'user' );
                edit_additional_field( $_COOKIE['user_id'], 'last_name', $last_name[0], 'user' );
                edit_additional_field( $_COOKIE['user_id'], 'website', $website[0], 'user' );

                if( isset( $send[0] ) )
                {
                    send_update_profile_notification( $first_name[0], $username[0], $email[0], $password[0] );
                }

                $alert = '<div class="alert_green_form">Your profile has succesfully updated.</div>';
            }
        }
        else
        {
            $alert = $validation_rs;
        }
    }

    $d = fetch_user( $_COOKIE['user_id'] );

    add_variable( 'email', $d['lemail'] );
    add_variable( 'username', $d['lusername'] );
    add_variable( 'last_name', get_additional_field( $d['luser_id'], 'last_name', 'user' ) );
    add_variable( 'first_name', get_additional_field( $d['luser_id'], 'first_name', 'user' ) );

    $website = get_additional_field( $d['luser_id'], 'website', 'user' );

    if( empty( $website ) )
    {
        add_variable( 'website',  HTSERVER . '//' );
    }
    else
    {
        add_variable( 'website',  get_additional_field( $d['luser_id'], 'website', 'user' ) );
    }

    //-- find user display name
    $display_name = '
    <select name="display_name[0]">';
        foreach( opt_display_name( $d['luser_id'] ) as $key => $val )
        {
            if( $key == $d['ldisplay_name'] )
            {
                $display_name .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
            }
            else
            {
                $display_name .= '<option value="' . $key . '">' . $val . '</option>';
            }
        }
        $display_name .= '
    </select>';

    //-- Gender
    $sexar = array( '1' => 'Male', '2' => 'Female' );
    $sex = '
    <select name="sex[0]">
        <option value="">Select Gender</option>';
            foreach( $sexar as $key => $val )
            {
                if( $key == $d['lsex'] )
                {
                    $sex .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                }
                else
                {
                    $sex .= '<option value="' . $key . '">' . $val . '</option>';
                }
            }
            $sex .= '
    </select>';

    $birthday   = ( !empty( $d['lbirthday'] ) ) ? date( 'j', strtotime( $d['lbirthday'] ) ) : '';
    $birthmonth = ( !empty( $d['lbirthday'] ) ) ? date( 'n', strtotime( $d['lbirthday'] ) ) : '';
    $birthyear  = ( !empty( $d['lbirthday'] ) ) ? date( 'Y', strtotime( $d['lbirthday'] ) ) : '';

    get_date_picker( 'js_elements', $birthday, $birthmonth, $birthyear, true, 0 );

    if( is_administrator() )
    {
        $send_option = '
        <fieldset>
            <p><label>Send Details?</label></p>
            <input type="checkbox" name="send[{i}]" {send_checked} /> Send all details to the new user via email.
        </fieldset>';
        
        add_variable( 'send_option', $send_option );
    }

    add_variable( 'sex', $sex );
    add_variable( 'prc', 'Edit Profile' );
    add_variable( 'display_name', $display_name );
    add_variable( 'save_user', button( "button=save_changes&label=Save User" ) );
    add_variable( 'cancel', button( "button=cancel", get_state_url( 'users' ) ) );

    add_actions( 'section_title', 'Edit Profile' );

    add_actions( 'js_elements', 'get_javascript', 'password_strength' );

    parse_template( 'usersEdit', 'uEdit' );

    return return_template( 'users' );
}

/**
* Edit the user picture profile 
*
* @author Wahya Biantara
*
* @since alpha
*
* @return string The picture profile HTML Design
*
*/
function edit_profile_picture()
{
    global $db;

    $alert = '';

    if( isset( $_POST['upload'] ) && isset( $_FILES['theavatar']['error'] ) && $_FILES['theavatar']['error'] == 0 )
    {
        $file_name   = $_FILES['theavatar']['name'];
        $file_size   = $_FILES['theavatar']['size'];
        $file_type   = $_FILES['theavatar']['type'];
        $file_source = $_FILES['theavatar']['tmp_name'];

        if( is_allow_file_size( $file_size ) )
        {
            if( is_allow_file_type( $file_type, 'image' ) )
            {
                $fix_file_name = file_name_filter( $_COOKIE['username'] );
                $file_ext      = file_name_filter( $file_name, true );

                $file_name_1 = $fix_file_name . '-1' . $file_ext;
                $file_name_2 = $fix_file_name . '-2' . $file_ext;
                $file_name_3 = $fix_file_name . '-3' . $file_ext;

                $destination1 = FILES_PATH . '/users/' . $file_name_1;
                $destination2 = FILES_PATH . '/users/' . $file_name_2;
                $destination3 = FILES_PATH . '/users/' . $file_name_3;

                upload_crop( $file_source, $destination3, $file_type, 32, 32 );
                upload_crop( $file_source, $destination2, $file_type, 50, 50 );
                upload_crop( $file_source, $destination1, $file_type, 250, 300 );

                $avatar = $file_name_1 . '|' . $file_name_2 . '|' . $file_name_3;

                $s = 'UPDATE lumonata_users SET lavatar = %s WHERE luser_id = %d';
                $q = $db->prepare_query( $s, $avatar, $_COOKIE['user_id'] );
                $r = $db->do_query( $q );
            }
            else
            {
                $alert = '<div class="alert_yellow_form">Please upload file image</div>';
            }
        }
        else
        {
            $alert = '<div class="alert_yellow_form">The maximum file size is 2MB</div>';
        }
    }

    set_template( TEMPLATE_PATH . '/template/users.html', 'users' );

    add_block( 'usersEdit', 'uEdit', 'users' );
    add_block( 'usersAddNew', 'uAddNew', 'users' );
    add_block( 'profilePicture', 'pPicture', 'users' );

    $get_avatar = get_avatar( $_COOKIE['user_id'] );

    if( empty( $get_avatar ) )
    {
        $the_avatar  = '';
    }
    else
    {        
        $d = fetch_user( $_COOKIE['user_id'] );

        $the_avatar = '<img src="' . $get_avatar . '" alt="' . $d['ldisplay_name'] . '" title="' . $d['ldisplay_name'] . '" />';
    }

    add_variable( 'alert', $alert );
    add_variable( 'the_avatar', $the_avatar );
    add_variable( 'prc', 'Edit Profile Picture' );
    add_variable( 'upload_button', upload_button() );

    add_actions( 'section_title', 'Edit Profile Picture' );

    parse_template( 'profilePicture', 'pPicture' );

    return return_template( 'users' );
}

function delete_batch_users()
{
    set_template( TEMPLATE_PATH . '/template/users-batch-delete.html', 'users-batch-delete' );

    add_block( 'delete-loop-block', 'dl-block', 'users-batch-delete' );
    add_block( 'delete-block', 'd-block', 'users-batch-delete' );

    foreach( $_POST[ 'select' ] as $key => $val )
    {
        $d = fetch_user( $val );

        add_variable( 'users_id', $d[ 'luser_id' ] );
        add_variable( 'users_title', $d[ 'ldisplay_name' ] );

        parse_template( 'delete-loop-block', 'dl-block', true );
    }

    add_variable( 'title', 'Users' );
    add_variable( 'backlink', get_state_url( 'users' ) );
    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST[ 'select' ] ) == 1 ? 'this user' : 'these users' ) );
    
    add_actions( 'section_title', 'Users' );

    parse_template( 'delete-block', 'd-block', 'users-batch-delete' );

    return return_template( 'users-batch-delete' );
}

/**
* Grab user ID data by Email 
*
* @author Wahya Biantara
* 
* @since alpha
* 
* @return User ID 
* 
*/
function fetch_user_id_by_email( $email )
{
    global $db;

    if( empty( $email ) )
    {
        return '';
    }

    $s = 'SELECT * FROM lumonata_users WHERE lemail = %s';
    $q = $db->prepare_query( $s, $email );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d['luser_id'];
}

/**
* Grab user ID data by username 
*
* @author Wahya Biantara
* 
* @since alpha
*  
* @return User ID 
*  
*/
function fetch_user_id_by_username( $username )
{
    global $db;

    if( empty( $username ) )
    {
        return '';
    }

    $s = 'SELECT * FROM lumonata_users WHERE lusername = %s';
    $q = $db->prepare_query( $s, $username );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d['luser_id'];
}

/**
* Grab user data by the user ID or User name
*
* @author Wahya Biantara
* 
* @since alpha
*  
* @return array user data by ID or Username 
*  
*/
function fetch_user( $id )
{
    global $db;

    $s = 'SELECT * FROM lumonata_users WHERE luser_id = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        return $db->fetch_array( $r );
    }
    else
    {
        $s2 = 'SELECT * FROM lumonata_users WHERE lusername = %s';
        $q2 = $db->prepare_query( $s2, $id );
        $r2 = $db->do_query( $q2 );

        return $db->fetch_array( $r2 );
    }
}

/**
* Create the option display name from First and Last Name
*
* @author Wahya Biantara
* 
* @since alpha
*  
* @return array Display name options  
*  
*/
function opt_display_name( $id )
{
    $d = fetch_user( $id );

    $last_name  = get_additional_field( $id, 'last_name', 'user' );
    $first_name = get_additional_field( $id, 'first_name', 'user' );

    $display_name[ $d['ldisplay_name'] ] = $d['ldisplay_name'];

    if( !empty( $first_name ) && !empty( $last_name ) )
    {
        $display_name[ $first_name . ' ' . $last_name ] = $first_name . ' ' . $last_name;
        $display_name[ $last_name . ' ' . $first_name ] = $last_name . ' ' . $first_name;
    }
    else
    {
        if( !empty( $first_name ) )
        {
            $display_name[ $first_name ] = $first_name;
        }

        if( !empty( $last_name ) )
        {
            $display_name[ $last_name ] = $last_name;
        }
    }

    return $display_name;
}

/**
* Check if the given user_id is a standard user or no 
*
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $user_id User ID
* 
* @return boolean   
* 
*/
function is_standard_user( $user_id = 0 )
{
    if( empty( $user_id ) )
    {
        if( $_COOKIE['user_type'] == 'standard' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        $user = fetch_user( $user_id );

        if( $user['luser_type'] == 'standard' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

/**
* Check if the given user_id is a contributor or no 
*
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $user_id User ID
*  
* @return boolean  
*  
*/
function is_contributor( $user_id = 0 )
{
    if( empty( $user_id ) )
    {
        if( $_COOKIE['user_type'] == 'contributor' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        $user = fetch_user( $user_id );

        if( $user['luser_type'] == 'contributor' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

/**
* Check if the given user_id is an author or no
*
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $user_id User ID
*  
* @return boolean     
*  
*/
function is_author( $user_id = 0 )
{
    if( empty( $user_id ) )
    {
        if( $_COOKIE['user_type'] == 'author' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        $user = fetch_user( $user_id );

        if( $user['luser_type'] == 'author' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

/**
* Check if the given user_id is an editor or no 
*
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $user_id User ID
*  
* @return boolean  
*  
*/
function is_editor( $user_id = 0 )
{
    if( empty( $user_id ) )
    {
        if( $_COOKIE['user_type'] == 'editor' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        $user = fetch_user( $user_id );

        if( $user['luser_type'] == 'editor' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

/**
* Check if the given user_id is an administrator or no 
*
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $user_id User ID
*  
* @return boolean   
*   
*/
function is_administrator( $user_id = 0 )
{
    if( empty( $user_id ) )
    {
        if( $_COOKIE['user_type'] == 'administrator' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        $user = fetch_user( $user_id );

        if( $user['luser_type'] == 'administrator' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

/**
* Get the user avatar image 
*
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $user_id User ID
* @param integer $image_size 1=Large,2=Medium,3=Small 
*  
* @return boolean     
*  
*/
function get_avatar( $user_id, $image_size = 1 )
{
    $d = fetch_user( $user_id );

    if( !isset( $d['lavatar'] ) || ( isset( $d['lavatar'] ) && empty( $d['lavatar'] ) ) )
    {
        if( isset( $d['lsex'] ) && $d['lsex'] == 1 )
        {
            return site_url( 'lumonata-content/files/users/man-' . $image_size . '.jpg' );
        }
        else
        {
            return site_url( 'lumonata-content/files/users/woman-' . $image_size . '.jpg' );
        }
    }
    else
    {
        $file_name = explode( '|', $d['lavatar'] );

        switch( $image_size )
        {
            case 1:
                $file = $file_name[0];
            break;

            case 2:
                $file = $file_name[1];
            break;

            case 3:
                $file = $file_name[2];
            break;
        }

        if( file_exists( FILES_PATH . '/users/' . $file ) )
        {
            return site_url( 'lumonata-content/files/users/' . $file );
        }
        else
        {
            if( $d['lsex'] == 1 )
            {
                return site_url( 'lumonata-content/files/users/man-' . $image_size . '.jpg' );
            }
            else
            {
                return site_url( 'lumonata-content/files/users/woman-' . $image_size . '.jpg' );
            }
        }
    }
}

/**
* Get the display name of user 
*
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $id User ID
*  
* @return boolean  
*  
*/
function get_display_name( $id )
{
    $d = fetch_user( $id );

    return $d['ldisplay_name'];
}

function new_user_type_form()
{
    global $apps_privileges;

	$typeform = "
	<a class=\"add_new_user_type_link\">Add New</a>
	<a class=\"edit_user_type_link\">Edit</a>	
	<div id=\"add_new_user_type_dialog\" class=\"add_new_user_type_dialog\" title=\"Manage User Type\">
		<div>
			<fieldset>
				<input type=\"hidden\" name=\"actions\" autocomplete=\"off\" />
				<input type=\"hidden\" name=\"type_key\" autocomplete=\"off\" />
				<input type=\"hidden\" name=\"pkey\" value=\"manage-user-privileges\" autocomplete=\"off\" />
				<input type=\"text\" name=\"type_name\" class=\"textarea ui-widget-content ui-corner-all\" autocomplete=\"off\" placeholder=\"User Type Name\" />
			</fieldset>
			<fieldset>
				<ul class=\"user_type_list clearfix\">
					<li>
						<input id=\"dashboard-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"dashboard\" />
						<label for=\"dashboard-menu\">Dashboard</label>
					</li>
					<li>
						<input id=\"global-settings-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"global_settings\" />
						<label for=\"globalsettings-menu\">Settings</label>
					</li>
					<li>
						<input id=\"menus-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"menus\" />
						<label for=\"menus-menu\">Menus</label>
					</li>
					<li>
						<input id=\"applications-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"applications\" />
						<label for=\"applications-menu\">Applications</label>
					</li>
					<li>
						<input id=\"plugins-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"plugins\" />
						<label for=\"plugins-menu\">Plugins</label>
					</li>
					<li>
						<input id=\"articles-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"articles\" />
						<label for=\"articles-menu\">Articles</label>
					</li>
					<li>
						<input id=\"blogs-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"blogs\" />
						<label for=\"blogs-menu\">Blogs</label>
					</li>
					<li>
						<input id=\"pages-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"pages\" />
						<label for=\"pages-menu\">Pages</label>
					</li>
					<li>
						<input id=\"categories-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"categories\" />
						<label for=\"categories-menu\">Categories</label>
					</li>
					<li>
						<input id=\"tags-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"tags\" />
						<label for=\"tags-menu\">Tags</label>
					</li>
					<li>
						<input id=\"users-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"users\" />
						<label for=\"users-menu\">Users</label>
					</li>
                    <li>
                        <input id=\"users-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"media\" />
                        <label for=\"users-menu\">Media</label>
                    </li>
                    <li>
                        <input id=\"users-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"custom-post\" />
                        <label for=\"users-menu\">Custom Post</label>
                    </li>
                    <li>
                        <input id=\"users-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"custom-field\" />
                        <label for=\"users-menu\">Custom Field</label>
                    </li>";

                    if( isset( $apps_privileges ) && !empty( $apps_privileges ) )
                    {
                        foreach( $apps_privileges as $apps_name )
                        {
                            foreach( $apps_name as $key => $value )
                            {
                                $typeform .= " 
                                <li>
                                    <input id=\"" . $key . "-menu\" type=\"checkbox\" name=\"type_privileges[]\" class=\"privileges-input\" autocomplete=\"off\" value=\"" . $key . "\" />
                                    <label for=\"" . $key ."-menu\">" . $value . "</label>
                                </li>";
                            }
                        }
                    }
                    $typeform .= " 
				</ul>
			</fieldset>
		</div>
	</div>
	<style>
		.add_new_user_type_dialog{ padding:15px 0; }
		.add_new_user_type_dialog fieldset{ padding:0; border:none; margin-bottom:0; }
		.add_new_user_type_link{ margin:0 5px; cursor:pointer; text-decoration:underline; }
		.edit_user_type_link{ margin:0 5px; cursor:pointer; text-decoration:underline; }
		.user_type_list{ padding:0; }
		.user_type_list li{ list-style:none; float:left; width:25%; margin-bottom:10px; }
		.user_type_list input{ margin:0 5px 0; height:15px; vertical-align:middle; }
		.user_type_list label{ padding:0; vertical-align:middle; }
		.loader-img{ margin-left:15px; }
	</style>
	<script>
		(function($) {
			$.fn.serializeData = function() {
				var toReturn	= [];
				var els 		= $(this).find(':input').get();

				$.each(els, function() {
					if (this.name && !this.disabled && (this.checked || /select|textarea/i.test(this.nodeName) || /text|hidden|password/i.test(this.type))) {
						var val = $(this).val();
						toReturn.push( encodeURIComponent(this.name) + '=' + encodeURIComponent( val ) );
					}
				});

				return toReturn.join('&').replace(/%20/g, '+');
			}
		})(jQuery);

		function show_alert( htitle, message )
		{
		    jQuery('<div></div>').dialog({
		        modal: true,
		        title: htitle,
		        open: function()
		        {
		            jQuery(this).html(message);
		        },
		        buttons:
		        {
		            Close: function()
		            {
		                jQuery( this ).dialog('close');
		            }
		        }
		    });
		}

		jQuery('.add_new_user_type_link').click(function(){
			jQuery('#add_new_user_type_dialog input[name=type_key]').val('');
			jQuery('#add_new_user_type_dialog input[name=type_name]').val('');
			jQuery('#add_new_user_type_dialog input[name=actions]').val('add');
			jQuery('#add_new_user_type_dialog .privileges-input').removeAttr('checked');
			jQuery('#add_new_user_type_dialog').dialog('open');
			return false;
		});

		jQuery('.edit_user_type_link').click(function(){
			var url = '" . get_users_ajax_url() . "';
			var prm = new Object;
				prm.pkey      = 'get-user-privileges';
				prm.type_key  = $('.user-type-option').val();
				prm.type_name = $('.user-type-option option:selected').text();

			var loader = '<img class=\"loader-img\" src=\"" . get_admin_url() . "/themes/default/images/loading.gif\" alt=\"loading...\" />';

			jQuery(loader).insertAfter('.edit_user_type_link');
			jQuery.post(url, prm, function(e){

				jQuery('.loader-img').remove();

				if( e.result=='success')
				{
					if( !jQuery.isEmptyObject(e.data.privileges) )
					{
						jQuery('#add_new_user_type_dialog .privileges-input').each(function(){
							var vals = jQuery(this).val();
							if( jQuery.inArray(vals, e.data.privileges) != -1 )
							{
								jQuery(this).attr('checked', 'checked');
							}
						});
					}
				}

				jQuery('#add_new_user_type_dialog input[name=type_key]').val(prm.type_key);
				jQuery('#add_new_user_type_dialog input[name=type_name]').val(prm.type_name);
				jQuery('#add_new_user_type_dialog input[name=actions]').val('edit');
				jQuery('#add_new_user_type_dialog').dialog('open');

			},'json');
			
			return false;
		});
		
		jQuery('#add_new_user_type_dialog').dialog({
			bgiframe: true,
			autoOpen: false,
			resizable: false,
			modal: true,
			width: 700,
			buttons: {
				'Save': function(){
					var url = '" . get_users_ajax_url() . "';
					var sel = jQuery(this);
					var prm = jQuery(this).serializeData();
					jQuery.post(url, prm, function(e){
						if( e.result=='success')
						{
                            var option = '';
                            var optkey = jQuery('input[name=type_key]').val();
                            jQuery.each(e.data, function(i, ee){
                                option += '<option value=\"'+i+'\" '+( optkey==i ? \"selected\" : \"\" )+'>'+ee.name+'</option>';
                            });

                            if( option != '' )
                            {
                                jQuery('.user-type-option').html(option);
                            }

							if( jQuery('input[name=actions]').val()=='add' )
							{
								jQuery('input[name=type_key]').val('');
								jQuery('input[name=type_name]').val('');	
								jQuery('.privileges-input').removeAttr('checked');
								show_alert('Notifications', 'Add new user type success');
							}
							else
							{
								show_alert('Notifications', 'Edit user type success');
							}
						}
						else
						{
							if( jQuery('input[name=actions]').val()=='add' )
							{
								show_alert('Warning', 'Add new user type failed');
							}
							else
							{
								show_alert('Warning', 'Edit user type failed');
							}
						}
					},'json')
				},
				Close: function(){
					jQuery(this).dialog('close');
				}
			},
			close: function() {
				jQuery('input[name=actions]').val('')
				jQuery('input[name=type_key]').val('');
				jQuery('input[name=type_name]').val('');	
				jQuery('.privileges-input').removeAttr('checked');
			}
		});
	</script>";

	return $typeform;
}

function manage_user_privileges( $post )
{
	global $db;

	if( empty($_POST['type_key']) )
	{
		$_POST['type_key'] = generateSefUrl($_POST['type_name']);	
	}

	if( !isset($_POST['type_privileges']) )
	{
		$_POST['type_privileges'] = '';
	}

	$s = 'SELECT * FROM lumonata_meta_data WHERE lmeta_name=%s AND lapp_name=%s';
	$q = $db->prepare_query($s, $_POST['type_key'], 'user_privileges');
	$r = $db->do_query($q);

	$value = array( 'name'=>$_POST['type_name'], 'privileges'=>$_POST['type_privileges'] );

	if( $db->num_rows($r)==0 )
	{
		return set_meta_data($_POST['type_key'], json_encode($value), 'user_privileges');
	}
	else
	{
		return update_meta_data($_POST['type_key'], json_encode($value), 'user_privileges');
	}
}

function set_privileges_admin()
{
    if( is_user_logged() )
    {
        global $db;
    
        $s = 'SELECT * FROM lumonata_meta_data WHERE lmeta_name=%s AND lapp_name=%s';
        $q = $db->prepare_query($s, $_COOKIE['user_type'], 'user_privileges');
        $r = $db->do_query($q);

        if( $db->num_rows($r) > 0 )
        {
            $d = $db->fetch_array($r);
            $v = json_decode($d['lmeta_value'], true);

            if( json_last_error() == JSON_ERROR_NONE )
            {
                foreach ($v['privileges'] as $privileges)
                {
                    add_privileges($_COOKIE['user_type'], $privileges, 'insert');
                    add_privileges($_COOKIE['user_type'], $privileges, 'update');
                    add_privileges($_COOKIE['user_type'], $privileges, 'delete');
                    add_privileges($_COOKIE['user_type'], $privileges, 'upload');
                    add_privileges($_COOKIE['user_type'], $privileges, 'preview');
                    add_privileges($_COOKIE['user_type'], $privileges, 'request');
                    add_privileges($_COOKIE['user_type'], $privileges, 'approve');                   
                }
            }
        }
    }
}

function user_ajax()
{
    global $db;

    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-users-data' )
    {
        $data = get_users_list_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'delete-user' )
    {
        if( delete_user( $_POST['id'] ) )
        {
            $result = array( 'result' => 'success' );
        }
        else
        {
            $result = array( 'result' => 'failed' );
        }

        echo json_encode( $result );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'manage-user-privileges' )
    {
        if( manage_user_privileges( $_POST ) )
        {
            $arr = array();
            
            $s = 'SELECT * FROM lumonata_meta_data WHERE lapp_name=%s';
            $q = $db->prepare_query( $s, 'user_privileges' );
            $r = $db->do_query( $q );
            
            while( $d = $db->fetch_array( $r ) )
            {
                $arr[ $d[ 'lmeta_name' ] ] = json_decode( $d[ 'lmeta_value' ], true );
            }
            
            echo json_encode( array( 'result' => 'success', 'data' => $arr ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }
    elseif( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'get-user-privileges' )
    {
        $s = 'SELECT * FROM lumonata_meta_data WHERE lmeta_name=%s AND lapp_name=%s';
        $q = $db->prepare_query( $s, $_POST[ 'type_key' ], 'user_privileges' );
        $r = $db->do_query( $q );
        
        if( $db->num_rows( $r ) > 0 )
        {
            $d = $db->fetch_array( $r );
            $v = json_decode( $d[ 'lmeta_value' ], true );
            
            echo json_encode( array( 
                'result' => 'success', 
                'data'   => array(
                    'key'        => $d[ 'lmeta_name' ],
                    'name'       => ( isset( $v[ 'name' ] ) ? $v[ 'name' ] : '' ),
                    'privileges' => ( isset( $v[ 'privileges' ] ) ? $v[ 'privileges' ] : '' ) 
                ) 
            ));
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    exit;
}

?>