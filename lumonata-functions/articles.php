<?php

add_actions( 'article_admin_page', 'article_ajax' );

/*
| -----------------------------------------------------------------------------
| Admin Article Post
| -----------------------------------------------------------------------------
*/
function get_admin_article( $type = 'blogs', $thetitle = 'Blog|Blogs', $tabs = array(), $category_tabs = true, $tags_tabs = true )
{
    $post_id         = 0;
    $articletabtitle = explode( '|', $thetitle );
    $default_tab     = array( 'blogs' => $articletabtitle[ 1 ] );
    
    if( $category_tabs )
    {
        $default_tab[ 'categories' ] = 'Categories';
    }
    
    if( $tags_tabs )
    {
        $default_tab[ 'tags' ] = 'Tags';
    }
    
    $tabs = array_merge( $default_tab, $tabs );
    
    if( is_contributor() || is_author() )
    {
        foreach( $tabs as $key => $val )
        {
            if( is_grant_app( $key ) )
            {
                $thetabs[ $key ] = $val;
            }
        }
        
        $tabs = $thetabs;
    }
    else
    {
        $tabs = $tabs;
    }
    
    //-- Configure the tabs $the_tab is the selected tab
    $tab_keys = array_keys( $tabs );
    $the_tab  = empty( $_GET[ 'tab' ] ) ? $tab_keys[ 0 ] : $_GET[ 'tab' ];
    
    $articles_tabs = set_tabs( $tabs, $the_tab );

    add_variable( 'tab', $articles_tabs );
    
    //-- Filter the tabs. Articles tab can be accessed by all user type.
    //-- But for Categories and Tags tabs only granted for Editor and Administrator.
    if( $the_tab == 'blogs' )
    {
        run_article_actions( $type );

        //-- Is add new Article, View the desain
        if( is_add_new() )
        {
            return add_new_article( $post_id, $type, $thetitle );
        }
        elseif( is_edit() )
        {
            if( is_contributor() || is_author() )
            {
                if( is_num_articles( 'id=' . $_GET[ 'id' ] . '&type=' . $type ) > 0 )
                {
                    return edit_article( $_GET[ 'id' ], $type, $thetitle );
                }
                else
                {
                    return '
                    <div class="alert_red_form">
                        You don\'t have an authorization to access this page
                    </div>';
                }
            }
            else
            {
                return edit_article( $_GET[ 'id' ], $type, $thetitle );
            }
        }
        elseif( is_delete_all() )
        {
            return delete_batch_article( 0, $type, $thetitle, $articles_tabs );
        }
        elseif( is_confirm_delete() )
        {
            foreach( $_POST[ 'id' ] as $key => $val )
            {
                run_actions( $type . '_additional_delete' );

                delete_article( $val, $type );
            }
        }
        
        //-- Display Article Lists
        if( is_num_articles( 'type=' . $type ) > 0 )
        {
            return get_article_list( $type, $thetitle, $articles_tabs );
        }
        elseif( is_num_articles( 'type=' . $type ) == 0 && !isset( $_GET[ 'prc' ] ) )
        {
            if( $_GET[ 'state' ] != 'articles' )
            {
                if( isset( $_GET[ 'sub' ] ) && $_GET[ 'sub' ] != '' )
                {
                    header( 'location:' . get_state_url( $_GET[ 'state' ] . '&sub=' . $_GET[ 'sub' ] . '&prc=add_new' ) );

                    exit;
                }
                else
                {
                    header( 'location:' . get_state_url( $_GET[ 'state' ] . '&prc=add_new' ) );

                    exit;
                }
            }
            else
            {
                header( 'location:' . get_state_url( $type . '&prc=add_new' ) );

                exit;
            }
        }
        else
        {
            header( 'location:' . get_state_url( $type . '&prc=add_new' ) );
        }
    }
    elseif( ( $the_tab == 'categories' || $the_tab == 'tags' ) && ( is_administrator() || is_editor() ) )
    {
        return get_admin_rule( $the_tab, $type, $thetitle, $tabs );
    }
    else
    {
        global $actions;
        
        $extendname = $the_tab . '_article_extends';
        
        if( is_administrator() && isset( $actions->action[ $extendname ] ) && !empty( $actions->action[ $extendname ][ 'func_name' ][ 0 ] ) )
        {
            return call_user_func( $actions->action[ $extendname ][ 'func_name' ][ 0 ], $tabs );
        }
        else
        {
            return '
            <div class="alert_red_form">
                You don\'t have an authorization to access this page
            </div>';
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post Set Apss To Admin Menu
| -----------------------------------------------------------------------------
*/
function set_plugin_menu_blog( $apps = array() )
{
    return $apps;
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post Actions
| -----------------------------------------------------------------------------
*/
function run_article_actions( $type )
{
    global $db;

    //-- Publish, Unpublish & Save Draft 
    //-- Actions From List Page
    if( isset( $_POST[ 'select' ] ) )
    {
        $status = is_save_draft() ? 'draft' : ( is_unpublish() ? 'unpublish' : 'publish' );
        
        foreach( $_POST[ 'select' ] as $key => $val )
        {
            update_articles_status( $val, $status );
        }
    }
    else
    {
        //-- Publish or Save Draft Actions
        if( is_save_draft() || is_publish() )
        {
            //-- Set status and hook actions defined
            $status = is_save_draft() ? 'draft' : ( is_unpublish() ? 'unpublish' : 'publish' );
            
            if( is_add_new() )
            {
                $content   = $_POST[ 'post' ][ 0 ];
                $title     = $_POST[ 'title' ][ 0 ];
                $temp_id   = $_POST[ 'post_id' ][ 0 ];
                $sef_box   = isset( $_POST[ 'sef_box' ][ 0 ] ) ? $_POST[ 'sef_box' ][ 0 ] : '';
                $comments  = isset( $_POST[ 'allow_comments' ][ 0 ] ) ? 'allowed' : 'not_allowed';
                $share_opt = isset( $_POST[ 'share_option' ][ 0 ] ) ? $_POST[ 'share_option' ][ 0 ] : '';
                $post_date = isset( $_POST[ 'post_date' ][ 0 ] ) ? date('Y-m-d H:i:s', strtotime( $_POST[ 'post_date' ][ 0 ] ) ) : date('Y-m-d H:i:s');

                //-- Save the article
                save_article( $title, $content, $status, $type, $post_date, $comments, $sef_box, $share_opt );
                
                //-- Syncronize with the attachment data if user do upload files
                //-- and syncronize additional field
                $post_id = $db->insert_id();

                attachment_sync( $temp_id, $post_id );

                additional_field_sync( $temp_id, $post_id );
                
                //-- Insert additional fields
                if( isset( $_POST[ 'additional_fields' ] ) )
                {
                    foreach( $_POST[ 'additional_fields' ] as $key => $val )
                    {
                        if( $key == 'custom_field' )
                        {
                            $field = array();

                            foreach( $val as $subkey => $subval )
                            {
                                foreach( $subval[ 'type' ] as $i => $type_dt )
                                {
                                    $field[] = array( 'type' => $type_dt, 'label' => $subval[ 'label' ][ $i ], 'value' => $subval[ 'value' ][ $i ] );
                                }
                            }

                            edit_additional_field( $post_id, $key, json_encode( $field, JSON_HEX_QUOT | JSON_HEX_TAG ), $type );
                        }
                        else
                        {
                            foreach( $val as $subkey => $subval )
                            {
                                $subval = is_array( $subval ) ? json_encode( $subval ) : $subval;

                                edit_additional_field( $post_id, $key, $subval, $type );
                            }
                        }
                    }
                }
                
                //-- Insert the categories into the rule_relationship table
                if( isset( $_POST[ 'category' ][ 0 ] ) )
                {
                    foreach( $_POST[ 'category' ][ 0 ] as $val )
                    {
                        insert_rules_relationship( $post_id, $val );
                    }
                }
                else
                {
                    insert_rules_relationship( $post_id, 1 );
                }
                
                //-- Insert the tags into rules and rules_relationship table
                if( isset( $_POST[ 'tags' ][ 0 ] ) )
                {
                    foreach( $_POST[ 'tags' ][ 0 ] as $val )
                    {
                        $rule_id = insert_rules( 0, $val, '', 'tags', $type, false );

                        insert_rules_relationship( $post_id, $rule_id );
                    }
                }
            }
            elseif( is_edit() )
            {
                $content   = $_POST[ 'post' ][ 0 ];
                $title     = $_POST[ 'title' ][ 0 ];
                $post_id   = $_POST[ 'post_id' ][ 0 ];
                $sef_box   = isset( $_POST[ 'sef_box' ][ 0 ] ) ? $_POST[ 'sef_box' ][ 0 ] : '';
                $comments  = isset( $_POST[ 'allow_comments' ][ 0 ] ) ? 'allowed' : 'not_allowed';
                $share_opt = isset( $_POST[ 'share_option' ][ 0 ] ) ? $_POST[ 'share_option' ][ 0 ] : '';
                $post_date = isset( $_POST[ 'post_date' ][ 0 ] ) ? date('Y-m-d H:i:s', strtotime( $_POST[ 'post_date' ][ 0 ] ) ) : date('Y-m-d H:i:s');

                //-- Update the article
                update_article( $post_id, $title, $content, $status, $type, $post_date, $comments, $share_opt );
                
                //-- Update additional fields
                if( isset( $_POST[ 'additional_fields' ] ) )
                {
                    foreach( $_POST[ 'additional_fields' ] as $key => $val )
                    {
                        if( $key == 'custom_field' )
                        {
                            $field = array();

                            foreach( $val as $subkey => $subval )
                            {
                                foreach( $subval[ 'type' ] as $i => $type_dt )
                                {
                                    $field[] = array( 'type' => $type_dt, 'label' => $subval[ 'label' ][ $i ], 'value' => $subval[ 'value' ][ $i ] );
                                }
                            }

                            edit_additional_field( $post_id, $key, json_encode( $field, JSON_HEX_QUOT | JSON_HEX_TAG ), $type );
                        }
                        else
                        {
                            foreach( $val as $subkey => $subval )
                            {
                                $subval = is_array( $subval ) ? json_encode( $subval ) : $subval;
                                
                                edit_additional_field( $post_id, $key, $subval, $type );
                            }
                        }
                    }
                }
                
                //-- Update the categories at rule_relationship table
                if( isset( $_POST[ 'category' ][ 0 ] ) )
                {
                    delete_rules_relationship( 'app_id=' . $post_id, 'category' );
                    delete_rules_relationship( 'app_id=' . $post_id, 'categories' );
                    
                    foreach( $_POST[ 'category' ][ 0 ] as $key => $val )
                    {
                        insert_rules_relationship( $post_id, $val );
                    }
                }
                else
                {
                    delete_rules_relationship( 'app_id=' . $post_id, 'category' );
                    delete_rules_relationship( 'app_id=' . $post_id, 'categories' );

                    insert_rules_relationship( $post_id, 1 );
                }
                
                //-- Update the tags at rule_relationship table
                delete_rules_relationship( 'app_id=' . $post_id, 'tags' );
                
                if( isset( $_POST[ 'tags' ][ 0 ] ) )
                {
                    foreach( $_POST[ 'tags' ][ 0 ] as $val )
                    {
                        $rule_id = insert_rules( 0, $val, '', 'tags', $type );

                        insert_rules_relationship( $post_id, $rule_id );
                    }
                }
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - List
| -----------------------------------------------------------------------------
*/
function get_article_list( $type, $title, $articles_tabs )
{
    $title  = explode( '|', $title );
    $url    = is_admin_application() ? get_application_url( $type ) : get_state_url( $type );

    set_template( TEMPLATE_PATH . '/template/article-list.html', 'article' );

    add_block( 'list-block', 'l-block', 'article' );

    add_variable( 'type', $type );
    add_variable( 'title', $title[1] );
    add_variable( 'tab', $articles_tabs );
    add_variable( 'limit', post_viewed() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'button', get_admin_button( $url ) );
    add_variable( 'ajax-url', get_article_ajax_url() );
    add_variable( 'view-option', get_article_view_option() );
    add_variable( 'tab-css', empty( $articles_tabs ) ? 'hidden' : '' );
    add_variable( 'tab-border', empty( $articles_tabs ) ? 'style="border-top:1px solid #bbbbbb;"' : '' );    
    
    add_actions( 'section_title', $title[1] );

    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );

    parse_template( 'list-block', 'l-block', 'article' );

    return return_template( 'article' );
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Add New
| -----------------------------------------------------------------------------
*/
function add_new_article( $post_id = 0, $type, $title )
{
    global $thepost;

    $index    = 0;
    $args     = array( $index, $post_id );
    $app_url  = isset( $_GET['sub'] ) ? $_GET['state'] . '&sub=' . $type : $type;

    $url      = is_admin_application() ? get_application_url( $type ) : get_state_url( $app_url );
    $ajax_url = get_article_ajax_url();
    $title    = explode( '|', $title );

    $thepost->post_id    = $post_id;
    $thepost->post_index = $index;
    
    set_template( TEMPLATE_PATH . '/template/article-form.html', 'article' );

    add_block( 'form-language-btn-block', 'flb-block', 'article' );
    add_block( 'form-language-loop-block', 'fll-block', 'article' );
    add_block( 'form-block', 'f-block', 'article' );

    add_variable( 'title', '' );
    add_variable( 'app_title', 'Add New ' . $title[0] );    
    add_variable( 'textarea', textarea( 'post[0]', 0 ) );
    add_variable( 'add_new_tag', add_new_tag( $post_id, 0 ) );
    add_variable( 'most_used_tags', get_most_used_tags( $type ) );
    add_variable( 'all_tags', get_post_tags( $post_id, 0, $type ) );
    add_variable( 'most_used_categories', get_most_used_categories( $type ) );
    add_variable( 'all_categories', article_all_categories( 0, 'categories', $type ) );
    
    add_variable( 'i', 0 );
    add_variable( 'group', $type );
    add_variable( 'post_id', $post_id );
    add_variable( 'ajax_url', $ajax_url );
    add_variable( 'site_url', site_url() );
    add_variable( 'imgs_url', get_theme_img() );
    add_variable( 'post_date', date('d F Y' ) );
    add_variable( 'button', get_admin_button( $url, true ) );
    add_variable( 'sef_scheme', get_sef_scheme( $type, $ajax_url ) );
    add_variable( 'additional_data', attemp_actions( $type . '_additional_field' ) );

    if( is_editor() || is_administrator() )
    {
        add_variable( 'add_new_category', article_new_category( 0, 'categories', $type ) );
    }
    
    article_language_field( $post_id, $type, $title[0] );

    add_actions( 'section_title', $title[0] . ' - Add New' );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/css/select2.min.css' );

    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/js/select2.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/tinymce@4.9.11/tinymce.min.js' );
    add_actions( 'js_elements', 'get_javascript', 'tinymce' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'article' );
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Edit
| -----------------------------------------------------------------------------
*/
function edit_article( $post_id = 0, $type, $title )
{
    global $thepost;
    global $actions;

    $index    = 0;
    $args     = array( $index, $post_id, $type );
    $app_url  = isset( $_GET['sub'] ) ? $_GET['state'] . '&sub=' . $type : $type;

    $url      = is_admin_application() ? get_application_url( $type ) : get_state_url( $app_url );   
    $ajax_url = get_article_ajax_url(); 
    $title    = explode( '|', $title );

    $d = fetch_articles( 'id=' . $post_id . '&type=' . $type );
    
    $selected_categories = find_selected_rules( $post_id, 'categories', $type );
    $thepost->post_id    = $post_id;
    $thepost->post_index = $index;
    
    set_template( TEMPLATE_PATH . '/template/article-form.html', 'article' );

    add_block( 'form-language-btn-block', 'flb-block', 'article' );
    add_block( 'form-language-loop-block', 'fll-block', 'article' );
    add_block( 'form-block', 'f-block', 'article' );

    add_variable( 'app_title', 'Edit ' . $title[0] );
    add_variable( 'add_new_tag', add_new_tag( $post_id, $index ) );
    add_variable( 'all_tags', get_post_tags( $post_id, $index, $type ) );
    add_variable( 'most_used_tags', get_most_used_tags( $type, $index ) );
    add_variable( 'title', rem_slashes( $d[ 'larticle_title' ] ) );
    add_variable( 'post_date', date( 'd F Y', strtotime( $d[ 'lpost_date' ] ) ) );
    add_variable( 'most_used_categories', get_most_used_categories( $type, $index, $selected_categories ) );
    add_variable( 'all_categories', article_all_categories( $index, 'categories', $type, $selected_categories ) );
    add_variable( 'textarea', textarea( 'post[' . $index . ']', $index, rem_slashes( $d[ 'larticle_content' ] ), $post_id ) );
    
    add_variable( 'i', $index );
    add_variable( 'group', $type );
    add_variable( 'ajax_url', $ajax_url );
    add_variable( 'site_url', site_url() );
    add_variable( 'imgs_url', get_theme_img() );
    add_variable( 'button', get_admin_button( $url, true ) );
    add_variable( 'content_lang_css', get_language_content_css() );
    add_variable( 'additional_data', attemp_actions( $type . '_additional_field' ) );
    add_variable( 'sef_scheme', get_sef_scheme( $type, $ajax_url, $index, $post_id, $d, true ) );

    if( is_editor() || is_administrator() )
    {
        add_variable( 'add_new_category', article_new_category( $index, 'categories', $type ) );
    }
    
    article_language_field( $post_id, $type, $title[0] );

    add_actions( 'section_title', $title[0] . ' - Edit' );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/css/select2.min.css' );

    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/js/select2.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/tinymce@4.9.11/tinymce.min.js' );
    add_actions( 'js_elements', 'get_javascript', 'tinymce' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'article' );
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Language Field
| -----------------------------------------------------------------------------
*/
function article_language_field( $post_id = 0, $type, $title, $index = 0 )
{    
    //-- Set Language Form
    $use_multi_lang = get_meta_data( 'multi_language' );
    $lang_list_data = get_all_language();
    
    if( $use_multi_lang == 1 && empty( $lang_list_data ) === FALSE )
    {
        $current_lang = '';

        foreach( $lang_list_data as $d )
        {
            extract( $d );

            $larticle_title   = get_additional_field( $post_id, 'larticle_title_' . $llanguage_code, $type );
            $larticle_content = get_additional_field( $post_id, 'larticle_content_' . $llanguage_code, $type );

            add_variable( 'llanguage', $llanguage );
            add_variable( 'llanguage_index', $index );
            add_variable( 'llanguage_code', $llanguage_code );
            add_variable( 'llanguage_btn_cls', $ldefault == 1 ? 'active' : '' );
            add_variable( 'llanguage_content_css', 'content-lang ' . $llanguage_code );            
            add_variable( 'llanguage_additional_data', attemp_actions( $type . '_additional_field_' . $llanguage_code ) );

            add_variable( 'llanguage_larticle_title', $larticle_title );
            add_variable( 'llanguage_textarea', textarea( 'additional_fields[larticle_content_' . $llanguage_code . '][0]', 0, $larticle_content, $post_id, true, false ) );

            add_variable( 'template_url', TEMPLATE_URL );
                
            parse_template( 'form-language-btn-block', 'flb-block', true );

            if( $ldefault == 1 )
            {
                $current_lang = $llanguage_code;

                continue;
            }

            parse_template( 'form-language-loop-block', 'fll-block', true );
        }

        add_variable( 'conten_css', 'content-lang ' . $current_lang );
    }
    else
    {
        add_variable( 'btn_cls', 'hidden' );
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Batch Delete
| -----------------------------------------------------------------------------
*/
function delete_batch_article( $idx, $type, $title, $articles_tabs )
{
    $title    = explode( '|', $title );
    $backlink = is_admin_application() ? get_application_url( $type ) : get_state_url( $type );

    set_template( TEMPLATE_PATH . '/template/article-batch-delete.html', 'article-batch-delete' );

    add_block( 'delete-loop-block', 'dl-block', 'article-batch-delete' );
    add_block( 'delete-block', 'd-block', 'article-batch-delete' );

    foreach( $_POST[ 'select' ] as $key => $val )
    {
        $d = fetch_articles( 'id=' . $val . '&type=' . $type );

        add_variable( 'article_id', $d[ 'larticle_id' ] );
        add_variable( 'article_title', $d[ 'larticle_title' ] );

        parse_template( 'delete-loop-block', 'dl-block', true );
    }

    add_variable( 'type', $type );
    add_variable( 'title', $title[ 1 ] );
    add_variable( 'tab', $articles_tabs );
    add_variable( 'backlink', $backlink );
    add_variable( 'tab-css', empty( $articles_tabs ) ? 'hidden' : '' );
    add_variable( 'tab-border', empty( $articles_tabs ) ? 'style="border-top:1px solid #bbbbbb;"' : '' );    
    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST[ 'select' ] ) == 1 ? 'this ' . $title[ 0 ] : 'these ' . $title[ 1 ] ) );
    
    add_actions( 'section_title', $title[ 1 ] );

    parse_template( 'delete-block', 'd-block', 'article-batch-delete' );

    return return_template( 'article-batch-delete' );
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Table Query
| -----------------------------------------------------------------------------
*/
function get_article_list_query()
{    
    global $db;

    extract( $_POST );

    $rdata = $_REQUEST;
    $cols  = array( 
        1 => 'a.larticle_title',
        3 => 'a.lpost_by',
        4 => 'a.lpost_date', 
        5 => 'a.larticle_status'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            if( isset( $cols[ $rdata['order'][$i]['column'] ] ) )
            {
                $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
            }
        }

        $order = !empty( $o ) ? 'ORDER BY ' . implode( ', ', $o ) : '';
    }
    else
    {
        $order = 'ORDER BY a.lorder ASC';
    }

    if( isset( $show ) && !empty( $show ) && $show != 'all' )
    {
        $where = $db->prepare_query( ' AND larticle_status = %s', $show );
    }
    else
    {
        $where = '';
    }

    if( empty( $rdata['search']['value']) )
    {
        $q = 'SELECT * FROM lumonata_articles AS a WHERE a.larticle_type = "' . $type . '" ' . $where . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $q = 'SELECT * FROM lumonata_articles AS a WHERE a.larticle_type = "' . $type . '" ' . $where . ' AND ( ' . implode( ' OR ', $search ) . ' ) ' . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }
    
    $data = array();

    if( $n2 > 0 )
    {
        $url = is_admin_application() ? get_application_url( $type ) : get_state_url( $type );

        while( $d2 = $db->fetch_array( $r2 ) )
        {   
            $order     = $d2[ 'lorder' ];
            $id        = $d2[ 'larticle_id' ];
            $user      = fetch_user( $d2[ 'lpost_by' ] );
            $user_link = user_url( $user[ 'luser_id' ] );
            $avatar    = get_avatar( $user[ 'luser_id' ], 3 );
            $category  = get_article_category_list( $d2['larticle_id'], $type );
            $date      = date( get_date_format(), strtotime( $d2[ 'lpost_date' ] ) );
            $title     = isset( $d2[ 'larticle_title_lang' ] ) ? $d2[ 'larticle_title_lang' ] : $d2[ 'larticle_title' ];
            $status    = '<strong style="' . ( $d2[ 'larticle_status' ] != 'publish' ? 'color:red;' : '' ) . '">' . ucfirst( $d2[ 'larticle_status' ] ) . '</strong>';

            $data[] = array(
                'id'        => $id,
                'order'     => $order,
                'date'      => $date,
                'title'     => $title,
                'status'    => $status,
                'avatar'    => $avatar,
                'category'  => $category,
                'user_link' => $user_link,
                'ajax_link' => get_article_ajax_url(),
                'author'    => $user[ 'ldisplay_name' ],
                'edit_link' => $url . '&prc=edit&id=' . $id
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - All Category Link
| -----------------------------------------------------------------------------
*/
function get_article_category_list( $id, $type )
{
    $c = array();
    $r = fetch_rule_relationship( 'app_id=' . $id );
    $u = is_admin_application() ? get_application_url( $type ) : get_state_url( $type );
    
    foreach( $r as $id )
    {
        $d   = fetch_rule( 'rule_id=' . $id );
        $c[] = '<a class="field-text" data-field-id="' . $id . '" data-field-name="lname" href="' . $u . '&category_name=' . $d[ 'lsef' ] . '">' . $d[ 'lname' ] . '</a>';
    }

    if( empty( $c ) )
    {
        return '-';
    }
    else
    {
        return implode( ', ', $c );
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - View Option
| -----------------------------------------------------------------------------
*/
function get_article_view_option()
{
    $opt_viewed   = '';    
    $show_data    = isset( $_POST[ 'data_to_show' ] ) ? $_POST[ 'data_to_show' ] : ( isset( $_GET[ 'data_to_show' ] ) ? $_GET[ 'data_to_show' ] : '' );
    $data_to_show = array( 'all' => 'All', 'publish' => 'Publish', 'unpublish' => 'Unpublish', 'draft' => 'Draft' );
    
    foreach( $data_to_show as $key => $val )
    {
        if( isset( $show_data ) )
        {
            if( $show_data == $key )
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" checked="checked" />
                <label>' . $val . '</label>';
            }
            else
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" />
                <label>' . $val . '</label>';
            }
        }
        elseif( $key == 'all' )
        {
            $opt_viewed .= '
            <input type="radio" name="data_to_show" value="' . $key . '" checked="checked" />
            <label>' . $val . '</label>';
        }
        else
        {
            $opt_viewed .= '
            <input type="radio" name="data_to_show" value="' . $key . '" />
            <label>' . $val . '</label>';
        }
    }

    return $opt_viewed;
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Additional Data
| -----------------------------------------------------------------------------
*/
function additional_data( $name, $tag, $display = true, $args = array() )
{
    global $thepost;
    
    $data = '
    <fieldset>
        <div class="additional_data">
            <h2 id="' . $tag . '_' . $thepost->post_index . '">' . $name . '</h2>
            <div class="additional_content" id="' . $tag . '_details_' . $thepost->post_index . '" ' . ( $display == false ? 'style="display:none;"' : $display ) . '>
               ' . ( function_exists( $tag ) ? call_user_func_array( $tag, $args ) : $tag ) . '
            </div>
            <script type="text/javascript">
                jQuery(function(){
                    jQuery("#' . $tag . '_' . $thepost->post_index . '").click(function(){
                        jQuery("#' . $tag . '_details_' . $thepost->post_index . '").slideToggle(100);
                        return false;
                    });
                });
            </script>
        </div>
    </fieldset>';

    return $data;    
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - New Category
| -----------------------------------------------------------------------------
*/
function article_new_category( $index, $rule_name, $group )
{
    $return = '
    <div class="add_new_cattags clearfix">
        <input type="text" class="new-category-textbox-' . $index . ' category_textbox" name="new_category[' . $index . ']" value="" placeholder="New category name" autocomplete="off" />

        <div id="select_parent_' . $index . '" class="select_parent_' . $index . '">
            ' . article_category_list_option( $index, $rule_name, $group ) . '
        </div>

        <div class="add_cattags_button">
            <input type="button" name="add_category_btn[' . $index . ']" value="Add" class="new-category-button-' . $index . ' button" />
        </div>
    </div>';

    return $return;
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - All Category
| -----------------------------------------------------------------------------
*/
function article_all_categories( $index, $rule_name, $group, $rule_id = array() )
{
    $return  = recursive_taxonomy( $index, $rule_name, $group, 'checkbox', $rule_id, null, 0, 0, false, true );
    $return .= '<span id="selected_category_' . $index . '" class="selected_category_' . $index . '">' . json_encode( $rule_id ) .'</span>';

    return $return;
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Category List Options
| -----------------------------------------------------------------------------
*/
function article_category_list_option( $index, $rule_name, $group )
{
    $return = '
    <select class="category_combobox" name="parent[' . $index . ']" autocomplete="off">
        <option value="0">Parent</option>
        ' . recursive_taxonomy( $index, $rule_name, $group, 'select' ) . '
    </select>';

    return $return;
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Save Data Article
| -----------------------------------------------------------------------------
*/
function save_article( $title, $content, $status, $type, $post_date = '', $comments = '', $sef = '', $share_to = 0 )
{
    global $db, $allowedposttags, $allowedtitletags;
    
    $title     = empty( $title ) ? 'Untitled' : kses( rem_slashes( $title ), $allowedtitletags );
    $content   = kses( rem_slashes( $content ), $allowedposttags );
    $post_date = empty( $post_date ) ? date( 'Y-m-d H:i:s' ) : $post_date;

    if( empty( $sef ) )
    {
        $num_by_title_and_type = is_num_articles( 'title=' . $title . '&type=' . $type );
        
        if( $num_by_title_and_type > 0 )
        {
            for( $i = 2; $i <= $num_by_title_and_type + 1; $i++ )
            {
                $sef = generateSefUrl( $title ) . '-' . $i;
                
                if( is_num_articles( 'sef=' . $sef . '&type=' . $type ) < 1 )
                {
                    $sef = $sef;

                    break;
                }
            }
        }
        else
        {
            $sef = generateSefUrl( $title );
        }
    }
    
    $s = 'INSERT INTO lumonata_articles(
                larticle_title,
                larticle_content,
                larticle_status,
                larticle_type,
                lcomment_status,
                lsef,
                lpost_by,
                lpost_date,
                lupdated_by,
                ldlu,
                lshare_to)
             VALUES( %s, %s, %s, %s, %s, %s, %d, %s, %d, %s, %d )';
    $q = $db->prepare_query( $s, $title, $content, $status, $type, $comments, $sef, $_COOKIE[ 'user_id' ], $post_date, $_COOKIE[ 'user_id' ], $post_date, $share_to );
    
    if( reset_order_id( 'lumonata_articles' ) )
    {
        $r = $db->do_query( $q );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    return false;
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Syncronize Attachment Data Article
| -----------------------------------------------------------------------------
*/
function attachment_sync( $old_id, $new_id )
{
    global $db;

    $s = 'UPDATE lumonata_attachment SET larticle_id=%d WHERE larticle_id=%d';
    $q = $db->prepare_query( $s, $new_id, $old_id );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Syncronize Additional Data Article
| -----------------------------------------------------------------------------
*/
function additional_field_sync( $old_id, $new_id )
{
    global $db;

    $s = 'UPDATE lumonata_additional_fields SET lapp_id=%d WHERE lapp_id=%d';
    $q = $db->prepare_query( $s, $new_id, $old_id );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Update Article
| -----------------------------------------------------------------------------
*/
function update_article( $post_id, $title, $content, $status, $type, $post_date = '', $comments = '', $share_to = 0 )
{
    global $db, $allowedposttags, $allowedtitletags;

    $title     = empty( $title ) ? 'Untitled' : kses( rem_slashes( $title ), $allowedtitletags );
    $content   = kses( rem_slashes( $content ), $allowedposttags );
    $post_date = empty( $post_date ) ? date( 'Y-m-d H:i:s' ) : $post_date;
    
    $s = 'UPDATE lumonata_articles SET 
            larticle_title = %s,
            larticle_content = %s,
            larticle_status = %s,
            larticle_type = %s,
            lcomment_status = %s,
            lupdated_by = %s,
            lpost_date = %s,
            ldlu = %s,
            lshare_to = %d
          WHERE larticle_id = %d';
    $q = $db->prepare_query( $s, $title, $content, $status, $type, $comments, $_COOKIE[ 'user_id' ], $post_date, date( 'Y-m-d H:i:s' ), $share_to, $post_id );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Update Sef URL Article
| -----------------------------------------------------------------------------
*/
function update_sef( $post_id, $title, $sef, $type )
{
    global $db, $allowedposttags, $allowedtitletags;

    $update = false;
    
    if( empty( $title ) )
    {
        $title = 'Untitled';
    }
    else
    {
        $title = kses( rem_slashes( $title ), $allowedtitletags );
    }
    
    if( empty( $sef ) )
    {
        $update = false;
    }
    else
    {
        $num = is_num_articles( 'sef=' . $sef . '&type=' . $type );

        if( $num > 0 )
        {
            $update = false;
        }
        else
        {
            $sef    = generateSefUrl( $sef );
            $update = true;
        }        
    }
    
    if( $update )
    {
        $s = 'UPDATE lumonata_articles SET lsef=%s WHERE larticle_id=%d';
        $q = $db->prepare_query( $s, kses( rem_slashes( $sef ), $allowedtitletags ), $post_id );
        $r = $db->do_query( $q );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    return false;
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Delete Article
| -----------------------------------------------------------------------------
*/
function delete_article( $article_id, $app_name )
{
    global $db;

    delete_attachment_by_article( $article_id );
    
    if( delete_additional_field( $article_id, $app_name ) )
    {
        $d = fetch_rule_relationship( 'app_id=' . $article_id );

        if( delete_rules_relationship( 'app_id=' . $article_id ) )
        {
            if( is_array( $d ) )
            {
                foreach( $d as $key => $val )
                {
                    $rule_count = count_rules_relationship( 'rule_id=' . $val );

                    update_rule_count( $val, $rule_count );
                }
                
                $s = 'DELETE FROM lumonata_articles WHERE larticle_id=%d';
                $q = $db->prepare_query( $s, $article_id );
                $r = $db->do_query( $q );

                if( is_array( $r ) )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Check Number of Articles
| -----------------------------------------------------------------------------
*/
function is_num_articles( $args = '' )
{
    global $db;

    $var_name[ 'id' ]    = '';
    $var_name[ 'sef' ]   = '';
    $var_name[ 'title' ] = '';
    $var_name[ 'type' ]  = 'pages';
    
    $args = str_replace( '&amp;', 'U+0026', $args );
    
    if( !empty( $args ) )
    {
        $args = explode( '&', $args );

        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );

            if( $variable == 'title' || $variable == 'id' || $variable == 'type' || $variable == 'sef' )
            {
                $var_name[ $variable ] = $value;
            }

            $var_name[ $variable ] = str_replace( 'U+0026', '&amp;', $var_name[ $variable ] );
        }
    }
    
    $w = '';

    if( isset( $_COOKIE[ 'user_type' ] ) && ( $_COOKIE[ 'user_type' ] == 'contributor' || $_COOKIE[ 'user_type' ] == 'author' ) )
    {
        $w = ' lpost_by=' . $_COOKIE[ 'user_id' ] . ' AND ';
    }
    
    if( !empty( $var_name[ 'title' ] ) && !empty( $var_name[ 'id' ] ) )
    {
        $s = 'SELECT * FROM lumonata_articles WHERE ' . $w . ' larticle_type=%s AND larticle_id=%d';
        $q = $db->prepare_query( $s, $var_name[ 'type' ], $var_name[ 'id' ] );
    }
    elseif( !empty( $var_name[ 'id' ] ) && empty( $var_name[ 'title' ] ) )
    {
        $s = 'SELECT * FROM lumonata_articles WHERE ' . $w . ' larticle_type=%s AND larticle_id=%d';
        $q = $db->prepare_query( $s, $var_name[ 'type' ], $var_name[ 'id' ] );
    }
    elseif( !empty( $var_name[ 'title' ] ) && empty( $var_name[ 'id' ] ) )
    {
        $s = 'SELECT * FROM lumonata_articles WHERE ' . $w . ' larticle_type=%s AND larticle_title=%s';
        $q = $db->prepare_query( $s, $var_name[ 'type' ], $var_name[ 'title' ] );        
    }
    elseif( !empty( $var_name[ 'sef' ] ) )
    {
        $s = 'SELECT * FROM lumonata_articles WHERE ' . $w . ' larticle_type=%s AND lsef=%s';
        $q = $db->prepare_query( $s, $var_name[ 'type' ], $var_name[ 'sef' ] );
    }
    else
    {
        $s = 'SELECT * from lumonata_articles WHERE ' . $w . ' larticle_type=%s';
        $q = $db->prepare_query( $s, $var_name[ 'type' ] );
    }
    
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return 0;
    }
    else
    {
        return $db->num_rows( $r );
    }    
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Check If Articles Data Exist
| -----------------------------------------------------------------------------
*/
function is_have_articles( $type = 'articles' )
{
    if( is_num_articles( 'type=' . $type ) > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Batch Update Article Order
| -----------------------------------------------------------------------------
*/
function update_articles_order( $order, $start, $app_name )
{
    global $db;

    foreach( $order as $key => $val )
    {
        $s = 'UPDATE lumonata_articles SET 
                lupdated_by = %s,
                lorder = %d,
                ldlu = %s
              WHERE larticle_id = %d AND larticle_type = %s';
        $q = $db->prepare_query( $s, $_COOKIE[ 'user_id' ], $key + $start, date( 'Y-m-d H:i:s' ), $val, $app_name );
        $r = $db->do_query( $q );
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Update Article Order
| -----------------------------------------------------------------------------
*/
function update_article_order( $id, $order, $app_name )
{
    global $db;

    $s = 'UPDATE lumonata_articles SET
            lupdated_by = %s,
            lorder = %d,
            ldlu = %s
          WHERE larticle_id = %d AND larticle_type = %s';
    $q = $db->prepare_query( $s, $_COOKIE[ 'user_id' ], $order, date( 'Y-m-d H:i:s' ), $id, $app_name );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Update Article Status
| -----------------------------------------------------------------------------
*/
function update_articles_status( $id, $status )
{
    global $db;
    
    $s = 'UPDATE lumonata_articles SET 
            larticle_status = %s,
            lupdated_by = %s,
            ldlu = %s
          WHERE larticle_id = %d';
    $q = $db->prepare_query( $s, $status, $_COOKIE[ 'user_id' ], date( 'Y-m-d H:i:s' ), $id );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Fetch Post Data By article id
| -----------------------------------------------------------------------------
*/
function fetch_articles_by_id( $article_id )
{
    global $db;

    $s = 'SELECT *, (
                SELECT JSON_ARRAYAGG( JSON_OBJECT( 
                    "lapp_id", lapp_id, 
                    "lkey", lkey, 
                    "lvalue", lvalue,
                    "lapp_name", lapp_name 
                ))
                FROM lumonata_additional_fields AS a2 
                WHERE a2.lapp_id = a.larticle_id 
                AND a2.lapp_name = a.larticle_type 
             ) AS ladditional, (
                SELECT JSON_ARRAYAGG( JSON_OBJECT( 
                    "lattach_id", lattach_id, 
                    "larticle_id", larticle_id, 
                    "lattach_loc", lattach_loc, 
                    "lattach_loc_thumb", lattach_loc_thumb,
                    "lattach_loc_medium", lattach_loc_medium,
                    "lattach_loc_large", lattach_loc_large,
                    "lapp_name", lapp_name,
                    "ltitle", ltitle,
                    "lcontent", lcontent,
                    "lalt_text", lalt_text,
                    "lcaption", lcaption,
                    "mime_type", mime_type,
                    "lorder", lorder,
                    "upload_date", upload_date,
                    "date_last_update", date_last_update
                ))
                FROM lumonata_attachment AS a2 
                WHERE a2.larticle_id = a.larticle_id
                ORDER BY a2.lorder
             ) AS lattachment
         FROM lumonata_articles AS a
         WHERE larticle_id = %d';
    $q = $db->prepare_query( $s, $article_id );    
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return array();
    }
    else
    {
        $d = $db->fetch_array( $r );

        $ladditional = json_decode( $d['ladditional'], true );

        if( $ladditional !== null && json_last_error() === JSON_ERROR_NONE )
        {
            unset( $d['ladditional'] );

            foreach( $ladditional as $a )
            {
                $d['additional'][ $a['lkey'] ] = $a['lvalue'];
            }
        }

        $lattachment = json_decode( $d['lattachment'], true );

        if( $lattachment !== null && json_last_error() === JSON_ERROR_NONE )
        {
            unset( $d['lattachment'] );

            foreach( $lattachment as $a )
            {
                $d['attachment'][ $a['lapp_name'] ][] = $a;
            }
        }

        return $d;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Fetch Post Data By article id
| -----------------------------------------------------------------------------
*/
function fetch_articles_by_rule( $args = '' )
{
    global $db;

    $dt = array();

    $var_name[ 'id' ]     = '';
    $var_name[ 'sef' ]    = '';
    $var_name[ 'name' ]   = '';
    $var_name[ 'group' ]  = 'blogs';
    $var_name[ 'status' ] = 'publish';
    $var_name[ 'order' ]  = 'b.lorder';
    $var_name[ 'rule' ]   = 'category';
    
    if( !empty( $args ) )
    {
        $args = explode( '&', $args );

        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );

            if( $variable == 'id' || $variable == 'sef' || $variable == 'name' || $variable == 'group' || $variable == 'order' || $variable == 'status' || $variable == 'rule' )
            {
                $var_name[ $variable ] = $value;
            }            
        }
    }

    $w = array();

    if( isset( $_COOKIE[ 'user_type' ] ) && isset( $_COOKIE[ 'user_type' ] ) )
    {
        if( $_COOKIE[ 'user_type' ] == 'contributor' || $_COOKIE[ 'user_type' ] == 'author' )
        {
            $w[] = $db->prepare_query( 'lpost_by = %s', $_COOKIE[ 'user_id' ] );
        }
    }
    
    if( !empty( $var_name[ 'sef' ] ) )
    {
        $w[] = $db->prepare_query( 'c.lsef = %s', $var_name[ 'sef' ] );
    }
    
    if( !empty( $var_name[ 'id' ] ) )
    {
        $w[] = $db->prepare_query( 'c.lrule_id = %s', $var_name[ 'id' ] );
    }
    
    if( !empty( $var_name[ 'group' ] ) )
    {
        $w[] = $db->prepare_query( 'c.lgroup = %s', $var_name[ 'group' ] );
    }
    
    if( !empty( $var_name[ 'name' ] ) )
    {
        $w[] = $db->prepare_query( 'c.lname = %s', $var_name[ 'name' ] );
    }
    
    if( !empty( $var_name[ 'status' ] ) )
    {
        $w[] = $db->prepare_query( 'b.larticle_status = %s', $var_name[ 'status' ] );
    }

    if( empty( $w ) )
    {
        $s = 'SELECT * FROM lumonata_rule_relationship AS a
              LEFT JOIN lumonata_articles AS b ON a.lapp_id = b.larticle_id
              LEFT JOIN lumonata_rules AS c ON a.lrule_id = c.lrule_id
              ORDER BY ' . $var_name['order'];
        $q = $db->prepare_query( $s );
    }
    else
    {
        $s = 'SELECT * FROM lumonata_rule_relationship AS a
              LEFT JOIN lumonata_articles AS b ON a.lapp_id = b.larticle_id
              LEFT JOIN lumonata_rules AS c ON a.lrule_id = c.lrule_id
              WHERE ' . implode( ' AND ', $w ) . '
              ORDER BY ' . $var_name['order'];
        $q = $db->prepare_query( $s );
    }

    $r = $db->do_query( $q );

    if( !is_array( $r ) )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $s2 = 'SELECT * FROM lumonata_additional_fields AS a2 WHERE a2.lapp_id = %d AND a2.lapp_name = %d';
            $q2 = $db->prepare_query( $s2, $d['larticle_id'], $d['larticle_type'] );
            $r2 = $db->do_query( $q2 );       

            if( !is_array( $r2 ) )
            {
                while( $d2 = $db->fetch_array( $r2 ) )
                {
                    $d['additional'][ $d2['lkey'] ] = $d2['lvalue'];
                }
            }

            $s3 = 'SELECT * FROM lumonata_attachment AS a2 WHERE a2.larticle_id = %d ORDER BY a2.lorder';
            $q3 = $db->prepare_query( $s3, $d['larticle_id'] );
            $r3 = $db->do_query( $q3 );       

            if( !is_array( $r3 ) )
            {
                while( $d3 = $db->fetch_array( $r3 ) )
                {
                    $d['attachment'][ $d3['lapp_name'] ][] = $d3;
                }
            }

            $dt[] = $d;
        }
    }

    return $dt;
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Fetch Post Data
| -----------------------------------------------------------------------------
*/
function fetch_articles( $args = '', $fetch = true )
{
    global $db;

    $var_name[ 'id' ]     = '';
    $var_name[ 'sef' ]    = '';
    $var_name[ 'title' ]  = '';
    $var_name[ 'limit' ]  = '';
    $var_name[ 'exid' ]   = '';
    $var_name[ 'type' ]   = 'pages';
    $var_name[ 'order' ]  = 'lorder';
    $var_name[ 'status' ] = 'publish';
    
    if( !empty( $args ) )
    {
        $args = explode( '&', $args );

        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );

            if( $variable == 'sef' || $variable == 'title' || $variable == 'id' || $variable == 'type' || $variable == 'order' || $variable == 'status' || $variable == 'limit' || $variable == 'exid' )
            {
                $var_name[ $variable ] = $value;
            }
        }
    }

    $w = array();

    if( isset( $_COOKIE[ 'user_type' ] ) && isset( $_COOKIE[ 'user_type' ] ) )
    {
        if( $_COOKIE[ 'user_type' ] == 'contributor' || $_COOKIE[ 'user_type' ] == 'author' )
        {
            $w[] = $db->prepare_query( 'lpost_by = %s', $_COOKIE[ 'user_id' ] );
        }
    }
    
    if( !empty( $var_name[ 'sef' ] ) )
    {
        $w[] = $db->prepare_query( 'lsef = %s', $var_name[ 'sef' ] );
    }
    
    if( !empty( $var_name[ 'id' ] ) )
    {
        $w[] = $db->prepare_query( 'larticle_id = %s', $var_name[ 'id' ] );
    }
    
    if( !empty( $var_name[ 'type' ] ) )
    {
        $w[] = $db->prepare_query( 'larticle_type = %s', $var_name[ 'type' ] );
    }
    
    if( !empty( $var_name[ 'title' ] ) )
    {
        $w[] = $db->prepare_query( 'larticle_title = %s', $var_name[ 'title' ] );
    }
    
    if( !empty( $var_name[ 'status' ] ) )
    {
        $w[] = $db->prepare_query( 'larticle_status = %s', $var_name[ 'status' ] );
    }
    
    if( !empty( $var_name[ 'exid' ] ) )
    {
        $w[] = $db->prepare_query( 'larticle_id <> %s', $var_name[ 'exid' ] );
    }

    if( empty( $w ) )
    {
        $s = 'SELECT * FROM lumonata_articles AS a ORDER BY ' . $var_name['order'];
        $q = $db->prepare_query( $s );
    }
    else
    {
        $s = 'SELECT * FROM lumonata_articles AS a WHERE ' . implode( ' AND ', $w ) . ' ORDER BY ' . $var_name['order'];
        $q = $db->prepare_query( $s );
    }
    
    if( !empty( $var_name[ 'limit' ] ) )
    {
        $s = $s . ' LIMIT ' . $var_name[ 'limit' ];
        $q = $db->prepare_query( $s );
    }

    $r = $db->do_query( $q );       

    if( is_array( $r ) )
    {    
        return array();
    }
    else
    {
        $data = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $s2 = 'SELECT * FROM lumonata_additional_fields AS a2 WHERE a2.lapp_id = %d AND a2.lapp_name = %d';
            $q2 = $db->prepare_query( $s2, $d['larticle_id'], $d['larticle_type'] );
            $r2 = $db->do_query( $q2 );       

            if( !is_array( $r2 ) )
            {
                while( $d2 = $db->fetch_array( $r2 ) )
                {
                    $d['additional'][ $d2['lkey'] ] = $d2['lvalue'];
                }
            }

            $s3 = 'SELECT * FROM lumonata_attachment AS a2 WHERE a2.larticle_id = %d ORDER BY a2.lorder';
            $q3 = $db->prepare_query( $s3, $d['larticle_id'] );
            $r3 = $db->do_query( $q3 );       

            if( !is_array( $r3 ) )
            {
                while( $d3 = $db->fetch_array( $r3 ) )
                {
                    $d['attachment'][ $d3['lapp_name'] ][] = $d3;
                }
            }

            $s4 = 'SELECT b2.* 
                   FROM lumonata_rule_relationship AS a2
                   LEFT JOIN lumonata_rules AS b2 ON a2.lrule_id = b2.lrule_id 
                   WHERE a2.lapp_id = %d ORDER BY b2.lorder';
            $q4 = $db->prepare_query( $s4, $d['larticle_id'] );
            $r4 = $db->do_query( $q4 );       

            if( !is_array( $r4 ) )
            {
                $rule_name = array();
                $rule_sef  = array();

                while( $d4 = $db->fetch_array( $r4 ) )
                {
                    $d['rule'][] = $d4;

                    $rule_name[] = $d4['lname'];
                    $rule_sef[]  = $d4['lsef'];
                }

                $d['rule_name'] = implode( ', ', $rule_name );
                $d['rule_sef']  = implode( ', ', $rule_sef );
            }

            $data[] = $d;
        }

        if( $fetch && !isset( $data[0] ) )
        { 
            $data[0] = array();
        }
        
        return $fetch ? $data[0] : $data;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Get Post Title
| -----------------------------------------------------------------------------
*/
function get_article_title( $id )
{
    global $db;

    $s = 'SELECT larticle_title FROM lumonata_articles WHERE larticle_id = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d[ 'larticle_title' ];
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Get User Name
| -----------------------------------------------------------------------------
*/
function the_user( $id )
{
    $the_user = fetch_user( $id );

    return $the_user[ 'ldisplay_name' ];
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Get All Categories
| -----------------------------------------------------------------------------
*/
function the_categories( $post_id, $type )
{
    $the_selected_categories = find_selected_rules( $post_id, 'categories', $type );
    $the_categories          = recursive_taxonomy( 0, 'categories', $type, 'category', $the_selected_categories );

    return trim( $the_categories, ', ' );
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Get All Tags
| -----------------------------------------------------------------------------
*/
function the_tags( $post_id, $type )
{
    $the_selected_tags = find_selected_rules( $post_id, 'tags', $type );
    $the_tags          = recursive_taxonomy( 0, 'tags', $type, 'tag', $the_selected_tags );

    return trim( $the_tags, ', ' );
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Filter Content Text
| -----------------------------------------------------------------------------
*/
function filter_content( $the_content, $splitit = false )
{
    if( $splitit )
    {
        $match       = preg_split( '/<!--\s+pagebreak\s+-->/', $the_content );
        $the_content = $match[ 0 ];
    }
    
    return $the_content;
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_article_ajax_url()
{
    return get_state_url( 'ajax&apps=article' );
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Get Post Image
| -----------------------------------------------------------------------------
*/
function get_post_image()
{
    global $db;

    $s = 'SELECT * FROM lumonata_attachment AS a 
          WHERE a.larticle_id = %d 
          AND a.lapp_name = %s 
          AND a.mime_type LIKE %s 
          ORDER BY a.lorder';
    $q = $db->prepare_query( $s, $_POST['post_id'], $_POST['app_name'], '%image%' );
    $r = $db->do_query( $q );

    $data = array();

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $filename = explode( '/', $d['lattach_loc_thumb'] );
            $filename = array_reverse( $filename );

            $data[] = array(
                'filename'  => $filename,
                'title'     => $d['ltitle'],
                'content'   => $d['lcontent'],
                'caption'   => $d['lcaption'],
                'alt'       => $d['lalt_text'],
                'attach_id' => $d['lattach_id'],
                'post_id'   => $d['larticle_id'],
                'fileturl'  => site_url( $d[ 'lattach_loc_thumb' ] ),
                'fileurl'   => site_url( $d[ 'lattach_loc_medium' ] )                 
            );
        }
    }

    if( empty( $data ) )
    {
        return array( 'result' => 'failed' );
    }
    else
    {
        return array( 'result' => 'success', 'data' => $data );
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Upload Post Files
| -----------------------------------------------------------------------------
*/
function upload_post_file()
{
    global $db;

    if( isset( $_FILES['file'] ) && $_FILES['file']['error']==0 )
    {
        $post_id     = $_POST['post_id'];
        $app_name    = $_POST['app_name'];
        $allowed     = $_POST['allowed_file'];
        $file_name   = $_FILES['file']['name'];
        $file_size   = $_FILES['file']['size'];
        $file_type   = $_FILES['file']['type'];
        $file_source = $_FILES['file']['tmp_name'];
        $is_batch    = isset( $_POST['batch'] ) ? $_POST['batch'] : false;

        if( is_allow_file_type( $file_type, $allowed ) )
        {
            //-- CHECK FOLDER EXIST
            $folder = upload_folder_name();

            if( !is_dir( FILES_PATH . '/' . $folder ) )
            {
                create_dir( FILES_PATH . '/' . $folder );
            }

            $ext    = file_name_filter( $file_name, true );
            $name   = file_name_filter( $file_name, false ) . '-' . time();
            $name_f = $name . $ext;

            $ldestination = FILES_PATH . '/' . $folder . '/' . $name_f;
            $mdestination = FILES_PATH . '/' . $folder . '/' . $name_f;
            $tdestination = FILES_PATH . '/' . $folder . '/' . $name_f;
            $fdestination = FILES_PATH . '/' . $folder . '/' . $name_f;

            if( $allowed == 'pdf' )
            {
                $fileurl = template_url( 'images/ico-upload-pdf.svg', true );
            }
            else
            {
                $fileurl = template_url( 'images/ico-upload-doc.svg', true );
            }

            if( upload( $file_source, $fdestination ) )
            {
                $loc        = '/lumonata-content/files/' . $folder . '/' . $name_f;
                $loc_thumb  = '/lumonata-content/files/' . $folder . '/' . $name_f;
                $loc_medium = '/lumonata-content/files/' . $folder . '/' . $name_f;
                $loc_large  = '/lumonata-content/files/' . $folder . '/' . $name_f;

                $d = get_attachment_by_app_name( $post_id, $app_name, false );

                if( empty( $d ) || $is_batch )
                {
                    if( insert_attachment( $post_id, 0, $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, $app_name, false ) )
                    {
                        $attach_id = $db->insert_id();

                        if( empty( $attach_id ) )
                        {
                            //-- Delete Original Image
                            if( file_exists( $fdestination ) )
                            {
                                unlink( $fdestination );
                            }

                            return array( 'result' => 'failed', 'message' => 'Error uploading file' );
                        }
                        else
                        {
                            $s = 'SELECT * FROM lumonata_attachment AS a  WHERE a.lattach_id = %d';
                            $q = $db->prepare_query( $s, $attach_id );
                            $r = $db->do_query( $q );

                            if( is_array( $r ) )
                            {
                                return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                            }
                            else
                            {
                                $d = $db->fetch_array( $r );

                                return array(
                                    'result' => 'success',
                                    'data'   => array(
                                        'filename'  => $name_f,
                                        'post_id'   => $post_id,
                                        'attach_id' => $attach_id,
                                        'title'     => $d['ltitle'],
                                        'content'   => $d['lcontent'],
                                        'caption'   => $d['lcaption'],
                                        'alt'       => $d['lalt_text'],
                                        'fileturl'  => $fileurl,
                                        'fileurl'   => $fileurl
                                    )
                                );
                            }
                        }
                    }
                    else
                    {
                        //-- Delete Original Image
                        if( file_exists( $fdestination ) )
                        {
                            unlink( $fdestination );
                        }

                        return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                    }
                }
                else
                {
                    if( update_attachment( $d['lattach_id'], $post_id, 0, $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, $app_name ) )
                    {
                        //-- Delete Original Image
                        if( !empty( $d[ 'lattach_loc' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc' ] ) )
                        {
                            unlink( ROOT_PATH . $d[ 'lattach_loc' ] );
                        }
                    }

                    $s = 'SELECT * FROM lumonata_attachment AS a  WHERE a.lattach_id = %d';
                    $q = $db->prepare_query( $s, $d['lattach_id'] );
                    $r = $db->do_query( $q );

                    if( is_array( $r ) )
                    {
                        return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                    }
                    else
                    {
                        $d = $db->fetch_array( $r );

                        return array(
                            'result' => 'success',
                            'data'   => array(
                                'filename'  => $name_f,
                                'post_id'   => $post_id,
                                'title'     => $d['ltitle'],
                                'content'   => $d['lcontent'],
                                'caption'   => $d['lcaption'],
                                'alt'       => $d['lalt_text'],
                                'attach_id' => $d['lattach_id'],
                                'fileturl'  => $fileurl,
                                'fileurl'   => $fileurl
                            )
                        );
                    }
                }
            }
            else
            {
                return array( 'result' => 'failed', 'error' => 'Error uploading file' );
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Delete Post File
| -----------------------------------------------------------------------------
*/
function delete_post_file()
{
    if( isset( $_POST['attach_id'] ) )
    {
        if( delete_attachment( $_POST['attach_id'] ) )
        {
            return array( 'result' => 'success' );
        }
        else
        {
            return array( 'result' => 'failed' );
        }
    }
    else
    {
        if( empty( $_POST['post_id'] ) )
        {
            return array( 'result' => 'failed' );
        }
        else
        {
            $d = get_attachment_by_app_name( $_POST['post_id'], $_POST['app_name'], false );

            if( delete_attachment( $d['lattach_id'] ) )
            {
                return array( 'result' => 'success' );
            }
            else
            {
                return array( 'result' => 'failed' );
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Upload Post Image
| -----------------------------------------------------------------------------
*/
function upload_post_image()
{
    global $db;

    if( isset( $_FILES['file'] ) && $_FILES['file']['error']==0 )
    {
        $post_id     = $_POST['post_id'];
        $app_name    = $_POST['app_name'];
        $file_name   = $_FILES['file']['name'];
        $file_size   = $_FILES['file']['size'];
        $file_type   = $_FILES['file']['type'];
        $file_source = $_FILES['file']['tmp_name'];
        $is_batch    = isset( $_POST['batch'] ) ? $_POST['batch'] : false;

        if( is_allow_file_size( $file_size ) )
        {
            if( is_allow_file_type( $file_type, 'image' ) )
            {
                //-- CHECK FOLDER EXIST
                $folder = upload_folder_name();

                if( !is_dir( FILES_PATH . '/' . $folder ) )
                {
                    create_dir( FILES_PATH . '/' . $folder );
                }

                $ext    = file_name_filter( $file_name, true );
                $name   = file_name_filter( $file_name, false ) . '-' . time();
                $name_t = $name . '-thumbnail' . $ext;
                $name_m = $name . '-medium' . $ext;
                $name_l = $name . '-large' . $ext;
                $name_f = $name . $ext;

                $ldestination = FILES_PATH . '/' . $folder . '/' . $name_l;
                $mdestination = FILES_PATH . '/' . $folder . '/' . $name_m;
                $tdestination = FILES_PATH . '/' . $folder . '/' . $name_t;
                $fdestination = FILES_PATH . '/' . $folder . '/' . $name_f;

                if( $file_type == 'image/svg+xml' )
                {
                    if( upload( $file_source, $fdestination ) )
                    {
                        $loc        = '/lumonata-content/files/' . $folder . '/' . $name_f;
                        $loc_thumb  = '/lumonata-content/files/' . $folder . '/' . $name_f;
                        $loc_medium = '/lumonata-content/files/' . $folder . '/' . $name_f;
                        $loc_large  = '/lumonata-content/files/' . $folder . '/' . $name_f;

                        $d = get_attachment_by_app_name( $post_id, $app_name, false );

                        if( empty( $d ) || $is_batch )
                        {
                            if( insert_attachment( $post_id, 0, $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, $app_name, false ) )
                            {
                                $attach_id = $db->insert_id();

                                if( empty( $attach_id ) )
                                {
                                    //-- Delete Original Image
                                    if( file_exists( $fdestination ) )
                                    {
                                        unlink( $fdestination );
                                    }

                                    return array( 'result' => 'failed', 'message' => 'Error uploading file' );
                                }
                                else
                                {
                                    $s = 'SELECT * FROM lumonata_attachment AS a  WHERE a.lattach_id = %d';
                                    $q = $db->prepare_query( $s, $attach_id );
                                    $r = $db->do_query( $q );

                                    if( is_array( $r ) )
                                    {
                                        return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                                    }
                                    else
                                    {
                                        $d = $db->fetch_array( $r );

                                        return array(
                                            'result' => 'success',
                                            'data'   => array(
                                                'filename'  => $name_m,
                                                'post_id'   => $post_id,
                                                'attach_id' => $attach_id,
                                                'title'     => $d['ltitle'],
                                                'content'   => $d['lcontent'],
                                                'caption'   => $d['lcaption'],
                                                'alt'       => $d['lalt_text'],
                                                'fileturl'  => site_url( $d[ 'lattach_loc_thumb' ] ),
                                                'fileurl'   => site_url( $d[ 'lattach_loc_medium' ] )
                                            )
                                        );
                                    }
                                }
                            }
                            else
                            {
                                //-- Delete Original Image
                                if( file_exists( $fdestination ) )
                                {
                                    unlink( $fdestination );
                                }

                                return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                            }
                        }
                        else
                        {
                            if( update_attachment( $d['lattach_id'], $post_id, 0, $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, $app_name ) )
                            {
                                //-- Delete Original Image
                                if( !empty( $d[ 'lattach_loc' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc' ] ) )
                                {
                                    unlink( ROOT_PATH . $d[ 'lattach_loc' ] );
                                }
                            }

                            $s = 'SELECT * FROM lumonata_attachment AS a  WHERE a.lattach_id = %d';
                            $q = $db->prepare_query( $s, $d['lattach_id'] );
                            $r = $db->do_query( $q );

                            if( is_array( $r ) )
                            {
                                return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                            }
                            else
                            {
                                $d = $db->fetch_array( $r );

                                return array(
                                    'result' => 'success',
                                    'data'   => array(
                                        'filename'  => $name_m,
                                        'post_id'   => $post_id,
                                        'title'     => $d['ltitle'],
                                        'content'   => $d['lcontent'],
                                        'caption'   => $d['lcaption'],
                                        'alt'       => $d['lalt_text'],
                                        'attach_id' => $d['lattach_id'],
                                        'fileturl'  => site_url( $d[ 'lattach_loc_thumb' ] ),
                                        'fileurl'   => site_url( $d[ 'lattach_loc_medium' ] )
                                    )
                                );
                            }
                        }
                    }
                    else
                    {
                        return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                    }
                }
                else
                {
                    if( upload_resize( $file_source, $ldestination, $file_type, 1920, 1920 ) )
                    {
                        if( upload_resize( $file_source, $mdestination, $file_type, 1024, 1024 ) )
                        {
                            if( upload_crop( $file_source, $tdestination, $file_type, 550, 550 ) )
                            {
                                if( upload( $file_source, $fdestination ) )
                                {
                                    $loc        = '/lumonata-content/files/' . $folder . '/' . $name_f;
                                    $loc_thumb  = '/lumonata-content/files/' . $folder . '/' . $name_t;
                                    $loc_medium = '/lumonata-content/files/' . $folder . '/' . $name_m;
                                    $loc_large  = '/lumonata-content/files/' . $folder . '/' . $name_l;

                                    $d = get_attachment_by_app_name( $post_id, $app_name, false );

                                    if( empty( $d ) || $is_batch )
                                    {
                                        if( insert_attachment( $post_id, 0, $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, $app_name, false ) )
                                        {
                                            $attach_id = $db->insert_id();

                                            if( empty( $attach_id ) )
                                            {
                                                //-- Delete Original Image
                                                if( file_exists( $fdestination ) )
                                                {
                                                    unlink( $fdestination );
                                                }

                                                //-- Delete Thumb Image
                                                if( file_exists( $tdestination ) )
                                                {
                                                    unlink( $tdestination );
                                                }

                                                //-- Delete Medium Image
                                                if( file_exists( $mdestination ) )
                                                {
                                                    unlink( $mdestination );
                                                }

                                                //-- Delete Large Image
                                                if( file_exists( $ldestination ) )
                                                {
                                                    unlink( $ldestination );
                                                }

                                                return array( 'result' => 'failed', 'message' => 'Error uploading file' );
                                            }
                                            else
                                            {
                                                $s = 'SELECT * FROM lumonata_attachment AS a  WHERE a.lattach_id = %d';
                                                $q = $db->prepare_query( $s, $attach_id );
                                                $r = $db->do_query( $q );

                                                if( is_array( $r ) )
                                                {
                                                    return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                                                }
                                                else
                                                {
                                                    $d = $db->fetch_array( $r );

                                                    return array(
                                                        'result' => 'success',
                                                        'data'   => array(
                                                            'filename'  => $name_m,
                                                            'post_id'   => $post_id,
                                                            'attach_id' => $attach_id,
                                                            'title'     => $d['ltitle'],
                                                            'content'   => $d['lcontent'],
                                                            'caption'   => $d['lcaption'],
                                                            'alt'       => $d['lalt_text'],
                                                            'fileturl'  => site_url( $d[ 'lattach_loc_thumb' ] ),
                                                            'fileurl'   => site_url( $d[ 'lattach_loc_medium' ] )
                                                        )
                                                    );
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //-- Delete Original Image
                                            if( file_exists( $fdestination ) )
                                            {
                                                unlink( $fdestination );
                                            }

                                            //-- Delete Thumb Image
                                            if( file_exists( $tdestination ) )
                                            {
                                                unlink( $tdestination );
                                            }

                                            //-- Delete Medium Image
                                            if( file_exists( $mdestination ) )
                                            {
                                                unlink( $mdestination );
                                            }

                                            //-- Delete Large Image
                                            if( file_exists( $ldestination ) )
                                            {
                                                unlink( $ldestination );
                                            }

                                            return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                                        }
                                    }
                                    else
                                    {
                                        if( update_attachment( $d['lattach_id'], $post_id, 0, $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, $app_name ) )
                                        {
                                            //-- Delete Original Image
                                            if( !empty( $d[ 'lattach_loc' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc' ] ) )
                                            {
                                                unlink( ROOT_PATH . $d[ 'lattach_loc' ] );
                                            }

                                            //-- Delete Thumb Image
                                            if( !empty( $d[ 'lattach_loc_thumb' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc_thumb' ] ) )
                                            {
                                                unlink( ROOT_PATH . $d[ 'lattach_loc_thumb' ] );
                                            }

                                            //-- Delete Medium Image
                                            if( !empty( $d[ 'lattach_loc_medium' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc_medium' ] ) )
                                            {
                                                unlink( ROOT_PATH . $d[ 'lattach_loc_medium' ] );
                                            }

                                            //-- Delete Large Image
                                            if( !empty( $d[ 'lattach_loc_large' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc_large' ] ) )
                                            {
                                                unlink( ROOT_PATH . $d[ 'lattach_loc_large' ] );
                                            }
                                        }

                                        $s = 'SELECT * FROM lumonata_attachment AS a  WHERE a.lattach_id = %d';
                                        $q = $db->prepare_query( $s, $d['lattach_id'] );
                                        $r = $db->do_query( $q );

                                        if( is_array( $r ) )
                                        {
                                            return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                                        }
                                        else
                                        {
                                            $d = $db->fetch_array( $r );

                                            return array(
                                                'result' => 'success',
                                                'data'   => array(
                                                    'filename'  => $name_m,
                                                    'post_id'   => $post_id,
                                                    'title'     => $d['ltitle'],
                                                    'content'   => $d['lcontent'],
                                                    'caption'   => $d['lcaption'],
                                                    'alt'       => $d['lalt_text'],
                                                    'attach_id' => $d['lattach_id'],
                                                    'fileturl'  => site_url( $d[ 'lattach_loc_thumb' ] ),
                                                    'fileurl'   => site_url( $d[ 'lattach_loc_medium' ] )
                                                )
                                            );
                                        }
                                    }
                                }
                                else
                                {
                                    //-- Delete Thumb Image
                                    if( file_exists( $tdestination ) )
                                    {
                                        unlink( $tdestination );
                                    }

                                    //-- Delete Medium Image
                                    if( file_exists( $mdestination ) )
                                    {
                                        unlink( $mdestination );
                                    }

                                    //-- Delete Large Image
                                    if( file_exists( $ldestination ) )
                                    {
                                        unlink( $ldestination );
                                    }

                                    return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                                }
                            }
                            else
                            {
                                //-- Delete Medium Image
                                if( file_exists( $mdestination ) )
                                {
                                    unlink( $mdestination );
                                }

                                //-- Delete Large Image
                                if( file_exists( $ldestination ) )
                                {
                                    unlink( $ldestination );
                                }

                                return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                            }
                        }
                        else
                        {
                            //-- Delete Large Image
                            if( file_exists( $ldestination ) )
                            {
                                unlink( $ldestination );
                            }

                            return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                        }
                    }
                    else
                    {
                        return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                    }
                }
            }
            else
            {
                return array( 'result' => 'failed', 'error' => 'File is not an image' );
            }
        }
        else
        {
            return array( 'result' => 'failed', 'error' => 'Maximum file size is 2MB' );
        }
    }
    else
    {
        return array( 'result' => 'failed', 'error' => 'No image was found' );
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Delete Post Image
| -----------------------------------------------------------------------------
*/
function delete_post_image()
{
    if( isset( $_POST['attach_id'] ) )
    {
        if( delete_attachment( $_POST['attach_id'] ) )
        {
            return array( 'result' => 'success' );
        }
        else
        {
            return array( 'result' => 'failed' );
        }
    }
    else
    {
        if( empty( $_POST['post_id'] ) )
        {
            return array( 'result' => 'failed' );
        }
        else
        {
            $d = get_attachment_by_app_name( $_POST['post_id'], $_POST['app_name'], false );

            if( delete_attachment( $d['lattach_id'] ) )
            {
                return array( 'result' => 'success' );
            }
            else
            {
                return array( 'result' => 'failed' );
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Reorder Post Image
| -----------------------------------------------------------------------------
*/
function reorder_post_image()
{
    global $db;

    if( isset( $_POST['value'] ) && !empty( $_POST['value'] ) )
    {
        $value = json_decode( $_POST['value'], true );

        foreach( $value as $i => $val )
        {
            $s = 'UPDATE lumonata_attachment AS a SET a.lorder = %d WHERE a.lattach_id = %d AND a.larticle_id = %d';
            $q = $db->prepare_query( $s, ( $i + 1 ), $val, $_POST['id'] );
            $r = $db->do_query( $q );
        }

        return array( 'result' => 'success' );
    }
    else
    {
        return array( 'result' => 'failed' );
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Edit Info Post Image
| -----------------------------------------------------------------------------
*/
function edit_post_image_info()
{
    if( edit_attachment( $_POST['attach_id'], $_POST['title'], null, $_POST['alt'], $_POST['caption'], $_POST['content'] ) )
    {
        return array( 'result' => 'success' );
    }
    else
    {
        return array( 'result' => 'failed' );
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Article Post - Ajax Function
| -----------------------------------------------------------------------------
*/
function article_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-article-data' )
    {
        $data = get_article_list_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'delete-articles' )
    {
        if( is_delete( $_POST['state'] ) )
        {
            run_actions( $_POST['state'] . '_additional_delete' );
            
            if( delete_article( $_POST['id'], $_POST['state'] ) )
            {
                $result = array( 'result' => 'success' );
            }
            else
            {
                $result = array( 'result' => 'failed' );
            }
        }
        else
        {
            $result = array( 'result' => 'failed' );
        }

        echo json_encode( $result );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'reorder-data' )
    {
        $result = array( 'result' => 'failed' );

        if( !empty( $_POST['reorder'] ) )
        {
            foreach( $_POST['reorder'] as $id => $order )
            {
                if( update_article_order( $id, $order, $_POST['state'] ) )
                {
                    $result = array( 'result' => 'success' );
                }
            }
        }

        echo json_encode( $result );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'add-new-category' )
    {
        extract( $_POST );

        $rule_id = insert_rules( $parent, $name, $description, $rule, $group, false );

        if( empty( $selected ) )
        {
            $merge_selected = array( $rule_id );
        }
        else
        {
            $merge_selected = array_merge( json_decode( rem_slashes( $selected ) ), array( $rule_id ) );
        }

        $all_category = article_all_categories( $index, $rule, $group, $merge_selected );
        $sel_option   = article_category_list_option( $index, $rule, $group );

        echo json_encode( array( 'data' => $all_category, 'options' => $sel_option ) );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'get-sef-scheme' )
    {
        echo get_sef_scheme_content();
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'update-sef-scheme' )
    {
        if( update_sef( $_POST['post_id'], $_POST['title'], $_POST['new_sef'], $_POST['type'] ) )
        {
            $result = array( 'result' => 'success' );
        }
        else
        {
            $result = array( 'result' => 'failed' );
        }

        echo json_encode( $result );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'upload-files' )
    {
        $data = upload_post_file();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-uploaded-files' )
    {
        $data = delete_post_file();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'upload-images' )
    {
        $data = upload_post_image();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-uploaded-images' )
    {
        $data = delete_post_image();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'get-images' )
    {
        $data = get_post_image();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'edit-image-info' )
    {
        $data = edit_post_image_info();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'reorder-images' )
    {
        $data = reorder_post_image();

        echo json_encode( $data );
    }

    exit;
}

?>