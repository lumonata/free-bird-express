<?php
            
add_actions( 'rule_admin_page', 'rule_ajax' );

/*
| -----------------------------------------------------------------------------
| Admin Rule
| -----------------------------------------------------------------------------
*/
function get_admin_rule( $rule, $group, $thetitle, $tabs, $subsite = 'arunna' )
{
    $tabs     = is_contributor() || is_author() ? $tabs[ 0 ] : $tabs;
    $tab_keys = array_keys( $tabs );
    $the_tab  = empty( $_GET[ 'tab' ] ) ?  $tab_keys[ 0 ] : $_GET[ 'tab' ];
    $tabs     = set_tabs( $tabs, $the_tab );
    $urlquery = $rule == 'tags' ? 'rule=' . $rule : 'rule=' . $rule . '&group=' . $group;

    add_variable( 'tab', $tabs );

    run_rule_actions( $rule, $group, $subsite );

    if( count_rules( $urlquery ) == 0 && !isset( $_GET[ 'prc' ] ) )
    {
        if( $rule == 'tags' )
        {
            if( isset( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'applications' )
            {
                header( 'location:' . get_application_url( $group . '&tab=tags&prc=add_new' ) );
            }
            else
            {
                header( 'location:' . get_state_url( $group . '&tab=tags&prc=add_new' ) );
            }
        }
        elseif( $rule == 'categories' )
        {
            if( count_rules( 'rule=category&group=default' ) == 0 )
            {
                insert_rules( 0, 'Uncategorized', '', 'category', 'default', true );
            }
        }
    }

    if( is_add_new() )
    {
        return add_new_rule( $rule, $group, $thetitle );
    }
    elseif( is_edit() )
    {
        return edit_rule( $rule, $group, $thetitle, $_GET[ 'id' ] );
    }
    elseif( is_delete_all() )
    {
        return delete_batch_rule( $rule, $group, $thetitle, $tabs );
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST[ 'id' ] as $key => $val )
        {
            delete_rule( $val, $group );
        }
    }

    return get_rule_list( $rule, $group, $thetitle, $tabs );
}

/*
| -----------------------------------------------------------------------------
| Admin Rule Actions
| -----------------------------------------------------------------------------
*/
function run_rule_actions( $rule, $group, $subsite = 'arunna' )
{
    if( is_publish() )
    {
        run_actions( $rule . '_save' );

        if( is_add_new() )
        {
            $parent  = $rule == 'tags' ? 0 : $_POST[ 'parent' ][ 0 ];

            $rule_id = insert_rules( $parent, $_POST[ 'name' ][ 0 ], rem_slashes( $_POST[ 'description' ][ 0 ] ), $rule, $group, false, $subsite );

            rule_attachment_sync( $_POST[ 'rule_id' ][ 0 ], $rule_id );

            rule_additional_field_sync( $_POST[ 'rule_id' ][ 0 ], $rule_id );

            if( isset( $_POST[ 'additional_fields' ] ) )
            {
                foreach( $_POST[ 'additional_fields' ] as $key => $val )
                {
                    foreach( $val as $subkey => $subval )
                    {
                        add_additional_rule_field( $rule_id, $key, $subval, $group );
                    }
                }
            }

            if( is_admin_application() )
            {
                header( 'location:' . get_application_url( $group ) . '&tab=' . $rule . '&prc=add_new' );
            }
            else
            {
                header( 'location:' . get_state_url( $group ) . '&tab=' . $rule . '&prc=add_new' );
            }
        }
        elseif( is_edit() )
        {
            run_actions( $rule . '_edit' );

            if( $rule != 'categories' )
            {
                $_POST[ 'parent' ][ 0 ] = 0;
            }

            if( update_rules( $_POST[ 'rule_id' ][ 0 ], $_POST[ 'parent' ][ 0 ], $_POST[ 'name' ][ 0 ], $_POST[ 'description' ][ 0 ], $rule, $group, $subsite ) )
            {
                if( isset( $_POST[ 'additional_fields' ] ) )
                {
                    foreach( $_POST[ 'additional_fields' ] as $key => $val )
                    {
                        foreach( $val as $subkey => $subval )
                        {
                            edit_additional_rule_field( $_POST[ 'rule_id' ][ 0 ], $key, $subval, $group );
                        }
                    }
                }
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Rule - List
| -----------------------------------------------------------------------------
*/
function get_rule_list( $rule, $group, $title, $tabs )
{
    $url    = is_admin_application() ? get_application_url( $group . '&tab=' . $rule ) : get_state_url( $group . '&tab=' . $rule );
    $button = get_rule_admin_button( $url );
    $title  = explode( '|', $title );

    set_template( TEMPLATE_PATH . '/template/rule-list.html', 'rule-template' );

    add_block( 'list-block', 'l-block', 'rule-template' );

    add_variable( 'tab', $tabs );
    add_variable( 'rule', $rule );
    add_variable( 'group', $group );
    add_variable( 'button', $button );
    add_variable( 'title', $title[1] );
    add_variable( 'limit', post_viewed() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_rule_ajax_url() );
    add_variable( 'tab-css', empty( $tabs ) ? 'hidden' : '' );
    add_variable( 'tab-border', empty( $tabs ) ? 'style="border-top:1px solid #bbbbbb;"' : '' );    
    
    add_actions( 'section_title', $title[1] . ' - ' . ucfirst( $rule ) );

    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );

    parse_template( 'list-block', 'l-block', 'rule-template' );

    return return_template( 'rule-template' );
}

/*
| -----------------------------------------------------------------------------
| Admin Rule - Add New
| -----------------------------------------------------------------------------
*/
function add_new_rule( $rule, $group, $title )
{
    $url    = is_admin_application() ? get_application_url( $group . '&tab=' . $rule ) : get_state_url( $group . '&tab=' . $rule );
    $button = get_rule_admin_button( $url, true );

    $thetitle = explode( '|', $title );
    $thetitle = $thetitle[ 0 ];
    $rule_id  = time();

    set_template( TEMPLATE_PATH . '/template/rule-form.html', 'rule-template' );

    add_block( 'form-language-btn-block', 'flb-block', 'rule-template' );
    add_block( 'form-language-loop-block', 'fll-block', 'rule-template' );
    add_block( 'form-block', 'f-block', 'rule-template' );

    add_variable( 'group', $group );
    add_variable( 'rule_id', $rule_id );
    add_variable( 'site_url', site_url() );
    add_variable( 'imgs_url', get_theme_img() );
    add_variable( 'ajax_url', get_rule_ajax_url() );
    add_variable( 'rule_parent', set_rule_parent( 0, $rule, $group ) );
    add_variable( 'rule_description', rule_textarea( 'description[0]', 0, '', '' ) );
    
    add_variable( 'button', $button );
    add_variable( 'app_title', 'Add New ' . $thetitle . ' ' . ucwords( $rule ) );
    add_variable( 'application_additional_data', attemp_actions( $group . '_rule_additional_field' ) );
    
    rule_language_field( $rule_id, $group, $thetitle );

    add_actions( 'section_title', $thetitle . ' - Add New ' . ucwords( $rule ) );

    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/tinymce@4.9.11/tinymce.min.js' );
    add_actions( 'js_elements', 'get_javascript', 'tinymce' );
    
    parse_template( 'form-block', 'f-block', false );

    return return_template( 'rule-template' );
}

/*
| -----------------------------------------------------------------------------
| Admin Rule - Add Edit
| -----------------------------------------------------------------------------
*/
function edit_rule( $rule, $group, $title, $rule_id = 0 )
{
    $url    = is_admin_application() ? get_application_url( $group . '&tab=' . $rule ) : get_state_url( $group . '&tab=' . $rule );
    $data   = count_rules( 'rule_id=' . $rule_id, false );
    $button = get_rule_admin_button( $url, true );

    $thetitle = explode( '|', $title );

    if( isset( $_POST[ 'select' ] ) && count( $_POST[ 'select' ] ) > 1 )
    {
        if( count( $thetitle ) == 2 )
        {
            $thetitle = $thetitle[ 1 ];
        }
        else
        {
            $thetitle = $thetitle[ 0 ];
        }
    }
    else
    {
        $thetitle = $thetitle[ 0 ];
    }

    set_template( TEMPLATE_PATH . '/template/rule-form.html', 'rule-template' );

    add_block( 'form-language-btn-block', 'flb-block', 'rule-template' );
    add_block( 'form-language-loop-block', 'fll-block', 'rule-template' );
    add_block( 'form-block', 'f-block', 'rule-template' );

    add_variable( 'group', $group );
    add_variable( 'rule_id', $rule_id );
    add_variable( 'site_url', site_url() );
    add_variable( 'imgs_url', get_theme_img() );
    add_variable( 'rule_name', $data[ 'lname' ] );
    add_variable( 'ajax_url', get_rule_ajax_url() );
    add_variable( 'rule_parent', set_rule_parent( 0, $rule, $group, array( $data[ 'lparent' ]  ) ) );
    add_variable( 'rule_description', rule_textarea( 'description[0]', 0, rem_slashes( $data[ 'ldescription' ] ), $rule_id, true ) );

    add_variable( 'button', $button );
    add_variable( 'app_title', 'Edit ' . $thetitle . ' ' . ucwords( $rule ) );
    add_variable( 'application_additional_data', attemp_actions( $group . '_rule_additional_field' ) );
    
    rule_language_field( $rule_id, $group, $thetitle );
    
    add_actions( 'section_title', 'Edit - ' . $thetitle . ' ' . ucwords( $rule ) );

    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/tinymce@4.9.11/tinymce.min.js' );
    add_actions( 'js_elements', 'get_javascript', 'tinymce' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'rule-template' );
}

/*
| -----------------------------------------------------------------------------
| Admin Rule - Language Field
| -----------------------------------------------------------------------------
*/
function rule_language_field( $rule_id = 0, $type, $title, $index = 0 )
{    
    //-- Set Language Form
    $use_multi_lang = get_meta_data( 'multi_language' );
    $lang_list_data = get_all_language();
    
    if( $use_multi_lang == 1 && empty( $lang_list_data ) === FALSE )
    {
        $current_lang = '';

        foreach( $lang_list_data as $d )
        {
            extract( $d );

            $lname        = get_rule_additional_field( $rule_id, 'lname_' . $llanguage_code, $type );
            $ldescription = get_rule_additional_field( $rule_id, 'ldescription_' . $llanguage_code, $type );

            add_variable( 'llanguage', $llanguage );
            add_variable( 'llanguage_code', $llanguage_code );
            add_variable( 'llanguage_btn_cls', $ldefault == 1 ? 'active' : '' );
            add_variable( 'llanguage_content_css', 'content-lang ' . $llanguage_code );
            add_variable( 'llanguage_application_additional_data', attemp_actions( $type . '_llanguage_rule_additional_field' ) );

            add_variable( 'llanguage_lname', $lname );
            add_variable( 'llanguage_textarea', textarea( 'additional_fields[ldescription_' . $llanguage_code . '][]', 0, $ldescription, $rule_id, true, false ) );

            add_variable( 'template_url', TEMPLATE_URL );
                
            parse_template( 'form-language-btn-block', 'flb-block', true );

            if( $ldefault == 1 )
            {
                $current_lang = $llanguage_code;

                continue;
            }

            parse_template( 'form-language-loop-block', 'fll-block', true );
        }

        add_variable( 'conten_css', 'content-lang ' . $current_lang );
    }
    else
    {
        add_variable( 'btn_cls', 'hidden' );
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Rule - Batch Delete
| -----------------------------------------------------------------------------
*/
function delete_batch_rule( $rule, $group, $title, $tabs )
{
    $title    = explode( '|', $title );
    $backlink = is_admin_application() ? get_application_url( $group. '&tab=' . $rule ) : get_state_url( $group. '&tab=' . $rule );

    set_template( TEMPLATE_PATH . '/template/rule-batch-delete.html', 'rule-batch-delete' );

    add_block( 'delete-loop-block', 'dl-block', 'rule-batch-delete' );
    add_block( 'delete-block', 'd-block', 'rule-batch-delete' );

    foreach( $_POST[ 'select' ] as $key => $val )
    {
        $d = count_rules( 'rule_id=' . $val, false );

        add_variable( 'rule_id', $d[ 'lrule_id' ] );
        add_variable( 'rule_name', $d[ 'lname' ] );

        parse_template( 'delete-loop-block', 'dl-block', true );
    }

    add_variable( 'tab', $tabs );
    add_variable( 'type', $rule );
    add_variable( 'title', $title[ 1 ] );
    add_variable( 'backlink', $backlink );
    add_variable( 'tab-css', empty( $tabs ) ? 'hidden' : '' );
    add_variable( 'tab-border', empty( $tabs ) ? 'style="border-top:1px solid #bbbbbb;"' : '' );    
    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST[ 'select' ] ) == 1 ? 'this ' . $title[ 0 ] : 'these ' . $title[ 1 ] ) );
    
    add_actions( 'section_title', $title[ 1 ] );

    parse_template( 'delete-block', 'd-block', 'rule-batch-delete' );

    return return_template( 'rule-batch-delete' );
}

/*
| -----------------------------------------------------------------------------
| Admin Rule - Table Query
| -----------------------------------------------------------------------------
*/
function get_rule_list_query()
{    
    global $db;

    extract( $_POST );

    $rdata = $_REQUEST;
    $cols  = array( 
        2 => 'a.lparent',
        3 => 'a.lname',
        4 => 'a.lcount'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            if( isset( $cols[ $rdata['order'][$i]['column'] ] ) )
            {
                $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
            }
        }

        $order = !empty( $o ) ? 'ORDER BY ' . implode( ', ', $o ) : '';
    }
    else
    {
        $order = 'ORDER BY a.lorder ASC';
    }

    if( empty( $rdata['search']['value']) )
    {
        $q = 'SELECT * FROM lumonata_rules AS a WHERE a.lrule = "' . $rule . '" AND a.lgroup = "' . $group . '" ' . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

        $q = 'SELECT * FROM lumonata_rules AS a WHERE a.lrule = "' . $rule . '" AND a.lgroup = "' . $group . '" ' . ' AND ( ' . implode( ' OR ', $search ) . ' ) ' . $order;
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $url = is_admin_application() ? get_application_url( $group . '&tab=' . $rule ) : get_state_url( $group . '&tab=' . $rule );

        while( $d2 = $db->fetch_array( $r2 ) )
        {   
            $sef       = $d2[ 'lsef' ];
            $order     = $d2[ 'lorder' ];
            $count     = $d2[ 'lcount' ];
            $id        = $d2[ 'lrule_id' ];
            $parent    = get_rule_structure( $d2[ 'lrule_id' ], $d2[ 'lrule_id' ] );
            $desc      = empty( $d2[ 'ldescription' ] ) ? '-' : $d2[ 'ldescription' ];
            $name      = isset( $d2[ 'lname_lang' ] ) ? $d2[ 'lname_lang' ] : $d2[ 'lname' ];

            $data[] = array(
                'id'        => $id,
                'sef'       => $sef,
                'desc'      => $desc,
                'name'      => $name,
                'order'     => $order,
                'count'     => $count,
                'parent'    => $parent,
                'ajax_link' => get_rule_ajax_url(),
                'edit_link' => $url . '&prc=edit&id=' . $id
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

/*
| -----------------------------------------------------------------------------
| Admin Rule - Attachment Sync
| -----------------------------------------------------------------------------
*/
function rule_attachment_sync( $old_id, $new_id )
{
    global $db;

    $s = 'UPDATE lumonata_attachment SET lrule_id = %d WHERE lrule_id = %d';
    $q = $db->prepare_query( $s, $new_id, $old_id );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Rule - Field Sync
| -----------------------------------------------------------------------------
*/
function rule_additional_field_sync( $old_id, $new_id )
{
    global $db;

    $s = 'UPDATE lumonata_additional_rule_fields SET lrule_id = %d WHERE lrule_id = %d';
    $q = $db->prepare_query( $s, $new_id, $old_id );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Rule - Action Button
| -----------------------------------------------------------------------------
*/
function get_rule_admin_button( $new_url = '', $is_form = false )
{
    if( $is_form )
    {
        if( !is_contributor() && !is_author() )
        {
            return '
            <li>' . button( 'button=publish&label=Save' ) . '</li>
            <li>' . button( 'button=cancel', $new_url ) . '</li>
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>';
        }
    }
    else
    {
        return '
        <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
        <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Rule - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_rule_ajax_url()
{
    return get_state_url( 'ajax&apps=rule' );
}

/*
| -----------------------------------------------------------------------------
| Admin Rule - Rule Parent
| -----------------------------------------------------------------------------
*/
function set_rule_parent( $index = 0, $rule, $group, $parent = array() )
{
    if( $rule == 'categories' )
    {
        return recursive_taxonomy( $index, $rule, $group, 'select', $parent );
    }
}

/**
* To show the structure of each rule if they has Child.    
* 
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $start_id Is used to identify the root ID. This ID is not change in the recursive and must be the same ID with $rule_id for the first call  
* @param integer $rule_id The ID of the rule that you want get the structure
* @param integer $level The recursive level. 
* 
* 
* @return string Example output: Root >> Category1 >> Category2
*      
*/
function get_rule_structure( $start_id, $rule_id, $level = 0 )
{
    global $db;

    $s = 'SELECT * FROM lumonata_rules WHERE lrule_id=%d';
    $q = $db->prepare_query( $s, $rule_id );
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    if( $n > 0 )
    {
        $level += 1;
        $dashed = '&raquo;';
    }
    else
    {
        $level  = $level;
        $dashed = '';
    }

    $items = '';

    while( $d = $db->fetch_array( $r ) )
    {
        $next_level = get_rule_structure( $start_id, $d[ 'lparent' ], $level );

        if( $start_id == $rule_id )
        {
            $items .= 'Root ' . $dashed . ' ';
        }

        $items .= $next_level;

        if( $start_id != $rule_id )
        {
            $items .= '<span class="field-text" data-field-id="' . $d[ 'lrule_id' ] . '" data-field-name="lname">' . $d[ 'lname' ] . '</span> ' . $dashed . ' ';
        }
    }

    return $items;
}

/**
* Get all rule from Root to the last child using this recursive function. 
* This function is also detected if there any rules are selected.
* You also can choose the order type by Descending or Ascending.    
* 
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $index Index
* @param integer $rule_name The Rule Name. By default it could be categories or tags  
* @param integer $group The application name
* @param string $type Output type: select, checkbox, li alse only with link 
* @param array $rule_id Selected rule_id if any
* @param string $order Ordering type: ASC, DESC
* @param integer $parent The rule parent ID
* @param integer $level recursive level
* @param boolean $related_to_article FALSE=all category will shown, TRUE= Only Category that has correlation with article will shown
* 
* @return string Example output: Root >> Category1 >> Category2
*      
*/
function recursive_taxonomy( $index, $rule_name, $group, $type = 'select', $rule_id = array(), $order = 'ASC', $parent = 0, $level = 0, $related_to_article = false, $include_default = false )
{
    global $db;

    if( $related_to_article )
    {
        $s = 'SELECT a.* FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
              WHERE a.lrule = %s AND a.lgroup = %s
              AND a.lparent = %d AND a.lrule_id = b.lrule_id 
              AND b.lapp_id = c.larticle_id AND c.lshare_to = 0 
              GROUP BY a.lrule_id ORDER BY a.lorder ' . $order;
    }
    else
    {
        $s = 'SELECT * FROM lumonata_rules AS a
              WHERE a.lrule = %s AND a.lgroup = %s
              AND a.lparent = %d ORDER BY a.lorder ' . $order;
    }

    $q = $db->prepare_query( $s, $rule_name, $group, $parent );
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    $items    = '';
    $end_item = '';
    $sts      = '';

    if( $type == 'li' && $n > 0 )
    {
        $items    = '<ul>';
        $end_item = '</ul>';
    }
    elseif( $type == 'checkbox' )
    {
        $items    = '<ul class="' . ( $level == 0 ? 'the_categories' : '' ) . '">';
        $end_item = '</ul>';
    }

    if( $n > 0 )
    {
        $level += 1;
    }
    else
    {
        $level = $level;
    }

    $dashed = '';

    for( $i = 0; $i < $level; $i++ )
    {
        $x = $level - 1;

        if( $x == $i )
        {
            $dashed .= '';
        }
        else
        {
            $dashed .= '&nbsp;&nbsp;&nbsp;';
        }
    }

    while( $d = $db->fetch_array( $r ) )
    {
        $next_level = recursive_taxonomy( $index, $rule_name, $group, $type, $rule_id, $order, $d[ 'lrule_id' ], $level, $related_to_article );
        $lname      = $d[ 'lname' ];

        if( $type == 'select' )
        {
            $sts    = in_array( $d[ 'lrule_id' ], $rule_id ) ? 'selected="selected"' : '';
            $items .= '<option data-level="' . $level . '" value="' . $d[ 'lrule_id' ] . '" ' . $sts . '>' . $dashed . $lname . '</option>';
            $items .= $next_level;
        }
        elseif( $type == 'checkbox' )
        {
            if( is_array( $rule_id ) )
            {
                $sts = in_array( $d[ 'lrule_id' ], $rule_id ) ? 'checked="checked"' : '';
            }

            $items .= '
            <li>
                <input type="checkbox" name="category[' . $index . '][]" value="' . $d[ 'lrule_id' ] . '" id="the_category_' . $index . '_' . $d[ 'lrule_id' ] . '" ' . $sts . ' autocomplete="off"/>
                <label for="the_category_' . $index . '_' . $d[ 'lrule_id' ] . '" class="forclone" rel="checkbox-rule" data-rule="' . $rule_name . '">' . $lname . '</label>
                <script>
                    jQuery("#the_category_' . $index . '_' . $d[ 'lrule_id' ] . '").click(function(){
                        var selected_val   = jQuery(this).val();
                        var checked_status = this.checked;

                        jQuery("#the_most_category_' . $index . '_' . $d[ 'lrule_id' ] . '").each(function(){
                            if( jQuery(this).val() == selected_val )
                            {
                                this.checked = checked_status;
                            }
                        });
                    });
                </script>
            </li>';

            $items .= $next_level;
        }
        elseif( $type == 'li' )
        {
            if( $rule_name == 'categories' )
            {
                if( is_permalink() )
                {
                    $the_link = site_url( $group . '/' . $d[ 'lsef' ] );
                }
                else
                {
                    $the_link = site_url( '?app_name=' . $group . '&cat_id=' . $d[ 'lrule_id' ] );
                }
            }
            elseif( $rule_name == 'tags' )
            {
                if( is_permalink() )
                {
                    $the_link = site_url( 'tag/' . $d[ 'lsef' ] );
                }
                else
                {
                    $the_link = site_url( '?tag=' . $d[ 'lsef' ] );
                }
            }
            $items .= '<li><a href="' . $the_link . '">' . $lname . '</a>' . $next_level . '</li>';
        }
        else
        {
            if( is_array( $rule_id ) )
            {
                if( in_array( $d[ 'lrule_id' ], $rule_id ) )
                {
                    $sign = ', ';

                    if( $rule_name == 'categories' )
                    {
                        if( is_permalink() )
                        {
                            $the_category_link = site_url( $group . '/' . $d[ 'lsef' ] );
                        }
                        else
                        {
                            $the_category_link = site_url( '?app_name=' . $group . '&cat_id=' . $d[ 'lrule_id' ] );
                        }

                        $items .= '<a href="' . $the_category_link . '">' . $lname . '</a>' . $sign;
                    }
                    elseif( $rule_name == 'tags' )
                    {
                        if( is_permalink() )
                        {
                            $the_tag_link = site_url( 'tag/' . $d[ 'lsef' ] );
                        }
                        else
                        {
                            $the_tag_link = site_url( '?tag=' . $d[ 'lsef' ] );
                        }

                        $items .= '<a href="' . $the_tag_link . '">' . $lname . '</a>' . $sign;
                    }
                }
            }

            $items .= $next_level;
        }

        $items .= '';
    }

    if( $include_default )
    {
        $s = 'SELECT * FROM lumonata_rules AS a
              WHERE a.lrule = %s AND a.lgroup = %s
              AND a.lparent = %d';
        $q = $db->prepare_query( $s, 'category', 'default', $parent );
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        while( $d = $db->fetch_array( $r ) )
        {
            $lname = $d[ 'lname' ];

            if( $type == 'select' )
            {
                $sts    = in_array( $d[ 'lrule_id' ], $rule_id ) ? 'selected="selected"' : '';
                $items .= '<option data-level="' . $level . '" value="' . $d[ 'lrule_id' ] . '" ' . $sts . '>' . $dashed . $lname . '</option>';
            }
            elseif( $type == 'checkbox' )
            {
                if( is_array( $rule_id ) )
                {
                    $sts = in_array( $d[ 'lrule_id' ], $rule_id ) ? 'checked="checked"' : '';
                }

                $items .= '
                <li>
                    <input type="checkbox" name="category[' . $index . '][]" value="' . $d[ 'lrule_id' ] . '" id="the_category_' . $index . '_' . $d[ 'lrule_id' ] . '" ' . $sts . ' autocomplete="off"/>
                    <label for="the_category_' . $index . '_' . $d[ 'lrule_id' ] . '" class="forclone" rel="checkbox-rule" data-rule="' . $rule_name . '">' . $lname . '</label>
                    <script>
                        jQuery("#the_category_' . $index . '_' . $d[ 'lrule_id' ] . '").click(function(){
                            var selected_val   = jQuery(this).val();
                            var checked_status = this.checked;

                            jQuery("#the_most_category_' . $index . '_' . $d[ 'lrule_id' ] . '").each(function(){
                                if( jQuery(this).val() == selected_val )
                                {
                                    this.checked = checked_status;
                                }
                            });
                        });
                    </script>
                </li>';
            }
            elseif( $type == 'li' )
            {
                if( $rule_name == 'categories' )
                {
                    if( is_permalink() )
                    {
                        $the_link = site_url( $group . '/' . $d[ 'lsef' ] );
                    }
                    else
                    {
                        $the_link = site_url( '?app_name=' . $group . '&cat_id=' . $d[ 'lrule_id' ] );
                    }
                }
                elseif( $rule_name == 'tags' )
                {
                    if( is_permalink() )
                    {
                        $the_link = site_url( 'tag/' . $d[ 'lsef' ] );
                    }
                    else
                    {
                        $the_link = site_url( '?tag=' . $d[ 'lsef' ] );
                    }
                }

                $items .= '<li><a href="' . $the_link . '">' . $lname . '</a></li>';
            }
            else
            {
                if( is_array( $rule_id ) )
                {
                    if( in_array( $d[ 'lrule_id' ], $rule_id ) )
                    {
                        $sign = ', ';

                        if( $rule_name == 'categories' )
                        {
                            if( is_permalink() )
                            {
                                $the_category_link = site_url( $group . '/' . $d[ 'lsef' ] );
                            }
                            else
                            {
                                $the_category_link = site_url( '?app_name=' . $group . '&cat_id=' . $d[ 'lrule_id' ] );
                            }

                            $items .= '<a href="' . $the_category_link . '">' . $lname . '</a>' . $sign;
                        }
                        elseif( $rule_name == 'tags' )
                        {
                            if( is_permalink() )
                            {
                                $the_tag_link = site_url( 'tag/' . $d[ 'lsef' ] );
                            }
                            else
                            {
                                $the_tag_link = site_url( '?tag=' . $d[ 'lsef' ] );
                            }

                            $items .= '<a href="' . $the_tag_link . '">' . $lname . '</a>' . $sign;
                        }
                    }
                }
            }
        }
    }

    $items .= $end_item;

    return $items;
}

/**
* Get the 15 most used categories.
* 
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $group The application name
* @param integer $index Index 
* @param array $rule_id Selected rule_id if any
* 
* @return string 15 most used categories
*      
*/
function get_most_used_categories( $group, $index = 0, $rule_id = array() )
{
    global $db;
    $s = 'SELECT a.lname, a.lrule_id FROM lumonata_rules a
              WHERE a.lrule=%s AND (a.lgroup=%s OR a.lgroup=%s)
              ORDER BY a.lcount DESC LIMIT 15';
    $q = $db->prepare_query( $s, 'categories', $group, 'default' );
    $r = $db->do_query( $q );
    if( $db->num_rows( $r ) == 0 )
    {
        $notag = '<li>No category found, please add new category.</li>';
    }
    else
    {
        $notag = '';
    }
    $return = '
        <ul class="the_categories">';
    $return .= $notag;
    while( $d = $db->fetch_array( $r ) )
    {
        $sts = in_array( $d[ 'lrule_id' ], $rule_id ) ? 'checked="checked"' : '';
        $return .= '
                <li>
                    <input type="checkbox" name="most_used_category[' . $index . '][]" value="' . $d[ 'lrule_id' ] . '" id="the_most_category_' . $index . '_' . $d[ 'lrule_id' ] . '" ' . $sts . ' />
                    <label class="forclone" rel="checkbox-rule" data-rule="categories">' . $d[ 'lname' ] . '</label>
                    <script type="text/javascript">
                        $("#the_most_category_' . $index . '_' . $d[ 'lrule_id' ] . '").click(function(){
                            var selected_val   = $(this).val();
                            var checked_status = this.checked;
                            $("#the_category_' . $index . '_' . $d[ 'lrule_id' ] . '").each(function(){
                                if( $(this).val()==selected_val )
                                {
                                    this.checked = checked_status;
                                }
                            });
                        });
                    </script>
                </li>';
    }
    $return .= '
        </ul>';
    return $return;
}

/**
* Get the tag that been used for an article and show it in add new or edit article  
* 
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $post_id article ID 
* @param integer $index Index
* @param integer $group The application name
* 
* @return string tags in each post
*      
*/
function get_post_tags( $post_id, $index, $group )
{
    global $db;
    $sql       = $db->prepare_query( "SELECT a.lname, a.lrule_id

                            FROM lumonata_rules a, lumonata_rule_relationship b

                            WHERE a.lrule_id=b.lrule_id AND a.lrule=%s AND a.lgroup=%s AND b.lapp_id=%d", 'tags', $group, $post_id );
    $r         = $db->do_query( $sql );
    $count_row = $db->num_rows( $r );
    if( $count_row == 0 )
        $notag = "No tag found, please add new tag or choose from most used tags.";
    else
        $notag = "";
    $return = "<div class=\"the_categories\" id=\"tag_list_" . $index . "\">";
    $return .= $notag;
    $i = $count_row - 1;
    while( $d = $db->fetch_array( $r ) )
    {
        $return .= "<div class=\"tag_list tag_index_" . $index . " clearfix\" id=\"the_tag_list_" . $index . "_" . $i . "\">

                            <div class=\"tag_name\">" . $d[ 'lname' ] . "</div>

                            <div class=\"tag_action\"><a href=\"javascript:;\" onclick=\"$('#the_tag_list_" . $index . "_" . $i . "').remove();\">X</a></div>

                            <input type=\"hidden\" name=\"tags[$index][]\" value=\"" . $d[ 'lname' ] . "\" />

                     </div>";
        $i--;
    }
    $return .= "</div>";
    return $return;
}

/**
* 15 tags that used mostly  
* 
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $group The application name
* @param integer $index Index
* 
* @return string 15 tags that used mostly  
*      
*/
function get_most_used_tags( $group, $index = 0 )
{
    global $db;
    $s      = 'SELECT a.lname, a.lrule_id 
              FROM lumonata_rules a
              WHERE a.lrule=%s 
              AND a.lgroup NOT IN ("global_settings", "profile")
              ORDER BY a.lcount DESC
              LIMIT 15';
    $q      = $db->prepare_query( $s, 'tags' );
    $r      = $db->do_query( $q );
    $n      = $db->num_rows( $r );
    $notag  = $n == 0 ? 'No tag found, please add new tag.' : '';
    $return = '
        <div class="the_categories">';
    $return .= $notag;
    $i = $n - 1;
    while( $d = $db->fetch_array( $r ) )
    {
        $return .= "
                <div class=\"most_tag_list most_tag_" . $index . "_" . $i . " clearfix\">
                    <div class=\"tag_name\" data-id=\"" . $d[ 'lrule_id' ] . "\">" . $d[ 'lname' ] . "</div>
                    <div class=\"tag_action\"><a href=\"javascript:;\" id=\"append_tags_" . $index . "_" . $i . "\">+</a></div>
                </div>

                <script type=\"text/javascript\">
                    $(\"#append_tags_" . $index . "_" . $i . "\").click(function(){
                        var taglabel    = '" . $d[ 'lname' ] . "';
                        var count_child = $('.tag_index_" . $index . "').length;

                        thetag  = '<div class=\"tag_list tag_index_" . $index . " clearfix\" id=\"the_tag_list_" . $index . "_'+count_child+'\" >';
                        thetag += '     <div class=\"tag_name\">" . $d[ 'lname' ] . "</div>';
                        thetag += '     <div class=\"tag_action\">';
                        thetag += '         <a href=\"javascript:;\" id=\"remove_tag_'+count_child+'\" onclick=\"$(\'#the_tag_list_" . $index . "_'+count_child+'\').remove(); $(\'.most_tag_" . $index . "_" . $i . "\').animate({\'backgroundColor\':\'#FFFFFF\' },500);\">X</a>';
                        thetag += '     </div>';
                        thetag += '     <input type=\"hidden\" name=\"tags[" . $index . "][]\" value=\"" . $d[ 'lname' ] . "\" />';
                        thetag += '</div>';

                        if(count_child==0)
                        {
                            $(\"#tag_list_" . $index . "\").html('');
                            $(\"#tag_list_" . $index . "\").append(thetag);
                        }
                        else
                        {
                            $(\".tag_index_" . $index . ":first\").before(thetag);
                        }

                        $('.most_tag_" . $index . "_" . $i . "').animate({'backgroundColor':'#FF6666' },500);
                        $('.most_tag_" . $index . "_" . $i . "').animate({'backgroundColor':'#cccccc' },500);
                    });
                </script>";
        $i++;
    }
    $return .= '
        </div>';
    return $return;
}

/**
* Add new tag box when edit or add new article  
* 
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $post_id article ID
* @param integer $index Index
* 
* @return string Add new tag box when edit or add new article  
*      
*/
function add_new_tag( $post_id, $index )
{
    $return = '
        <div class="add_new_cattags clearfix\">
            <input type="text" class="new-tag-textbox-' . $index . ' category_textbox" name="new_tag[' . $index . ']" placeholder="New tags" />
            <em>Separate tags with commas.</em>
            <div class="add_cattags_button">
                <input type="button" name="add_tag[' . $index . ']" value="Add" class="new-tag-button-' . $index . ' button" />
            </div>
        </div>';
    return $return;
}

/**
* Insert new rule into lumonata_rules table  
* 
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $parent parent ID of the new rule, if the new rule are child. If the new rule are Root then the $parent ID must be 0
* @param string $name Rule name
* @param string $description Rule description
* @param string $rule The rule name. "categories" and "tags" are the rule name by default
* @param string $group Application name
* @param boolean $insert_default If the variable set TRUE means that you insert the default rule. The rule ID, will automatically set to 1
* 
* @return integer If the rule are 'tags' and then tags name are exist in database then the function will return the exist tag ID. 
*         But if it is other that 'tags' it will return the new inserted ID  
*
*/
function insert_rules( $parent, $name, $description, $rule, $group, $insert_default = false, $subsite = 'arunna' )
{
    global $db;
    $parent      = rem_slashes( $parent );
    $name        = rem_slashes( $name );
    $sef         = $name; // A AM [20130819]
    $sef         = generateSefUrl( $sef );
    $description = rem_slashes( $description );
    $rule        = rem_slashes( $rule );
    $group       = rem_slashes( $group );
    $subsite     = rem_slashes( $subsite );
    //count rule by sef and group
    $count_rule  = count_rules( "sef=" . $sef . "&group=" . $group );
    if( $rule == 'tags' )
    {
        //count rule by sef and rule
        $count_rule = count_rules( "sef=" . $sef . "&rule=" . $rule . "&group=" . $group );
        if( $count_rule > 0 )
        {
            $fetch_rule = count_rules( "sef=" . $sef . "&rule=" . $rule, false );
            return $fetch_rule[ 'lrule_id' ];
        }
    }
    else
    {
        if( $count_rule > 0 )
        {
            $count_rule++;
            $sef = $sef . "-" . $count_rule;
        }
    }
    if( $insert_default )
    {
        $sql = $db->prepare_query( "INSERT INTO lumonata_rules(lrule_id,

                                                            lparent,

                                                            lname,

                                                            lsef,

                                                            ldescription,

                                                            lrule,

                                                            lgroup,

                                                            lsubsite)

                                VALUES(%d,%d,%s,%s,%s,%s,%s,%s)", 1, $parent, $name, $sef, $description, $rule, $group, $subsite );
    }
    else
    {
        $sql = $db->prepare_query( "INSERT INTO lumonata_rules(lparent,

                                                                lname,

                                                                lsef,

                                                                ldescription,

                                                                lrule,

                                                                lgroup,

                                                                lsubsite)

                                    VALUES(%d,%s,%s,%s,%s,%s,%s)", $parent, $name, $sef, $description, $rule, $group, $subsite );
    }
    if( reset_order_id( "lumonata_rules" ) )
        if( $db->do_query( $sql ) )
            return $db->insert_id();
}

/**
* Edit the rules  
* 
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $rule_id The rule ID that will be edited
* @param string $parent The parent ID of edited rule
* @param string $description Rule description
* @param string $rule The rule name. "categories" and "tags" are the rule name by default
* @param string $group Application name
* 
* @return If the rule are 'tags' and then tags name are exist in database then the function will return the exist tag ID.
*         But if it is other that 'tags' it will return TRUE if the edit process success and false if fail  
*      
*/
function update_rules( $rule_id, $parent, $name, $description, $rule, $group, $subsite = 'arunna' )
{
    global $db;
    $parent      = rem_slashes( $parent );
    $name        = rem_slashes( $name );
    $sef         = $name;
    $sef         = generateSefUrl( $sef );
    $description = rem_slashes( $description );
    $rule        = rem_slashes( $rule );
    $group       = rem_slashes( $group );
    $subsite     = rem_slashes( $subsite );
    //count rule by sef and group
    $count_rule  = count_rules( "sef=" . $sef . "&group=" . $group . "&rule_id=" . $rule_id ); // A AM [20130819]
    if( $rule == 'tags' )
    {
        //count rule by sef and rule
        $count_rule = count_rules( "sef=" . $sef . "&rule=" . $rule . "&group=" . $group . "&rule_id=" . $rule_id ); // A AM [20130819]
        if( $count_rule > 0 )
        {
            $fetch_rule = count_rules( "sef=" . $sef . "&rule=" . $rule, false );
            return $fetch_rule[ 'lrule_id' ];
        }
    }
    else
    {
        if( $count_rule > 0 )
        {
            $count_rule++;
            $sef = $sef . "-" . $count_rule;
        }
    }
    /*$sql=$db->prepare_query("UPDATE lumonata_rules
    
    SET lparent=%d,
    
    lname=%s,
    
    lsef=%s,
    
    ldescription=%s,
    
    lrule=%s,
    
    lgroup=%s,
    
    lsubsite=%s
    
    WHERE lrule_id=%d",
    
    $parent,
    
    $name,
    
    $sef,
    
    $description,
    
    $rule,
    
    $group,
    
    $subsite,
    
    $rule_id
    
    );*/
    $sql = $db->prepare_query( "UPDATE lumonata_rules

                                 SET lparent=%d,

                                     lname=%s,

                                     ldescription=%s,

                                     lrule=%s,

                                     lgroup=%s,

                                     lsubsite=%s

                                WHERE lrule_id=%d", $parent, $name, $description, $rule, $group, $subsite, $rule_id );
    return $db->do_query( $sql );
}

/**
* Update or set the new number of  rule being used by articles or other applications  
* 
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $rule_id The rule ID that will be edited
* @param integer $rule_count The number of used
* 
* @return boolean  
*
*/
function update_rule_count( $rule_id, $rule_count )
{
    global $db;
    $sql = $db->prepare_query( "UPDATE lumonata_rules

                                SET lcount=%d

                                WHERE lrule_id=%d", $rule_count, $rule_id );
    return $db->do_query( $sql );
}

/**
* Delete the spesific rule by ID  
* 
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $id The rule ID that will be deleted
* 
* @return boolean  
*      
*/
function delete_rule( $id, $group )
{
    global $db;
    $d         = count_rules( "rule_id=" . $id, false );
    $theparent = $d[ 'lparent' ];
    $rule      = $d[ 'lrule' ];
    //Update child parent into the deleted data parent if any 
    $sql       = $db->prepare_query( "UPDATE lumonata_rules SET lparent=%d WHERE lparent=%d", $theparent, $id );
    if( $db->do_query( $sql ) )
    {
        //If the rule is categories, then update the post into default Uncategorized category
        if( $rule == "categories" )
        {
            //find which post that using deleted category
            $category_in_post = fetch_rule_relationship( 'rule_id=' . $id, 'lapp_id' );
            foreach( $category_in_post as $key => $val )
            {
                $sql = $db->prepare_query( "SELECT a.lrule_id

                                            FROM lumonata_rules a, lumonata_rule_relationship b

                                            WHERE a.lrule_id=b.lrule_id AND b.lapp_id=%d AND a.lrule='categories'", $val );
                $r   = $db->do_query( $sql );
                if( $db->num_rows( $r ) <= 1 )
                    update_rules_relationship( $id, $val, 1 );
                else
                    delete_rules_relationship( "rule_id=" . $id . "&app_id=" . $val );
            }
            $sql = $db->prepare_query( "DELETE FROM lumonata_rules WHERE lrule_id=%d", $id );
            if( $db->do_query( $sql ) )
            {
                delete_additional_rule_field( $id, $group );
                return true;
            }
        }
        elseif( $rule == "tags" )
        {
            //Delete the tag from the post that using deleted tags
            if( delete_rules_relationship( "rule_id=" . $id ) )
            {
                $sql = $db->prepare_query( "DELETE FROM lumonata_rules WHERE lrule_id=%d", $id );
                if( $db->do_query( $sql ) )
                {
                    delete_additional_rule_field( $id, $group );
                    return true;
                }
            }
        }
        else
        {
            if( delete_rules_relationship( "rule_id=" . $id ) )
            {
                $sql = $db->prepare_query( "DELETE FROM lumonata_rules WHERE lrule_id=%d", $id );
                if( $db->do_query( $sql ) )
                {
                    delete_additional_rule_field( $id, $group );
                    return true;
                }
            }
        }
    }
    return false;
}

/**
* Update rule relationship between article  
* 
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $rule_id The rule ID that will be edited
* @param integer $app_id application ID (article ID)
* @param integer $new_rule_id New rule ID that replace the existing ID
* 
* @return boolean  
*      
*/
function update_rules_relationship( $rule_id, $app_id, $new_rule_id )
{
    global $db;
    $sql = $db->prepare_query( "UPDATE lumonata_rule_relationship SET lrule_id=%d

                                 WHERE lrule_id=%d AND lapp_id=%d", $new_rule_id, $rule_id, $app_id );
    if( $db->do_query( $sql ) )
    {
        $rule_count = count_rules_relationship( "rule_id=$rule_id" );
        return update_rule_count( $rule_id, $rule_count );
    }
}

/**
* Insert rule relationship between article and category or tag  
* 
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $app_id application ID (article ID)
* @param integer $rule_id The rule ID that will be edited
* 
* @return boolean  
*
*/
function insert_rules_relationship( $app_id, $rule_id )
{
    global $db;
    if( count_rules_relationship( "app_id=$app_id&rule_id=$rule_id" ) == 0 )
    {
        $sql = $db->prepare_query( "INSERT INTO lumonata_rule_relationship(lapp_id,lrule_id)

                                      VALUES(%d,%d)", $app_id, $rule_id );
        if( $db->do_query( $sql ) )
        {
            $rule_count = count_rules_relationship( "rule_id=$rule_id" );
            return update_rule_count( $rule_id, $rule_count );
        }
    }
}

/**
* Count how many articles or applications are using the rule, There are two $args that you can use: app_id and rule_id
* 
* @example count_rules_relationship();
* @example count_rules_relationship("app_id=1"); //use this if you only want to count it per application ID
* @example count_rules_relationship("app_id=1&rule_id=3"); //use this if you only want to count it per application ID and rule ID
*
* @author Wahya Biantara
* 
* @since alpha
* 
* @param string $args Argument that can be specify by you. 
* 
* @return boolean  
*
*/
function count_rules_relationship( $args = '' )
{
    global $db;
    $var_name[ 'app_id' ]  = '';
    $var_name[ 'rule_id' ] = '';
    $where                 = "";
    if( !empty( $args ) )
    {
        $args  = explode( '&', $args );
        $where = " WHERE ";
        $i     = 1;
        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );
            if( $variable == 'app_id' || $variable == 'rule_id' )
            {
                $where .= "l" . $variable . "=" . $db->_real_escape( $value );
                if( $i != count( $args ) )
                {
                    $where .= " AND ";
                }
            }
            $i++;
        }
    }
    $sql = $db->prepare_query( "SELECT * FROM lumonata_rule_relationship $where " );
    return $db->num_rows( $db->do_query( $sql ) );
}

/**
* Delete the rule relationship, there are two $args that you can use: app_id and rule_id
* 
* @example delete_rules_relationship("app_id=1"); //use this if you only want to delete the relationship per application ID
* @example delete_rules_relationship("app_id=1&rule_id=3"); //use this if you only want to delete the relationship per application ID and rule ID
* @example delete_rules_relationship("app_id=1","categories"); //use this if you only want to delete the relationship by the rule name
*
* @author Wahya Biantara
* 
* @since alpha
* 
* @param string $args Argument that can be specify by you. 
* @param string $rule Rule name
* 
* @return boolean  
*      
*/
function delete_rules_relationship( $args = '', $rule = '', $group = '' )
{
    global $db;
    $var_name[ 'app_id' ]  = '';
    $var_name[ 'rule_id' ] = '';
    $where                 = "";
    if( !empty( $args ) )
    {
        $args  = explode( '&', $args );
        $where = " WHERE ";
        $i     = 1;
        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );
            if( $variable == 'app_id' || $variable == 'rule_id' )
            {
                $where .= "a.l" . $variable . "=" . $db->_real_escape( $value );
                if( $i != count( $args ) )
                {
                    $where .= " AND ";
                }
            }
            $i++;
        }
        if( !empty( $rule ) && empty( $group ) )
        {
            $where .= " AND b.lrule='" . $db->_real_escape( $rule ) . "' AND a.lrule_id=b.lrule_id";
            $sql = $db->prepare_query( "DELETE a FROM lumonata_rule_relationship AS a, lumonata_rules AS b $where " );
        }
        elseif( !empty( $group ) && empty( $rule ) )
        {
            $where .= " AND b.lgroup='" . $db->_real_escape( $group ) . "' AND a.lrule_id=b.lrule_id";
            $sql = $db->prepare_query( "DELETE a FROM lumonata_rule_relationship AS a, lumonata_rules AS b $where " );
        }
        elseif( !empty( $group ) && !empty( $rule ) )
        {
            $where .= " AND b.lgroup='" . $db->_real_escape( $group ) . "' AND b.lrule='" . $db->_real_escape( $rule ) . "' AND a.lrule_id=b.lrule_id";
            $sql = $db->prepare_query( "DELETE a FROM lumonata_rule_relationship AS a, lumonata_rules AS b $where " );
        }
        else
        {
            $sql = $db->prepare_query( "DELETE a FROM lumonata_rule_relationship AS a $where " );
        }
        return $db->do_query( $sql );
    }
}

/**
* To fetch the rule relationship and there are two $args that you can use: app_id and rule_id
*
* @author Wahya Biantara
* 
* @since alpha
* 
* @param string $args Argument that can be specify by you. 
* @param string $return_value Specify the return value that your want to get, by default the return value is 'lrule_id'
* 
* @example delete_rules_relationship("app_id=1"); //use this if you only want to fetch the relationship per application ID
* @example delete_rules_relationship("app_id=1&rule_id=3"); //use this if you only want to fetch the relationship per application ID and rule ID
* @example delete_rules_relationship("app_id=1&rule_id=3","lrule_id"); //use this if you only want to fetch the relationship per application ID and rule ID and return the lrule_id as the result 
* 
* @return $return_value  
*
*/
function fetch_rule_relationship( $args = '', $return_value = 'lrule_id' )
{
    global $db;
    $return                = array();
    $var_name[ 'app_id' ]  = '';
    $var_name[ 'rule_id' ] = '';
    $where                 = "";
    if( !empty( $args ) )
    {
        $args  = explode( '&', $args );
        $where = " WHERE ";
        $i     = 1;
        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );
            if( $variable == 'app_id' || $variable == 'rule_id' )
            {
                $where .= "l" . $variable . "=" . $db->_real_escape( $value );
                if( $i != count( $args ) )
                {
                    $where .= " AND ";
                }
            }
            $i++;
        }
        $sql = $db->prepare_query( "SELECT * FROM lumonata_rule_relationship $where " );
        $r   = $db->do_query( $sql );
        while( $d = $db->fetch_array( $r ) )
        {
            $return[] = $d[ $return_value ];
        }
        return $return;
    }
}

/**
* To fetch the rule and there are three $args that you can use: sef, rule_id and group
* Default value for 'group' args are 'articles'  
* 
* @author Wahya Biantara
* 
* @since alpha
* 
* @param string $args Argument that can be specify by you. 
* 
* @example delete_rules_relationship("sef=category-1"); //use this if you only want to fetch the rule by the given sef
* @example delete_rules_relationship("sef=category-1&group=articles"); //use this if you only want to fetch the rule by the given sef in given application name ($group) 
* 
* @return array Fetch result  
*      
*/
function fetch_rule( $args = '', $fetch = true )
{
    global $db;

    $var_name[ 'parent' ]  = '';
    $var_name[ 'name' ]    = '';
    $var_name[ 'rule' ]    = '';
    $var_name[ 'group' ]   = '';
    $var_name[ 'sef' ]     = '';
    $var_name[ 'rule_id' ] = '';
    $where                 = '';

    if( !empty( $args ) )
    {
        $args  = explode( '&', $args );
        $where = ' WHERE ';
        $i     = 1;

        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );

            if( $variable == 'parent' || $variable == 'name' || $variable == 'rule' || $variable == 'group' || $variable == 'sef' || $variable == 'rule_id' )
            {
                if( $variable == 'parent' )
                {
                    $where .= 'l' . $variable . '=' . $db->_real_escape( $value );
                }
                else
                {
                    $where .= 'l' . $variable . '="' . $db->_real_escape( $value ) . '"';
                }

                if( $i != count( $args ) )
                {
                    $where .= ' AND ';
                }
            }
            
            $i++;
        }
    }

    $s = 'SELECT * FROM lumonata_rules AS a ' . $where;
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );      

    if( is_array( $r ) )
    {
        return array();
    }
    else
    {
        $data = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $s2 = 'SELECT * FROM lumonata_additional_rule_fields AS a2 WHERE a2.lrule_id = %d AND a2.lapp_name = %d';
            $q2 = $db->prepare_query( $s2, $d['lrule_id'], $d['lgroup'] );
            $r2 = $db->do_query( $q2 );       

            if( !is_array( $r2 ) )
            {
                while( $d2 = $db->fetch_array( $r2 ) )
                {
                    $d['additional'][ $d2['lkey'] ] = $d2['lvalue'];
                }
            }

            $s3 = 'SELECT * FROM lumonata_attachment AS a2 WHERE a2.lrule_id = %d ORDER BY a2.lorder';
            $q3 = $db->prepare_query( $s3, $d['lrule_id'] );
            $r3 = $db->do_query( $q3 );       

            if( !is_array( $r3 ) )
            {
                while( $d3 = $db->fetch_array( $r3 ) )
                {
                    $d['attachment'][ $d3['lapp_name'] ][] = $d3;
                }
            }

            $data[] = $d;
        }

        return $fetch ? $data[0] : $data;
    }
    
    return $r;
}

/**
* To count the specifiy rule conditions
* There are fice $args that you can use: parent,name,rule,group,sef,rule_id
*  
* @author Wahya Biantara
* 
* @since alpha
* 
* @param string $args Argument that can be specify by you. 
* @param boolean $count is this variable set to false, the function will return the fetching results 
* 
* @return the number of count if $count=TRUE and fetch array if $count=FALSE 
*
*/
function count_rules( $args = '', $count = true )
{
    global $db;
    $var_name[ 'parent' ]  = '';
    $var_name[ 'name' ]    = '';
    $var_name[ 'rule' ]    = '';
    $var_name[ 'group' ]   = '';
    $var_name[ 'sef' ]     = '';
    $var_name[ 'rule_id' ] = '';
    $where                 = "";
    if( !empty( $args ) )
    {
        $args  = explode( '&', $args );
        $where = " WHERE ";
        $i     = 1;
        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );
            if( $variable == 'parent' || $variable == 'name' || $variable == 'rule' || $variable == 'group' || $variable == 'sef' || $variable == 'rule_id' )
            {
                //$var_name[$variable]=$value;
                if( $variable == 'parent' )
                {
                    $where .= "l" . $variable . "=" . $db->_real_escape( $value );
                }
                else if( $variable == 'rule_id' )
                {
                    if( $count )
                    {
                        $where .= "l" . $variable . "<>" . $db->_real_escape( $value );
                    }
                    else
                    {
                        $where .= "l" . $variable . "='" . $db->_real_escape( $value ) . "'";
                    }
                }
                else
                {
                    $where .= "l" . $variable . "='" . $db->_real_escape( $value ) . "'";
                }
                if( $i != count( $args ) )
                {
                    $where .= " AND ";
                }
            }
            $i++;
        }
    }
    $sql = $db->prepare_query( "SELECT * from lumonata_rules " . $where );
    $r   = $db->do_query( $sql );
    if( $count )
    {
        return $db->num_rows( $r );
    }
    else
    {
        return $db->fetch_array( $r );
    }
}

/**
* This function to the query to select the selected rule in mention article. 
* 
* @author Wahya Biantara
* 
* @since alpha
* 
* @param integer $post_id Article ID 
* @param string $rule The rule name (categories or tags)
* @param string $group The application name 
* 
* @return array Return a set of Rule ID as per query specify 
*
*/
function find_selected_rules( $post_id, $rule, $group )
{
    global $db;
    $result = array();
    $sql    = $db->prepare_query( "SELECT a.lrule_id

                                 FROM lumonata_rule_relationship a, lumonata_rules b

                                 WHERE a.lrule_id=b.lrule_id AND (b.lrule=%s or b.lrule='category') AND (b.lgroup=%s or b.lgroup=%s) AND a.lapp_id=%d", $rule, $group, 'default', $post_id );
    $r      = $db->do_query( $sql );
    while( $d = $db->fetch_array( $r ) )
    {
        $result[] = $d[ 'lrule_id' ];
    }
    return $result;
}

function update_taxonomy_order( $id, $order, $rule, $group )
{
    global $db;

    $s = 'UPDATE lumonata_rules SET lorder = %d WHERE lrule_id = %d AND lrule = %s AND lgroup = %s';
    $q = $db->prepare_query( $s, $order, $id, $rule, $group );
    
    if( $db->do_query( $q ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function get_expertise_categories()
{
    global $db;
    $query = $db->prepare_query( "SELECT * FROM lumonata_rules 

                                    WHERE lrule='categories' 

                                    AND lgroup='global_settings'

                                    AND lsubsite='arunna'

                                    ORDER BY lorder" );
    return $result = $db->do_query( $query );
}

function get_multilevel_rule_list( $sef = '', $rule, $group )
{
    global $db;

    $s    = 'SELECT * from lumonata_rules WHERE lrule=%s AND lgroup=%s';
    $q    = $db->prepare_query( $s, $rule, $group );
    $r    = $db->do_query( $q );

    if( $db->num_rows( $r ) == 0 )
    {
        return array();
    }

    $data = array();

    while( $d = $db->fetch_array( $r ) )
    {
        $data[] = array(
            'id' => $d[ 'lrule_id' ],
            'sef' => $d[ 'lsef' ],
            'title' => $d[ 'lname' ],
            'parent_id' => $d[ 'lparent' ],
            'link' => site_url( $group . '/' . $d[ 'lsef' ] ),
            'active' => ( $d[ 'lsef' ] == $sef ? true : false ) 
        );
    }

    return build_tree( $data );
}

function build_tree( Array $data, $parent = 0 )
{
    $tree = array();

    foreach( $data as $d )
    {
        if( $d[ 'parent_id' ] == $parent )
        {
            $children = build_tree( $data, $d[ 'id' ] );

            if( $children )
            {
                $d[ 'child' ] = $children;
            }

            $tree[] = $d;
        }
    }

    return $tree;
}

function upload_rule_file()
{    
    global $db;

    if( isset( $_FILES['file'] ) && $_FILES['file']['error'] == 0 )
    {
        $post_id     = $_POST['post_id'];
        $app_name    = $_POST['app_name'];
        $allowed     = $_POST['allowed_file'];
        $file_name   = $_FILES['file']['name'];
        $file_size   = $_FILES['file']['size'];
        $file_type   = $_FILES['file']['type'];
        $file_source = $_FILES['file']['tmp_name'];
        $is_batch    = isset( $_POST['batch'] ) ? $_POST['batch'] : false;

        if( is_allow_file_type( $file_type, $allowed ) )
        {
            //-- CHECK FOLDER EXIST
            $folder = upload_folder_name();

            if( !is_dir( FILES_PATH . '/' . $folder ) )
            {
                create_dir( FILES_PATH . '/' . $folder );
            }

            $ext    = file_name_filter( $file_name, true );
            $name   = file_name_filter( $file_name, false ) . '-' . time();
            $name_f = $name . $ext;

            $ldestination = FILES_PATH . '/' . $folder . '/' . $name_f;
            $mdestination = FILES_PATH . '/' . $folder . '/' . $name_f;
            $tdestination = FILES_PATH . '/' . $folder . '/' . $name_f;
            $fdestination = FILES_PATH . '/' . $folder . '/' . $name_f;

            if( upload( $file_source, $fdestination ) )
            {
                $loc        = '/lumonata-content/files/' . $folder . '/' . $name_f;
                $loc_thumb  = '/lumonata-content/files/' . $folder . '/' . $name_f;
                $loc_medium = '/lumonata-content/files/' . $folder . '/' . $name_f;
                $loc_large  = '/lumonata-content/files/' . $folder . '/' . $name_f;

                $d = get_rule_attachment_by_app_name( $post_id, $app_name, false );

                if( empty( $d ) || $is_batch )
                {
                    if( insert_attachment( 0, $post_id, $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, $app_name, false ) )
                    {
                        $attach_id = $db->insert_id();

                        if( empty( $attach_id ) )
                        {
                            //-- Delete Original Image
                            if( file_exists( $fdestination ) )
                            {
                                unlink( $fdestination );
                            }

                            return array( 'result' => 'failed', 'message' => 'Error uploading file' );
                        }
                        else
                        {
                            $s = 'SELECT * FROM lumonata_attachment AS a  WHERE a.lattach_id = %d';
                            $q = $db->prepare_query( $s, $attach_id );
                            $r = $db->do_query( $q );

                            if( is_array( $r ) )
                            {
                                return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                            }
                            else
                            {
                                $d = $db->fetch_array( $r );

                                return array(
                                    'result' => 'success',
                                    'data'   => array(
                                        'filename'  => $name_f,
                                        'post_id'   => $post_id,
                                        'attach_id' => $attach_id,
                                        'title'     => $d['ltitle'],
                                        'content'   => $d['lcontent'],
                                        'caption'   => $d['lcaption'],
                                        'alt'       => $d['lalt_text'],
                                        'fileturl'  => site_url( $d[ 'lattach_loc_thumb' ] ),
                                        'fileurl'   => get_theme_img() . '/ico-upload-' . $allowed . '.svg'
                                    )
                                );
                            }
                        }
                    }
                    else
                    {
                        //-- Delete Original Image
                        if( file_exists( $fdestination ) )
                        {
                            unlink( $fdestination );
                        }

                        return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                    }
                }
                else
                {
                    if( update_attachment( $d['lattach_id'], 0, $post_id, $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, $app_name ) )
                    {
                        //-- Delete Original Image
                        if( !empty( $d[ 'lattach_loc' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc' ] ) )
                        {
                            unlink( ROOT_PATH . $d[ 'lattach_loc' ] );
                        }
                    }

                    $s = 'SELECT * FROM lumonata_attachment AS a  WHERE a.lattach_id = %d';
                    $q = $db->prepare_query( $s, $d['lattach_id'] );
                    $r = $db->do_query( $q );

                    if( is_array( $r ) )
                    {
                        return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                    }
                    else
                    {
                        $d = $db->fetch_array( $r );

                        return array(
                            'result' => 'success',
                            'data'   => array(
                                'filename'  => $name_f,
                                'post_id'   => $post_id,
                                'title'     => $d['ltitle'],
                                'content'   => $d['lcontent'],
                                'caption'   => $d['lcaption'],
                                'alt'       => $d['lalt_text'],
                                'attach_id' => $d['lattach_id'],
                                'fileturl'  => site_url( $d[ 'lattach_loc_thumb' ] ),
                                'fileurl'   => site_url( $d[ 'lattach_loc_medium' ] )
                            )
                        );
                    }
                }
            }
            else
            {
                return array( 'result' => 'failed', 'error' => 'Error uploading file' );
            }
        }
        else
        {
            return array( 'result' => 'failed', 'error' => 'File is not allowed' );
        }
    }
}

function delete_rule_file()
{
    if( isset( $_POST['attach_id'] ) )
    {
        if( delete_attachment( $_POST['attach_id'] ) )
        {
            return array( 'result' => 'success' );
        }
        else
        {
            return array( 'result' => 'failed' );
        }
    }
    else
    {
        if( empty( $_POST['post_id'] ) )
        {
            return array( 'result' => 'failed' );
        }
        else
        {
            $d = get_rule_attachment_by_app_name( $_POST['post_id'], $_POST['app_name'], false );

            if( delete_attachment( $d['lattach_id'] ) )
            {
                return array( 'result' => 'success' );
            }
            else
            {
                return array( 'result' => 'failed' );
            }
        }
    }
}

function upload_rule_image()
{    
    global $db;

    if( isset( $_FILES['file'] ) && $_FILES['file']['error']==0 )
    {
        $post_id     = $_POST['post_id'];
        $app_name    = $_POST['app_name'];
        $file_name   = $_FILES['file']['name'];
        $file_size   = $_FILES['file']['size'];
        $file_type   = $_FILES['file']['type'];
        $file_source = $_FILES['file']['tmp_name'];
        $is_batch    = isset( $_POST['batch'] ) ? $_POST['batch'] : false;

        if( is_allow_file_size( $file_size ) )
        {
            if( is_allow_file_type( $file_type, 'image' ) )
            {
                //-- CHECK FOLDER EXIST
                $folder = upload_folder_name();

                if( !is_dir( FILES_PATH . '/' . $folder ) )
                {
                    create_dir( FILES_PATH . '/' . $folder );
                }

                $ext    = file_name_filter( $file_name, true );
                $name   = file_name_filter( $file_name, false ) . '-' . time();
                $name_t = $name . '-thumbnail' . $ext;
                $name_m = $name . '-medium' . $ext;
                $name_l = $name . '-large' . $ext;
                $name_f = $name . $ext;

                $ldestination = FILES_PATH . '/' . $folder . '/' . $name_l;
                $mdestination = FILES_PATH . '/' . $folder . '/' . $name_m;
                $tdestination = FILES_PATH . '/' . $folder . '/' . $name_t;
                $fdestination = FILES_PATH . '/' . $folder . '/' . $name_f;

                if( $file_type == 'image/svg+xml' )
                {
                    if( upload( $file_source, $fdestination ) )
                    {
                        $loc        = '/lumonata-content/files/' . $folder . '/' . $name_f;
                        $loc_thumb  = '/lumonata-content/files/' . $folder . '/' . $name_f;
                        $loc_medium = '/lumonata-content/files/' . $folder . '/' . $name_f;
                        $loc_large  = '/lumonata-content/files/' . $folder . '/' . $name_f;

                        $d = get_attachment_by_app_name( $post_id, $app_name, false );

                        if( empty( $d ) || $is_batch )
                        {
                            if( insert_attachment( 0, $post_id, $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, $app_name, false ) )
                            {
                                $attach_id = $db->insert_id();

                                if( empty( $attach_id ) )
                                {
                                    //-- Delete Original Image
                                    if( file_exists( $fdestination ) )
                                    {
                                        unlink( $fdestination );
                                    }

                                    return array( 'result' => 'failed', 'message' => 'Error uploading file' );
                                }
                                else
                                {
                                    $s = 'SELECT * FROM lumonata_attachment AS a  WHERE a.lattach_id = %d';
                                    $q = $db->prepare_query( $s, $attach_id );
                                    $r = $db->do_query( $q );

                                    if( is_array( $r ) )
                                    {
                                        return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                                    }
                                    else
                                    {
                                        $d = $db->fetch_array( $r );

                                        return array(
                                            'result' => 'success',
                                            'data'   => array(
                                                'filename'  => $name_m,
                                                'post_id'   => $post_id,
                                                'attach_id' => $attach_id,
                                                'title'     => $d['ltitle'],
                                                'content'   => $d['lcontent'],
                                                'caption'   => $d['lcaption'],
                                                'alt'       => $d['lalt_text'],
                                                'fileturl'  => site_url( $d[ 'lattach_loc_thumb' ] ),
                                                'fileurl'   => site_url( $d[ 'lattach_loc_medium' ] )
                                            )
                                        );
                                    }
                                }
                            }
                            else
                            {
                                //-- Delete Original Image
                                if( file_exists( $fdestination ) )
                                {
                                    unlink( $fdestination );
                                }

                                return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                            }
                        }
                        else
                        {
                            if( update_attachment( $d['lattach_id'], 0, $post_id, $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, $app_name ) )
                            {
                                //-- Delete Original Image
                                if( !empty( $d[ 'lattach_loc' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc' ] ) )
                                {
                                    unlink( ROOT_PATH . $d[ 'lattach_loc' ] );
                                }
                            }

                            $s = 'SELECT * FROM lumonata_attachment AS a  WHERE a.lattach_id = %d';
                            $q = $db->prepare_query( $s, $d['lattach_id'] );
                            $r = $db->do_query( $q );

                            if( is_array( $r ) )
                            {
                                return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                            }
                            else
                            {
                                $d = $db->fetch_array( $r );

                                return array(
                                    'result' => 'success',
                                    'data'   => array(
                                        'filename'  => $name_m,
                                        'post_id'   => $post_id,
                                        'title'     => $d['ltitle'],
                                        'content'   => $d['lcontent'],
                                        'caption'   => $d['lcaption'],
                                        'alt'       => $d['lalt_text'],
                                        'attach_id' => $d['lattach_id'],
                                        'fileturl'  => site_url( $d[ 'lattach_loc_thumb' ] ),
                                        'fileurl'   => site_url( $d[ 'lattach_loc_medium' ] )
                                    )
                                );
                            }
                        }
                    }
                    else
                    {
                        return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                    }
                }
                else
                {
                    if( upload_resize( $file_source, $ldestination, $file_type, 1920, 1920 ) )
                    {
                        if( upload_resize( $file_source, $mdestination, $file_type, 1024, 1024 ) )
                        {
                            if( upload_crop( $file_source, $tdestination, $file_type, 550, 550 ) )
                            {
                                if( upload( $file_source, $fdestination ) )
                                {
                                    $loc        = '/lumonata-content/files/' . $folder . '/' . $name_f;
                                    $loc_thumb  = '/lumonata-content/files/' . $folder . '/' . $name_t;
                                    $loc_medium = '/lumonata-content/files/' . $folder . '/' . $name_m;
                                    $loc_large  = '/lumonata-content/files/' . $folder . '/' . $name_l;

                                    $d = get_rule_attachment_by_app_name( $post_id, $app_name, false );

                                    if( empty( $d ) || $is_batch )
                                    {
                                        if( insert_attachment( 0, $post_id, $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, $app_name, false ) )
                                        {
                                            $attach_id = $db->insert_id();

                                            if( empty( $attach_id ) )
                                            {
                                                //-- Delete Original Image
                                                if( file_exists( $fdestination ) )
                                                {
                                                    unlink( $fdestination );
                                                }

                                                //-- Delete Thumb Image
                                                if( file_exists( $tdestination ) )
                                                {
                                                    unlink( $tdestination );
                                                }

                                                //-- Delete Medium Image
                                                if( file_exists( $mdestination ) )
                                                {
                                                    unlink( $mdestination );
                                                }

                                                //-- Delete Large Image
                                                if( file_exists( $ldestination ) )
                                                {
                                                    unlink( $ldestination );
                                                }

                                                return array( 'result' => 'failed', 'message' => 'Error uploading file' );
                                            }
                                            else
                                            {
                                                $s = 'SELECT * FROM lumonata_attachment AS a  WHERE a.lattach_id = %d';
                                                $q = $db->prepare_query( $s, $attach_id );
                                                $r = $db->do_query( $q );

                                                if( is_array( $r ) )
                                                {
                                                    return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                                                }
                                                else
                                                {
                                                    $d = $db->fetch_array( $r );

                                                    return array(
                                                        'result' => 'success',
                                                        'data'   => array(
                                                            'filename'  => $name_m,
                                                            'post_id'   => $post_id,
                                                            'attach_id' => $attach_id,
                                                            'title'     => $d['ltitle'],
                                                            'content'   => $d['lcontent'],
                                                            'caption'   => $d['lcaption'],
                                                            'alt'       => $d['lalt_text'],
                                                            'fileturl'  => site_url( $d[ 'lattach_loc_thumb' ] ),
                                                            'fileurl'   => site_url( $d[ 'lattach_loc_medium' ] )
                                                        )
                                                    );
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //-- Delete Original Image
                                            if( file_exists( $fdestination ) )
                                            {
                                                unlink( $fdestination );
                                            }

                                            //-- Delete Thumb Image
                                            if( file_exists( $tdestination ) )
                                            {
                                                unlink( $tdestination );
                                            }

                                            //-- Delete Medium Image
                                            if( file_exists( $mdestination ) )
                                            {
                                                unlink( $mdestination );
                                            }

                                            //-- Delete Large Image
                                            if( file_exists( $ldestination ) )
                                            {
                                                unlink( $ldestination );
                                            }

                                            return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                                        }
                                    }
                                    else
                                    {
                                        if( update_attachment( $d['lattach_id'], 0, $post_id, $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, $app_name ) )
                                        {
                                            //-- Delete Original Image
                                            if( !empty( $d[ 'lattach_loc' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc' ] ) )
                                            {
                                                unlink( ROOT_PATH . $d[ 'lattach_loc' ] );
                                            }

                                            //-- Delete Thumb Image
                                            if( !empty( $d[ 'lattach_loc_thumb' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc_thumb' ] ) )
                                            {
                                                unlink( ROOT_PATH . $d[ 'lattach_loc_thumb' ] );
                                            }

                                            //-- Delete Medium Image
                                            if( !empty( $d[ 'lattach_loc_medium' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc_medium' ] ) )
                                            {
                                                unlink( ROOT_PATH . $d[ 'lattach_loc_medium' ] );
                                            }

                                            //-- Delete Large Image
                                            if( !empty( $d[ 'lattach_loc_large' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc_large' ] ) )
                                            {
                                                unlink( ROOT_PATH . $d[ 'lattach_loc_large' ] );
                                            }
                                        }

                                        $s = 'SELECT * FROM lumonata_attachment AS a  WHERE a.lattach_id = %d';
                                        $q = $db->prepare_query( $s, $d['lattach_id'] );
                                        $r = $db->do_query( $q );

                                        if( is_array( $r ) )
                                        {
                                            return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                                        }
                                        else
                                        {
                                            $d = $db->fetch_array( $r );

                                            return array(
                                                'result' => 'success',
                                                'data'   => array(
                                                    'filename'  => $name_m,
                                                    'post_id'   => $post_id,
                                                    'title'     => $d['ltitle'],
                                                    'content'   => $d['lcontent'],
                                                    'caption'   => $d['lcaption'],
                                                    'alt'       => $d['lalt_text'],
                                                    'attach_id' => $d['lattach_id'],
                                                    'fileturl'  => site_url( $d[ 'lattach_loc_thumb' ] ),
                                                    'fileurl'   => site_url( $d[ 'lattach_loc_medium' ] )
                                                )
                                            );
                                        }
                                    }
                                }
                                else
                                {
                                    //-- Delete Thumb Image
                                    if( file_exists( $tdestination ) )
                                    {
                                        unlink( $tdestination );
                                    }

                                    //-- Delete Medium Image
                                    if( file_exists( $mdestination ) )
                                    {
                                        unlink( $mdestination );
                                    }

                                    //-- Delete Large Image
                                    if( file_exists( $ldestination ) )
                                    {
                                        unlink( $ldestination );
                                    }

                                    return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                                }
                            }
                            else
                            {
                                //-- Delete Medium Image
                                if( file_exists( $mdestination ) )
                                {
                                    unlink( $mdestination );
                                }

                                //-- Delete Large Image
                                if( file_exists( $ldestination ) )
                                {
                                    unlink( $ldestination );
                                }

                                return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                            }
                        }
                        else
                        {
                            //-- Delete Large Image
                            if( file_exists( $ldestination ) )
                            {
                                unlink( $ldestination );
                            }

                            return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                        }
                    }
                    else
                    {
                        return array( 'result' => 'failed', 'error' => 'Error uploading file' );
                    }
                }
            }
            else
            {
                return array( 'result' => 'failed', 'error' => 'File is not an image' );
            }
        }
        else
        {
            return array( 'result' => 'failed', 'error' => 'Maximum file size is 2MB' );
        }
    }
    else
    {
        return array( 'result' => 'failed', 'error' => 'No image was found' );
    }
}

function delete_rule_image()
{
    if( isset( $_POST['attach_id'] ) )
    {
        if( delete_attachment( $_POST['attach_id'] ) )
        {
            return array( 'result' => 'success' );
        }
        else
        {
            return array( 'result' => 'failed' );
        }
    }
    else
    {
        if( empty( $_POST['post_id'] ) )
        {
            return array( 'result' => 'failed' );
        }
        else
        {
            $d = get_rule_attachment_by_app_name( $_POST['post_id'], $_POST['app_name'], false );

            if( delete_attachment( $d['lattach_id'] ) )
            {
                return array( 'result' => 'success' );
            }
            else
            {
                return array( 'result' => 'failed' );
            }
        }
    }
}

function get_rule_image()
{   
    global $db;

    $s = 'SELECT * FROM lumonata_attachment AS a 
          WHERE a.lrule_id = %d 
          AND a.lapp_name = %s 
          AND a.mime_type LIKE %s 
          ORDER BY a.lorder';
    $q = $db->prepare_query( $s, $_POST['post_id'], $_POST['app_name'], '%image%' );
    $r = $db->do_query( $q );

    $data = array();

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $filename = explode( '/', $d['lattach_loc_thumb'] );
            $filename = array_reverse( $filename );

            $data[] = array(
                'filename'  => $filename,
                'title'     => $d['ltitle'],
                'content'   => $d['lcontent'],
                'caption'   => $d['lcaption'],
                'alt'       => $d['lalt_text'],
                'attach_id' => $d['lattach_id'],
                'post_id'   => $d['larticle_id'],
                'fileturl'  => site_url( $d[ 'lattach_loc_thumb' ] ),
                'fileurl'   => site_url( $d[ 'lattach_loc_medium' ] )                 
            );
        }
    }

    if( empty( $data ) )
    {
        return array( 'result' => 'failed' );
    }
    else
    {
        return array( 'result' => 'success', 'data' => $data );
    } 
}

function edit_rule_image_info()
{
    if( edit_attachment( $_POST['attach_id'], $_POST['title'], null, $_POST['alt'], $_POST['caption'], $_POST['content'] ) )
    {
        return array( 'result' => 'success' );
    }
    else
    {
        return array( 'result' => 'failed' );
    }
}

function reorder_rule_image()
{
    global $db;

    if( isset( $_POST['value'] ) && !empty( $_POST['value'] ) )
    {
        $value = json_decode( $_POST['value'], true );

        foreach( $value as $i => $val )
        {
            $s = 'UPDATE lumonata_attachment AS a SET a.lorder = %d WHERE a.lattach_id = %d AND a.lrule_id = %d';
            $q = $db->prepare_query( $s, ( $i + 1 ), $val, $_POST['id'] );
            $r = $db->do_query( $q );
        }

        return array( 'result' => 'success' );
    }
    else
    {
        return array( 'result' => 'failed' );
    }
}

/**
* This function use to processing ajax request. 
* 
* @author Ngurah Rai
* 
* @since alpha
* 
* @return string Return a json string data 
*
*/
function rule_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-rule-data' )
    {
        $data = get_rule_list_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'delete-rule' )
    {
        if( is_delete( $_POST['group'] ) )
        {
            if( delete_rule( $_POST['id'], $_POST['group'] ) )
            {
                $result = array( 'result' => 'success' );
            }
            else
            {
                $result = array( 'result' => 'failed' );
            }
        }
        else
        {
            $result = array( 'result' => 'failed' );
        }

        echo json_encode( $result );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'reorder-data' )
    {
        $result = array( 'result' => 'failed' );

        if( !empty( $_POST['reorder'] ) )
        {
            foreach( $_POST['reorder'] as $id => $order )
            {
                if( update_taxonomy_order( $id, $order, $_POST['rule'], $_POST['group'] ) )
                {
                    $result = array( 'result' => 'success' );
                }
            }
        }

        echo json_encode( $result );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'upload-files' )
    {
        $data = upload_rule_file();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-uploaded-files' )
    {
        $data = delete_rule_file();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'upload-images' )
    {
        $data = upload_rule_image();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'delete-uploaded-images' )
    {
        $data = delete_rule_image();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'get-images' )
    {
        $data = get_rule_image();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'edit-image-info' )
    {
        $data = edit_rule_image_info();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'reorder-images' )
    {
        $data = reorder_rule_image();

        echo json_encode( $data );
    }

    exit;
}

?>