<?php

add_actions( 'custom-post', 'get_admin_custom_post' );
add_actions( 'custom-post_admin_page', 'custom_post_ajax' );
add_actions( 'custom-post-config', 'config_admin_custom_post' );

/*
| -----------------------------------------------------------------------------
| Admin Custom Post Set
| -----------------------------------------------------------------------------
*/
function get_admin_custom_post()
{
    run_custom_post_actions();
    
    //-- Display add new form
    if( is_add_new() )
    {
        return add_new_custom_post();
    }
    elseif( is_edit() )
    {
        if( is_contributor() || is_author() )
        {
            if( is_num_articles( 'id=' . $_GET[ 'id' ] ) > 0 )
            {
                return edit_custom_post( $_GET[ 'id' ] );
            }
            else
            {
                return '
                <div class="alert_red_form">
                    You don\'t have an authorization to access this page
                </div>';
            }
        }
        else
        {
            return edit_custom_post( $_GET[ 'id' ] );
        }
    }
    elseif( is_delete_all() )
    {
        return delete_batch_custom_post();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST[ 'id' ] as $key => $val )
        {
            delete_custom_post( $val );
        }
    }
    
    //-- Automatic to display add new when there is no records on database
    if( is_num_custom_post() == 0 )
    {
        header( 'location:' . get_state_url( 'custom-post&prc=add_new' ) );
    }
    else
    {
        return get_custom_post_list();
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post Actions
| -----------------------------------------------------------------------------
*/
function run_custom_post_actions()
{
    global $db;

    //-- Publish, Unpublish & Save Draft 
    //-- Actions From List Page
    if( isset( $_POST[ 'select' ] ) )
    {
        $status = ( is_save_draft() ? 'draft' : ( is_unpublish() ? 'unpublish' : 'publish' ) );
        
        foreach( $_POST[ 'select' ] as $key => $val )
        {
            update_custom_post_status( $val, $status );
        }
    }
    else
    {
        if( is_save_draft() || is_publish() )
        {
            $id     = $_POST['lcustom_id'];
            $title  = $_POST['lcustom_post'];
            $status = is_save_draft() ? 'draft' : 'publish';
            $icon   = isset( $_POST['lcustom_icon'] ) ? $_POST['lcustom_icon'] : '';
            $sef    = empty( $_POST['lsef'] ) ? generateSefUrl( $title ) : generateSefUrl( $_POST['lsef'] );

            if( is_add_new() )
            {
                save_custom_post( $title, $sef, $status, $icon );
            
                $post_id = $db->insert_id();
            }
            elseif( is_edit() )
            {
                update_custom_post( $id, $title, $sef, $status, $icon );
            
                $post_id = $id;
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - List
| -----------------------------------------------------------------------------
*/
function get_custom_post_list()
{
    set_template( TEMPLATE_PATH . '/template/custom-post-list.html', 'custom-post' );

    add_block( 'list-block', 'l-block', 'custom-post' );

    add_variable( 'title', 'Post Type' );
    add_variable( 'limit', post_viewed() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_custom_post_ajax_url() );
    add_variable( 'view-option', get_custom_post_view_option() );
    add_variable( 'button', get_custom_post_admin_button( get_state_url( 'custom-post' ) ) );
    
    add_actions( 'section_title', 'Custom Post' );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );

    parse_template( 'list-block', 'l-block', 'custom-post' );

    return return_template( 'custom-post' );
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - Add New
| -----------------------------------------------------------------------------
*/
function add_new_custom_post()
{
    $ajax_url = get_custom_post_ajax_url();
    $url      = get_state_url( 'custom-post' );
    $button   = get_custom_post_admin_button( $url, true );

    set_template( TEMPLATE_PATH . '/template/custom-post-form.html', 'custom-post' );

    add_block( 'form-block', 'f-block', 'custom-post' );

    add_variable( 'lsef', '' );
    add_variable( 'lcustom_id', '' );
    add_variable( 'lcustom_icon', '' );
    add_variable( 'lcustom_post', '' );
    
    add_variable( 'button', $button );
    add_variable( 'ajax_url', $ajax_url );
    add_variable( 'page_title', 'Add New Post Type' );

    add_actions( 'section_title', 'Custom Post - Add New' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'custom-post' );
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - Edit
| -----------------------------------------------------------------------------
*/
function edit_custom_post()
{
    $ajax_url = get_custom_post_ajax_url();
    $url      = get_state_url( 'custom-post' );
    $data     = fetch_custom_post( 'id=' . $_GET['id'] );
    $button   = get_custom_post_admin_button( $url, true );

    set_template( TEMPLATE_PATH . '/template/custom-post-form.html', 'custom-post' );

    add_block( 'form-block', 'f-block', 'custom-post' );

    add_variable( 'lsef', $data[ 'lsef' ] );
    add_variable( 'lcustom_id', $data[ 'lcustom_id' ] );
    add_variable( 'lcustom_post', rem_slashes( $data[ 'lcustom_post' ] ) );
    add_variable( 'lcustom_icon', get_custom_post_icon_content( $data[ 'lcustom_icon' ] ) );
    
    add_variable( 'button', $button );
    add_variable( 'ajax_url', $ajax_url );
    add_variable( 'page_title', 'Edit Post Type' );
    
    add_actions( 'section_title', 'Custom Post - Edit' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'custom-post' );
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - Batch Delete
| -----------------------------------------------------------------------------
*/
function delete_batch_custom_post()
{
    set_template( TEMPLATE_PATH . '/template/custom-post-batch-delete.html', 'custom-post-batch-delete' );

    add_block( 'delete-loop-block', 'dl-block', 'custom-post-batch-delete' );
    add_block( 'delete-block', 'd-block', 'custom-post-batch-delete' );

    foreach( $_POST[ 'select' ] as $key => $val )
    {
        $d = fetch_custom_post( 'id=' . $val );

        add_variable( 'custom-post-id', $d[ 'lcustom_id' ] );
        add_variable( 'custom-post-title', $d[ 'lcustom_post' ] );

        parse_template( 'delete-loop-block', 'dl-block', true );
    }

    add_variable( 'title', 'Batch Delete Custom Posts' );
    add_variable( 'backlink', get_state_url( 'custom-post' ) );
    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST[ 'select' ] ) == 1 ? 'this post type' : 'these post types' ) );
    
    add_actions( 'section_title', 'Custom Posts' );

    parse_template( 'delete-block', 'd-block', 'custom-post-batch-delete' );

    return return_template( 'custom-post-batch-delete' );
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - Table Query
| -----------------------------------------------------------------------------
*/
function get_custom_post_list_query()
{
    global $db;

    extract( $_POST );

    $rdata = $_REQUEST;
    $cols  = array( 
        1 => 'a.lcustom_post',
        2 => 'a.lsef',
        3 => 'a.lcustom_status'
    );
    
    //-- Set Order Column
    if( isset( $rdata['order'] ) && !empty( $rdata['order'] ) )
    {
        $o = array();

        foreach( $rdata['order'] as $i => $od )
        {
            if( isset( $cols[ $rdata['order'][$i]['column'] ] ) )
            {
                $o[] = $cols[ $rdata['order'][$i]['column'] ] . ' ' . $rdata['order'][$i]['dir'];
            }
        }

        $order = !empty( $o ) ? 'ORDER BY ' . implode( ', ', $o ) : '';
    }
    else
    {
        $order = 'ORDER BY a.lorder ASC';
    }

    if( isset( $show ) && !empty( $show ) && $show != 'all' )
    {
        $where = $db->prepare_query( 'lcustom_status = %s', $show );
    }
    else
    {
        $where = '';
    }

    if( empty( $rdata['search']['value']) )
    {
    	if( empty( $where ) )
        {
        	$q = 'SELECT * FROM lumonata_post_type AS a ' . $order;
        }
        else
        {
        	$q = 'SELECT * FROM lumonata_post_type AS a WHERE ' . $where . $order;
        }

        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }
    else
    {
        $search = array();

        foreach( $cols as $col )
        {
            $search[] = $db->prepare_query( $col . ' LIKE %s', '%' . $rdata['search']['value'] . '%' );
        }

    	if( empty( $where ) )
        {
        	$q = 'SELECT * FROM lumonata_post_type AS a WHERE ( ' . implode( ' OR ', $search ) . ' ) ' . $order;
        }
        else
        {
        	$q = 'SELECT * FROM lumonata_post_type AS a WHERE ' . $where . ' AND ( ' . implode( ' OR ', $search ) . ' ) ' . $order;
        }

        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        $q2 = $q . ' LIMIT ' . $rdata['start'] . ', ' . $rdata['length'];
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );
    }

    $data = array();

    if( $n2 > 0 )
    {
        $url = get_state_url( 'custom-post' );

        while( $d2 = $db->fetch_array( $r2 ) )
        {
            $data[] = array(
                'sef'       => $d2[ 'lsef' ],
                'order'     => $d2[ 'lorder' ],
                'id'        => $d2[ 'lcustom_id' ],
                'title'     => $d2[ 'lcustom_post' ],
                'status'    => $d2[ 'lcustom_status' ],
                'ajax_link' => get_custom_post_ajax_url(),
                'edit_link' => $url . '&prc=edit&id=' . $d2[ 'lcustom_id' ]
            );
        }
    }
    else
    {
        $n = 0;
    }

    $result = array(
        'draw' => intval( $rdata['draw'] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data
    );

    return $result;
}

function is_num_custom_post( $args = '' )
{
    global $db;

    $var_name[ 'id' ]    = '';
    $var_name[ 'sef' ]   = '';
    $var_name[ 'title' ] = '';
    
    $args = str_replace( '&amp;', 'U+0026', $args );
    
    if( !empty( $args ) )
    {
        $args = explode( '&', $args );

        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );

            if( $variable == 'title' || $variable == 'id' || $variable == 'sef' )
            {
                $var_name[ $variable ] = $value;
            }

            $var_name[ $variable ] = str_replace( 'U+0026', '&amp;', $var_name[ $variable ] );
        }
    }
    
    if( !empty( $var_name[ 'title' ] ) && !empty( $var_name[ 'id' ] )  && !empty( $var_name[ 'sef' ] ) )
    {
        $s = 'SELECT * FROM lumonata_post_type WHERE lcustom_id = %d AND lcustom_post = %s AND lsef = %s';
        $q = $db->prepare_query( $s, $var_name[ 'id' ], $var_name[ 'title' ], $var_name[ 'sef' ] );
    }
    elseif( empty( $var_name[ 'title' ] ) && !empty( $var_name[ 'id' ] )  && !empty( $var_name[ 'sef' ] ) )
    {
        $s = 'SELECT * FROM lumonata_post_type WHERE lcustom_id = %d AND lsef = %s';
        $q = $db->prepare_query( $s, $var_name[ 'id' ], $var_name[ 'sef' ] );
    }
    elseif( empty( $var_name[ 'title' ] ) && empty( $var_name[ 'id' ] )  && !empty( $var_name[ 'sef' ] ) )
    {
        $s = 'SELECT * FROM lumonata_post_type WHERE lsef = %s';
        $q = $db->prepare_query( $s, $var_name[ 'sef' ] );        
    }
    elseif( !empty( $var_name[ 'title' ] ) && empty( $var_name[ 'id' ] )  && !empty( $var_name[ 'sef' ] ) )
    {
        $s = 'SELECT * FROM lumonata_post_type WHERE lcustom_post = %s AND lsef = %s';
        $q = $db->prepare_query( $s, $var_name[ 'title' ], $var_name[ 'sef' ] );
    }
    elseif( !empty( $var_name[ 'title' ] ) && empty( $var_name[ 'id' ] )  && empty( $var_name[ 'sef' ] ) )
    {
        $s = 'SELECT * FROM lumonata_post_type WHERE lcustom_post = %';
        $q = $db->prepare_query( $s, $var_name[ 'title' ] );
    }
    elseif( !empty( $var_name[ 'title' ] ) && !empty( $var_name[ 'id' ] )  && empty( $var_name[ 'sef' ] ) )
    {
        $s = 'SELECT * FROM lumonata_post_type WHERE lcustom_id = % AND lcustom_post = %s';
        $q = $db->prepare_query( $s, $var_name[ 'id' ], $var_name[ 'title' ]  );
    }
    elseif( empty( $var_name[ 'title' ] ) && !empty( $var_name[ 'id' ] )  && empty( $var_name[ 'sef' ] ) )
    {
        $s = 'SELECT * FROM lumonata_post_type WHERE lcustom_id = %';
        $q = $db->prepare_query( $s, $var_name[ 'id' ] );
    }
    else
    {
        $s = 'SELECT * FROM lumonata_post_type';
        $q = $db->prepare_query( $s );
    }
    
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return 0;
    }
    else
    {
        return $db->num_rows( $r );
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - Fetch Data
| -----------------------------------------------------------------------------
*/
function fetch_custom_post( $args = '', $fetch = true )
{
    global $db;

    $var_name[ 'id' ]     = '';
    $var_name[ 'sef' ]    = '';
    $var_name[ 'title' ]  = '';
    $var_name[ 'status' ] = '';
    $var_name[ 'order' ]  = 'lorder';
    
    if( !empty( $args ) )
    {
        $args = explode( '&', $args );

        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );

            if( $variable == 'sef' || $variable == 'title' || $variable == 'id' || $variable == 'status' || $variable == 'order' )
            {
                $var_name[ $variable ] = $value;
            }            
        }
    }

    $w = array();
    
    if( !empty( $var_name[ 'id' ] ) )
    {
    	$w[] = $db->prepare_query( 'lcustom_id = %s', $var_name[ 'id' ] );
    }
    
    if( !empty( $var_name[ 'title' ] ) )
    {
    	$w[] = $db->prepare_query( 'lcustom_post = %s', $var_name[ 'title' ] );
    }
    
    if( !empty( $var_name[ 'sef' ] ) )
    {
    	$w[] = $db->prepare_query( 'lsef = %s', $var_name[ 'sef' ] );
    }
    
    if( !empty( $var_name[ 'status' ] ) )
    {
    	$w[] = $db->prepare_query( 'lcustom_status = %s', $var_name[ 'status' ] );
    }

    if( empty( $w ) )
    {
    	$s = 'SELECT * FROM lumonata_post_type ORDER BY ' . $var_name['order'];
        $q = $db->prepare_query( $s );
    }
    else
    {
    	$s = 'SELECT * FROM lumonata_post_type WHERE ' . implode( ' AND ', $w ) . ' ORDER BY ' . $var_name['order'];
        $q = $db->prepare_query( $s );
    }

    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {    
        if( $fetch == true )
        {
            return array();
        }
    }
    else
    {
        if( $fetch == true )
        {
            return $db->fetch_array( $r );
        }        
    }
    
    return $r;
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - Save Actions
| -----------------------------------------------------------------------------
*/
function save_custom_post( $title = '', $sef = '', $status = '', $icon = '' )
{
    global $db, $allowedposttags, $allowedtitletags;
    
    $title = empty( $title ) ? 'Untitled' : kses( rem_slashes( $title ), $allowedtitletags );

    if( empty( $sef ) )
    {
    	$sef = generateSefUrl( $title );
    }

    $s = 'SELECT COUNT( a.lsef ) AS num FROM lumonata_post_type AS a WHERE a.lsef LIKE %s';
    $q = $db->prepare_query( $s, '%' . $sef . '%' );
    $r = $db->do_query( $q );

    if( is_array( $r ) === false )
    {
    	$d = $db->fetch_array( $r );

    	$sef = $d['num'] > 0 ? $sef . '-' . ( $d['num'] + 1 ) : $sef;
    }
    
    $s2 = 'INSERT INTO lumonata_post_type(
            lcustom_post,
            lcustom_status,
            lcustom_icon,
            lsef )
          VALUES( %s, %s, %s, %s )';
    $q2 = $db->prepare_query( $s2, $title, $status, $icon, $sef );
    
    if( reset_order_id( 'lumonata_post_type' ) )
    {
        $r2 = $db->do_query( $q2 );

        if( is_array( $r2 ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    return false;
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - Update Actions
| -----------------------------------------------------------------------------
*/
function update_custom_post( $id = '', $title = '', $sef = '', $status = '', $icon = '' )
{
    global $db, $allowedposttags, $allowedtitletags;
    
    $title = empty( $title ) ? 'Untitled' : kses( rem_slashes( $title ), $allowedtitletags );

    if( empty( $sef ) )
    {
    	$sef = generateSefUrl( $title );
    }

    $s = 'SELECT COUNT( a.lsef ) AS num FROM lumonata_post_type AS a WHERE a.lsef LIKE %s AND a.lcustom_id <> %d';
    $q = $db->prepare_query( $s, '%' . $sef . '%', $id );
    $r = $db->do_query( $q );

    if( is_array( $r ) === false )
    {
    	$d = $db->fetch_array( $r );

    	$sef = $d['num'] > 0 ? $sef . '-' . ( $d['num'] + 1 ) : $sef;
    }

    $s2 = 'UPDATE lumonata_post_type SET 
            lcustom_status = %s,
            lcustom_post = %s,
            lcustom_icon = %s,
            lsef = %s
          WHERE lcustom_id = %d';
    $q2 = $db->prepare_query( $s2, $status, $title, $icon, $sef, $id );
    $r2 = $db->do_query( $q2 );

    if( is_array( $r2 ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - Update Custom Post Icon
| -----------------------------------------------------------------------------
*/
function update_custom_post_icon( $id, $icon )
{
    global $db;
    
    $s = 'UPDATE lumonata_post_type SET lcustom_icon = %s WHERE lcustom_id = %d';
    $q = $db->prepare_query( $s, $icon, $id );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - Update Custom Post Status
| -----------------------------------------------------------------------------
*/
function update_custom_post_status( $id, $status )
{
    global $db;
    
    $s = 'UPDATE lumonata_post_type SET lcustom_status = %s WHERE lcustom_id = %d';
    $q = $db->prepare_query( $s, $status, $id );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - Update Custom Post Order
| -----------------------------------------------------------------------------
*/
function update_custom_post_order( $id, $order )
{
    global $db;

    $s = 'UPDATE lumonata_post_type SET lorder = %d WHERE lcustom_id = %d';
    $q = $db->prepare_query( $s, $order, $id );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - Delete
| -----------------------------------------------------------------------------
*/
function delete_custom_post( $id )
{
    global $db;

	$s = 'DELETE FROM lumonata_post_type WHERE lcustom_id = %d';
    $q = $db->prepare_query( $s, $id );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - Action Button
| -----------------------------------------------------------------------------
*/
function get_custom_post_admin_button( $new_url = '', $is_form = false )
{
    if( $is_form )
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=save_draft' ) . '</li>
            <li>' . button( 'button=cancel', $new_url ) . '</li>
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=publish' ) . '</li>  
            <li>' . button( 'button=save_draft' ) . '</li>                    
            <li>' . button( 'button=cancel', $new_url ) . '</li>  
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
    }
    else
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>
            <li>' . button( 'button=publish&type=submit&enable=false' ) . '</li>
            <li>' . button( 'button=unpublish&type=submit&enable=false' ) . '</li>';
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - View Option
| -----------------------------------------------------------------------------
*/
function get_custom_post_view_option()
{
    $opt_viewed   = '';    
    $show_data    = isset( $_POST[ 'data_to_show' ] ) ? $_POST[ 'data_to_show' ] : ( isset( $_GET[ 'data_to_show' ] ) ? $_GET[ 'data_to_show' ] : '' );
    $data_to_show = array( 'all' => 'All', 'publish' => 'Publish', 'unpublish' => 'Unpublish', 'draft' => 'Draft' );
    
    foreach( $data_to_show as $key => $val )
    {
        if( isset( $show_data ) )
        {
            if( $show_data == $key )
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" checked="checked" />
                <label>' . $val . '</label>';
            }
            else
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" />
                <label>' . $val . '</label>';
            }
        }
        elseif( $key == 'all' )
        {
            $opt_viewed .= '
            <input type="radio" name="data_to_show" value="' . $key . '" checked="checked" />
            <label>' . $val . '</label>';
        }
        else
        {
            $opt_viewed .= '
            <input type="radio" name="data_to_show" value="' . $key . '" />
            <label>' . $val . '</label>';
        }
    }

    return $opt_viewed;
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_custom_post_ajax_url()
{
    return get_state_url( 'ajax&apps=custom-post' );
}

/*
| -----------------------------------------------------------------------------
| Custom Post Configuration
| -----------------------------------------------------------------------------
*/
function config_admin_custom_post()
{
	global $db;

	$r = fetch_custom_post( null, false );

	if( !is_array( $r ) )
	{
        $post_type = array();
        $post_icon = array();
        $post_arry = array();

		while( $d = $db->fetch_array( $r ) )
		{
            $type = array( $d['lsef'] => $d['lcustom_post'] );
			$tabs = array( 
				'blogs'      => $d['lcustom_post'], 
				'categories' => 'Categories', 
				'setting'    => 'Setting'
			);

            $post_icon[ $d['lsef'] ] = $d['lcustom_icon'];
            $post_arry[ $d['lsef'] ] = $d['lcustom_post'];

            array_push( $post_type, $type );

		    add_main_menu( $type );

		    add_privileges( 'administrator', $d['lsef'], 'request' );
		    add_privileges( 'administrator', $d['lsef'], 'insert' );
		    add_privileges( 'administrator', $d['lsef'], 'update' );
		    add_privileges( 'administrator', $d['lsef'], 'delete' );

		    add_actions( $d['lsef'] . '_additional_field', 'custom_post_data_apps_func', $d['lsef'] );
		    add_actions( $d['lsef'] . '_additional_delete', 'custom_post_delete_apps_func', $d['lsef'] );
            add_actions( $d['lsef'] . '_rule_additional_field', 'custom_post_rule_data_apps_func', $d['lsef'] );

            add_actions( $d['lsef'] . '_setting_additional_field', 'custom_post_data_apps_func', $d['lsef'] . '_setting' );
            add_actions( $d['lsef'] . '_setting_additional_delete', 'custom_post_delete_apps_func', $d['lsef'] . '_setting' );

		    add_actions( $d['lsef'], 'get_admin_article', $d['lsef'], $d['lcustom_post'] . '|' . $d['lcustom_post'], $tabs, true, false);

		    add_actions( 'plugin_menu_set', '<option value="' . $d['lsef'] . '">' . $d['lcustom_post'] . '</option>' );

		    add_actions( 'setting_article_extends', 'custom_post_setting_form', $tabs );
		}
        
        add_actions( 'plugin_menu_post_type', 'set_plugin_menu_post_type', $post_arry );
        add_actions( 'css_elements', 'custom_post_menu_icon_func', $post_icon );
	}
}

function set_plugin_menu_post_type( $post_arry = array() )
{
    return $post_arry;
}

function custom_post_menu_icon_func( $icons )
{
    $style = '';

    foreach( $icons as $type => $icon )
    {
        if( empty( $icon ) )
        {
            $style .= '.lumonata_menu ul li.' . $type . '{ background-image:url(' . template_url( 'images/ico-themes.png', true ) . '); }';
        }
        else
        {
            $style .= '.lumonata_menu ul li.' . $type . '{ background-image:url(' . site_url( $icon ) . '); }';
        }
    }

    if( empty( $style ) === false )
    {
        return '
        <style>
            ' . $style . '
        </style>';
    }
}

function custom_post_menu_func( $apps )
{
    return $apps;
}

function custom_post_data_apps_func( $apps )
{
	return attemp_actions( $apps . '_additional_data_content' );
}

function custom_post_delete_apps_func( $apps )
{
	return attemp_actions( $apps . '_additional_data_delete' );
}

function custom_post_rule_data_apps_func( $apps )
{
    return attemp_actions( $apps . '_rule_additional_data_content' );
}

function custom_post_setting_form( $tabs )
{
    extract( $_GET );

    $d = fetch_articles( 'type=' . $state . '-setting' );

    $post_id      = isset( $d['larticle_id'] ) ? $d['larticle_id'] : time();
    $post_title   = isset( $d['larticle_title'] ) ? $d['larticle_title'] : '';
    $post_content = isset( $d['larticle_content'] ) ? $d['larticle_content'] : '';

    run_custom_post_setting_actions( $d, $state );

    set_template( TEMPLATE_PATH . '/template/custom-post-setting-form.html', 'custom-post' );

    add_block( 'form-language-btn-block', 'flb-block', 'custom-post' );
    add_block( 'form-language-loop-block', 'fll-block', 'custom-post' );
    add_block( 'form-block', 'f-block', 'custom-post' );

    add_variable( 'site_url', site_url() );
    add_variable( 'imgs_url', get_theme_img() );
    add_variable( 'ajax_url', get_article_ajax_url());

    add_variable( 'id', $post_id );
    add_variable( 'title', rem_slashes( $post_title ) );
    add_variable( 'button', button( 'button=publish' ) );
    add_variable( 'content_lang_css', get_language_content_css() );
    add_variable( 'additional_data', attemp_actions( $state . '_setting_additional_field' ) );
    add_variable( 'textarea', textarea( 'post[0]', 0, rem_slashes( $post_content ), $post_id ) );
    
    language_field( $post_id );

    add_actions( 'section_title', $tabs['blogs'] );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/css/select2.min.css' );

    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/js/select2.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/tinymce@4.9.11/tinymce.min.js' );
    add_actions( 'js_elements', 'get_javascript', 'tinymce' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'custom-post' );
}

function run_custom_post_setting_actions( $data = array(), $appname = '' )
{
    global $db;

    if( is_publish() )
    {
        $temp_id   = $_POST[ 'post_id' ][ 0 ];
        $title     = $_POST[ 'title' ][ 0 ];
        $content   = $_POST[ 'post' ][ 0 ];
        $type      = $appname . '-setting';
        $post_date = date('Y-m-d H:i:s');
        $comments  = 'not_allowed';
        $status    = 'publish';
        $share_opt = null;
        $sef_box   = null;

        if( empty( $data ) )
        {
            //-- Save the article
            save_article( $title, $content, $status, $type, $post_date, $comments, $sef_box, $share_opt );
            
            //-- Syncronize with the attachment data if user do upload files
            //-- and syncronize additional field
            $post_id = $db->insert_id();

            attachment_sync( $temp_id, $post_id );

            additional_field_sync( $temp_id, $post_id );
        }
        else
        {
            //-- Update the article
            $post_id = $data['larticle_id'];

            update_article( $post_id, $title, $content, $status, $type, $post_date, $comments, $share_opt );
        }
            
        //-- Insert additional fields
        if( isset( $_POST[ 'additional_fields' ] ) )
        {
            foreach( $_POST[ 'additional_fields' ] as $key => $val )
            {
                if( $key == 'custom_field' )
                {
                    $field = array();

                    foreach( $val as $subkey => $subval )
                    {
                        foreach( $subval[ 'type' ] as $i => $type_dt )
                        {
                            $field[] = array( 'type' => $type_dt, 'label' => $subval[ 'label' ][ $i ], 'value' => $subval[ 'value' ][ $i ] );
                        }
                    }

                    edit_additional_field( $post_id, $key, json_encode( $field, JSON_HEX_QUOT | JSON_HEX_TAG ), $type );
                }
                else
                {
                    foreach( $val as $subkey => $subval )
                    {
                        $subval = is_array( $subval ) ? json_encode( $subval ) : $subval;

                        edit_additional_field( $post_id, $key, $subval, $type );
                    }
                }
            }
        }

        header( 'location:' . get_state_url( $appname . '&tab=setting' ) );

        exit;
    }
}

function upload_custom_post_icon()
{
    $data   = array();
    $folder = upload_folder_name();

    if( isset( $_FILES[ 'file_name' ]['error'] ) && $_FILES[ 'file_name' ]['error'] == 0 )
    {
        if( !defined( 'FILES_LOCATION' ) )
        {
            define( 'FILES_LOCATION', '/lumonata-content/files' );
        }

        if( is_dir( FILES_PATH .'/'. $folder ) === FALSE )
        {
            if( create_dir( FILES_PATH .'/'. $folder ) === FALSE )
            {
                return $data;
            }
        }

        $file_name   = $_FILES[ 'file_name' ][ 'name' ];
        $file_size   = $_FILES[ 'file_name' ][ 'size' ];
        $file_type   = $_FILES[ 'file_name' ][ 'type' ];
        $file_source = $_FILES[ 'file_name' ][ 'tmp_name' ];

        extract( $_POST );

        if( is_allow_file_type( $file_type, 'image' ) )
        {
            if( is_allow_file_size( $file_size ) )
            {
                $default_title   = file_name_filter( $file_name );
                $file_name       = character_filter( $file_name );
                $file_name       = file_name_filter( $file_name ) . '-' . time() . file_name_filter( $file_name, true );

                $file_location    = FILES_LOCATION . '/' . $folder . '/' . $file_name;
                $file_destination = FILES_PATH . '/' . $folder . '/' . $file_name;

                if( upload( $file_source, $file_destination ) )
                {
                    if( empty( $custom_id ) === false )
                    {                            
                        $odata = fetch_custom_post( 'id=' . $custom_id );

                        if( update_custom_post_icon( $custom_id, $file_location ) )
                        {
                            if( isset( $odata['lcustom_icon'] ) && empty( $odata['lcustom_icon'] ) === false && file_exists( base_url( $odata['lcustom_icon'] ) ) )
                            {
                                unlink( base_url( $odata['lcustom_icon'] ) );
                            }
                        }
                    }
                    elseif( isset( $custom_icon ) && $custom_icon != 'undefined' )
                    {
                        if( update_custom_post_icon( $custom_id, $file_location ) )
                        {
                            if( empty( $custom_icon ) === false && file_exists( base_url( $custom_icon ) ) )
                            {
                                unlink( base_url( $custom_icon ) );
                            }
                        }
                    }

                    return $file_location;
                }
            }
        }
    }
}

function delete_custom_post_icon( $custom_id, $custom_icon )
{
    if( empty( $custom_id ) && isset( $custom_icon ) && $custom_icon != 'undefined' )
    {
        if( empty( $custom_icon ) === false && file_exists( base_url( $custom_icon ) ) )
        {
            if( unlink( base_url( $custom_icon ) ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        $data = fetch_custom_post( 'id=' . $custom_id );

        if( update_custom_post_icon( $custom_id, '' ) )
        {
            if( isset( $data['lcustom_icon'] ) && empty( $data['lcustom_icon'] ) === false && file_exists( base_url( $data['lcustom_icon'] ) ) )
            {
                if( unlink( base_url( $data['lcustom_icon'] ) ) )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}

function language_field( $post_id )
{    
    //-- Set Language Form
    $use_multi_lang = get_meta_data( 'multi_language' );
    $lang_list_data = get_all_language();
    
    if( $use_multi_lang == 1 && empty( $lang_list_data ) === FALSE )
    {
        $current_lang = '';

        foreach( $lang_list_data as $d )
        {
            extract( $d );

            $larticle_title   = get_additional_field( $post_id, 'larticle_title_' . $llanguage_code, 'landing' );
            $larticle_content = get_additional_field( $post_id, 'larticle_content_' . $llanguage_code, 'landing' );

            add_variable( 'llanguage', $llanguage );
            add_variable( 'llanguage_index', $index );
            add_variable( 'llanguage_code', $llanguage_code );
            add_variable( 'llanguage_btn_cls', $ldefault == 1 ? 'active' : '' );
            add_variable( 'llanguage_content_css', 'content-lang ' . $llanguage_code );            
            add_variable( 'llanguage_additional_data', attemp_actions( 'language_additional_data_' . $llanguage_code ) );

            add_variable( 'llanguage_larticle_title', $larticle_title );
            add_variable( 'llanguage_textarea', textarea( 'additional_fields[larticle_content_' . $llanguage_code . '][0]', 0, $larticle_content, $post_id, true, false ) );

            add_variable( 'template_url', TEMPLATE_URL );
                
            parse_template( 'form-language-btn-block', 'flb-block', true );

            if( $ldefault == 1 )
            {
                $current_lang = $llanguage_code;

                continue;
            }

            parse_template( 'form-language-loop-block', 'fll-block', true );
        }

        add_variable( 'conten_css', 'content-lang ' . $current_lang );
    }
    else
    {
        add_variable( 'btn_cls', 'hidden' );
    }
}

function get_custom_post_icon_content( $lcustom_icon )
{
    if( empty( $lcustom_icon ) === false )
    {
        return '
        <div class="box">
            <img src="' . site_url( $lcustom_icon ) . '" />
            <input type="text" class="hidden" name="lcustom_icon" value="' . $lcustom_icon . '" autocomplete="off" />
            <div class="overlay">
                <a class="img-delete" title="Delete">
                    <img src="' . get_theme_img() . '/delete_hover.png">
                </a>
            </div>
        </div>';
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Custom Post - Ajax Function
| -----------------------------------------------------------------------------
*/
function custom_post_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'load-custom-post-data' )
    {
        $data = get_custom_post_list_query();

        echo json_encode( $data );
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'delete-custom-post' )
    {
        if( is_delete( 'custom-post' ) )
        {
            if( delete_custom_post( $_POST['id'] ) )
            {
                $result = array( 'result' => 'success' );
            }
            else
            {
                $result = array( 'result' => 'failed' );
            }
        }
        else
        {
            $result = array( 'result' => 'failed' );
        }

        echo json_encode( $result );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'upload-custom-post-icon' )
    {
        $icon = upload_custom_post_icon();

        if( empty( $icon ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'success', 'content' => get_custom_post_icon_content( $icon ) ) );
        }
    }

    if( isset( $_POST['pkey'] ) && $_POST[ 'pkey' ] == 'delete-custom-post-icon' )
    {
        if( delete_custom_post_icon( $_POST['custom_id'], $_POST['custom_icon'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['pkey'] ) and $_POST['pkey'] == 'reorder-data' )
    {
        $result = array( 'result' => 'failed' );

        if( !empty( $_POST['reorder'] ) )
        {
            foreach( $_POST['reorder'] as $id => $order )
            {
                if( update_custom_post_order( $id, $order ) )
                {
                    $result = array( 'result' => 'success' );
                }
            }
        }

        echo json_encode( $result );
    }

    exit;
}