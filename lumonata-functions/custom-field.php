<?php

add_actions( 'custom-field_admin_page', 'custom_field_ajax' );
add_actions( 'custom-field-config', 'config_admin_custom_field' );

function custom_font_icon()
{
	$def_url = template_url( 'font/default/selection.json', true );
	$icm_url = template_url( 'font/icomoon/selection.json', true );

	$default = curl_get( $def_url );
	$icomoon = curl_get( $icm_url );

	$default = json_decode( $default, true );
	$icomoon = json_decode( $icomoon, true );

	$options = array();

	if( isset( $default[ 'icons' ] ) )
    {
    	foreach( $default[ 'icons' ] as $d )
    	{
    		$options[] = $default[ 'preferences' ][ 'fontPref' ][ 'prefix' ] . $d[ 'properties' ][ 'name' ];
    	}
    }

	if( isset( $icomoon[ 'icons' ] ) )
    {
    	foreach( $icomoon[ 'icons' ] as $d )
    	{
    		$options[] = $icomoon[ 'preferences' ][ 'fontPref' ][ 'prefix' ] . $d[ 'properties' ][ 'name' ];
    	}
    }

    return $options;
}

function config_admin_custom_field()
{
	global $db;

	if( isset( $_GET['tab'] ) && $_GET['tab'] == 'setting' )
	{
		$post_st = fetch_articles( 'type=' . $_GET['state'] . '-setting' );
		$post_id = isset( $post_st['larticle_id'] ) ? $post_st['larticle_id'] : 0;
	}
    else
    {
    	$post_id = isset( $_GET['id'] ) ? $_GET['id'] : 0;
    }

	$s = 'SELECT * FROM lumonata_custom_field AS a WHERE a.lcustom_field_status = %s ORDER BY a.lorder';
	$q = $db->prepare_query( $s, 'publish' );
	$r = $db->do_query( $q );

	if( !is_array($r ) )
	{
		//-- Get Multi Language
		$use_multi_lang = get_meta_data( 'multi_language' );
		$lang_list_data = get_all_language();

		while( $d = $db->fetch_array( $r ) )
		{
			$group = $d['lcustom_field_name'];
			$types = $d['lcustom_field_type'];
			$field = json_decode( $d['lcustom_field_value'], true );

			if( $field !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$post_type = json_decode( $d['lcustom_field_apply_on'], true );
				$param     = array( 'group' => $group, 'field' => $field, 'types' => $types, 'fonts' => custom_font_icon() );

				if( $post_type !== null && json_last_error() === JSON_ERROR_NONE )
				{
					foreach( $post_type as $dt )
					{
						$apps = fetch_custom_post( 'id=' . $dt, true );

						if( $types == '0' )
						{
							add_actions( $apps['lsef'] . '_additional_data_content', 'init_custom_field', 0, $post_id, $apps['lsef'], $param );
						}
						else
						{
							add_actions( $apps['lsef'] . '_rule_additional_data_content', 'init_custom_field', 0, $post_id, $apps['lsef'], $param );
						}
					}
				}

				$post_type_setting = json_decode( $d['lcustom_field_apply_on_setting'], true );

				if( $post_type_setting !== null && json_last_error() === JSON_ERROR_NONE )
				{
					foreach( $post_type_setting as $dt )
					{
						$apps = fetch_custom_post( 'id=' . $dt, true );

						add_actions( $apps['lsef'] . '_setting_additional_data_content', 'init_custom_field', 0, $post_id, $apps['lsef'] . '-setting', $param );
					}
				}

				if( $d['lcustom_field_apply_on_pages'] )
				{
					add_actions( 'pages_additional_field', 'init_custom_field', 0, $post_id, 'pages', $param );

					if( $use_multi_lang == 1 && empty( $lang_list_data ) === FALSE )
					{
					    foreach( $lang_list_data as $dl )
					    {
							add_actions( 'pages_additional_field_' . $dl['llanguage_code'], 'init_custom_field', 0, $post_id, 'pages', $param,  '_' . $dl['llanguage_code'] );
					    }
					}
				}

				if( $d['lcustom_field_apply_on_blogs'] )
				{
					if( $types == '0' )
					{
						add_actions( 'blogs_additional_field', 'init_custom_field', 0, $post_id, 'blogs', $param );
					}
					else
					{
						add_actions( 'blogs_rule_additional_field', 'init_custom_field', 0, $post_id, 'blogs', $param );
					}

					if( $use_multi_lang == 1 && empty( $lang_list_data ) === FALSE )
					{
					    foreach( $lang_list_data as $dl )
					    {
							if( $types == '0' )
							{
								add_actions( 'blogs_additional_field_' . $dl['llanguage_code'], 'init_custom_field', 0, $post_id, 'blogs', $param,  '_' . $dl['llanguage_code'] );
							}
							else
							{
								add_actions( 'blogs_rule_additional_field_' . $dl['llanguage_code'], 'init_custom_field', 0, $post_id, 'blogs', $param,  '_' . $dl['llanguage_code'] );
							}
					    }
					}
				}

				$app_id = json_decode( $d['lapp_id'], true );

				if( $app_id !== null && json_last_error() === JSON_ERROR_NONE )
				{
					foreach( $app_id as $dt )
					{
						if( $dt == $post_id )
						{
							$apps = fetch_articles( 'id=' . $dt, true );

							if( $apps['larticle_type'] == 'pages' )
							{
								add_actions( 'pages_additional_field', 'init_custom_field', 0, $post_id, 'pages', $param );

								if( $use_multi_lang == 1 && empty( $lang_list_data ) === FALSE )
								{
								    foreach( $lang_list_data as $dl )
								    {
										add_actions( 'pages_additional_field_' . $dl['llanguage_code'], 'init_custom_field', 0, $post_id, 'pages', $param,  '_' . $dl['llanguage_code'] );
								    }
								}
							}
							elseif( $apps['larticle_type'] == 'blogs' )
							{
								if( $types == '0' )
								{
									add_actions( 'blogs_additional_field', 'init_custom_field', 0, $post_id, 'blogs', $param );
								}
								else
								{
									add_actions( 'blogs_rule_additional_field', 'init_custom_field', 0, $post_id, 'blogs', $param );
								}

								if( $use_multi_lang == 1 && empty( $lang_list_data ) === FALSE )
								{
								    foreach( $lang_list_data as $dl )
								    {
										if( $types == '0' )
										{
											add_actions( 'blogs_additional_field_' . $dl['llanguage_code'], 'init_custom_field', 0, $post_id, 'blogs', $param,  '_' . $dl['llanguage_code'] );
										}
										else
										{
											add_actions( 'blogs_rule_additional_field_' . $dl['llanguage_code'], 'init_custom_field', 0, $post_id, 'blogs', $param,  '_' . $dl['llanguage_code'] );
										}
								    }
								}
							}
							else
							{
								if( $types == '0' )
								{
									add_actions( $apps['larticle_type'] . '_additional_data_content', 'init_custom_field', 0, $post_id, $apps['larticle_type'], $param );
								}
								else
								{
									add_actions( $apps['larticle_type'] . '_rule_additional_data_content', 'init_custom_field', 0, $post_id, $apps['larticle_type'], $param );
								}
							}
						}
					}
				}
			}
		}

	    add_actions( 'js_elements', 'get_javascript', 'jquery.mjs.nestedSortable.js' );
	}
}

function init_custom_field( $i, $post_id, $apps, $param = array(), $code_lang = '' )
{
    if( is_array( $param ) && !empty( $param ) )
    {
    	if( array_key_exists( 'group', $param ) === false )
    	{
    		$content = '';

    		foreach( $param as $idx => $prm )
    		{
    			$content .= init_custom_field( $i, $post_id, $apps, $prm, $code_lang );
    		}

    		return $content;
    	}

    	//-- Filter additional field if use language
    	if( empty( $code_lang ) === false )
    	{
    		$param[ 'field' ] = array_filter( $param[ 'field' ], function($x) { return in_array( $x['type'], array( 'textbox', 'textarea' ) ); });
    	}

    	if( empty( $param[ 'field' ] ) === false )
    	{
	        $content = '
	        <fieldset>
		        <div class="additional_data">
		            <h2>' . $param[ 'group' ] . '</h2>
		            <div class="additional_content">
		                <div class="boxs-wrapp">
		                    <div class="boxs">';
		        
		                        foreach( $param[ 'field' ] as $idx => $obj )
		                        {
		                            $class_group = '';

		                            if( isset( $obj[ 'class' ] ) )
		                            {
		                                $class_group = $obj[ 'class' ];
		                            }

		                            if( is_array( $obj[ 'name' ] ) === false )
		                            {
					                	if( $param[ 'types' ] == '0' )
									    {
		                                	$data = get_additional_field( $post_id, $obj[ 'name' ] . $code_lang, $apps );
		                                }
		                                else
		                                {
		                                	$data = get_rule_additional_field( $post_id, $obj[ 'name' ] . $code_lang, $apps );
		                                }
		                            }

		                            $pholder = isset( $obj[ 'placeholder' ] ) ? 'placeholder="' . $obj[ 'placeholder' ] . '"' : '';
		                            
		                            if( $obj[ 'type' ] == 'textbox' )
		                            {
		                                $content .= '
	                                	<div class="field-wrapp textbox-wrapp">
		                                	<p ' . ( empty( $obj[ 'label' ] ) ? 'class="hidden"' : '' ) . '>' . $obj[ 'label' ] . '</p>
	                                		<input type="text" class="textbox ' . $class_group . '" name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . ']" autocomplete="off" value="' . $data . '" ' . $pholder . ' />
	                                	</div>';
		                            }
		                            elseif( $obj[ 'type' ] == 'number' )
		                            {
		                                $content .= '
	                                	<div class="field-wrapp textbox-wrapp">
		                                	<p ' . ( empty( $obj[ 'label' ] ) ? 'class="hidden"' : '' ) . '>' . $obj[ 'label' ] . '</p>
	                                		<input type="number" class="textbox ' . $class_group . '" name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . ']" autocomplete="off" value="' . $data . '" ' . $pholder . ' />
	                                	</div>';
		                            }
		                            elseif( $obj['type'] == 'select' )
		                            {
		                                if( empty( $obj[ 'options' ] ) === false )
		                                {
		                                    $content .= '
		                                	<div class="field-wrapp select-wrapp">
		                                		<p ' . ( empty( $obj[ 'label' ] ) ? 'class="hidden"' : '' ) . '>' . $obj[ 'label' ] . '</p>
			                                    <select autocomplete="off" name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . ']" style="min-width:250px;">';

			                                        foreach( $obj[ 'options' ] as $idx => $dt )
			                                        {
			                                            $content .= '<option value="' . $idx . '" ' . ( $data == $idx ? 'selected' : '' ) . '>' . $dt . '</option>';
			                                        }

			                                        $content .= '
			                                    </select>
			                                </div>';
		                                }
		                            }
		                            elseif( $obj['type'] == 'date-range' )
		                            {
		                                $content .= '
		                                <div class="field-wrapp date-range-wrapp">
		                                	<p ' . ( empty( $obj[ 'label' ] ) ? 'class="hidden"' : '' ) . '>' . $obj[ 'label' ] . '</p>';

		                                    if( is_array( $obj[ 'name' ] ) )
		                                    {
		                                        foreach( $obj[ 'name' ] as $name )
		                                        {
								                	if( $param[ 'types' ] == '0' )
												    {
		                                            	$data = get_additional_field( $post_id, $name . $code_lang, $apps );
		                                            }
		                                            else
		                                            {
		                                				$data = get_rule_additional_field( $post_id, $name . $code_lang, $apps );
		                                            }

		                                            $content .= '<input type="text" class="textbox date-range ' . $name . ' ' . $class_group . '" name="additional_fields[' . $name . $code_lang . '][' . $i . ']" autocomplete="off" value="' . $data . '" ' . $pholder . ' />';
		                                        }
		                                    }

		                                    $content .= '
			                            </div>';
		                            }
		                            elseif( $obj['type'] == 'date' )
		                            {
		                                $field = '
		                                <div class="field-wrapp date-wrapp">
		                                	<p ' . ( empty( $obj[ 'label' ] ) ? 'class="hidden"' : '' ) . '>' . $obj[ 'label' ] . '</p>
		                                	<input type="text" class="textbox date ' . $obj[ 'name' ] . ' ' . $class_group . '" name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . ']" autocomplete="off" value="' . $data . '" ' . $pholder . ' />
			                            </div>';
		                            }
		                            elseif( $obj['type'] == 'textarea' )
		                            {
		                                $content .= '
		                                <div class="field-wrapp textarea-wrapp">
		                                	<p ' . ( empty( $obj[ 'label' ] ) ? 'class="hidden"' : '' ) . '>' . $obj[ 'label' ] . '</p>
		                                	<textarea class="textarea ' . $class_group . '" name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . ']" autocomplete="off" rows="10">' . $data . '</textarea>
		                                </div>';
		                            }
		                            elseif( $obj['type'] == 'tinymce' )
		                            {
		                                $content .= '
		                                <div class="field-wrapp textarea-wrapp">
		                                	<p ' . ( empty( $obj[ 'label' ] ) ? 'class="hidden"' : '' ) . '>' . $obj[ 'label' ] . '</p>
		                                	<textarea class="tinymce textarea ' . $class_group . '" name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . ']" autocomplete="off" rows="10">' . $data . '</textarea>
		                                </div>';
		                            }
		                            elseif( $obj['type'] == 'upload-img' )
		                            {
		                            	$att_name = generateSefUrl( implode( ' ', explode( '_', $obj[ 'name' ] ) ) ) . '-' . $apps;
		                                $content .= '                                
							            <div class="field-wrapp upload-img-wrapp">
		                                	<p ' . ( empty( $obj[ 'label' ] ) ? 'class="hidden"' : '' ) . '>' . $obj[ 'label' ] . '</p>
							                <div class="upload-img clearfixed">';

							                	if( $param[ 'types' ] == '0' )
											    {
											    	$featured = get_attachment_by_app_name( $post_id, $att_name, false );
											    }
											    else
											    {
											    	$featured = get_rule_attachment_by_app_name( $post_id, $att_name, false );
											    }

											    if( !empty( $featured ) )
											    {
										            $content .= '
										            <div class="box">
										                <img src="' . site_url() . $featured['lattach_loc_medium'] . '" />
										                <div class="overlay">
										                    <a data-post-id="' . $post_id . '" class="img-delete" title="Delete">
										                        <img src="' . get_theme_img() . '/delete_hover.png">
										                    </a>
										                </div>
										            </div>';
											    }

										    	$content .= '
										    </div>
							                <input type="file" name="' . $obj[ 'name' ] . $code_lang . '" class="upload-img-button" data-attname="' . $att_name . '" autocomplete="off" />
							            </div>';
		                            }
		                            elseif( $obj['type'] == 'upload-file' )
		                            {
		                            	$att_name = generateSefUrl( implode( ' ', explode( '_', $obj[ 'name' ] ) ) ) . '-' . $apps;
		                                $content .= '                                
							            <div class="field-wrapp upload-file-wrapp">
		                                	<p ' . ( empty( $obj[ 'label' ] ) ? 'class="hidden"' : '' ) . '>' . $obj[ 'label' ] . '</p>
							                <div class="upload-file clearfixed">';

							                	if( $param[ 'types' ] == '0' )
											    {
											    	$featured = get_attachment_by_app_name( $post_id, $att_name, false );
											    }
											    else
											    {
											    	$featured = get_rule_attachment_by_app_name( $post_id, $att_name, false );
											    }

											    if( !empty( $featured ) )
											    {
										            $content .= '
										            <div class="box">
										                <img src="' . get_theme_img() . '/ico-upload-' . $obj['allowed'] . '.svg" />
										                <div class="overlay">
										                    <a data-post-id="' . $post_id . '" class="file-delete" title="Delete">
										                        <img src="' . get_theme_img() . '/delete_hover.png">
										                    </a>
										                </div>
										            </div>';
											    }

										    	$content .= '
										    </div>
							                <input type="file" name="' . $obj[ 'name' ] . $code_lang . '" class="upload-file-button" data-attname="' . $att_name . '" data-allowed="' . $obj['allowed'] . '" autocomplete="off" />
							            </div>';
		                            }
		                            elseif( $obj['type'] == 'upload-gallery' )
		                            {
		                            	$att_name = generateSefUrl( implode( ' ', explode( '_', $obj[ 'name' ] ) ) ) . '-' . $apps;
		                            	$site_url = site_url();
		                                $content .= '   
							            <div class="field-wrapp upload-gallery-wrapp">
		                                	<p ' . ( empty( $obj[ 'label' ] ) ? 'class="hidden"' : '' ) . '>' . $obj[ 'label' ] . '</p>
							                <div id="block-gallery-' . $att_name . '-' . $post_id . '" class="block-gallery" data-attname="' . $att_name . '">
							                    <div id="dropzone-' . $att_name . '-' . $post_id . '">
							                        <div class="dropzone needsclick dz-clickable" id="gallery-upload-' . $att_name . '-' . $post_id . '">
							                            <div class="dz-message needsclick">
							                                Drop files here or click to upload.<br>
							                                <span class="note needsclick">( maximum size for upload image is 2MB )</span>
							                            </div>
							                        </div>
							                    </div>

							                    <div id="preview-template-' . $att_name . '-' . $post_id . '" class="dz-preview dz-file-preview" style="display:none;">
							                        <div class="dz-preview dz-file-preview">
							                            <div class="dz-image"><img data-dz-thumbnail data-dz-thumbnail-id/></div>
							                            <div class="dz-details">
							                                <a class="dz-edit">
							                                    <img src="' . get_theme_img() . '/ico-edit.png" alt="Edit" />
							                                </a>
							                                <a class="dz-delete">
							                                    <img src="' . get_theme_img() . '/ico-delete.png" alt="delete" />
							                                </a>
							                            </div>
							                            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
							                           	<div class="dz-error-message"><span data-dz-errormessage></span></div>
							                            <div class="dz-detail-box">
							                                <div class="detail">
							                                    <fieldset>
							                                        <p>Image Title</p>
							                                        <input type="text" class="gallery-title textbox" />
							                                    </fieldset>
							                                    <fieldset>
							                                        <p>Image Link</p>
							                                        <input type="text" class="gallery-alt textbox" />
							                                    </fieldset>
							                                    <fieldset>
							                                        <p>Image Caption</p>
							                                        <input type="text" class="gallery-caption textbox" />
							                                    </fieldset>
							                                    <fieldset>
							                                        <p>Image Content</p>
							                                        <textarea cols="95" rows="20" class="gallery-content textbox gtiny_mce" autocomplete="off"></textarea>
							                                    </fieldset>
							                                    <fieldset>
							                                        <input type="button" class="save-detail" value="Save"/>
							                                        <input type="button" class="close-detail" value="Close"/>
							                                    </fieldset>
							                                </div>
							                            </div>
							                        </div>
							                    </div>
							                </div>
							                <p class="note">Recommended resolution for sideshow image is <b>' . $obj[ 'res' ] . '</b>, with maximum upload size <b>2MB</b></p>
							            </div>';
		                            }
		                            elseif( $obj['type'] == 'post-link' )
		                            {
		                            	$options  = post_list_option( $data, ( isset( $obj['post'] ) ? $obj['post'] : null ) );
	                                    $content .= '
	                                	<div class="field-wrapp select-wrapp">
		                                	<p ' . ( empty( $obj[ 'label' ] ) ? 'class="hidden"' : '' ) . '>' . $obj[ 'label' ] . '</p>';

	                                		if( isset( $obj['multiple'] ) && $obj['multiple'] )
	                                		{
	                                			$content .= '
	                                            <select name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . '][]" class="slide-post-link-id" style="width:100%;" multiple>
	                                                ' . $options . '
	                                            </select>';
	                                		}
	                                		else
	                                		{
	                                			$content .= '
	                                            <select name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . '][]" class="slide-post-link-id" style="width:100%;">
	                                                ' . $options . '
	                                            </select>';
	                                		}

	                                		$content .= '
		                                </div>';
                                    }
		                            elseif( $obj['type'] == 'maps' )
		                            {
		                            	$dcoordinate = empty( $data ) ? array( -8.6794999, 115.1580949 ) : explode( ',', trim( $data ) );
            							$gmapkey     = get_meta_data( 'map_api_key' );

	                                    $content .= '
	                                	<div class="field-wrapp select-wrapp">
		                                	<p>Longitude / Latitude : </p>
	                                		<input type="text" class="textbox map-coordinate" name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . ']" autocomplete="off" value="' . $data . '" />	                                		
		                                </div>
	                                	<div class="field-wrapp select-wrapp">
		                                	<p ' . ( empty( $obj[ 'label' ] ) ? 'class="hidden"' : '' ) . '>' . $obj[ 'label' ] . '</p>
				                            <input type="hidden" class="textbox" name="default_latitude" value="' . $dcoordinate[0] . '">
				                            <input type="hidden" class="textbox" name="default_longitude" value="' . $dcoordinate[1] . '">
				                            <div id="map" class="map" style="height:500px;"></div>
		                                </div>';

		                                add_actions( 'other_elements', 'get_custom_javascript', '//maps.googleapis.com/maps/api/js?key=' . $gmapkey . '&callback=initmap', true, true );
		                            }
		                            elseif( $obj['type'] == 'group-field' )
		                            {
		                            	if( isset( $obj['field'] ) && is_array( $obj['field'] ) && !empty( $obj['field'] ) )
		                            	{
			                            	$content .= '
		                                	<div class="field-wrapp group-field-wrapp">
		                                		<p ' . ( empty( $obj[ 'label' ] ) ? 'class="hidden"' : '' ) . '>' . $obj[ 'label' ] . '</p>
		                                		<div class="new-field clearfix">
		                                			<span class="add">+ Add New</span>
		                                		</div>';

		                                		$data = json_decode( $data, true );

		                                		if( empty( $data ) === false && json_last_error() === JSON_ERROR_NONE )
	                                			{
				                            		$content .= '
			                                		<div class="table">
			                                			<div class="t-head">' . $param[ 'group' ] . ' List</div>
			                                			<div class="t-list">';

			                                				foreach( $data as $i2 => $d2 )
	                                						{
							                            		$content .= '	
					                                			<div class="item">
				                                					<div class="cell">
				                                						<span class="move"></span>
				                                					</div>
				                                					<div class="cell">';

					                                					foreach( $d2 as $i3 => $d3 )
					                                					{
							                            					$content .= '					                                						
					                            							<p class="hidden"><input type="text" class="textbox textbox-type" name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . '][' . $i2 . '][' . $i3 . '][type]" value="' . $d3['type'] . '" autocomplete="off" /></p>';

				                                							$flabel = isset( $d3['label'] ) ? $d3['label'] : '';
		                            										$pholde = isset( $d3['placeholder'] ) ? $d3['placeholder'] : '';

				                                							if( !empty( $d3['label'] ) )
				                                							{
				                                								$content .= '<input type="text" class="textbox textbox-label" name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . '][' . $i2 . '][' . $i3 . '][label]" value="' . $flabel . '" autocomplete="off" />';
				                                							}

				                                							if( $d3['type'] == 'textbox' )
				                                							{
				                                								$content .= '<p><input placeholder="' . $pholde . '" type="text" class="textbox" name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . '][' . $i2 . '][' . $i3 . '][value]" value="' . $d3['value'] . '" autocomplete="off" /></p>';
				                                							}
				                                							elseif( $d3['type'] == 'textarea' )
				                                							{
				                                								$content .= '<p><textarea placeholder="' . $pholde . '" rows="5" class="textbox" name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . '][' . $i2 . '][' . $i3 . '][value]">' . $d3['value'] . '</textarea></p>';
				                                							}
				                                							elseif( $d3['type'] == 'tinymce' )
				                                							{
				                                								$content .= '<p><textarea placeholder="' . $pholde . '" rows="5" class="tinymce textbox" name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . '][' . $i2 . '][' . $i3 . '][value]" value="' . $d3['value'] . '" autocomplete="off"></textarea></p>';
				                                							}
				                                							elseif( $d3['type'] == 'iconpicker' )
				                                							{
					                                							$content .= '
					                                							<p>
					                                								<select class="iconpickers" name="additional_fields[' . $obj[ 'name' ] . $code_lang . '][' . $i . '][' . $i2 . '][' . $i3 . '][value]" autocomplete="off">
					                                									<option value="">No icon</option>';

					                                									if( is_array( $param[ 'fonts' ] ) && !empty( $param[ 'fonts' ] ) )
					                                									{
					                                										foreach( $param[ 'fonts' ] as $pf )
					                                										{
					                                											$content .= '<option value="' . $pf . '" ' . ( $d3['value'] == $pf ? 'selected' : '' ) . '>' . $pf . '</option>';
					                                										}
					                                									}

					                                									$content .= '
					                                								</select>
																				</p>';
																			}
				                                							elseif( $d3['type'] == 'upload-img' )
				                                							{
	                                											$att_name = generateSefUrl( implode( ' ', explode( '_', $i3 ) ) ) . '-' . $apps . '-' . $i2;
					                                							$content .= ' 
					                                							<p>                               
																		            <div class="field-wrapp upload-img-wrapp">
					                                									<span ' . ( empty( $pholde ) ? 'class="hidden"' : '' ) . '>' . $pholde . '</span>
																		                <div class="upload-img clearfixed">';

																		                	if( $param[ 'types' ] == '0' )
																						    {
																						    	$featured = get_attachment_by_app_name( $post_id, $att_name, false );
																						    }
																						    else
																						    {
																						    	$featured = get_rule_attachment_by_app_name( $post_id, $att_name, false );
																						    }

																						    if( !empty( $featured ) )
																						    {
																					            $content .= '
																					            <div class="box">
																					                <img src="' . site_url() . $featured['lattach_loc_medium'] . '" />
																					                <div class="overlay">
																					                    <a data-post-id="' . $post_id . '" class="img-delete" title="Delete">
																					                        <img src="' . get_theme_img() . '/delete_hover.png">
																					                    </a>
																					                </div>
																					            </div>';
																						    }

																					    	$content .= '
																					    </div>
																		                <input type="file" class="upload-img-button" name="' . $i3 . $code_lang . '" class="upload-img-button" data-attname="' . $att_name . '" data-index="' . $i . '" autocomplete="off" />
																		            </div>
																				</p>';
				                                							}
					                                					}	

		                            									$content .= '
		                                							</div>
				                                					<div class="cell">
				                                						<a class="delete"></a>
				                                					</div>
				                                				</div>';
	                                						}
							                            	
							                            	$content .= '
			                                			</div>
			                                		</div>';
			                                	}
			                                	else
			                                	{
				                            		$content .= '
			                                		<div class="table hidden">
			                                			<div class="t-head">' . $param[ 'group' ] . ' List</div>
			                                			<div class="t-list"></div>
			                                		</div>';
			                                	}
				                            	
				                            	$content .= '
		                                		<div class="cloned-wrapp hidden">
		                                			<div class="item">
	                                					<div class="cell">
	                                						<span class="move"></span>
	                                					</div>
	                                					<div class="cell">';

	                                						foreach( $obj['field'] as $idx => $f )
	                                						{
	                                							$content .= '<p class="hidden"><input type="text" class="textbox textbox-type" data-parent="' . $obj[ 'name' ] . $code_lang . '" data-name="' . $f['name'] . '" data-index="' . $i . '" value="' . $f['type'] . '" autocomplete="off" disabled /></p>';

	                                							$flabel = isset( $f['label'] ) ? $f['label'] : '';
	                                							$pholde = isset( $f['placeholder'] ) ? $f['placeholder'] : '';

	                                							if( !empty( $f['label'] ) )
	                                							{
	                                								$content .= '<p><input type="text" class="textbox textbox-label" data-parent="' . $obj[ 'name' ] . $code_lang . '" data-name="' . $f['name'] . '" data-index="' . $i . '" value="' . $flabel . '" autocomplete="off" disabled /></p>';
	                                							}

	                                							if( $f['type'] == 'textbox' )
	                                							{
	                                								$content .= '<p><input placeholder="' . $pholde . '" type="text" class="textbox" data-parent="' . $obj[ 'name' ] . $code_lang . '" data-name="' . $f['name'] . '" data-index="' . $i . '" autocomplete="off" disabled /></p>';
	                                							}
	                                							elseif( $f['type'] == 'textarea' )
	                                							{
	                                								$content .= '<p><textarea placeholder="' . $pholde . '" rows="5" class="textbox" data-parent="' . $obj[ 'name' ] . $code_lang . '" data-name="' . $f['name'] . '" data-index="' . $i . '" autocomplete="off" disabled></textarea></p>';
	                                							}
	                                							elseif( $f['type'] == 'tinymce' )
	                                							{
	                                								$content .= '<p><textarea placeholder="' . $pholde . '" rows="5" class="tinymce-2 textbox" data-parent="' . $obj[ 'name' ] . $code_lang . '" data-name="' . $f['name'] . '" data-index="' . $i . '" autocomplete="off" disabled></textarea></p>';
	                                							}
	                                							elseif( $f['type'] == 'iconpicker' )
	                                							{
		                                							$content .= '
		                                							<p>
		                                								<select class="iconpicker" data-parent="' . $obj[ 'name' ] . $code_lang . '" data-name="' . $f['name'] . '" data-index="' . $i . '" autocomplete="off" disabled>
		                                									<option value="">No icon</option>';

		                                									if( is_array( $param[ 'fonts' ] ) && !empty( $param[ 'fonts' ] ) )
		                                									{
		                                										foreach( $param[ 'fonts' ] as $pf )
		                                										{
		                                											$content .= '<option value="' . $pf . '">' . $pf . '</option>';
		                                										}
		                                									}

		                                									$content .= '
		                                								</select>
																	</p>';
																}
	                                							elseif( $f['type'] == 'upload-img' )
	                                							{
	                                								$att_name = generateSefUrl( implode( ' ', explode( '_', $f[ 'name' ] ) ) ) . '-' . $apps;
									                                $content .= ' 
		                                							<p>                               
															            <div class="field-wrapp upload-img-wrapp">
		                                									<span ' . ( empty( $f[ 'placeholder' ] ) ? 'class="hidden"' : '' ) . '>' . $f[ 'placeholder' ] . '</span>
															                <div class="upload-img clearfixed">
																		    </div>
															                <input type="file" class="upload-img-button" name="' . $f[ 'name' ] . $code_lang . '" class="upload-img-button" data-name="' . $att_name . '" data-index="' . $i . '" autocomplete="off" />
															            </div>
																	</p>';
	                                							}
	                                						}

				                                			$content .= '
	                                					</div>
	                                					<div class="cell">
	                                						<a class="delete"></a>
	                                					</div>
		                                			</div>
		                                		</div>
		                                	</div>';
		                            	}
		                            }
		                        }
		                        
		                        $content .= '
		                    </div>
		                </div>
		            </div>
		        </div>
		    </fieldset>';
        
        	return $content;
        }
    }
}

function post_list_option( $post_id = array(), $post_type = array() )
{
    global $db;

    $post_id_arr = json_decode( $post_id, true );

    if( empty( $post_id_arr ) === false && json_last_error() === JSON_ERROR_NONE )
    {
    	$post_id = $post_id_arr;
    }

    $option  = '<option value="">-- Choose option --</option>';

    if( empty( $post_type ) )
    {
    	$option .= '<option value="h">Homepage</option>';
    }

    $s = 'SELECT * FROM lumonata_post_type AS a WHERE a.lcustom_status = %s ORDER BY a.lorder';
    $q = $db->prepare_query( $s, 'publish' );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $data = array();

        while( $d = $db->fetch_array( $r ) )
        {
		    if( empty( $post_type ) )
		    {
            	$data[]  = $d;

                if( ( is_array( $post_id ) && in_array( 'a|' . $d[ 'lcustom_id' ], $post_id ) ) || ( !is_array( $post_id ) && 'a|' . $d[ 'lcustom_id' ] == $post_id ) )
                {
	            	$option .= '<option selected value="a|' . $d[ 'lcustom_id' ] . '">' . $d[ 'lcustom_post' ] . ' Archive</option>';
	            }
	            else
	            {	            	
	            	$option .= '<option value="a|' . $d[ 'lcustom_id' ] . '">' . $d[ 'lcustom_post' ] . ' Archive</option>';
	            }
	       	}
	       	else
	       	{
	       		if( in_array( $d[ 'lsef' ], $post_type ) )
	       		{
            		$data[] = $d;
	       		}
	       	}
        }

        foreach( $data as $d )
        {
            $s2 = 'SELECT * FROM lumonata_articles AS a WHERE a.larticle_type = %s ORDER BY a.lorder';
            $q2 = $db->prepare_query( $s2, $d[ 'lsef' ] );
            $r2 = $db->do_query( $q2 );

            if( $db->num_rows( $r2 ) > 0 )
            {
	            $option .= '<optgroup label="' . ucfirst( $d[ 'lsef' ] ) . '">';

	            while( $d2 = $db->fetch_array( $r2 ) )
	            {
	                if( ( is_array( $post_id ) && in_array( 'd|' . $d2[ 'larticle_id' ], $post_id ) ) || ( !is_array( $post_id ) && 'd|' . $d2[ 'larticle_id' ] == $post_id ) )
	                {
	                    $option .= '<option selected value="d|' . $d2[ 'larticle_id' ] . '">' . $d2[ 'larticle_title' ] . '</option>';
	                }
	                else
	                {
	                    $option .= '<option value="d|' . $d2[ 'larticle_id' ] . '">' . $d2[ 'larticle_title' ] . '</option>';
	                }
	            }

	            $option .= '</optgroup>';
            }

            $s3 = 'SELECT * FROM lumonata_rules AS a WHERE a.lgroup = %s AND a.lrule = %s ORDER BY a.lorder';
            $q3 = $db->prepare_query( $s3, $d[ 'lsef' ], 'categories' );
            $r3 = $db->do_query( $q3 );

            if( $db->num_rows( $r3 ) > 0 )
            {
	            $option .= '<optgroup label="' . $d[ 'lcustom_post' ] . ' - Categories">';

	            while( $d3 = $db->fetch_array( $r3 ) )
	            {
	                if( ( is_array( $post_id ) && in_array( 'r|' . $d3[ 'lrule_id' ], $post_id ) ) || ( !is_array( $post_id ) && 'r|' . $d3[ 'lrule_id' ] == $post_id ) )
	                {
	                    $option .= '<option selected value="r|' . $d3[ 'lrule_id' ] . '">' . $d3[ 'lname' ] . '</option>';
	                }
	                else
	                {
	                    $option .= '<option value="r|' . $d3[ 'lrule_id' ] . '">' . $d3[ 'lname' ] . '</option>';
	                }
	            }

	            $option .= '</optgroup>';
            }
        }
    }

    $s = 'SELECT * FROM lumonata_articles AS a WHERE a.larticle_status = %s AND a.larticle_type = %s ORDER BY a.lorder';
    $q = $db->prepare_query( $s, 'publish', 'pages' );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
    	$option .= '<optgroup label="Pages">';

        while( $d = $db->fetch_array( $r ) )
        {
        	if( ( is_array( $post_id ) && in_array( 'd|' . $d[ 'larticle_id' ], $post_id ) ) || ( !is_array( $post_id ) && 'd|' . $d[ 'larticle_id' ] == $post_id ) )
            {
                $option .= '<option selected value="d|' . $d[ 'larticle_id' ] . '">' . $d[ 'larticle_title' ] . '</option>';
            }
            else
            {
                $option .= '<option value="d|' . $d[ 'larticle_id' ] . '">' . $d[ 'larticle_title' ] . '</option>';
            }
        }

        $option .= '</optgroup>';
    }

    return $option;
}

function additional_permalink( $param )
{
	$param = json_decode( $param, true );

	if( $param !== null && json_last_error() === JSON_ERROR_NONE )
	{
		if( $param[0] == '' )
		{
			return 'javascript:;';
		}
		elseif( $param[0] == 'h' )
		{
			return site_url();
		}
		else
		{
			list( $type, $id ) = explode( '|', $param[0] );

			if( $type == 'a' )
			{
				$d = fetch_custom_post( 'id=' . $id );

				if( empty( $d ) )
				{
					return 'javascript:;';
				}
				else
				{
					return site_url( $d[ 'lsef' ] );
				}
			}
			elseif( $type == 'd' )
			{
				return permalink( $id );
			}
			elseif( $type == 'r' )
			{
				return rule_permalink( $id );
			}
		}
	}
	else
	{
		return 'javascript:;';
	}
}