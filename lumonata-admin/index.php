<?php

session_start();

ob_start();

if( file_exists( '../lumonata_load.php' ) )
{
    $lumonata_admin = true;
    
    require_once( '../lumonata_load.php' );
}
else
{
	echo '<code>lumonata_load.php</code> File Not Found!';

	die();
}

?>