<?php

function get_admin_button( $url = '', $is_form = false )
{
    if( $is_form )
    {
        if( is_administrator() || is_editor() )
        {
            return '
            <li>' . button( 'button=publish' ) . '</li>
            <li>' . button( 'button=save_draft' ) . '</li>            
            <li>' . button( 'button=cancel', $url ) . '</li>
            <li>' . button( 'button=add_new', $url . '&prc=add_new' ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=add_new', $url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=save_draft&label=Save' ) . '</li>
            <li>' . button( 'button=cancel', $url ) . '</li>';
        }
    }
    else
    {
        if( is_admin_user() )
        {
            if( is_administrator() || is_editor() )
            {
                return '
                <li>' . button( 'button=add_new', $url . '&prc=add_new' ) . '</li>
                <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
            }
            else
            {
                return '
                <li>' . button( 'button=add_new', $url . '&prc=add_new' ) . '</li>
                <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
            }
        }
        else
        {
            if( is_administrator() || is_editor() )
            {
                return '
                <li>' . button( 'button=add_new', $url . '&prc=add_new' ) . '</li>
                <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>
                <li>' . button( 'button=publish&type=submit&enable=false' ) . '</li>
                <li>' . button( 'button=unpublish&type=submit&enable=false' ) . '</li>';
            }
            else
            {
                return '
                <li>' . button( 'button=add_new', $url . '&prc=add_new' ) . '</li>
                <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
            }
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Sef URL Box
| -------------------------------------------------------------------------------------
*/
function get_sef_scheme( $type = 'pages', $url = '', $i = 0, $post_id = 0, $d = null, $is_edit = false )
{
    if( $is_edit && is_permalink() )
    {
        $sef_url = $type == 'pages' ? site_url() : site_url( $type );
        $sef     = isset( $_POST['sef_box'][$i] ) ? $_POST['sef_box'][$i] : $d['lsef'];
        $more    = strlen( $sef ) > 50 ? '...' : '';

        return '
        <div id="sef_scheme_0">
            <strong>Permalink : </strong>
            <span>' . $sef_url . '</span>
            <span id="the_sef_' . $i . '">
                <span id="the_sef_content_' . $i . '" class="sef_content">' . substr( $sef, 0, 50 ) . $more . '</span>/
                <input type="button" value="Edit" id="edit_sef_' . $i . '" class="button_bold">
            </span>
            <span id="sef_box_' . $i . '" class="sef_box_act">
                <span>
                    <input type="text" name="sef_box[' . $i . ']" value="' . $sef . '" id="sef_box_val_' . $i . '" />/
                    <input type="button" value="Done" id="done_edit_sef_' . $i . '" class="button_bold">
                </span>
            </span>
                      
            <script type="text/javascript">
                jQuery(document).ready(function(){
                    sef_scheme_action( ' . $i . ', ' . $post_id . ', "' . $type . '", "' . $sef . '", "' . $url . '" );
                });
            </script>
        </div>';
    }
    else
    {
        return '
        <div id="sef_scheme_' . $i . '"></div>
        <script type="text/javascript">
            jQuery(function(){
                jQuery(".article-title").on("blur", function(){
                    var url = "' . $url . '";
                    var prm = new Object;
                        prm.title = jQuery(this).val();
                        prm.pkey  = "get-sef-scheme";
                        prm.type  = "' . $type . '";
                        prm.index = 0;
                    jQuery.post( url, prm, function( e ){
                        jQuery("#sef_scheme_' . $i . '").html( e );
                    });                     
                });
            });
        </script>'; 
    }
}

function get_sef_scheme_content()
{
    global $allowedtitletags;
    
    extract( $_POST );

    if( empty( $title ) )
    {
       $title = 'Untitled';
    }
    else
    {
       $title = kses( rem_slashes( $title ), $allowedtitletags );
    }

    $num_by_title_and_type = is_num_articles( 'title=' . $title . '&type='. $type );

    if( $num_by_title_and_type > 0 )
    {
        for( $i = 2; $i <= $num_by_title_and_type + 1; $i++ )
        {
            $sef = generateSefUrl( $title ) . '-' . $i;

            if( is_num_articles( 'sef=' . $sef . '&type=' . $type ) < 1 )
            {
                $sef = $sef;

                break;
            }
         }
    }
    else
    {
        $sef = generateSefUrl( $title );
    }

    if( strlen( $sef ) > 50 )
    {
        $more = '...';
    }
    else
    {
        $more = '';
    }

    if( $type == 'pages' )
    {
        $link_structure = site_url();
        $ext = '/';
    }
    else
    {
        $link_structure = site_url( $type );
        $ext = '.html';
    }

    $content = '
    <strong>Permalink:</strong>' . $link_structure . '
    <span id="the_sef_' . $_POST['index'] . '">
        <span id="the_sef_content_' . $_POST['index'] . '"  style="background:#FFCC66; cursor:pointer;">
            ' . substr( $sef, 0, 50 ) . $more . '
        </span>
        ' . $ext . '
        <input type="button" value="Edit" id="edit_sef_' . $_POST['index'] . '" class="button_bold">
    </span>

    <span id="sef_box_' . $_POST['index'] . '" style="display:none;">
        <span>
            <input class="sef-box-' . $_POST['index'] . '" type="text" name="sef_box[' . $_POST['index'] . ']" value="' . $sef . '" style="border:1px solid #CCC; width:300px; font-size:11px;" />
            ' . $ext . ' <input type="button" value="Done" id="done_edit_sef_' . $_POST['index'] . '" class="button_bold">
        </span>
    </span>

    <script type="text/javascript">
        jQuery(document).ready(function(){
            sef_scheme_action( ' . $_POST['index'] . ' );
        });
    </script>';

    return $content;
}

function set_tabs( $tabs, $selected_tab )
{
    $tab = '';
    
    foreach( $tabs as $key => $val )
    {
        if( $selected_tab == $key )
        {
            $tab .= '<li class="active"><a href="' . get_tab_url( $key ) . '">' . $val . '</a></li>';
        }
        else
        {
            $tab .= '<li><a href="' . get_tab_url( $key ) . '">' . $val . '</a></li>';
        }
    }
    
    return $tab;
}

function return_tab_admin( $tabs )
{
    return $tabs;
}

function get_global_settings()
{
    global $actions;
    
    $tabs = array(
        'general' => 'General',
        'reading' => 'Reading',
        'writing' => 'Writing' 
    );

    $multi_language = get_meta_data( 'multi_language' );
    
    if( $multi_language == 1 )
    {
        add_actions( 'tab_admin_aditional', 'multi_language_setting', array( 'multi_language' => 'Language' ), $tabs );
    }

    if( isset( $actions->action[ 'tab_admin_aditional' ] ) )
    {
        for( $i = 0; $i < count( $actions->action[ 'tab_admin_aditional' ][ 'args' ]); $i++ )
        {
            for( $ii = 0; $ii < count( $actions->action[ 'tab_admin_aditional' ][ 'args' ][ $i ] ); $ii++ )
            {
                if( is_array( $actions->action[ 'tab_admin_aditional' ][ 'args' ][ $i ][ $ii ] ) )
                {
                    $tabs = array_merge( $tabs, $actions->action[ 'tab_admin_aditional' ][ 'args' ][ $i ][ $ii ] );
                }
            }
        }
    }

    add_actions( 'tab_admin', 'return_tab_admin', $tabs );
    
    if( empty( $_GET[ 'tab' ] ) )
    {
        $the_tab = 'general';
    }
    else
    {
        $the_tab = $_GET[ 'tab' ];
    }
    
    add_variable( 'tab', set_tabs( $tabs, $the_tab ) );
    add_variable( 'save_changes_botton', save_changes_botton() );
    
    $update = false;
    
    if( !empty( $_POST[ 'update' ] ) )
    {
        $update = $_POST[ 'update' ];
    }
    
    if( $the_tab == 'general' )
    {
        //-- General
    	set_template( TEMPLATE_PATH . '/template/general-form.html', 'global' );

    	add_block( 'date-list-setting', 'dl-block', 'global' );
    	add_block( 'time-list-setting', 'tl-block', 'global' );
    	add_block( 'general-setting', 'g-block', 'global' );
        
        if( $update )
        {
            update_global_settings();
        }

        $google_service_account = get_meta_data( 'google_service_account' );
        $analytic_view_id       = get_meta_data( 'analytic_view_id' );
        $ssl_config             = get_meta_data( 'ssl_config' );
        
        add_variable( 'analytic_view_id', $analytic_view_id );
        add_variable( 'google_service_account', $google_service_account );
        add_variable( 'google_service_css', empty( $google_service_account ) ? 'hidden' : '' );

        add_variable( 'smtp', get_smtp() );
        add_variable( 'email', get_email() );
        add_variable( 'web_name', web_name() );
        add_variable( 'web_title', web_title() );
        add_variable( 'tagline', web_tagline() );
        add_variable( 'alert', message_block() );
        add_variable( 'htserver', HTSERVER . '//' );
        add_variable( 'site_url', get_meta_data( 'site_url' ) );
        add_variable( 'timezone', get_timezone( get_meta_data( 'time_zone' ) ) );
        
        set_timezone( 'UTC' );
        set_timezone( get_meta_data( 'time_zone' ) );
        
        add_variable( 'utc_time', date( 'Y/m/d H:i', time() ) );
        add_variable( 'local_time', date( 'Y/m/d H:i', time() ) );

        if( $multi_language == 0 )
        {
            add_variable( 'multi_language_inactive', 'selected' );
            add_variable( 'multi_language_active', '' );
        }
        else
        {
            add_variable( 'multi_language_inactive', '' );
            add_variable( 'multi_language_active', 'selected' );
        }

        if( $ssl_config == 0 )
        {
            add_variable( 'ssl_config_inactive', 'selected' );
            add_variable( 'ssl_config_active', '' );
        }
        else
        {
            add_variable( 'ssl_config_inactive', '' );
            add_variable( 'ssl_config_active', 'selected' );
        }
        
        $date_format = array( 'F j, Y', 'Y/m/d', 'm/d/Y', 'd/m/Y' );
        $curr_format = get_date_format();        
        $cust_format = true;

        $index = 0;

        foreach( $date_format as $val )
        {
            if( $val == $curr_format )
            {
                $checked     = 'checked="checked"';
                $cust_format = false;
            }
            else
            {
                $checked = '';
            }

			add_variable( 'value', $val );
			add_variable( 'index', $index );
			add_variable( 'checked', $checked );
			add_variable( 'value_format', date( $val, time() ) );

			parse_template( 'date-list-setting', 'dl-block', true );

			$index++;
        }

        add_variable( 'current_date_format', $curr_format );
        add_variable( 'custome_date_checked', $cust_format ? 'checked="checked"' : '' );
        
        $time_format = array( 'g:i a', 'g:i A', 'H:i' );
        $curr_format = get_time_format();
        $cust_format = true;

        $index = 0;
        
        foreach( $time_format as $val )
        {
            if( $val == $curr_format )
            {
                $checked     = 'checked="checked"';
                $cust_format = false;
            }
            else
            {
                $checked = '';
            }

            add_variable( 'value', $val );
            add_variable( 'index', $index );
            add_variable( 'checked', $checked );
            add_variable( 'value_format', date( $val, time() ) );

            parse_template( 'time-list-setting', 'tl-block', true );
        }

        add_variable( 'current_time_format', $curr_format );
        add_variable( 'custome_time_checked', $cust_format ? 'checked="checked"' : '' );
        
        add_actions( 'section_title', 'General - Settings' );
        
        parse_template( 'general-setting', 'g-block' );

        return return_template( 'global' );
    }
    elseif( $the_tab == 'reading' )
    {
        //-- Reading
        set_template( TEMPLATE_PATH . '/template/reading-form.html', 'global' );

        add_block( 'rss-view-list-setting', 'rvl-block', 'global' );
        add_block( 'sef-list-setting', 'sl-block', 'global' );
        add_block( 'reading-setting', 'r-block', 'global' );
        
        if( $update )
        {
            if( update_global_settings() )
            {
                if( $_POST[ 'is_rewrite' ] == 'yes' )
                {
                    create_htaccess_file();
                }
                else
                {
                    if( file_exists( ROOT_PATH . '/.htaccess' ) )
                    {
                        unlink( ROOT_PATH . '/.htaccess' );
                    }
                }
            }
        }
        
        add_variable( 'alert', message_block() );
        add_variable( 'rss_viewed', rss_viewed() );
        add_variable( 'post_viewed', post_viewed() );
        add_variable( 'status_viewed', status_viewed() );
        
        //-- RSS Viewed Options
        $rss_view_format  = array( 'full_text' => 'Full Text', 'summary' => 'Summary' );
        $curr_view_format = rss_view_format();
        
        foreach( $rss_view_format as $key => $val )
        {
            if( $key == $curr_view_format )
            {
                $checked = 'checked="checked"';
            }
            else
            {
                $checked = '';
            }

            add_variable( 'value', $key );
            add_variable( 'format', $val );
            add_variable( 'checked', $checked );

            parse_template( 'rss-view-list-setting', 'rvl-block', true );
        }
        
        //-- SEF URL Options
        $sef_format = array( 'yes' => 'Yes', 'no' => 'No' );
        $is_rewrite = is_rewrite();
        
        foreach( $sef_format as $key => $val )
        {
            if( $key == $is_rewrite )
            {
                $checked = 'checked="checked"';
            }
            else
            {
                $checked = '';
            }

            add_variable( 'value', $key );
            add_variable( 'option', $val );
            add_variable( 'checked', $checked );

            parse_template( 'sef-list-setting', 'sl-block', true );
        }
        
        add_actions( 'section_title', 'Reading - Settings' );
        
        parse_template( 'reading-setting', 'r-block' );

        return return_template( 'global' );
    }
    elseif( $the_tab == 'writing' )
    {
        //-- Writing
        set_template( TEMPLATE_PATH . '/template/writing-form.html', 'global' );

        add_block( 'editor-list-setting', 'edl-block', 'global' );
        add_block( 'email-list-setting', 'eml-block', 'global' );
        add_block( 'writing-setting', 'w-block', 'global' );
        
        if( $update )
        {
            update_global_settings();
        }
        
        //-- Email Format Options
        $email_format = array( 'html' => 'HTML', 'plain_text' => 'Plain Text' );
        $curr_format  = email_format();
        
        foreach( $email_format as $key => $val )
        {
            if( $key == $curr_format )
            {
                $checked = 'checked="checked"';
            }
            else
            {
                $checked = '';
            }

            add_variable( 'value', $key );
            add_variable( 'format', $val );
            add_variable( 'checked', $checked );

            parse_template( 'email-list-setting', 'eml-block', true );
        }

        //-- Text Editor Options
        $text_editor = array( 'tiny_mce' => 'Tiny MCE', 'none' => 'None' );
        $curr_editor = text_editor();

        foreach( $text_editor as $key => $val )
        {
            if( $key == $curr_editor )
            {
                $checked = 'checked="checked"';
            }
            else
            {
                $checked = '';
            }

            add_variable( 'value', $key );
            add_variable( 'editor', $val );
            add_variable( 'checked', $checked );

            parse_template( 'editor-list-setting', 'edl-block', true );
        }
        
        add_variable( 'thumbnail_image_height', thumbnail_image_height() );
        add_variable( 'thumbnail_image_width', thumbnail_image_width() );
        add_variable( 'medium_image_height', medium_image_height() );
        add_variable( 'medium_image_width', medium_image_width() );
        add_variable( 'large_image_height', large_image_height() );
        add_variable( 'large_image_width', large_image_width() );
        add_variable( 'list_viewed', list_viewed() );
        add_variable( 'alert', message_block() );

        add_actions( 'section_title', 'Writing - Settings' );
        
        parse_template( 'writing-setting', 'w-block' );

        return return_template( 'global' );
    }
    else
    {
        if( isset( $actions->action[ 'tab_admin_aditional' ] ) )
        {
            for( $i = 0; $i < count( $actions->action[ 'tab_admin_aditional' ][ 'args' ] ); $i++ )
            {
                for( $ii = 0; $ii < count( $actions->action[ 'tab_admin_aditional' ][ 'args' ][ $i ] ); $ii++ )
                {
                    $tabs = $actions->action[ 'tab_admin_aditional' ][ 'args' ][ $i ][ $ii ];
                    
                    if( is_array( $tabs ) )
                    {
                        foreach( $tabs as $key => $val )
                        {
                            if( $_GET[ 'tab' ] == $key )
                            {
                                $title = $val;
                                
                                add_actions( 'section_title', $title );
                                
                                if( function_exists( $actions->action[ 'tab_admin_aditional' ][ 'func_name' ][ $i ] ) )
                                {
                                    $lbl = 'tab_admin_aditional';
                                    $j   = $i;
                                    
                                    return call_user_func_array( $actions->action[ $lbl ][ 'func_name' ][ $j ], ( is_array( $actions->action[ $lbl ][ 'args' ][ $j ] ) ) ? $actions->action[ $lbl ][ 'args' ][ $j ] : array( $actions->action[ $lbl ][ 'args' ][ $j ] ) );
                                }
                                
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}

function update_global_settings()
{
    global $flash;

    $thumbnail_image_width  = '';
    $thumbnail_image_height = '';
    $medium_image_width     = '';
    $medium_image_height    = '';
    $large_image_width      = '';
    $large_image_height     = '';
    $thumbnail_image_size   = '';
    $medium_image_size      = '';
    $large_image_size       = '';
    
    if( isset( $_POST[ 'thumbnail_image_width' ] ) )
    {
        if( $_POST[ 'thumbnail_image_width' ] == '' || !isInteger( $_POST[ 'thumbnail_image_width' ] ) )
        {
            $_POST[ 'thumbnail_image_width' ] = thumbnail_image_width();
        }
    }
    
    if( isset( $_POST[ 'thumbnail_image_height' ] ) )
    {
        if( $_POST[ 'thumbnail_image_height' ] == '' || !isInteger( $_POST[ 'thumbnail_image_height' ] ) )
        {
            $_POST[ 'thumbnail_image_height' ] = thumbnail_image_height();
        }
    }
    
    if( isset( $_POST[ 'medium_image_width' ] ) )
    {
        if( $_POST[ 'medium_image_width' ] == '' || !isInteger( $_POST[ 'medium_image_width' ] ) )
        {
            $_POST[ 'medium_image_width' ] = medium_image_width();
        }
    }
    
    if( isset( $_POST[ 'medium_image_height' ] ) )
    {
        if( $_POST[ 'medium_image_height' ] == '' || !isInteger( $_POST[ 'medium_image_height' ] ) )
        {
            $_POST[ 'medium_image_height' ] = medium_image_height();
        }
    }
    
    if( isset( $_POST[ 'large_image_width' ] ) )
    {
        if( $_POST[ 'large_image_width' ] == '' || !isInteger( $_POST[ 'large_image_width' ] ) )
        {
            $_POST[ 'large_image_width' ] = large_image_width();
        }
    }
    
    if( isset( $_POST[ 'large_image_height' ] ) )
    {
        if( $_POST[ 'large_image_height' ] == '' || !isInteger( $_POST[ 'large_image_height' ] ) )
        {
            $_POST[ 'large_image_height' ] = large_image_height();
        }
    }
    
    if( isset( $_POST[ 'thumbnail_image_width' ] ) && isset( $_POST[ 'thumbnail_image_height' ] ) )
    {
        $update = update_meta_data( 'thumbnail_image_size', $_POST[ 'thumbnail_image_width' ] . ':' . $_POST[ 'thumbnail_image_height' ] );
    }
    
    if( isset( $_POST[ 'medium_image_width' ] ) && isset( $_POST[ 'medium_image_height' ] ) )
    {
        $update = update_meta_data( 'medium_image_size', $_POST[ 'medium_image_width' ] . ':' . $_POST[ 'medium_image_height' ] );
    }
    
    if( isset( $_POST[ 'large_image_width' ] ) && isset( $_POST[ 'large_image_height' ] ) )
    {
        if( $large_image_size != ':' )
        {
            $update = update_meta_data( 'large_image_size', $_POST[ 'large_image_width' ] . ':' . $_POST[ 'large_image_height' ] );
        }
    }
    
    foreach( $_POST as $key => $val )
    {
        $val = str_replace( 'http://', '', $val );
        
        //-- value should be integer
        
        if( $key == 'post_viewed' || $key == 'rss_viewed' || $key == 'status_viewed' || $key == 'invitation_limit' )
        {
            if( $val == '' || !isInteger( $val ) )
            {
                $val = 10;
            }
        }
        elseif( $key == 'list_viewed' )
        {
            if( $val == '' || !isInteger( $val ) )
            {
                $val = 30;
            }
        }
        
        if( $key == 'invitation_limit' )
        {
            $update = update_meta_data( $key, $val, 'global_setting' );
        }
        else
        {
            $update = update_meta_data( $key, $val );
        }
    }
    
    if( isset( $_FILES[ 'google_service_account' ][ 'tmp_name' ] ) && $_FILES[ 'google_service_account' ][ 'error' ] == 0 )
    {
        $file_source      = $_FILES[ 'google_service_account' ][ 'tmp_name' ];
        $file_destination = ADMIN_PATH . '/service-account-credentials.json';
        
        if( upload( $file_source, $file_destination ) )
        {
            $update = update_meta_data( 'google_service_account', $file_destination, 'global_setting' );
        }
    }
    
    if( $update )
    {
        $flash->add( array( 'type'=> 'success', 'content' => array( 'Update process successfully.' ) ) );

        header( 'location:' . get_state_url( 'global_settings&tab=' . ( isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'general' ) ) );

        exit;
    }
    else
    {
        $flash->add( array( 'type'=> 'error', 'content' => array( 'Update process failed.' ) ) );

        return false;
    }
}

function message_block( $message = array() )
{
    global $flash;
    
    $message_sess = $flash->render();
    
    if( empty( $message ) || !isset( $message[ 'type' ] ) )
    {
        if( empty( $message_sess ) )
        {
            return '';
        }
        else
        {
            foreach( $message_sess as $m )
            {
                $message = $m;
            }
        }
    }
    
    if( $message[ 'type' ] == 'error' )
    {
        $bg_css = 'alert_red_form';
    }
    elseif( $message[ 'type' ] == 'success' )
    {
        $bg_css = 'alert_green_form';
    }
    
    $message_block = '
    <div class="' . $bg_css . '">';

        if( count( $message[ 'content' ] ) > 1 )
        {
            $message_block .= '
            <ul>';
        
                foreach( $message[ 'content' ] as $value )
                {
                    $message_block .= '<li>' . $value . '</li>';
                }
                
                $message_block .= '
            </ul>';
        }
        else
        {
            if( is_array( $message[ 'content' ] ) )
            {
                foreach( $message[ 'content' ] as $value )
                {
                    $message_block .=  $value;
                }           
            }
            else
            {
                $message_block .=  $message[ 'content' ];
            }
        }
    
        $message_block .= '
    </div>';
    
    return $message_block;
}

function is_dashboard()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'dashboard' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_global_settings()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'global_settings' && is_grant_app( 'global_settings' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_application()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'applications' && is_grant_app( 'applications' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_plugin()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'plugins' && is_grant_app( 'plugins' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_menus()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'menus' && is_grant_app( 'menus' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_themes()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'themes' && is_grant_app( 'themes' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_comment()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'comments' && is_grant_app( 'comments' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_article()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'blogs' && is_grant_app( 'blogs' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_media()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'media' && is_grant_app( 'media' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_page()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'pages' && is_grant_app( 'pages' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_user()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'users' && is_grant_app( 'users' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_admin_ajax()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'ajax' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_profile()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'my-profile' && isset( $_GET[ 'tab' ] ) && $_GET[ 'tab' ] == 'my-profile' )
    {
        return true;
    }
    
    if( ( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'users' && isset( $_GET[ 'tab' ] ) && $_GET[ 'tab' ] == 'my-profile' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_user_updates()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'my-profile' )
    {
        return true;
    }
    
    if( ( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'my-profile' ) && isset( $_GET[ 'tab' ] ) && $_GET[ 'tab' ] == 'my-updates' )
    {
        return true;
    }
    
    if( ( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'users' && isset( $_GET[ 'tab' ] ) && $_GET[ 'tab' ] == 'my-updates' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_profile_picture()
{
    if( ( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'my-profile' ) && isset( $_GET[ 'tab' ] ) && $_GET[ 'tab' ] == 'profile-picture' )
    {
        return true;
    }
    
    if( ( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'users' && isset( $_GET[ 'tab' ] ) && $_GET[ 'tab' ] == 'profile-picture' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_logout()
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == 'logout' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_state( $state )
{
    if( !empty( $_GET[ 'state' ] ) && $_GET[ 'state' ] == $state && is_grant_app( $state ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_ajax()
{
    if( ( ( isset( $_GET[ 'prc' ] ) && $_GET[ 'prc' ] == 'ajax' ) || isset( $_POST[ 'ajax_key' ] ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_view( $app_name )
{
    if( ( isset( $_GET[ 'prc' ] ) && $_GET[ 'prc' ] == 'view' && allow_action( $app_name, 'view' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_add_new()
{
    if( ( ( isset( $_GET[ 'prc' ] ) && $_GET[ 'prc' ] == 'add_new' ) || isset( $_POST[ 'add_new' ] ) ) && allow_action( $_GET[ 'state' ], 'insert' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_edit()
{
    if( ( isset( $_GET[ 'prc' ] ) && $_GET[ 'prc' ] == 'edit' && allow_action( $_GET[ 'state' ], 'update' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
    
}

function is_edit_all()
{
    if( ( isset( $_POST[ 'edit' ] ) && $_POST[ 'edit' ] == 'Edit' && allow_action( $_GET[ 'state' ], 'update' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_edit_all_comment()
{
    if( ( isset( $_POST[ 'edit' ] ) && $_POST[ 'edit' ] == 'Edit Comments' && allow_action( $_GET[ 'state' ], 'update' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_delete( $app_name )
{
    if( ( isset( $_POST[ 'prc' ] ) && $_POST[ 'prc' ] == 'delete' && allow_action( $app_name, 'delete' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_delete_all()
{
    if( ( isset( $_POST[ 'delete' ] ) && $_POST[ 'delete' ] == 'Delete' && allow_action( $_GET[ 'state' ], 'delete' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_confirm_delete()
{
    if( ( isset( $_POST[ 'confirm_delete' ] ) && $_POST[ 'confirm_delete' ] == 'Yes' && allow_action( $_GET[ 'state' ], 'delete' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_search()
{
    if( ( isset( $_POST[ 'prc' ] ) && $_POST[ 'prc' ] == 'search' ) )
    {
        return true;
    }
    
    if( ( isset( $_POST[ 'search' ] ) && $_POST[ 'search' ] == 'yes' ) )
    {
        return true;
    }
    
    if( ( isset( $_POST[ 's' ] ) && !empty( $_POST[ 's' ] ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_orderby()
{
    if( ( isset( $_GET[ 'orderby' ] ) && !empty( $_GET[ 'orderby' ] ) ) && ( isset( $_GET[ 'order' ] ) && !empty( $_GET[ 'order' ] ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function set_orderby()
{
    if( isset( $_GET[ 'order' ] ) && $_GET[ 'order' ] == 'asc' )
    {
        return 'desc';
    }
    else
    {
        return 'asc';
    }
}

function set_sortable( $field )
{
    if( isset( $_GET[ 'orderby' ] ) && $_GET[ 'orderby' ] == $field )
    {
        return 'sortable';
    }
    else
    {
        return '';
    }
}

function is_save_changes( $state = '' )
{
    if( empty( $state ) )
    {
        if( isset( $_POST[ 'save_changes' ] ) && ( allow_action( $_GET[ 'state' ], 'update' ) || allow_action( $_GET[ 'state' ], 'insert' ) || allow_action( $_GET[ 'state' ], 'approve' ) ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        if( isset( $_POST[ 'save_changes' ] ) && ( allow_action( $state, 'update' ) || allow_action( $state, 'insert' ) || allow_action( $state, 'approve' ) ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

function is_save_draft()
{
    if( isset( $_POST[ 'save_draft' ] ) && allow_action( $_GET[ 'state' ], 'insert' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_publish()
{
    if( isset( $_POST[ 'publish' ] ) && allow_action( $_GET[ 'state' ], 'insert' ) && $_COOKIE[ 'user_type' ] != 'contributor' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_unpublish()
{
    if( isset( $_POST[ 'unpublish' ] ) && allow_action( $_GET[ 'state' ], 'insert' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_approved()
{
    $return = false;
    
    if( isset( $_POST[ 'publish' ] ) && allow_action( $_GET[ 'state' ], 'approve' ) && ( is_administrator() || is_editor() ) )
    {
        $return = true;
    }
    
    if( ( isset( $_POST[ 'prc' ] ) && $_POST[ 'prc' ] == 'approve' && allow_action( $_POST[ 'state' ], 'approve' ) ) )
    {
        $return = true;
    }
    
    return $return;
}

function is_disapproved()
{
    $return = false;
    
    if( isset( $_POST[ 'unpublish' ] ) && allow_action( $_GET[ 'state' ], 'approve' ) && ( is_administrator() || is_editor() ) )
    {
        $return = true;
    }
    
    if( ( isset( $_POST[ 'prc' ] ) && $_POST[ 'prc' ] == 'disapprove' && allow_action( $_POST[ 'state' ], 'approve' ) ) )
    {
        $return = true;
    }
    
    return $return;
}

function is_saved()
{
    if( isset( $_POST[ 'article_saved' ] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_category_view()
{
    if( isset( $_GET[ 'category_name' ] ) && !empty( $_GET[ 'category_name' ] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_ajax_request()
{
    if( isset( $_POST[ 'ajax_key' ] ) || trim( get_appname() ) == 'ajax-request' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

?>