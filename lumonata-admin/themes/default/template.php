<?php

/*
Title: Aruna
Preview: preview.png
Author: Wahya Biantara
Author Url: http://wahya.biantara.com
Description: Default themes since March 2010
*/

if( !is_user_logged() )
{
    if( is_login_form() )
    {
        get_login_form();
    }
    elseif( is_thanks_page() )
    {
        resendPassword();
    }
    elseif( is_verify_account() )
    {
        verify_account();
    }
    elseif( is_forget_password() )
    {
        get_forget_password_form();
    }
    else
    {
        header( 'location:' . get_admin_url() . '/?state=login&redirect=' . base64_encode( current_page_url() ) );
    }
}
else
{
    if( is_admin_media() )
    {
        get_upload_media();
    }
    else
    {
        if( empty( $_GET[ 'state' ] ) )
        {
            header( 'location:' . get_admin_url() . '/?state=dashboard' );
        }

        if( isset( $actions->action['is_use_ajax'] ) && $actions->action['is_use_ajax']['func_name'][0] )
        {
            set_template( TEMPLATE_PATH . '/ajax.html' );
        }
        else
        {
            set_template( TEMPLATE_PATH . '/index.html' );
        }

        add_block( 'adminArea', 'aBlock' );
        
        add_variable( 'site_url', site_url() );
        add_variable( 'web_title', web_title() );
        add_variable( 'admin_url', get_admin_url() );
        add_variable( 'template_url', template_url( null, true ) );
        add_variable( 'avatar', get_avatar( $_COOKIE[ 'user_id' ], 2 ) );
        
        add_variable( 'logout_link', get_state_url( 'logout' ) );
        add_variable( 'profile_link', get_state_url( 'users&tab=my-profile' ) );
        add_variable( 'profile_picture_link', get_state_url( 'users&tab=profile-picture' ) );
        
        $d = fetch_user( $_COOKIE[ 'user_id' ] );
        
        add_variable( 'displayname', $d[ 'ldisplay_name' ] );
        
        /*
        | -------------------------------------------------------------------------------------
        | Setting up the header elements. CSS, Javascript, JQuery And the others
        | -------------------------------------------------------------------------------------
        */
        $settings_menu   = get_admin_menu( 'settings_menu' );
        $connection_menu = get_admin_menu( 'connection_menu' );
        
        //-- Get The Content Area
        add_variable( 'content_area', $thecontent );
        
        //-- Setting Up Navigation Menu
        add_variable( 'navmenu', get_admin_menu() );
        
        //-- Setting Up Settings Menu
        add_variable( 'settings_menu', $settings_menu );
        add_variable( 'settings_menu_class', empty( $settings_menu ) ? 'hidden' : '' );
        
        //-- Setting Up Connection Menu
        add_variable( 'connection_menu', $connection_menu );
        add_variable( 'connection_menu_class', empty( $connection_menu ) ? 'hidden' : '' );
        
        //-- Attempt the action that already add in the whole script
        add_variable( 'other_elements', attemp_actions( 'other_elements' ) );
        add_variable( 'css_elements', attemp_actions( 'css_elements' ) );
        add_variable( 'js_elements', attemp_actions( 'js_elements' ) );

        add_variable( 'section_title', attemp_actions( 'section_title' ) );
        add_variable( 'language_option', attemp_actions( 'language_option' ) );
        
        parse_template( 'adminArea', 'aBlock' );
        print_template();
    }
}

?>