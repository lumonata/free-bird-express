function initTinyMce( selector )
{
    tinymce.init({
        selector: selector,
        height: 500,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        toolbar1: 'undo redo | insert | table styleselect | bold underline italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink image | print preview media | forecolor backcolor emoticons | codesample code',
        toolbar2: 'up_images up_docs up_pdf up_music up_swf up_videos',
        table_class_list: [
            {title: 'None', value: ''},
            {title: 'Price List', value: 'price-list-table'},
            {title: 'Price List Note', value: 'price-list-note-table'}
        ],
        link_class_list: [
            {title: 'None', value: ''},
            {title: 'Download PDF Style', value: 'download-pdf-file'}
        ],
        image_advtab: true,
        media_live_embeds: true,
        content_css: [
            '../lumonata-admin/themes/default/css/textarea.css',
        ],
        relative_urls: false,
        remove_script_host: false,
        extended_valid_elements: 'span',
        convert_urls: true,
        setup: function (ed) {
            var post_id   = jQuery('.post-id-field').val();
            var post_type = jQuery('.post-type-field').val();

            ed.addButton('up_images', {
                tooltip: 'Attach Custom Images',
                image : '../lumonata-admin/themes/default/images/ico-upload-images.svg',
                onclick: function (ele) {
                    ed.windowManager.open({
                        title: 'Attach Custom Images',
                        url: '../lumonata-admin/?state=media&tab=from-computer&post_id=' + post_id + '&apps=' + post_type + '&type=image&textarea_id=' + ed.id,
                        width: 800,
                        height: 720
                    });
                }
            });

            ed.addButton('up_docs', {
                tooltip: 'Attach Custom Documents',
                image : '../lumonata-admin/themes/default/images/ico-upload-doc.svg',
                onclick: function () {
                    ed.windowManager.open({
                        title: 'Attach Custom Documents',
                        url: '../lumonata-admin/?state=media&tab=from-computer&post_id=' + post_id + '&apps=' + post_type + '&type=doc&textarea_id=' + ed.id,
                        width: 800,
                        height: 720
                    });
                }
            });

            ed.addButton('up_pdf', {
                tooltip: 'Attach Custom PDF',
                image : '../lumonata-admin/themes/default/images/ico-upload-pdf.svg',
                onclick: function () {
                    ed.windowManager.open({
                        title: 'Attach Custom PDF',
                        url: '../lumonata-admin/?state=media&tab=from-computer&post_id=' + post_id + '&apps=' + post_type + '&type=pdf&textarea_id=' + ed.id,
                        width: 800,
                        height: 720
                    });
                }
            });

            ed.addButton('up_music', {
                tooltip: 'Attach Custom Music File',
                image : '../lumonata-admin/themes/default/images/ico-upload-music.svg',
                onclick: function () {
                    ed.windowManager.open({
                        title: 'Attach Custom Music File',
                        url: '../lumonata-admin/?state=media&tab=from-computer&post_id=' + post_id + '&apps=' + post_type + '&type=music&textarea_id=' + ed.id,
                        width: 800,
                        height: 720
                    });
                }
            });

            ed.addButton('up_swf', {
                tooltip: 'Attach Custom Flash File',
                image : '../lumonata-admin/themes/default/images/ico-upload-flash.svg',
                onclick: function () {
                    ed.windowManager.open({
                        title: 'Attach Custom Flash File',
                        url: '../lumonata-admin/?state=media&tab=from-computer&post_id=' + post_id + '&apps=' + post_type + '&type=flash&textarea_id=' + ed.id,
                        width: 800,
                        height: 720
                    });
                }
            });

            ed.addButton('up_videos', {
                tooltip: 'Attach Custom Video',
                image : '../lumonata-admin/themes/default/images/ico-upload-video.svg',
                onclick: function () {
                    ed.windowManager.open({
                        title: 'Attach Custom Video',
                        url: '../lumonata-admin/?state=media&tab=from-computer&post_id=' + post_id + '&apps=' + post_type + '&type=video&textarea_id=' + ed.id,
                        width: 800,
                        height: 720
                    });
                }
            });
        }
    });
}

jQuery(function() {
    if( typeof set_mlanguage_forclone === 'undefined' )
    {
        initTinyMce( '.tinymce' );
    }
});

jQuery(function() {
    jQuery('.visual_view_button').attr('disabled', 'disabled');
    jQuery('.visual_view_button').removeClass('visual_view_button').addClass('visual_view_button_selected');
});

jQuery(function() {
    jQuery('.visual_view_button_selected').click(function() {
        var id = this.id.replace('visual_view_', '');

        jQuery('#html_view_' + id).removeClass('html_view_button_selected').addClass('html_view_button');
        jQuery('#html_view_' + id).removeAttr('disabled');
        jQuery('#visual_view_' + id).attr('disabled', 'disabled');
        jQuery('#visual_view_' + id).removeClass('visual_view_button').addClass('visual_view_button_selected');

        return false;
    });
});

jQuery(function() {
    jQuery('.html_view_button').click(function() {
        var id = this.id.replace('html_view_', '');

        jQuery('#visual_view_' + id).removeClass('visual_view_button_selected').addClass('visual_view_button');
        jQuery('#visual_view_' + id).removeAttr('disabled');
        jQuery('#html_view_' + id).attr('disabled', 'disabled');
        jQuery('#html_view_' + id).removeClass('html_view_button').addClass('html_view_button_selected');

        return false;
    });
});