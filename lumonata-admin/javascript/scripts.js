function show_popup( htitle, message, callback )
{
    jQuery('<div></div>').dialog({
        modal: true,
        draggable: false,
        resizable: false,
        title: htitle,
        open: function()
        {
            jQuery(this).html(message);
        },
        buttons:
        {
            Close: function()
            {
                jQuery( this ).dialog('close');

                if( callback!='' )
                {
                    eval(callback);
                }
            }
        }
    });
}

function delete_user_func( table, pkey )
{
    jQuery('.delete-link').unbind();
    jQuery('.delete-link').on('click', function(e){
        var url = jQuery(this).attr('href');
        var prm = new Object();
            prm.pkey = pkey;
            prm.prc  = 'delete';
            prm.id   = jQuery(this).attr('id');

        jQuery('<div></div>').dialog({
            modal: true,
            draggable: false,
            resizable: false,
            title: 'Notification',
            open: function(){
                jQuery(this).html( 'Are you sure want to delete this user?' );
            },
            buttons: {
                Yes: function(){
                    var popup = jQuery(this);

                    jQuery.ajax({
                        method: 'POST',
                        url: url,
                        data: prm,
                        dataType : 'json',
                        success: function(e){
                            popup.dialog('close');

                            table.ajax.reload(function(){

                                if( e.result == 'success' )
                                {
                                    show_popup( 'Notification', 'This user has been successfully deleted' );
                                }
                                else if( e.result == 'failed' )
                                {
                                    show_popup( 'Warning', 'Sorry, This user can\'t be deleted. Please try again later' );
                                }

                                set_button_access();

                                delete_user_func( table, pkey );

                            }, false);
                        },
                        error: function(e){
                           popup.dialog('close');

                           show_popup( 'Warning', 'Sorry, This user can\'t be deleted. Please try again later' );
                        }
                    });
                },
                No: function(){
                    jQuery( this ).dialog('close');
                }
            }
        });

        return false;
    });
}

function delete_article_func( table, pkey, type )
{
    jQuery('.delete-link').unbind();
    jQuery('.delete-link').on('click', function(e){
        var url = jQuery(this).attr('href');
        var prm = new Object();
            prm.pkey  = pkey;
            prm.state = type;
            prm.prc   = 'delete';
            prm.id    = jQuery(this).attr('id');

        jQuery('<div></div>').dialog({
            modal: true,
            draggable: false,
            resizable: false,
            title: 'Notification',
            open: function(){
                jQuery(this).html( 'Are you sure want to delete this data?' );
            },
            buttons: {
                Yes: function(){
                    var popup = jQuery(this);

                    jQuery.ajax({
                        method: 'POST',
                        url: url,
                        data: prm,
                        dataType : 'json',
                        success: function(e){
                            popup.dialog('close');

                            table.ajax.reload(function(){

                                if( e.result == 'success' )
                                {
                                    show_popup( 'Notification', 'This data has been successfully deleted' );
                                }
                                else if( e.result == 'failed' )
                                {
                                    show_popup( 'Warning', 'Sorry, This data can\'t be deleted. Please try again later' );
                                }

                                set_button_access();

                                delete_article_func( table, pkey, type );

                            }, false);
                        },
                        error: function(e){
                           popup.dialog('close');

                           show_popup( 'Warning', 'Sorry, This data can\'t be deleted. Please try again later' );
                        }
                    });
                },
                No: function(){
                    jQuery( this ).dialog('close');
                }
            }
        });

        return false;
    });
}

function delete_custom_post_func( table )
{
    jQuery('.delete-link').unbind();
    jQuery('.delete-link').on('click', function(e){
        var url = jQuery(this).attr('href');
        var prm = new Object();
            prm.pkey  = 'delete-custom-post';
            prm.prc   = 'delete';
            prm.id    = jQuery(this).attr('id');

        jQuery('<div></div>').dialog({
            modal: true,
            draggable: false,
            resizable: false,
            title: 'Notification',
            open: function(){
                jQuery(this).html( 'Are you sure want to delete this data?' );
            },
            buttons: {
                Yes: function(){
                    var popup = jQuery(this);

                    jQuery.ajax({
                        method: 'POST',
                        url: url,
                        data: prm,
                        dataType : 'json',
                        success: function(e){
                            popup.dialog('close');

                            table.ajax.reload(function(){

                                if( e.result == 'success' )
                                {
                                    show_popup( 'Notification', 'This data has been successfully deleted' );
                                }
                                else if( e.result == 'failed' )
                                {
                                    show_popup( 'Warning', 'Sorry, This data can\'t be deleted. Please try again later' );
                                }

                                set_button_access();

                                delete_custom_post_func( table );

                            }, false);
                        },
                        error: function(e){
                           popup.dialog('close');

                           show_popup( 'Warning', 'Sorry, This data can\'t be deleted. Please try again later' );
                        }
                    });
                },
                No: function(){
                    jQuery( this ).dialog('close');
                }
            }
        });

        return false;
    });
}

function delete_rule_func( table, pkey, type )
{
    jQuery('.delete-link').unbind();
    jQuery('.delete-link').on('click', function(e){
        var url = jQuery(this).attr('href');
        var prm = new Object();
            prm.pkey  = pkey;
            prm.group = type;
            prm.prc   = 'delete';
            prm.id    = jQuery(this).attr('id');

        jQuery('<div></div>').dialog({
            modal: true,
            draggable: false,
            resizable: false,
            title: 'Notification',
            open: function(){
                jQuery(this).html( 'Are you sure want to delete this data?' );
            },
            buttons: {
                Yes: function(){
                    var popup = jQuery(this);

                    jQuery.ajax({
                        method: 'POST',
                        url: url,
                        data: prm,
                        dataType : 'json',
                        success: function(e){
                            popup.dialog('close');

                            table.ajax.reload(function(){

                                if( e.result == 'success' )
                                {
                                    show_popup( 'Notification', 'This data has been successfully deleted' );
                                }
                                else if( e.result == 'failed' )
                                {
                                    show_popup( 'Warning', 'Sorry, This data can\'t be deleted. Please try again later' );
                                }

                                set_button_access();

                                delete_rule_func( table, pkey, type );

                            }, false);
                        },
                        error: function(e){
                           popup.dialog('close');

                           show_popup( 'Warning', 'Sorry, This data can\'t be deleted. Please try again later' );
                        }
                    });
                },
                No: function(){
                    jQuery( this ).dialog('close');
                }
            }
        });

        return false;
    });
}

function delete_language_func( table, pkey )
{
    jQuery('.delete-link').unbind();
    jQuery('.delete-link').on('click', function(e){
        var url = jQuery(this).attr('href');
        var prm = new Object();
            prm.pkey  = pkey;
            prm.prc   = 'delete';
            prm.id    = jQuery(this).attr('id');

        jQuery('<div></div>').dialog({
            modal: true,
            draggable: false,
            resizable: false,
            title: 'Notification',
            open: function(){
                jQuery(this).html( 'Are you sure want to delete this data?' );
            },
            buttons: {
                Yes: function(){
                    var popup = jQuery(this);

                    jQuery.ajax({
                        method: 'POST',
                        url: url,
                        data: prm,
                        dataType : 'json',
                        success: function(e){
                            popup.dialog('close');

                            table.ajax.reload(function(){

                                if( e.result == 'success' )
                                {
                                    show_popup( 'Notification', 'This language has been successfully deleted' );
                                }
                                else if( e.result == 'failed' )
                                {
                                    show_popup( 'Warning', 'Sorry, This language can\'t be deleted. Please try again later' );
                                }

                                set_button_access();

                                delete_language_func( table, pkey );

                            }, false);
                        },
                        error: function(e){
                           popup.dialog('close');

                           show_popup( 'Warning', 'Sorry, This language can\'t be deleted. Please try again later' );
                        }
                    });
                },
                No: function(){
                    jQuery( this ).dialog('close');
                }
            }
        });

        return false;
    });
}

function set_button_access()
{
    jQuery('input[name=select_all]').removeAttr('checked');

    jQuery('.table td input').each(function(){
        jQuery('.table td input').removeAttr('checked');
    });
    
    jQuery('input[name=select_all]').on('change', function(){
        var checked_status = this.checked;
        
        jQuery('.select').each(function(){
            this.checked = checked_status;

            if( checked_status )
            {
                jQuery('input[name=edit]').removeClass('btn_edit_disable');
                jQuery('input[name=edit]').addClass('btn_edit_enable');
                jQuery('input[name=edit]').removeAttr('disabled');
                
                jQuery('input[name=delete]').removeClass('btn_delete_disable');
                jQuery('input[name=delete]').addClass('btn_delete_enable');
                jQuery('input[name=delete]').removeAttr('disabled');
                
                jQuery('input[name=publish]').removeClass('btn_publish_disable');
                jQuery('input[name=publish]').addClass('btn_publish_enable');
                jQuery('input[name=publish]').removeAttr('disabled');
                
                jQuery('input[name=unpublish]').removeClass('btn_save_changes_disable');
                jQuery('input[name=unpublish]').addClass('btn_save_changes_enable');
                jQuery('input[name=unpublish]').removeAttr('disabled');
            }
            else
            {
                jQuery('input[name=edit]').removeClass('btn_edit_enable');
                jQuery('input[name=edit]').addClass('btn_edit_disable');
                jQuery('input[name=edit]').attr('disabled', 'disabled');
                
                jQuery('input[name=delete]').removeClass('btn_delete_enable');
                jQuery('input[name=delete]').addClass('btn_delete_disable');
                jQuery('input[name=delete]').attr('disabled', 'disabled');
                
                jQuery('input[name=publish]').removeClass('btn_publish_enable');
                jQuery('input[name=publish]').addClass('btn_publish_disable');
                jQuery('input[name=publish]').attr('disabled', 'disabled');
                
                jQuery('input[name=unpublish]').removeClass('btn_save_changes_enable');
                jQuery('input[name=unpublish]').addClass('btn_save_changes_disable');
                jQuery('input[name=unpublish]').attr('disabled', 'disabled');
                
            }
        });
    });

    jQuery('.select').unbind('change');
    jQuery('.select').on('change', function(){
       if( jQuery('.select:checked').length > 0 )
       {
            jQuery('input[name=edit]').removeClass('btn_edit_disable');
            jQuery('input[name=edit]').addClass('btn_edit_enable');
            jQuery('input[name=edit]').removeAttr('disabled');
            
            jQuery('input[name=delete]').removeClass('btn_delete_disable');
            jQuery('input[name=delete]').addClass('btn_delete_enable');
            jQuery('input[name=delete]').removeAttr('disabled');
            
            jQuery('input[name=publish]').removeClass('btn_publish_disable');
            jQuery('input[name=publish]').addClass('btn_publish_enable');
            jQuery('input[name=publish]').removeAttr('disabled');
            
            jQuery('input[name=unpublish]').removeClass('btn_save_changes_disable');
            jQuery('input[name=unpublish]').addClass('btn_save_changes_enable');
            jQuery('input[name=unpublish]').removeAttr('disabled');
       }
       else
       {
            jQuery('input[name=edit]').removeClass('btn_edit_enable');
            jQuery('input[name=edit]').addClass('btn_edit_disable');
            jQuery('input[name=edit]').attr('disabled', 'disabled');
            
            jQuery('input[name=delete]').removeClass('btn_delete_enable');
            jQuery('input[name=delete]').addClass('btn_delete_disable');
            jQuery('input[name=delete]').attr('disabled', 'disabled');
            
            jQuery('input[name=publish]').removeClass('btn_publish_enable');
            jQuery('input[name=publish]').addClass('btn_publish_disable');
            jQuery('input[name=publish]').attr('disabled', 'disabled');
            
            jQuery('input[name=unpublish]').removeClass('btn_save_changes_enable');
            jQuery('input[name=unpublish]').addClass('btn_save_changes_disable');
            jQuery('input[name=unpublish]').attr('disabled', 'disabled');
       }
    });
}

function set_language_button_action()
{
    jQuery('.button-language ul li').click(function(){
        var val = jQuery(this).find('a.btn').attr('data-filter');

        jQuery('.button-language ul li').removeClass('active');
        jQuery('.content-lang').removeClass('active');
        jQuery('.' + val).addClass('active');
        jQuery(this).addClass('active');

        return false;
    });
}

function sef_scheme_action( index, post_id, type, sef, url )
{    
    jQuery('#the_sef_' + index).click(function(){
        jQuery('#the_sef_' + index).hide();
        jQuery('#sef_box_' + index).show();
    });

    jQuery('#edit_sef_' + index).click(function(){
        jQuery('#the_sef_' + index).hide();
        jQuery('#sef_box_' + index).show();
    });

    if( typeof post_id != 'undefined' )
    {
        jQuery('#done_edit_sef_'+ index).on('click', function(){                                                              
            var new_sef = jQuery('#sef_box_val_'+ index).val();
            var more    = new_sef.length > 50 ? '...' : '';
            var prm     = new Object;
                prm.title      = jQuery('.article-title').val();
                prm.pkey       = "update-sef-scheme";
                prm.post_id    = post_id;
                prm.new_sef    = new_sef;
                prm.type       = type;

            jQuery('#the_sef_'+ index).show();
            jQuery('#sef_box_'+ index).hide();

            jQuery.ajax({
                url: url,
                data: prm,
                type: 'POST',
                dataType: 'json',
                success: function( e ){
                    if( e.result == 'failed' )
                    {
                        jQuery('#sef_box_val_'+ index).val( sef );
                        jQuery('#the_sef_content_'+ index).html( sef.substr( 0, 50 ) );
                    }
                    else if( e.result == 'success' )
                    {
                        console.log(new_sef);
                        jQuery('#the_sef_content_'+ index).html( new_sef.substr(0,50) + more );
                    }
                }
            });
        });
    }
    else
    {
        jQuery('#done_edit_sef_' + index).click(function(){
            var new_sef = jQuery('.sef-box-' + index).val();
            var more    = new_sef.length > 50 ? '...' : '';

            jQuery('#the_sef_content_' + index).html( new_sef.substr( 0, 50 ) + more );
            jQuery('#the_sef_' + index).show();
            jQuery('#sef_box_' + index).hide();
        });
    }
}

function set_navigation_action()
{
    jQuery('a#applications').click(function(){
        jQuery('#applications_list').slideToggle( 100 );

        return false;
    });


    jQuery('a#plugins').click(function(){
        jQuery('#plugins_list').slideToggle( 100 );

        return false;
    });
}

function set_custom_field_action()
{
    upload_img();
    upload_file();
    init_select2();
    upload_gallery();
    init_iconpicker();
    add_new_field_group();
}

function init_select2()
{
    if( jQuery('.select-wrapp select').length > 0 )
    {
        jQuery('.select-wrapp select').select2();
    }
}

function init_iconpicker()
{
    if( jQuery('.iconpickers').length > 0 )
    {
        jQuery('.iconpickers').fontIconPicker();
    }
}

function init_number( selector, types )
{
    if( typeof selector == 'undefined' )
    {
        selector = '.text-number';
    }

    if( jQuery( selector ).length > 0 )
    {
        var opt = Array();
        var dom = Array();

        jQuery( selector ).each( function( e ){
            var data = jQuery(this).data();
            var prm  = new Object();
                prm.roundingMethod   = AutoNumeric.options.roundingMethod.halfUpSymmetric;
                prm.unformatOnSubmit = true;
                
            if( typeof data.aSep != 'undefined' )
            {
                prm.digitGroupSeparator = data.aSep;
            }
                
            if( typeof data.aDec != 'undefined' )
            {
                prm.decimalCharacter = data.aDec;
            }
                
            if( typeof data.mDec != 'undefined' )
            {
                prm.decimalPlaces = data.mDec;
            }

            if( typeof data.aMin != 'undefined' )
            {
                prm.minimumValue = data.aMin;
            }

            if( typeof data.aMax != 'undefined' )
            {
                prm.maximumValue = data.aMax;
            }

            if( typeof data.aSign != 'undefined' )
            {
                prm.currencySymbol = data.pSign;
            }
            else
            {
                prm.currencySymbol = '';
            }

            if( typeof data.aPos != 'undefined' )
            {
                prm.currencySymbolPlacement = data.aPos;
            }

            dom.push( this );
            opt.push( prm );
        });
        
        new AutoNumeric.multiple( dom, opt );
    }
}

function add_new_field_group()
{
    delete_field_group();
    reorder_field_group();

    jQuery('.group-field-wrapp').each( function(){
        var parent = jQuery(this);

        parent.find('.new-field .add').unbind('click');
        parent.find('.new-field .add').on('click', function(){
            var sel = parent.find('.cloned-wrapp .item').clone(); 
            var idx = Math.round(new Date().getTime() + (Math.random() * 100));
            
            sel.find('input, select, textarea').each(function(i, e){
                var dt = jQuery(this).data();

                jQuery(this).prop('disabled', false);

                if( jQuery(this).hasClass('textbox-label') )
                {
                    jQuery(this).attr('name', 'additional_fields[' + dt.parent + '][' + dt.index + '][' + idx + '][' + dt.name + '][label]');
                }
                else if( jQuery(this).hasClass('textbox-type') )
                {
                    jQuery(this).attr('name', 'additional_fields[' + dt.parent + '][' + dt.index + '][' + idx + '][' + dt.name + '][type]');
                }
                else if( jQuery(this).hasClass('upload-img-button') )
                {
                    jQuery(this).data('attname', dt.name + '-' + idx );
                }
                else
                {
                    jQuery(this).attr('name', 'additional_fields[' + dt.parent + '][' + dt.index + '][' + idx + '][' + dt.name + '][value]');
                }
            });

            parent.find('.table').removeClass('hidden');
            parent.find('.table .t-list').prepend(sel);            
            parent.find('.table .t-list .item:first-child .tinymce-2').each(function(i, e){
                var id = Math.round(new Date().getTime() + (Math.random() * 100));
                jQuery(this).attr('id', id);
                initTinyMce( '#' + id );
            });

            parent.find('.table .t-list .item:first-child .iconpicker').each(function(i, e){
                var id  = Math.round(new Date().getTime() + (Math.random() * 100));
                var pic = jQuery(this).fontIconPicker();

                jQuery(this).attr('id', id);

                pic.setIcon( 'icomoon-radio-checked' );                
            });

            parent.find('.table .t-list .item:first-child .upload-img-wrapp').each(function(i, e){
                upload_img();
            });

            reorder_field_group();
            delete_field_group();
        });
    });
}

function delete_field_group()
{
    jQuery('.table .t-list .delete').unbind();
    jQuery('.table .t-list .delete').on('click', function(){
        if( jQuery(this).parent().parent().find('.tinymce-2').length > 0 )
        {
            jQuery(this).parent().parent().find('.tinymce-2').each(function(i, e){
                tinymce.remove( '#' + jQuery(this).attr('id') );
            });
        }
        
        jQuery(this).parent().parent().remove();
    });
}

function reorder_field_group()
{
    jQuery('.table .t-list').sortable({
        revert: true
    });
}

function upload_file()
{
    var site_url = jQuery('[name=site_url]').val();
    var app_name = jQuery('[name=app_name]').val();
    var ajax_url = jQuery('[name=ajax_url]').val();
    var imgs_url = jQuery('[name=imgs_url]').val();

    jQuery('.upload-file-button').change( function( el ){
        var sel  = jQuery(this);
        var data = new FormData();
            data.append( 'pkey', 'upload-files' );
            data.append( 'file', el.currentTarget.files[0] );
            data.append( 'app_name', jQuery(this).data('attname') );
            data.append( 'allowed_file', jQuery(this).data('allowed') );

            if( jQuery('.post-id-field').length > 0 )
            {
                data.append( 'post_id', jQuery('.post-id-field').val() );
            }

            if( jQuery('.rule-text-id').length > 0 )
            {
                data.append( 'post_id', jQuery('.rule-text-id').val() );
            }
        
        var xhrObject = new XMLHttpRequest();

        xhrObject.open( 'POST', ajax_url );  
        xhrObject.onreadystatechange = function(){
            if( xhrObject.readyState == 4 && xhrObject.status == 200 )
            {
                var e = JSON.parse(xhrObject.responseText);

                if( e.result == 'success' )
                {
                    var images =
                    '<div class="box">'+
                        '<img src="' + e.data.fileurl + '" />'+
                        '<div class="overlay">'+
                            '<a data-post-id="' + e.data.post_id + '" class="file-delete" title="Delete">'+
                                '<img src="' + imgs_url + '/delete_hover.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>';

                    sel.parent().find('.upload-file').html( images );

                    delete_uploaded_file( site_url, app_name );
                }
                else
                {
                    alert( e.error );
                }
            }
        }

        xhrObject.send( data );
    });

    delete_uploaded_file();
}

function delete_uploaded_file()
{
    var site_url = jQuery('[name=site_url]').val();
    var app_name = jQuery('[name=app_name]').val();
    var ajax_url = jQuery('[name=ajax_url]').val();

    jQuery('.upload-file .file-delete').click(function(){
        var sel = jQuery(this);
        var url = ajax_url;
        var prm = new Object;
            prm.pkey     = 'delete-uploaded-files';
            prm.app_name = jQuery(this).parents('.upload-file-wrapp').find('.upload-file-button').data('attname');

            if( jQuery('.post-id-field').length > 0 )
            {
                prm.post_id = jQuery('.post-id-field').val();
            }

            if( jQuery('.rule-text-id').length > 0 )
            {
                prm.post_id = jQuery('.rule-text-id').val();
            }

        jQuery.post( url, prm, function( e ){
            if( e.result == 'success' )
            {
                sel.parent().parent().remove();
            }
            else
            {
                alert('Failed to delete this file');
            }
        }, 'json');
    });
}
        
function upload_img()
{
    var site_url = jQuery('[name=site_url]').val();
    var app_name = jQuery('[name=app_name]').val();
    var ajax_url = jQuery('[name=ajax_url]').val();
    var imgs_url = jQuery('[name=imgs_url]').val();

    jQuery('.upload-img-button').change( function( el ){
        var sel  = jQuery(this);
        var data = new FormData();
            data.append( 'pkey', 'upload-images' );
            data.append( 'file', el.currentTarget.files[0] );
            data.append( 'app_name', jQuery(this).data('attname') );

            if( jQuery('.post-id-field').length > 0 )
            {
                data.append( 'post_id', jQuery('.post-id-field').val() );
            }

            if( jQuery('.rule-text-id').length > 0 )
            {
                data.append( 'post_id', jQuery('.rule-text-id').val() );
            }
        
        var xhrObject = new XMLHttpRequest();

        xhrObject.open( 'POST', ajax_url );  
        xhrObject.onreadystatechange = function(){
            if( xhrObject.readyState == 4 && xhrObject.status == 200 )
            {
                var e = JSON.parse(xhrObject.responseText);

                if( e.result == 'success' )
                {
                    var images =
                    '<div class="box">'+
                        '<img src="' + e.data.fileurl + '" />'+
                        '<div class="overlay">'+
                            '<a data-post-id="' + e.data.post_id + '" class="img-delete" title="Delete">'+
                                '<img src="' + imgs_url + '/delete_hover.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>';

                    sel.parent().find('.upload-img').html( images );

                    delete_uploaded_img( site_url, app_name );
                }
                else
                {
                    alert( e.error );
                }
            }
        }

        xhrObject.send( data );
    });

    delete_uploaded_img();
}

function delete_uploaded_img()
{
    var site_url = jQuery('[name=site_url]').val();
    var app_name = jQuery('[name=app_name]').val();
    var ajax_url = jQuery('[name=ajax_url]').val();

    jQuery('.upload-img .img-delete').click(function(){
        var sel = jQuery(this);
        var url = ajax_url;
        var prm = new Object;
            prm.pkey     = 'delete-uploaded-images';
            prm.app_name = jQuery(this).parents('.upload-img-wrapp').find('.upload-img-button').data('attname');

            if( jQuery('.post-id-field').length > 0 )
            {
                prm.post_id = jQuery('.post-id-field').val();
            }

            if( jQuery('.rule-text-id').length > 0 )
            {
                prm.post_id = jQuery('.rule-text-id').val();
            }

        jQuery.post( url, prm, function( e ){
            if( e.result == 'success' )
            {
                sel.parent().parent().remove();
            }
            else
            {
                alert('Failed to delete this image');
            }
        }, 'json');
    });
}

function upload_custom_post_setting_og_img()
{
    var site_url = jQuery('[name=site_url]').val();
    var ajax_url = jQuery('[name=ajax_url]').val();
    var app_name = jQuery('[name=app_name]').val();

    jQuery('[name=og_img]').change( function( el ){
        var data = new FormData();
            data.append( 'app_name', app_name );
            data.append( 'meta_name', 'setting_og_image' );
            data.append( 'file_name', el.currentTarget.files[0] );
            data.append( 'pkey', 'upload-custom-post-setting-og-image' );
        
        var xhrObject = new XMLHttpRequest();

        xhrObject.open('POST', ajax_url);  
        xhrObject.onreadystatechange = function(){
            if( xhrObject.readyState == 4 && xhrObject.status == 200 )
            {
                var e = JSON.parse(xhrObject.responseText);

                if( e.result == 'success' )
                {
                    jQuery('.og-img').html( e.content );

                    delete_custom_post_setting_og_img();
                }
                else
                {
                    alert( e.data );
                }
            }
        }

        xhrObject.send( data );
    });

    delete_custom_post_setting_og_img();
}

function delete_custom_post_setting_og_img()
{
    var ajax_url = jQuery('[name=ajax_url]').val();

    jQuery('.og-img .img-delete').click(function(){
        var prm = new Object;
            prm.pkey     = 'delete-custom-post-setting-image';
            prm.meta_id  = jQuery(this).data('meta-id');

        jQuery.post( ajax_url, prm, function( e ){
            if( e.result == 'success' )
            {
                jQuery('.og-img .box').remove();
            }
            else
            {
                alert('Failed to delete this image');
            }
        }, 'json');
    });
}

function upload_custom_post_setting_hero_img()
{
    var site_url = jQuery('[name=site_url]').val();
    var ajax_url = jQuery('[name=ajax_url]').val();
    var app_name = jQuery('[name=app_name]').val();

    jQuery('[name=hero_img]').change( function( el ){
        var data = new FormData();
            data.append( 'app_name', app_name );
            data.append( 'meta_name', 'setting_hero_image' );
            data.append( 'file_name', el.currentTarget.files[0] );
            data.append( 'pkey', 'upload-custom-post-setting-hero-image' );
        
        var xhrObject = new XMLHttpRequest();

        xhrObject.open('POST', ajax_url);  
        xhrObject.onreadystatechange = function(){
            if( xhrObject.readyState == 4 && xhrObject.status == 200 )
            {
                var e = JSON.parse(xhrObject.responseText);

                if( e.result == 'success' )
                {
                    jQuery('.hero-img').html( e.content );

                    delete_custom_post_setting_hero_img();
                }
                else
                {
                    alert( e.data );
                }
            }
        }

        xhrObject.send( data );
    });

    delete_custom_post_setting_hero_img();
}

function delete_custom_post_setting_hero_img()
{
    var ajax_url = jQuery('[name=ajax_url]').val();

    jQuery('.hero-img .img-delete').click(function(){
        var prm = new Object;
            prm.pkey     = 'delete-custom-post-setting-image';
            prm.meta_id  = jQuery(this).data('meta-id');

        jQuery.post( ajax_url, prm, function( e ){
            if( e.result == 'success' )
            {
                jQuery('.hero-img .box').remove();
            }
            else
            {
                alert('Failed to delete this image');
            }
        }, 'json');
    });
}

function upload_custom_post_icon()
{
    var ajax_url = jQuery('[name=ajax_url]').val();

    jQuery('[name=licon]').change( function( el ){
        var data = new FormData();
            data.append( 'custom_icon', jQuery('[name=lcustom_icon]').val() );
            data.append( 'custom_id', jQuery('[name=lcustom_id]').val() );
            data.append( 'file_name', el.currentTarget.files[0] );
            data.append( 'pkey', 'upload-custom-post-icon' );
        
        var xhrObject = new XMLHttpRequest();

        xhrObject.open('POST', ajax_url);  
        xhrObject.onreadystatechange = function(){
            if( xhrObject.readyState == 4 && xhrObject.status == 200 )
            {
                var e = JSON.parse(xhrObject.responseText);

                if( e.result == 'success' )
                {
                    jQuery('.custom-post-icon').html( e.content );

                    delete_custom_post_icon();
                }
                else
                {
                    alert( e.data );
                }
            }
        }

        xhrObject.send( data );
    });

    delete_custom_post_icon();
}

function delete_custom_post_icon()
{
    var ajax_url = jQuery('[name=ajax_url]').val();

    jQuery('.custom-post-icon .img-delete').click(function(){
        var prm = new Object;
            prm.pkey        = 'delete-custom-post-icon';
            prm.custom_id   = jQuery('[name=lcustom_id]').val();
            prm.custom_icon = jQuery('[name=lcustom_icon]').val();

        jQuery.post( ajax_url, prm, function( e ){
            if( e.result == 'success' )
            {
                jQuery('.custom-post-icon .box').remove();
            }
            else
            {
                alert('Failed to delete this image');
            }
        }, 'json');
    });
}

function set_custom_post_setting_action()
{
    upload_custom_post_icon();
    upload_custom_post_setting_og_img();
    upload_custom_post_setting_hero_img();

    jQuery('#hero_image_0').click(function(){
        jQuery('#hero_image_details_0').slideToggle( 100 );

        return false;
    });

    jQuery('#meta_data_0').click(function(){
        jQuery('#meta_data_details_0').slideToggle( 100 );

        return false;
    });
}

Dropzone.autoDiscover = false;

function upload_gallery()
{
    if( jQuery('.block-gallery').length > 0 )
    {
        var site_url = jQuery('[name=site_url]').val(); 
        var ajax_url = jQuery('[name=ajax_url]').val(); 

        jQuery('.block-gallery').each(function(i, e){
            var elementid = jQuery(this).attr('id');
            var gDropzone = new Dropzone('#' + elementid + ' .dropzone', { 
                previewTemplate: document.querySelector('#' + elementid + ' .dz-preview').innerHTML,
                maxFilesize: 2,
                url: ajax_url,
                init: function()
                {
                    var url = ajax_url;
                    var prm = new Object;
                        prm.pkey     = 'get-images';
                        prm.app_name = jQuery('#' + elementid ).data('attname');

                        if( jQuery('.post-id-field').length > 0 )
                        {
                            prm.post_id = jQuery('.post-id-field').val();
                        }

                        if( jQuery('.rule-text-id').length > 0 )
                        {
                            prm.post_id = jQuery('.rule-text-id').val();
                        }

                    jQuery.post(url, prm, function(e){
                        if( e.result == 'success' )
                        {
                            jQuery.each( e.data, function(i, ele){
                                var mockFile = { 
                                    url: ele.url,
                                    alt: ele.alt,
                                    title: ele.title,
                                    id: ele.attach_id,
                                    caption: ele.caption,
                                    content: ele.content
                                };

                                gDropzone.emit('addedfile', mockFile);
                                gDropzone.emit('thumbnail', mockFile, ele.fileturl);
                                gDropzone.emit('complete', mockFile);
                                gDropzone.files.push(mockFile);
                            });
                        }
                    },'json');

                    this.on('addedfile', function(file) {
                        var sel     = jQuery(file.previewElement);
                        var id      = typeof file.id != 'undefined' ? file.id : '';
                        var alt     = typeof file.alt != 'undefined' ? file.alt : '';
                        var title   = typeof file.title != 'undefined' ? file.title : '';
                        var caption = typeof file.caption != 'undefined' ? file.caption : '';
                        var content = typeof file.content != 'undefined' ? file.content : '';

                        sel.attr('id','gallery_'+id);
                        sel.find('.dz-detail-box .gallery-alt').val(alt);
                        sel.find('.dz-detail-box .gallery-title').val(title);
                        sel.find('.dz-detail-box .gallery-caption').val(caption);

                        sel.find('.dz-delete').attr('rel', id);
                        sel.find('.dz-detail-box .save-detail').attr('rel', id);
                        sel.find('.dz-detail-box .gallery-content').attr('id', 'gtiny-mce-' + id).val(content);

                        if( jQuery('#gtiny-mce-' + id).length > 0 )
                        {
                            initTinyMce( '#gtiny-mce-' + id );
                        }

                        sel.find('.dz-edit').unbind('click');
                        sel.find('.dz-edit').click(function(){
                            sel.toggleClass('opened');
                        });

                        sel.find('.dz-delete').unbind('click');
                        sel.find('.dz-delete').click(function(){
                            var url = ajax_url;
                            var prm = new Object;
                                prm.pkey      = 'delete-uploaded-images';
                                prm.attach_id = jQuery(this).attr('rel');

                            jQuery.post(url, prm, function(e){

                                if(e.result=='success')
                                {
                                    gDropzone.removeFile(file);
                                }
                                else
                                {
                                    alert('Failed to delete this image');
                                }

                            },'json');
                        }); 

                        sel.find('.dz-detail-box .save-detail').unbind('click');
                        sel.find('.dz-detail-box .save-detail').click(function(){
                            var aid = jQuery(this).attr('rel');
                            var url = ajax_url;
                            var prm = new Object;
                                prm.attach_id = aid;
                                prm.pkey      = 'edit-image-info';
                                prm.alt       = sel.find('.dz-detail-box .gallery-alt').val();  
                                prm.title     = sel.find('.dz-detail-box .gallery-title').val();
                                prm.caption   = sel.find('.dz-detail-box .gallery-caption').val(); 
                                prm.content   = tinymce.get('gtiny-mce-' + aid).getContent();

                            jQuery.post(url, prm, function(e){
                                sel.removeClass('opened');
                            },'json');

                            return false;
                        });

                        sel.find('.dz-detail-box .close-detail').unbind('click');
                        sel.find('.dz-detail-box .close-detail').click(function(){
                            sel.removeClass('opened');
                        });
                    });

                    this.on('error', function(file, message) { 
                        //gDropzone.removeFile(file);

                        jQuery(file.previewElement).addClass('dz-error').find('.dz-error-message').text( message );
                    });

                    this.on('success', function(file, responseText){
                        var e = jQuery.parseJSON(responseText);

                        if( e.result = 'success' )
                        {
                            jQuery(file.previewElement).attr('id','gallery_' + e.data.attach_id);
                            jQuery(file.previewElement).find('.dz-delete').attr('rel', e.data.attach_id);
                            jQuery(file.previewElement).find('.dz-detail-box .gallery-alt').val(e.data.alt);
                            jQuery(file.previewElement).find('.dz-detail-box .gallery-title').val(e.data.title);
                            jQuery(file.previewElement).find('.dz-detail-box .gallery-caption').val(e.data.caption);
                            jQuery(file.previewElement).find('.dz-detail-box .save-detail').attr('rel', e.data.attach_id);
                            jQuery(file.previewElement).find('.dz-detail-box .gallery-content').attr('id', 'gtiny-mce-' + e.data.attach_id).val(e.data.content);

                            if( jQuery('#gtiny-mce-' + e.data.attach_id).length > 0 )
                            {
                                initTinyMce( '#gtiny-mce-' + e.data.attach_id );
                            }
                        }
                        else
                        {
                            gDropzone.removeFile(file);   
                        }
                    });

                    this.on('sending', function(file, xhr, formData){
                        formData.append( 'batch', true );
                        formData.append( 'pkey', 'upload-images' );
                        formData.append( 'app_name', jQuery('#' + elementid ).data('attname') );

                        if( jQuery('.post-id-field').length > 0 )
                        {
                            formData.append( 'post_id', jQuery('.post-id-field').val() );
                        }

                        if( jQuery('.rule-text-id').length > 0 )
                        {
                            formData.append( 'post_id', jQuery('.rule-text-id').val() );
                        }
                    });
                }
            });
        });

        reorder_gallery();
    }
}

function reorder_gallery()
{
    var ajax_url = jQuery('[name=ajax_url]').val(); 

    jQuery('.block-gallery .dropzone').nestedSortable({
        handle: 'div',
        items: '.dz-preview',
        listType:'div',
        opacity: .6,
        tolerance: 'pointer',
        maxLevels:0,
        stop: function(eve, ui){
            var url  = ajax_url;
            var list = jQuery(this).nestedSortable('toArray');
            var objc = new Array;

            jQuery.each(list, function( i, e ){
                if( !jQuery.isEmptyObject(e.item_id) )
                {
                    objc.push(e.item_id);
                }
            });

            var param = new Object;
                param.pkey  = 'reorder-images';
                param.value = JSON.stringify(objc);

                if( jQuery('.post-id-field').length > 0 )
                {
                    param.id = jQuery('.post-id-field').val();
                }

                if( jQuery('.rule-text-id').length > 0 )
                {
                    param.id = jQuery('.rule-text-id').val();
                }

            jQuery.post(url, param, function( el ){             
                jQuery.each(objc, function( i, id ){
                    if( tinymce.EditorManager.execCommand( 'mceRemoveEditor', true, 'gtiny-mce-' + id ) )
                    {
                        tinymce.EditorManager.execCommand( 'mceAddEditor', true, 'gtiny-mce-' + id );
                    }
                });             
            },'json');
        }
    });
}

function initmap()
{
    var deflat  = jQuery('[name=default_latitude]').val();
    var deflong = jQuery('[name=default_longitude]').val();

    var geo = new google.maps.Geocoder();
    var map = new google.maps.Map( document.getElementById('map'), {
        center: { lat: Number( deflat ), lng: Number( deflong ) },
        zoom: 15
    });

    var marker = new google.maps.Marker({
        position: { lat: Number( deflat ), lng: Number( deflong ) },
        animation: google.maps.Animation.DROP,
        draggable: true,
        map: map
    });

    google.maps.event.addListener( marker, 'dragend', function( evt ){
        var long = evt.latLng.lng().toFixed(6) ;   
        var lat  = evt.latLng.lat().toFixed(6);
        var coor = lat + ',' + long;

        jQuery('.map-coordinate').val( coor );

        map.setCenter( marker.position );
    });

    jQuery('.map-coordinate').focusout(function(){
        var value = jQuery(this).val();
        var pos   = value.split( ',' );

        if( jQuery.isEmptyObject( pos ) === false && pos.length == 2 )
        {
            if( pos[0] == '' || pos[1] == '' || isNaN( Number( pos[0] ) ) || isNaN( Number( pos[1] ) ) )
            {
                jQuery('.map-coordinate').val('').attr( 'placeholder', 'Invalid coordinate' ).addClass('error');
            }
            else
            {
                var cord = new Object;
                    cord.lat = Number( pos[0] );
                    cord.lng = Number( pos[1] );

                map.setCenter( cord );
                marker.setPosition( cord );
            }
        }
        else
        {
            jQuery('.map-coordinate').val('').attr( 'placeholder', 'Invalid coordinate' ).addClass( 'error' );
        }
    });

    jQuery('.map-coordinate').focusin(function(){
        jQuery(this).removeAttr( 'placeholder' ).removeClass( 'error' );
    });
}

function set_select2()
{
    if( jQuery('.select-option').length > 0 )
    {
        jQuery('.select-option').select2();
    }
}

function set_datepicker()
{
    if( jQuery('.date_textbox').length > 0 )
    {
        jQuery('.date_textbox').datepicker({
            dateFormat:'dd MM yy'
        });
    }
}

jQuery(document).ready(function(){
    set_select2();
    set_datepicker();
    set_navigation_action();
    set_custom_field_action();
    set_language_button_action();
    set_custom_post_setting_action();
});