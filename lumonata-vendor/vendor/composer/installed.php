<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => 'f45c01b6f62d9f92e7d7ab50ee95a12382e50fc6',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'f45c01b6f62d9f92e7d7ab50ee95a12382e50fc6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doku/jokul-php-library' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'reference' => 'fafccb5eb1e91f1149bc1fc5d29864ce6cb6b44f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doku/jokul-php-library',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'dompdf/dompdf' => array(
            'pretty_version' => 'v0.8.3',
            'version' => '0.8.3.0',
            'reference' => '75f13c700009be21a1965dc2c5b68a8708c22ba2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dompdf/dompdf',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'paypal/rest-api-sdk-php' => array(
            'pretty_version' => '1.14.0',
            'version' => '1.14.0.0',
            'reference' => '72e2f2466975bf128a31e02b15110180f059fc04',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paypal/rest-api-sdk-php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phenx/php-font-lib' => array(
            'pretty_version' => '0.5.4',
            'version' => '0.5.4.0',
            'reference' => 'dd448ad1ce34c63d09baccd05415e361300c35b4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phenx/php-font-lib',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phenx/php-svg-lib' => array(
            'pretty_version' => 'v0.3.3',
            'version' => '0.3.3.0',
            'reference' => '5fa61b65e612ce1ae15f69b3d223cb14ecc60e32',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phenx/php-svg-lib',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpmailer/phpmailer' => array(
            'pretty_version' => 'v6.6.3',
            'version' => '6.6.3.0',
            'reference' => '9400f305a898f194caff5521f64e5dfa926626f3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpmailer/phpmailer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sabberworm/php-css-parser' => array(
            'pretty_version' => '8.4.0',
            'version' => '8.4.0.0',
            'reference' => 'e41d2140031d533348b2192a83f02d8dd8a71d30',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sabberworm/php-css-parser',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
