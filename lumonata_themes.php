<?php

//-- Check default template that set in database here
if( $lumonata_admin == false )
{
    $theme = set_front_end_themes();
    
    //-- After the templates name is found, the next step then is to configure the templates
    //-- and set all variable that need to be set in the template
    require_once( ROOT_PATH . '/lumonata-functions/template.php' );
    
    //-- you can use existing function set_front_end_content() to set default content;
    //-- or use your another function and set default content to empty
    $thecontent = set_front_end_content();
}
else
{
    $theme = set_admin_themes();
    
    //-- After the templates name is found, the next step then is to configure the templates
    //-- and set all variable that need to be set in the template
    require_once( ROOT_PATH . '/lumonata-functions/template.php' );
    
    $thecontent = set_admin_content();
}

$theme = ( empty( $theme ) ? 'default' : $theme );

function set_post_type_admin()
{
    run_actions( 'custom-post-config' );
    run_actions( 'custom-field-config' );
}

function set_admin_content()
{
    //-- Set privileges of login user
    //-- baseon privileges setting
    set_privileges_admin();
    set_post_type_admin();
    
    if( is_dashboard() )
    {
        $thecontent = get_dashboard();
    }
    elseif( is_global_settings() )
    {
        $thecontent = get_global_settings();
    }
    elseif( is_admin_themes() )
    {
        $thecontent = get_themes();
    }
    elseif( is_admin_page() )
    {
        $thecontent = get_admin_page();
    }
    elseif( is_admin_article() )
    {
        $thecontent = get_admin_article();
    }
    elseif( is_profile_picture() )
    {
        $thecontent = edit_profile_picture();
    }
    elseif( is_profile() )
    {
        $thecontent = edit_profile();
    }
    elseif( is_admin_user() )
    {
        $thecontent = get_admin_user();
    }
    elseif( is_admin_plugin() )
    {
        $thecontent = get_plugins();
    }
    elseif( is_admin_menus() )
    {
        $thecontent = set_menus();
    }
    elseif( is_admin_comment() )
    {
        $thecontent = admin_comments();
    }
    elseif( is_admin_ajax() )
    {
        $thecontent = run_actions( $_GET[ 'apps' ] . '_admin_page' );
    }
    elseif( is_logout() )
    {
        do_logout();
    }
    else
    {
        if( isset( $_GET[ 'sub' ] ) )
        {
            if( is_grant_app( $_GET[ 'sub' ] ) )
            {
                $thecontent = run_actions( $_GET[ 'sub' ] );
            }
        }
        elseif( isset( $_GET[ 'state' ] ) )
        {
            if( is_grant_app( $_GET[ 'state' ] ) )
            {
                $thecontent = run_actions( $_GET[ 'state' ] );
            }
        }
        
        if( empty( $thecontent ) )
        {
            $thecontent = '<div class="alert_red_form">You don\'t have an authorization to access this page</div>';
        }
        else
        {
            $thecontent = $thecontent;
        }        
    }
    
    //-- if still empty
    global $actions;
    
    if( isset( $actions->action[ 'is_use_ajax' ] ) && $actions->action[ 'is_use_ajax' ][ 'func_name' ][ 0 ] )
    {
        $thecontent = $thecontent;
    }
    else if( empty( $thecontent ) )
    {
        $thecontent = run_actions( 'the_404_page' );
    }
    
    return $thecontent;
    
}

function set_front_end_content()
{
    global $actions;

    $sef = get_uri_sef();

    if( isset( $actions->action[ 'is_use_ajax' ] ) && $actions->action[ 'is_use_ajax' ][ 'func_name' ][ 0 ] )
    {
        $thecontent = $thecontent;
    }
    elseif( isset( $actions->action[ $sef . '_page' ] ) && $actions->action[ $sef . '_page' ][ 'func_name' ][ 0 ] )
    {
        $thecontent = run_actions( $sef . '_page' );
    }
    else if( empty( $thecontent ) )
    {
        $thecontent = run_actions( 'the_404_page' );
    }
    
    return $thecontent;
}

function set_admin_themes()
{
    $theme = ( is_preview() ? $_GET[ 'theme' ] : get_meta_data( 'admin_theme', 'themes' ) );
    
    define( 'TEMPLATE_PATH', base_url( 'lumonata-admin/themes/' . $theme ) );
    define( 'TEMPLATE_URL', site_url( 'lumonata-admin/themes/' . $theme ) );
    define( 'FRONT_TEMPLATE_PATH', base_url( 'lumonata-content/themes' ) );
    define( 'FRONT_TEMPLATE_URL', site_url( 'lumonata-content/themes' ) );
    define( 'ADMIN_TEMPLATE_PATH', base_url( 'lumonata-admin/themes' ) );
    define( 'ADMIN_TEMPLATE_URL', site_url( 'lumonata-admin/themes' ) );
    define( 'LUMONATA_ADMIN', TRUE );
    
    return $theme;
}

function set_front_end_themes()
{
    $theme = ( is_preview() ? $_GET[ 'theme' ] : get_meta_data( 'front_theme', 'themes' ) );
    
    define( 'TEMPLATE_PATH', base_url( 'lumonata-content/themes/' . $theme ) );
    define( 'TEMPLATE_URL', site_url( 'lumonata-content/themes/' . $theme ) );
    define( 'FRONT_TEMPLATE_PATH', base_url( 'lumonata-content/themes' ) );
    define( 'FRONT_TEMPLATE_URL', site_url( 'lumonata-content/themes' ) );
    define( 'LUMONATA_ADMIN', FALSE );
    
    return $theme;
}

function get_custome_bg()
{
    $bgcolor      = get_meta_data( 'custome_bg_color', 'themes' );
    $bgimage_prev = get_meta_data( 'custome_bg_image_preview', 'themes' );
    $bgimage      = get_meta_data( 'custome_bg_image', 'themes' );
    $bgpos        = get_meta_data( 'custome_bg_pos', 'themes' );
    $bgrepeat     = get_meta_data( 'custome_bg_repeat', 'themes' );
    $bgattach     = get_meta_data( 'custome_bg_attachment', 'themes' );
    
    $style_ent = '';
    $style     = '';
    
    if( !empty( $bgcolor ) )
    {
        $style_ent .= 'background-color:#' . $bgcolor . ';';
    }
    
    if( !empty( $bgimage ) )
    {
        $style_ent .= 'background-image: url("' . site_url( $bgimage ) . '");';
    }
    
    if( !empty( $bgpos ) )
    {
        $style_ent .= 'background-position:' . $bgpos . ';';
    }
    
    if( !empty( $bgrepeat ) )
    {
        $style_ent .= 'background-repeat:' . $bgrepeat . ';';
    }
    
    if( !empty( $bgattach ) )
    {
        $style_ent .= 'background-attachment:' . $bgattach . ';';
    }
    
    if( !empty( $style_ent ) )
    {
        return $style = '<style type="text/css">body{' . $style_ent . '}</style>';
    }
}

if( file_exists( TEMPLATE_PATH . '/template.php' ) )
{
    require_once( TEMPLATE_PATH . '/template.php' );
}
else
{
    echo '<h1>Template File Not Found</h1><p>Make sure that you already create the <code>template.php</code> at ' . TEMPLATE_PATH . '</p>';

    die();
}

?>