<?php

class flash_message
{
    function render()
    {
        if( !isset( $_SESSION[ 'messages' ] ) )
        {
            return null;
        }

        $messages = $_SESSION[ 'messages' ];

        unset( $_SESSION[ 'messages' ] );

        return $messages;
    }

    function add( $message )
    {
        if( !isset( $_SESSION[ 'messages' ] ) )
        {
            $_SESSION[ 'messages' ] = array();
        }

        $_SESSION[ 'messages' ][] = $message;
    }

}

$flash = new flash_message();

?>