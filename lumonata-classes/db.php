<?php

class db
{
    public $ready = false;

    public $dbpassword;
    public $hostname;
    public $dbuser;
    public $result;
    public $dbc;
    
    function __construct( $hostname, $dbuser, $dbpassword, $dbname )
    {        
        $this->hostname   = $hostname;
        $this->dbuser     = $dbuser;
        $this->dbpassword = $dbpassword;
        
        $this->dbc = $this->lconnect();

        if( !$this->dbc )
        {
            $html = '
            <h1>Error establishing a database connection</h1>
            <p>
                This either means that the username and password information in your <code>lumonata-config.php</code> 
                file is incorrect or we can\'t contact the database server at <code>%s</code>. This could mean your host\'s database server is down.
            </p>
            <ul>
                <li>Are you sure you have the correct username and password?</li>
                <li>Are you sure that you have typed the correct hostname?</li>
                <li>Are you sure that the database server is running?</li>
            </ul>';

            $this->ready = false;

            $this->db_error( sprintf( $html, $hostname ) );

            return false;
        }

        $this->ready = true;

        $this->lselect_db( $dbname );
    }
    
    function db_error( $message )
    {
        echo $message;

        die();
    }

    function lconnect()
    {
        return $this->result = mysqli_connect( $this->hostname, $this->dbuser, $this->dbpassword );
    }

    function lselect_db( $dbname )
    {
        if( !mysqli_select_db( $this->dbc, $dbname ) )
        {
            $html = '
            <h1>Can&#8217;t select database</h1>
            <p>We were able to connect to the database server (which means your username and password is okay) but not able to select the <code>%1$s</code> database.</p>
            <ul>
                <li>Are you sure it exists?</li>
                <li>Does the user <code>%2$s</code> have permission to use the <code>%1$s</code> database?</li>
                <li>On some systems the name of your database is prefixed with your username, so it would be like <code>username_%1$s</code>. Could that be the problem?</li>
            </ul>
            <p>If you don\'t know how to setup a database you should <strong>contact your host</strong>.';

            $this->ready = false;

            $this->db_error( sprintf( $html, $this->dbc, DBUSER ) );
            
            return false;
        }
    }

    function do_query( $query )
    {
        if( !$this->ready )
        {
            return false;
        }

        $result = mysqli_query( $this->dbc, $query );

        if( $result === false )
        {
            return array( 'result'=>'error-query', 'error_code'=>mysqli_errno( $this->dbc ), 'message'=>mysqli_error( $this->dbc ) );
        }
        else
        {
            return $result;
        }
    }

    function begin()
    {
        $null = $this->do_query( 'START TRANSACTION' );

        return $this->do_query( 'BEGIN' );
    }

    function commit()
    {
        $this->do_query( 'COMMIT' );
    }

    function rollback()
    {
        $this->do_query( 'ROLLBACK' );
    }

    function fetch_array( $result )
    {
        return mysqli_fetch_array( $result );
    }

    function fetch_assoc( $result )
    {
        return mysqli_fetch_assoc( $result );
    }

    function fetch_object( $result )
    {
        return mysqli_fetch_object( $result );
    }

    function num_rows( $result )
    {
        return mysqli_num_rows( $result );
    }
    
    function insert( $table, $data )
    {
        $data = $this->add_magic_quotes( $data );

        $fields = array_keys( $data );
        
        return $this->do_query( "INSERT INTO $table (`" . implode( '`,`', $fields ) . "`) VALUES ('" . implode( "','", $data ) . "')" );
    }

    function insert_id()
    {
        return mysqli_insert_id( $this->dbc );
    }

    function update( $table, $data, $where )
    {
        $data = $this->add_magic_quotes( $data );
        $bits = $wheres = array();

        foreach( array_keys( $data ) as $k )
        {
            $bits[] = "`$k` = '$data[$k]'";
        }
        
        if( is_array( $where ) )
        {
            foreach( $where as $c => $v )
            {
                $wheres[] = "$c = '" . $this->escape( $v ) . "'";
            }
        }
        else
        {
            return false;
        }
        
        return $this->do_query( "UPDATE $table SET " . implode( ', ', $bits ) . ' WHERE ' . implode( ' AND ', $wheres ) );
    }

    public function delete( $table, $where )
    {
        $w = array(  );

        if ( is_array( $where ) )
        {
            foreach ( $where as $c => $v )
            {
                $w[  ] = "$c = '" . $this->escape( $v ) . "'";
            }
        }
        else
        {
            return false;
        }

        return $this->do_query( "DELETE FROM $table WHERE " . implode( ' AND ', $w ) );
    }

    function prepare_query( $args = NULL )
    {
        if( NULL === $args )
        {
            return;
        }

        $args  = func_get_args();
        $query = array_shift( $args );
        $query = str_replace( "'%s'", '%s', $query ); //-- in case someone mistakenly already singlequoted it
        $query = str_replace( '"%s"', '%s', $query ); //-- doublequote unquoting
        $query = str_replace( '%s', "'%s'", $query ); //-- quote the strings

        array_walk( $args, array( &$this, 'escape_by_ref' ) );

        return @vsprintf( $query, $args );
    }
    
    function add_magic_quotes( $array )
    {
        foreach( $array as $k => $v )
        {
            if( is_array( $v ) )
            {
                $array[$k] = $this->add_magic_quotes( $v );
            }
            else
            {
                $array[$k] = $this->escape( $v );
            }
        }

        return $array;
    }
    
    function _weak_escape( $string )
    {
        return addslashes( $string );
    }
    
    function _real_escape( $string )
    {
        if( $this->result )
        {
            return mysqli_real_escape_string( $this->dbc, $string );
        }
        else
        {
            return addslashes( $string );
        }
    }
    
    function _escape( $data )
    {
        if( is_array( $data ) )
        {
            foreach( (array) $data as $k => $v )
            {
                if( is_array( $v ) )
                {
                    $data[$k] = $this->_escape( $v );
                }
                else
                {
                    $data[$k] = $this->_real_escape( $v );
                }
            }
        }
        else
        {
            $data = $this->_real_escape( $data );
        }
        
        return $data;
    }
    
    /**
     * Escapes content for insertion into the database using addslashes(), for security
     *
     * @since 0.71
     *
     * @param string|array $data
     * @return string query safe string
     */
    function escape( $data )
    {
        if( is_array( $data ) )
        {
            foreach( (array) $data as $k => $v )
            {
                if( is_array( $v ) )
                {
                    $data[$k] = $this->escape( $v );
                }
                else
                {
                    $data[$k] = $this->_weak_escape( $v );
                }
            }
        }
        else
        {
            $data = $this->_weak_escape( $data );
        }
        
        return $data;
    }
    
    /**
     * Escapes content by reference for insertion into the database, for security
     *
     * @since 2.3.0
     *
     * @param string $s
     */
    function escape_by_ref( &$string )
    {
        $string = $this->_real_escape( $string );        
    }
}

$db = new db( HOSTNAME, DBUSER, DBPASSWORD, DBNAME );

?>