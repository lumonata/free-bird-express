<?php

class user_privileges
{
    var $user_type;
    var $the_privileges;
    var $the_actions;

    function __construct()
    {
        $this->the_privileges = array();
    }

    function add_privileges( $role, $app_name, $action )
    {
        $this->the_privileges[ $role ][ $app_name ][] = $action;
    }

    function get_privileges()
    {
        return $this->the_privileges;
    }
}

class apps_privileges
{
    var $app_name;

    function __construct()
    {
        $this->app_name = array();
    }

    function add_apps_privileges( $app_name, $label )
    {
        $this->app_name[ $app_name ] = $label;
    }

    function get_apps_privileges()
    {
        return $this->app_name;
    }
}

function add_privileges( $role, $app_name, $action )
{
    global $user_privileges;

    $user_privileges->add_privileges( $role, $app_name, $action );
}

function is_grant_app( $app_name )
{
    global $user_privileges;

    $the_privilege = $user_privileges->get_privileges();

    if( !isset( $_COOKIE['user_type'] ) )
    {
        return false;
    }

    if( isset( $the_privilege[ $_COOKIE['user_type'] ] ) )
    {
        foreach( $the_privilege[ $_COOKIE['user_type'] ] as $key => $val )
        {
            if( $key == $app_name )
            {
                return true;
            }
        }
    }

    return false;
}

function allow_action( $app_name, $action )
{
    global $user_privileges;

    $the_privilege = $user_privileges->get_privileges();

    if( !isset( $_COOKIE['user_type'] ) )
    {
        return false;
    }

    if( isset( $the_privilege[ $_COOKIE['user_type'] ][ $app_name ] ) )
    {
        foreach( $the_privilege[ $_COOKIE['user_type'] ][ $app_name ] as $key => $val )
        {
            if( $val == $action )
            {
                return true;

            }

        }
    }

    return false;
}

function add_apps_privileges( $app_name, $label )
{
    global $apps_privileges;

    $apps_privileges->add_apps_privileges( $app_name, $label );
}

$user_privileges = new user_privileges();
$apps_privileges = new apps_privileges();

?>