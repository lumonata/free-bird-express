<?php

$admin_menu = new admin_menu();

class admin_menu
{
    function __construct()
    {
        $this->apps_menu       = array();
        $this->connection_menu = array( 'dashboard' => 'Dashboard' );
        $this->main_menu       = array( 'blogs' => 'Blogs', 'pages' => 'Pages' );
        $this->plugins_menu    = array( 'installed' => 'Installed', 'active' => 'Active', 'inactive' => 'Inactive' );
        $this->settings_menu   = array( 'users' => 'Users', 'plugins' => 'Plugins', 'custom-post' => 'Post Type', 'custom-field' => 'Custom Fields', 'global_settings' => 'Settings', 'menus' => 'Menus' );
    }

    function add_main_menu( $menu )
    {
        if( !is_array( $menu ) )
        {
            return;
        }

        $this->main_menu = array_merge( $this->main_menu, $menu );
    }

    function add_sub_menu( $parent, $submenu )
    {
        if( !is_array( $submenu ) )
        {
            return;
        }

        $this->submenu[$parent] = $submenu;
    }

    function add_plugins_menu( $menu )
    {
        if( !is_array( $menu ) )
        {
            return;
        }

        $this->plugins_menu = array_merge( $this->plugins_menu, $menu );
    }

    function add_apps_menu( $menu )
    {
        if( !is_array( $menu ) )
        {
            return;
        }

        $this->apps_menu = array_merge( $this->apps_menu, $menu );
    }

    function get_admin_menu( $type = 'main_menu' )
    {
        switch( $type )
        {
            case 'main_menu':
                $themenu = $this->main_menu;
                break;
            case 'connection_menu':
                $themenu = $this->connection_menu;
                break;
            case 'settings_menu':
                $themenu = $this->settings_menu;
                break;
        }

        $menu = '';

        foreach( $themenu as $key => $val )
        {
            if( $key == 'applications' )
            {
                $sub = $this->get_apps_menu();
            }
            elseif( $key == 'plugins' )
            {
                $sub = $this->get_plugins_menu();
            }
            elseif( $key == 'people' )
            {
                $sub = $this->get_people_categories();
            }
            else
            {
                if( isset( $this->submenu[$key] ) && is_array( $this->submenu[$key] ) )
                {
                    if( $_GET['state'] == $key )
                    {
                        $display = '';
                    }
                    else
                    {
                        $display = 'style="display:none;"';
                    }

                    $sub = '
                    <ul id="' . $key . '_list" $display>';

	                    foreach( $this->submenu[$key] as $subkey => $subval )
	                    {
	                        if( is_preview() )
	                        {
	                            $theme = $_GET['theme'];
	                            $sub .= '
                                <li>
                                    <a href="?state=' . $key . '&sub=' . $subkey . '&preview=true&theme=' . $theme . '">
                                        ' . $subval . '
                                    </a>
                                </li>';
	                        }
	                        else
	                        {
	                            if( is_grant_app( $key ) )
                                {
	                                $sub .= '
                                    <li>
                                        <a href="?state=' . $key . '&sub=' . $subkey . '">
                                            ' . $subval . '
                                        </a>
                                    </li>';
                                }
	                        }
	                    }

                    	$sub .= '
                    </ul>

                    <script type=\"text/javascript\">
						$(function(){
							$("a#' . $key . '").click(function(){
								$("#' . $key . '_list").slideToggle(100);
								return false;
							});
						});
					</script>';
                }
                else
                {
                    $sub = '';
                }
            }

            $class_name = $key;

            if( is_preview() )
            {
                $theme = $_GET['theme'];

                if( is_grant_app( $key ) )
                {
                    $menu .= '
                    <li class="' . $class_name . '">
                        <a href="?state=' . $key . '&preview=true&theme=' . $theme .'" id="' . $key .'">
                            ' . $val . '
                        </a>
                        '. $sub .'
                    </li>';
                }
            }
            else
            {
                if( is_grant_app( $key ) )
                {
                    if( $key == 'applications' || $key == 'plugins' )
                    {
                        $menu .= '
                        <li class="' . $class_name . '">
                        	<a href="#" id="' . $key .'">
                                ' . $val . '
                            </a>
                            '. $sub .'
                        </li>';
                    }
                    else
                    {
                        $menu .= '
                        <li class="' . $class_name . '">
                        	<a href="?state=' . $key .'" id="' . $key .'">
                                ' . $val . '
                            </a>
                            '. $sub .'
                        </li>';
                    }
                }
            }
        }

        if( !empty( $menu ) )
        {
            $menu =  '<ul>' . $menu . '</ul>';
        }

        return $menu;
    }

    function get_apps_menu()
    {
        $menu_set = $this->apps_menu;

        if( empty( $menu_set ) )
        {
            return;
        }

        if( $_GET['state'] == 'applications' )
        {
            $display = "";
        }
        else
        {
            $display = "style='display:none;'";
        }

        $menu = "
        <ul id=\"applications_list\" $display>";
	        foreach( $menu_set as $key => $val )
	        {
	            if( is_grant_app( $key ) )
	            {
	                if( is_preview() )
	                {
	                    $theme = $_GET['theme'];
	                    $menu .= "<li><a href=\"?state=applications&sub=$key&preview=true&theme=$theme\">$val</a></li>";
	                }
	                else
	                {
	                    if( $key == 'installed' )
	                    {
	                        if( allow_action( 'applications', 'install' ) )
	                        {
	                            $menu .= "<li><a href=\"?state=applications&sub=$key\">$val</a></li>";
	                        }
	                    }
	                    else
	                    {
	                        if( is_grant_app( 'applications' ) )
	                        {
	                            $menu .= "<li><a href=\"?state=applications&sub=$key\">$val</a></li>";
	                        }
	                    }
	                }
	            }
	        }
        	$menu .= "
        </ul>";

        return $menu;
    }

    function get_plugins_menu()
    {
        $menu_set = $this->plugins_menu;

        if( empty( $menu_set ) )
        {
            return;
        }

        if( $_GET['state'] == 'plugins' )
        {
            $display = "";
        }
        else
        {
            $display = "style='display:none;'";
        }

        $menu = "
        <ul id=\"plugins_list\" $display>";
	        foreach( $menu_set as $key => $val )
	        {
	            if( is_preview() )
	            {
	                $theme = $_GET['theme'];
	                $menu .= "<li><a href=\"?state=plugins&sub=$key&preview=true&theme=$theme\">$val</a></li>";
	            }
	            else
	            {
	                if( $key == 'installed' )
	                {
	                    if( allow_action( 'plugins', 'install' ) )
	                    {
	                        $menu .= "<li><a href=\"?state=plugins&sub=$key\">$val</a></li>";
	                    }
	                }
	                else
	                {
	                    if( is_administrator() )
	                    {
	                        $menu .= "<li><a href=\"?state=plugins&sub=$key\">$val</a></li>";
	                    }
	                }
	            }
	        }
	        $menu .= "
		</ul>";

        return $menu;
    }

    function get_people_categories()
    {
        global $db;

        if( $_GET['state'] == 'people' )
        {
            $display = "";
        }
        else
        {
            $display = "style='display:none;'";
        }

        $menu = "
        <ul id=\"people_list\" $display>";
	        $expert = get_expertise_categories();

	        while( $themenu = $db->fetch_array( $expert ) )
	        {
	            if( is_preview() )
	            {
	                $theme = $_GET['theme'];
	                $menu .= "<li><a href=\"?state=people&cat=" . $themenu['lsef'] . "&preview=true&theme=$theme\">" . $themenu['lname'] . "</a></li>";
	            }
	            else
	            {
	                $menu .= "<li><a href=\"?state=people&cat=" . $themenu['lsef'] . "\">" . $themenu['lname'] . "</a></li>";
	            }
	        }
	        $menu .= "
	    </ul>";

        return $menu;
    }
}

function add_main_menu( $menu )
{
    global $admin_menu;
    $admin_menu->add_main_menu( $menu );
}

function add_sub_menu( $parent, $submenu )
{
    global $admin_menu;
    $admin_menu->add_sub_menu( $parent, $submenu );
}

function add_apps_menu( $menu )
{
    global $admin_menu;
    $admin_menu->add_apps_menu( $menu );
}

function add_plugins_menu( $menu )
{
    global $admin_menu;
    $admin_menu->add_plugins_menu( $menu );
}

function get_admin_menu( $type = 'main_menu' )
{
    global $admin_menu;
    return $admin_menu->get_admin_menu( $type );
}

?>