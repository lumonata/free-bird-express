<?php

/*
Title: Freebird Express
Preview: template.png
Author: Ngurah
Author Url: -
Description:a template for Freebird Express website 
*/

require_once( 'functions.php' );

$appname = trim( get_appname() );
$version = '1.0.2';

if( is_front_ajax( $appname ) )
{
    set_template( TEMPLATE_PATH . '/template/ajax.html' );
}
else
{
	set_template( TEMPLATE_PATH . '/template.html' );
}

add_block( 'mainBlock', 'mBlock' );

add_variable( 'popup', init_popup() );
add_variable( 'content', init_content() );
add_variable( 'body-class', init_body_class() );
add_variable( 'container-class', init_container_class() );

add_variable( 'company-email', get_meta_data( 'email' ) );
add_variable( 'company-address', get_meta_data( 'gbs_address' ) );
add_variable( 'company-phone', get_meta_data( 'gbs_phone_number' ) );
add_variable( 'company-wa-call', str_replace( ' ', '', get_meta_data( 'gbs_link_whatsapp' ) ) );
add_variable( 'company-phone-call', str_replace( ' ', '', get_meta_data( 'gbs_phone_number' ) ) );


add_variable( 'fb-link', get_meta_data( 'gbs_link_fb' ) );
add_variable( 'direction-link', get_meta_data( 'gbs_google_maps' ) );
add_variable( 'tripadvisors-link', get_meta_data( 'gbs_link_tripadvisors' ) );

add_variable( 'year', date( 'Y' ) );
add_variable( 'version', sprintf( '?v=%s', $version ) );

add_variable( 'site-url', site_url() );
add_variable( 'theme-url', get_theme_url() );

add_variable( 'include-js', attemp_actions( 'include-js' ) );
add_variable( 'include-css', attemp_actions( 'include-css' ) );

add_variable( 'meta-title', attemp_actions( 'meta-title' ) );
add_variable( 'meta-keywords', attemp_actions( 'meta-keywords' ) );
add_variable( 'meta-description', attemp_actions( 'meta-description' ) );

add_variable( 'footer-menu', the_footer_menus() );
add_variable( 'top-menu', the_menus( 'menuset=top-menu&show_title=false' ) );

parse_template( 'mainBlock', 'mBlock' );

print_template(); 

?>