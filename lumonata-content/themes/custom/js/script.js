function show_alert( htitle, message, callback )
{
    jQuery('<div></div>').dialog({
        modal: true,
        draggable: false,
        resizable: false,
        title: htitle,
        open: function()
        {
            jQuery(this).html(message);
        },
        buttons:
        {
            Close: function()
            {
                jQuery( this ).dialog('close');

                if( callback!='' )
                {
                    eval(callback);
                }
            }
        }
    });
}

function show_book_now()
{
	var hw = jQuery(window).height();

	if( hw < 780 )
	{
		jQuery('#book-now-mobile').addClass('scrolled-menu');
	}

	jQuery('#bg-book-now-mobile').fadeIn( 300 );
	jQuery('#book-now-mobile').animate({ 'width' : '380px' }, 500 );

	jQuery('body').addClass('disable-scroll');
}

function hidden_book_now()
{
	jQuery('#book-now-mobile').animate({ 'width' : '0px' }, 300, function(){
		jQuery('#bg-book-now-mobile').fadeOut( 300 );	
	});	
	
	jQuery('body').removeClass('disable-scroll');	
}

function jump_to_slide( index )
{
	jQuery('#home-slide').vegas( 'jump', index );
}

function close_popup_plugin( e )
{
	var url = jQuery('#site_url').val() + 'set-session-popup-static/';
	var prm = new Object()
		prm.action = 'close_popup';

	jQuery.post(url,prm,function(response){

	})

	jQuery('#popup-plugin').fadeOut(60)
	jQuery('#overlay-plugin').fadeOut(120)
}

function init_popup_info()
{
	if( jQuery('#popup-plugin').length > 0 )
	{
	    jQuery('#overlay-plugin').fadeIn( 60 );
	    jQuery('#popup-plugin').fadeIn( 120 );
	}
}

function init_gallery()
{
	if( jQuery('#zoom-gallery').length > 0 )
	{
		jQuery('#zoom-gallery a').each(function(){
			var sel  = jQuery(this);
			var href = sel.attr('href');

			if( href == '' || typeof href == 'undefined' )
			{
				var data_href = sel.attr('data-href');
				var data_src  = sel.find('img').attr('data-src');

				sel.attr( 'href', data_href );
				sel.find('img').attr( 'src', data_src );
			}
		});

		jQuery('#zoom-gallery').magnificPopup({
			mainClass: 'mfp-with-zoom mfp-img-mobile',
			closeOnContentClick: false,
			closeBtnInside: false,
			delegate: 'a',
			type: 'image',
			image: {
				verticalFit: true
			},
			gallery: {
				enabled: true
			},
			zoom: {
				enabled: true,
				duration: 300,
				opener: function(element) {
					return element.find('img');
				}
			}
		});
	}
}

function init_home_slide()
{
	if( jQuery('.home-slide-video').length == 0 && jQuery('.home-slide-image').length > 0 )
	{
		jQuery('.home-slide-container').slick({
			autoplaySpeed: 5000,
			slidesToScroll: 1,
			slidesToShow: 1,
			infinite: true,
			autoplay: true,
			speed: 300,
		});
	}
}

function init_slider()
{
	if( jQuery('.page-slide-container .slide-item').length > 0 )
	{
		jQuery('.page-slide-container').slick({
			asNavFor: '.page-slide-nav',
			adaptiveHeight: true,
			slidesToScroll: 1,
			slidesToShow: 1,
			fade: true
		});

		jQuery('.page-slide-nav').slick({
			asNavFor: '.page-slide-container',
			focusOnSelect: true,
			slidesToScroll: 1,
  			centerMode: true,
			slidesToShow: 5,
			responsive: [{
				breakpoint: 551,
				settings: {
					slidesToShow: 3
				}
			}]
		});
	}
}

function init_responsive_tabs()
{
	if( typeof RESPONSIVEUI !== 'undefined' )
	{
		RESPONSIVEUI.responsiveTabs();
	}
}

function init_scroll_top()
{
	jQuery('.footer .book-online').on('click', function(){
		jQuery('body, html').animate({ scrollTop: 0 }, 800);
		
		return false;
	});
}

function init_contact()
{
	if( jQuery('.form-contact').length > 0 )
	{
		var form = jQuery('.form-contact');

        if( form.data('anchor') )
        {
            var target = jQuery('#container-form-contact').position();
            var ww     = jQuery(window).width();
            var top    = target.top - 70;

            if( ww <= 500 )
            {
                top = top - 50;
            }

            jQuery('html, body').animate({
                scrollTop: top
            }, 1000);
        }

        form.on('submit', function( event ){
			if( this.checkValidity() === true )
			{
				grecaptcha.ready(function(){
	                grecaptcha.execute( form.find('[name=rkey]').val() , { action: 'contact' }).then( function( token ){
                    	form.find('[name=rresponse]').val( token );

	                	var url = form.attr('action');
			            var prm = form.serialize();
			            var sel = jQuery(this);

			            jQuery.ajax({
			                url: url,
			                data: prm,
			                type: 'POST',
			                dataType: 'json',
			                beforeSend: function( xhr ){
			                    form.find('.btn-submit').val('Processing...');
			                    form.find('.btn-submit').prop('disabled', true);
			                },
			                success: function( e ){
			                	if( e.result == 'success' )
			                	{
			                		form[0].reset();
			                	}

			                	show_alert( 'Notification', e.message );
			                },
			                complete: function( e ){
			                	form.find('.btn-submit').val('Send Inquiry')
			                	form.find('.btn-submit').prop('disabled', false);
			                },
			                error: function( e ){
			                	show_alert( 'Warning', 'Something wrong, please try again later' );
			                }
			            });
	                });
	            });
	        }

			event.preventDefault();
	        event.stopPropagation();
        });
	}
}

function init_number( selector, types )
{
    if( typeof selector == 'undefined' )
    {
        selector = '.text-number';
    }

    if( jQuery( selector ).length > 0 )
    {
        var opt = Array();
        var dom = Array();

        jQuery( selector ).each( function( e ){
            var data = jQuery(this).data();
            var prm  = new Object();
                prm.roundingMethod   = AutoNumeric.options.roundingMethod.halfUpSymmetric;
                prm.unformatOnSubmit = true;
                
            if( typeof data.aSep != 'undefined' )
            {
                prm.digitGroupSeparator = data.aSep;
            }
                
            if( typeof data.aDec != 'undefined' )
            {
                prm.decimalCharacter = data.aDec;
            }
                
            if( typeof data.mDec != 'undefined' )
            {
                prm.decimalPlaces = data.mDec;
            }

            if( typeof data.aMin != 'undefined' )
            {
                prm.minimumValue = data.aMin;
            }

            if( typeof data.aMax != 'undefined' )
            {
                prm.maximumValue = data.aMax;
            }

            if( typeof data.aSign != 'undefined' )
            {
                prm.currencySymbol = data.aSign;
            }
            else
            {
                prm.currencySymbol = '';
            }

            if( typeof data.aPos != 'undefined' )
            {
                prm.currencySymbolPlacement = data.aPos;
            }

            dom.push( this );
            opt.push( prm );
        });
        
        new AutoNumeric.multiple( dom, opt );
    }
}

function init_payment()
{
	init_number();

	jQuery('.container-payment [name=pmethod]').on('change', function(){
		jQuery('.container-payment .doku-form').addClass('hidden');
		jQuery('.container-payment .paypal-form').addClass('hidden');

		if( this.value == '1' )
		{
			jQuery('.container-payment .doku-form').removeClass('hidden');
		}
		else if( this.value == '2' )
		{
			jQuery('.container-payment .paypal-form').removeClass('hidden');
		}
	});

	jQuery('.container-payment [name=doku_confirm]').on('click', function(){
		if( jQuery('.container-payment [name=terms]:checked').length == 0 )
		{
			show_alert( 'Warning', 'Please agreed with the terms & conditions before submited' );
		}
		else
		{
			var form = jQuery('.container-payment .doku-form');
			var url  = form.attr('action');
		    var prm  = form.serialize();
		    var sel  = jQuery(this);

		    jQuery.ajax({
		        url: url,
		        data: prm,
		        type: 'POST',
		        dataType: 'json',
		        beforeSend: function( xhr ){
		            sel.text('Processing...');
		            sel.prop('disabled', true);
		        },
		        success: function( e ){
		        	if( e.result == 'failed' )
		        	{
		        		show_alert( 'Warning', e.message );
		        	}
		        	else
		        	{
		        		loadJokulCheckout( e.response.payment.url );
		        	}
		        },
		        complete: function( e ){
		        	sel.text('Confirm and Pay')
		        	sel.prop('disabled', false);
		        },
		        error: function( e ){
		        	show_alert( 'Warning', 'Something wrong, please try again later' );
		        }
		    });
		}

		return false;
	});

	jQuery('.container-payment [name=paypal_confirm]').on('click', function(){
		if( jQuery('.container-payment [name=terms]:checked').length == 0 )
		{
			show_alert( 'Warning', 'Please agreed with the terms & conditions before submited' );
		}
		else
		{
			var form = jQuery('.container-payment .paypal-form');
			var url  = form.attr('action');
		    var prm  = form.serialize();
		    var sel  = jQuery(this);

		    jQuery.ajax({
		        url: url,
		        data: prm,
		        type: 'POST',
		        dataType: 'json',
		        beforeSend: function( xhr ){
		            sel.text('Processing...');
		            sel.prop('disabled', true);
		        },
		        success: function( e ){
		        	if( e.result == 'success' )
		        	{
		        		window.location = e.link;
		        	}
		        	else
		        	{
		        		show_alert( 'Warning', e.message );
		        	}
		        },
		        complete: function( e ){
		        	sel.text('Confirm and Pay')
		        	sel.prop('disabled', false);
		        },
		        error: function( e ){
		        	show_alert( 'Warning', 'Something wrong, please try again later' );
		        }
		    });
		}

		return false;
	});
}

function init_widget()
{
	if( jQuery('.widget-search-home .txtdate').length > 0 )
	{
		jQuery('.widget-search-home [name=dep_date]').datepicker({
	        minDate: 0,
			dateFormat:'dd/mm/yy',
	        onSelect: function( date, obj ){
	        	jQuery('.widget-search-home [name=dep_date]').val( date );

	            var date2 = jQuery('.widget-search-home [name=ret_date]').datepicker('getDate');

	            if( date2 == null )
	            {
	                jQuery('.widget-search-home [name=ret_date]').datepicker('setDate', date);
	            }

	            jQuery('.widget-search-home [name=ret_date]').datepicker('option', 'minDate', date);
	        }
		});

	    jQuery('.widget-search-home [name=ret_date]').datepicker({
	        minDate: 0,
	        dateFormat:'dd/mm/yy',
	        onSelect: function( date, obj ){
	        	jQuery('.widget-search-home [name=ret_date]').val( date );

	            var date2 = jQuery('.widget-search-home [name=dep_date]').datepicker('getDate');

	            if( date2 == null )
	            {
	                jQuery('.widget-search-home [name=dep_date]').datepicker('setDate', date);
	            }
	            
	            jQuery('.widget-search-home [name=dep_date]').datepicker('option', 'maxDate', date);
	        }
	    });
	}

	if( jQuery('.form-book-now .txtdate').length > 0 )
	{
		jQuery('.form-book-now [name=dep_date]').datepicker({
	        minDate: 0,
			dateFormat:'dd/mm/yy',
	        onSelect: function( date, obj ){
	        	jQuery('.form-book-now [name=dep_date]').val( date );

	            var date2 = jQuery('.form-book-now [name=ret_date]').datepicker('getDate');

	            if( date2 == null )
	            {
	                jQuery('.form-book-now [name=ret_date]').datepicker('setDate', date);
	            }

	            jQuery('.form-book-now [name=ret_date]').datepicker('option', 'minDate', date);
	        }
		});

	    jQuery('.form-book-now [name=ret_date]').datepicker({
	        minDate: 0,
	        dateFormat:'dd/mm/yy',
	        onSelect: function( date, obj ){
	        	jQuery('.form-book-now [name=ret_date]').val( date );
	        	
	            var date2 = jQuery('.form-book-now [name=dep_date]').datepicker('getDate');

	            if( date2 == null )
	            {
	                jQuery('.form-book-now [name=dep_date]').datepicker('setDate', date);
	            }
	            
	            jQuery('.form-book-now [name=dep_date]').datepicker('option', 'maxDate', date);
	        }
	    });
	}

	jQuery(document).click(function(e) {
		if( !$(e.target).is('.trigger-location') )
		{
			jQuery('.option-location').fadeOut();
		}
	});
	
	jQuery('.txtlocation').on('keydown', function(){ return false; });
	jQuery('.txtlocation').on('focusin', function(){ return false; });
	jQuery('.trigger-location').on('click', function(){
		jQuery(this).next().fadeIn();

		return false;
	}).on('focusout', function(){
		 var next = jQuery(this).next();

		 jQuery( next ).fadeOut();			
	});
	
	jQuery('.option-location li').on('click', function(){
		var text = jQuery(this).text();
		var ul	 = jQuery(this).parent();

		ul.children().removeClass('selected');
		ul.prev().prev().val(text);
		ul.fadeOut();

		jQuery(this).addClass('selected');
		jQuery(this).parent().prev().prev().removeClass('error');

		return false;
	});

	jQuery('[name=trip_type]').on('click', function(){
		var state = jQuery(this).attr('data-rel');
		var id    = jQuery(this).val();

		if( id == 1 )
		{
			jQuery('#' + state + ' [name=ret_date]').prop('disabled', false);
			jQuery('#' + state + ' [name=ret_date]').removeClass('off');
		}
		else
		{
			jQuery('#' + state + ' [name=ret_date]').prop('disabled', true);
			jQuery('#' + state + ' [name=ret_date]').addClass('off');
		}
	});
}

function init_body_class()
{
	jQuery(window).scroll(function(){
		var top = jQuery(window).scrollTop();

		if( top > 100 && !jQuery('body').hasClass('scrolled') )
		{
			jQuery('body').addClass('scrolled hasscrolled');
		}
		else if( top <= 100 )
		{
			jQuery('body').removeClass('scrolled');
		}
	});
}

jQuery(document).ready(function(){
	init_widget();
	init_slider();
	init_payment();
	init_contact();
	init_gallery();
	init_popup_info();
	init_body_class();
	init_scroll_top();
	init_home_slide();
	init_responsive_tabs();
});