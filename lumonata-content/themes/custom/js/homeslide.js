var slideWrapper = jQuery('.home-slide-container');
var iframes = slideWrapper.find('iframe');

//-- POST commands to YouTube or Vimeo API
function postMessageToPlayer( player, command )
{
    if( player == null || command == null )
    {
        return;
    }

    player.contentWindow.postMessage( JSON.stringify( command ), '*' );
}

//-- When the slide is changing
function playPauseVideo( slick, control )
{
    var currentSlide, slideType, startTime, player, video;

    currentSlide = slick.find('.slick-current');
    slideType    = currentSlide.attr('class').split(' ')[1];
    player       = currentSlide.find('iframe').get(0);
    startTime    = currentSlide.data('video-start');

    if( slideType === 'youtube' )
    {
        switch( control )
        {
            case 'play':
                postMessageToPlayer( player, {
                    event: 'command',
                    func: 'mute'
                });

                postMessageToPlayer( player, {
                    event: 'command',
                    func: 'playVideo'
                });

                break;
            case 'pause':
                postMessageToPlayer( player, {
                    event: 'command',
                    func: 'pauseVideo'
                });

                break;
        }
    }
}

jQuery( function()
{
    //-- Initialize
    slideWrapper.on('init', function( slick ){
        setTimeout( function(){
            playPauseVideo( jQuery( slick.currentTarget ), 'play' );
        }, 1000);
    });

    slideWrapper.on('beforeChange', function( event, slick ){
        playPauseVideo( jQuery( slick.$slider ), 'pause' );
    });

    slideWrapper.on('afterChange', function( event, slick ){
        playPauseVideo( jQuery(slick.$slider), 'play' );
    });

    var duration = jQuery('#ytplayer').data('duration');

    if( typeof duration == 'undefined' || duration == '' )
    {
        duration = 5000;
    }

    slideWrapper.slick({
        cssEase: 'cubic-bezier(0.87, 0.03, 0.41, 0.9)',
        lazyLoad: 'progressive',
        autoplaySpeed: duration,
        autoplay: true,
        infinite: true,
        speed: 600,
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    dots: true
                }
            }
        ]
    });
});

var tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/player_api';

var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore( tag, firstScriptTag );

var player;

function onYouTubePlayerAPIReady()
{
    var videoId = jQuery('#ytplayer').data('id');

    player = new YT.Player( 'ytplayer', {
        fitToBackground: true,
        pauseOnScroll: false,
        videoId: videoId,
        playerVars: {
            controls: 0, // Show pause/play buttons in player
            iv_load_policy: 3, // Hide the Video Annotations
            autohide: 0, // Hide video controls when playing
            modestbranding: 1, // Hide the Youtube Logo
            autoplay: 1, // Auto-play the video on load
            cc_load_policy: 0, // Hide closed captions
            fs: 0, // Hide the full screen button
            showinfo: 0, // Hide the video title
            rel: 0, // Hide video recommendations
            playsinline: 1,
            disablekb: 1,
            mute: 1
        },
        events: {
            onStateChange: function( e ){
                if( e.data === YT.PlayerState.ENDED )
                {
                    player.seekTo(0);
                    player.playVideo();
                    player.mute();
                }
            },
            onReady: function( e ){
                player.mute();
                player.seekTo(0);
                player.playVideo();
            }
        }
    });
}