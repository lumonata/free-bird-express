// HAMBURGLERv2

function togglescroll () {
  $('body').on('touchstart', function(e){
    if ($('body').hasClass('noscroll')) {
      e.preventDefault();
    }
  });
}

$(document).ready(function () {
    togglescroll()
    $(".menu-mobile .icon").click(function () {
        $(".mobilenav").fadeToggle(500);
        $(".top-menu").toggleClass("top-animate");
        //$("body").toggleClass("noscroll");
        $(".mid-menu").toggleClass("mid-animate");
        $(".bottom-menu").toggleClass("bottom-animate");
        set_slide_menu_mobile()
    });
});

// PUSH ESC KEY TO EXIT

$(document).keydown(function(e) {
    if (e.keyCode == 27) {
        $(".mobilenav").fadeOut(500);
        $(".top-menu").removeClass("top-animate");
       // $("body").removeClass("noscroll");
        $(".mid-menu").removeClass("mid-animate");
        $(".bottom-menu").removeClass("bottom-animate");
        set_slide_menu_mobile()
    }
});

function set_slide_menu_mobile(){
    if(jQuery('#menu-mobile-ul').hasClass('show')){
        jQuery('#menu-mobile-ul').animate({height: '0px'},600).removeClass('show');
        jQuery('body').removeClass('noscroll');
    }else{
        var hm = jQuery(window).height();
        var hh = 70;
        var hmt = hm - hh;    
        console.log(hmt);

        jQuery('#menu-mobile-ul').animate({height: hmt+'px'},600).addClass('show');
        //jQuery('body').addClass('noscroll');
    }
}

