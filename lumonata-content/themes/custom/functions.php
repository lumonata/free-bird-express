<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//-- Default CSS
add_actions( 'include-css', 'get_custom_css', '//fonts.googleapis.com/css?family=Lato:400,300,700' );
add_actions( 'include-css', 'get_custom_css', '//cdn.jsdelivr.net/npm/jquery-ui@1.13.2/dist/themes/base/jquery-ui.min.css' );

//-- Default JS
add_actions( 'include-js', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js' );
add_actions( 'include-js', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/npm-modernizr@2.8.3/modernizr.min.js' );
add_actions( 'include-js', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/jquery-ui@1.13.2/dist/jquery-ui.min.js' );

//-- AJAX Request
add_actions( 'set-session-popup-static_ajax', 'init_session_popup' );
add_actions( 'send-inquiry_ajax', 'init_inquiry' );

function init_content()
{
    if( is_home() )
    {
        return init_homepage();
    }
    elseif( is_ticket_check_availability() )
    {
        return init_availability_result();
    }
    elseif( is_ticket_payment() )
    {
        return init_payment();
    }
    elseif( is_ticket_payment_failed() )
    {
        return init_payment_failed();
    }
    elseif( is_ticket_payment_success() )
    {
        return init_payment_success();
    }
    elseif( is_thank_you_page() )
    {
        return init_thank_you_page();
    }
    elseif( is_contact_and_inquiry() )
    {
        return init_contact_and_inquiry();
    }
    elseif( is_debug_script() )
    {
        return init_debug_script();
    }
    else
    {
        return init_custom_page();
    }
}

function init_homepage()
{
    global $db;

    $hid = get_meta_data( 'gbs_homepage' );

    if( empty( $hid ) === false )
    {
        $arr = explode( '|', $hid );

        if( isset( $arr[ 1 ] ) && !empty( $arr[ 1 ] ) )
        {
            $data = fetch_articles( 'id=' . $arr[ 1 ] );

            if( empty( $data ) === false )
            {
                set_template( TEMPLATE_PATH . '/template/home.html', 'h-template' );

                add_block( 'home-slide-video-block', 'hsv-block', 'h-template' );
                add_block( 'home-slide-loop-block', 'hsl-block', 'h-template' );
                add_block( 'home-slide-block', 'hs-block', 'h-template' );

                //-- Meta Data
                init_metadata( $data );

                $have_slide = false;

                if( isset( $data[ 'attachment' ][ 'gallery-pages' ] ) && !empty( $data[ 'attachment' ][ 'gallery-pages' ] ) )
                {
                    $have_slide = true;

                    foreach( $data[ 'attachment' ][ 'gallery-pages' ] as $i => $d )
                    {
                        add_variable( 'slide-img', optimize_img( site_url( $d['lattach_loc'] ), 1920, 1080 ) );

                        parse_template( 'home-slide-loop-block', 'hsl-block', true );
                    }
                }

                if( isset( $data[ 'additional' ][ 'youtube_video_id' ] ) && !empty( $data[ 'additional' ][ 'youtube_video_id' ] ) )
                {
                    $have_slide = true;

                    add_variable( 'slide-video', $data[ 'additional' ][ 'youtube_video_link' ] );
                    add_variable( 'slide-video-id', $data[ 'additional' ][ 'youtube_video_id' ] );
                    add_variable( 'slide-video-duration', $data[ 'additional' ][ 'youtube_video_duration' ] );

                    add_actions( 'include-js', 'get_custom_javascript', get_theme_url() . '/js/homeslide.js' );

                    parse_template( 'home-slide-video-block', 'hsv-block', true );
                }

                if( $have_slide )
                {
                    parse_template( 'home-slide-block', 'hs-block', false );
                }

                add_variable( 'site-url', site_url() );
                add_variable( 'theme-url', get_theme_url() );
                add_variable( 'content', $data[ 'larticle_content' ] );

                add_variable( 'gbs-why-option-1', nl2br( get_meta_data( 'gbs_why_option_1' ) ) );
                add_variable( 'gbs-why-option-2', nl2br( get_meta_data( 'gbs_why_option_2' ) ) );
                add_variable( 'gbs-why-option-3', nl2br( get_meta_data( 'gbs_why_option_3' ) ) );
                add_variable( 'gbs-why-option-4', nl2br( get_meta_data( 'gbs_why_option_4' ) ) );

                add_actions( 'include-css', 'get_custom_css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css' );
                add_actions( 'include-css', 'get_custom_css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css' );

                add_actions( 'include-js', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' );

                add_block( 'home-block', 'h-block', 'h-template' );

                parse_template( 'home-block', 'h-block', false );

                return return_template( 'h-template' );
            }
        }
    }
}

function init_contact_and_inquiry()
{
    $sef  = get_uri_sef();
    $data = fetch_articles( 'type=pages&sef=' . $sef );

    if( empty( $data ) === false )
    {
        set_template( TEMPLATE_PATH . '/template/contact.html', 'c-template' );

        add_block( 'contact-block', 'c-block', 'c-template' );

        $recaptcha = get_meta_data( 'gbs_capcha_site_key' );

        //-- Meta Data
        init_metadata( $data );

        if( isset( $_GET[ 'subject' ] ) && !empty( $_GET[ 'subject' ] ) )
        {
            add_variable( 'anchor', true );
            add_variable( 'subject', $_GET[ 'subject' ] );
        }

        add_variable( 'recaptcha', $recaptcha );
        add_variable( 'theme-url', get_theme_url() );
        add_variable( 'action', site_url( 'send-inquiry' ) );
        add_variable( 'slide-class', 'c-widget-search bg-cover' );

        add_variable( 'email', get_meta_data( 'email' ) );
        add_variable( 'address', get_meta_data( 'gbs_address' ) );
        add_variable( 'phone', get_meta_data( 'gbs_phone_number' ) );
        add_variable( 'phone-call', str_replace( ' ', '', get_meta_data( 'gbs_phone_number' ) ) );

        add_actions( 'include-js', 'get_custom_javascript', '//www.google.com/recaptcha/api.js?render=' . $recaptcha );

        parse_template( 'contact-block', 'c-block', false );

        return return_template( 'c-template' );
    }
}

function init_custom_page()
{
    $sef  = get_uri_sef();
    $data = fetch_articles( 'type=pages&sef=' . $sef );

    if( empty( $data ) === false )
    {
        set_template( TEMPLATE_PATH . '/template/page.html', 'p-template' );

        add_block( 'page-detail-content-custom', 'pdcc-block', 'p-template' );
        add_block( 'page-detail-content', 'pdc-block', 'p-template' );
        add_block( 'page-custom-content', 'pcc-block', 'p-template' );
        add_block( 'page-map-content', 'pmc-block', 'p-template' );
        add_block( 'page-image', 'pi-block', 'p-template' );

        add_block( 'page-thumb-slide-loop', 'ptsl-block', 'p-template' );
        add_block( 'page-slide-loop', 'psl-block', 'p-template' );
        add_block( 'page-slide', 'ps-block', 'p-template' );

        add_block( 'page-gallery-loop', 'pgl-block', 'p-template' );
        add_block( 'page-gallery', 'pg-block', 'p-template' );

        //-- Meta Data
        init_metadata( $data );

        add_variable( 'page-title', $data[ 'larticle_title' ] );

        //-- Heade Image Class
        if( $sef == 'routes' )
        {
            add_variable( 'slide-class', 'c-widget-search bg-cover hide-slide' );
        }
        else
        {
            add_variable( 'slide-class', 'c-widget-search bg-cover' );
        }

        //-- Page Slide
        if( $sef == 'gallery' )
        {
            if( isset( $data[ 'attachment' ][ 'gallery-pages' ] ) && !empty( $data[ 'attachment' ][ 'gallery-pages' ] ) )
            {
                foreach( $data[ 'attachment' ][ 'gallery-pages' ] as $i => $d )
                {
                    add_variable( 'image-medium', optimize_img( site_url( $d['lattach_loc'] ), 1024, null ) );
                    add_variable( 'image-thumb', optimize_img( site_url( $d['lattach_loc'] ), 356, 210 ) );

                    parse_template( 'page-gallery-loop', 'pgl-block', true );
                }

                add_variable( 'max', 15 );

                add_actions( 'include-css', 'get_custom_css', '//cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/magnific-popup.min.css' );

                add_actions( 'include-js', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/jquery.magnific-popup.min.js' );

                parse_template( 'page-gallery', 'pg-block' );
            }
        }
        elseif( $sef != 'packages-offers' )
        {
            if( isset( $data[ 'attachment' ][ 'gallery-pages' ] ) && !empty( $data[ 'attachment' ][ 'gallery-pages' ] ) )
            {
                if( count( $data[ 'attachment' ][ 'gallery-pages' ] ) > 1 )
                {
                    $num = 1;

                    foreach( $data[ 'attachment' ][ 'gallery-pages' ] as $i => $d )
                    {
                        add_variable( 'slide-idx', $i );
                        add_variable( 'slide-num', $num );
                        add_variable( 'slide-img', optimize_img( site_url( $d['lattach_loc'] ), 1200, null ) );
                        add_variable( 'slide-thumb', optimize_img( site_url( $d['lattach_loc'] ), 200, null ) );

                        parse_template( 'page-thumb-slide-loop', 'ptsl-block', true );
                        parse_template( 'page-slide-loop', 'psl-block', true );

                        $num++;
                    }

                    add_actions( 'include-css', 'get_custom_css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css' );
                    add_actions( 'include-css', 'get_custom_css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css' );

                    add_actions( 'include-js', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' );

                    parse_template( 'page-slide', 'ps-block' );
                }
                else
                {
                    foreach( $data[ 'attachment' ][ 'gallery-pages' ] as $i => $d )
                    {
                        add_variable( 'single-image', optimize_img( site_url( $d['lattach_loc'] ), 1200, null ) );
                    }

                    parse_template( 'page-image', 'pi-block' );
                }
            }
        }

        //-- Page Routes
        if( $sef == 'routes' )
        {
            add_variable( 'theme-url', get_theme_url() );
            add_variable( 'map-key', 'AIzaSyDG12o-8QS9jtVfPw7UB8EDbMH0Zkx574E' );

            parse_template( 'page-map-content', 'pmc-block' );
        }

        //-- Page Boats
        if( $sef == 'our-boats' )
        {
            add_actions( 'include-css', 'get_custom_css', get_theme_url() . '/css/responsive-tabs.css' );

            add_actions( 'include-js', 'get_custom_javascript', get_theme_url() . '/js/responsiveTabs.js' );
        }

        //-- Page Content
        if( $sef == 'about-us' )
        {
            add_variable( 'custom-content', $data[ 'larticle_content' ] );

            parse_template( 'page-custom-content', 'pcc-block' );
        }
        elseif( $sef != 'gallery' )
        {
            if( in_array( $sef, array( 'transport', 'general-tips', 'packages-offers' ) ) === false )
            {
                if( $sef == 'routes' )
                {
                    add_variable( 'wrap-desc-style', 'wrap-container-desc wrap-desc-gray wrap-routes' );
                }
                elseif( $sef == 'the-gilis' )
                {
                    add_variable( 'wrap-desc-style', 'wrap-container-desc wrap-desc-gray' );
                }
                else
                {
                    add_variable( 'wrap-desc-style', 'wrap-container-desc' );
                }

                add_variable( 'page-content', $data[ 'larticle_content' ] );

                parse_template( 'page-detail-content', 'pdc-block' );
            }
            else
            {
                add_variable( 'page-content', $data[ 'larticle_content' ] );

                parse_template( 'page-detail-content-custom', 'pdcc-block' );
            }
        }

        add_block( 'page-block', 'p-block', 'p-template' );

        parse_template( 'page-block', 'p-block', false );

        return return_template( 'p-template' );
    }
    else
    {
        return init_404();
    }
}

function init_404()
{
    http_response_code( 404 );

    set_template( TEMPLATE_PATH . '/template/static.html', 's-template' );

    add_block( 'static-block', 's-block', 's-template' );

    //-- Meta Data
    init_metadata();

    add_variable( 'title', 'Page Not Found' );
    add_variable( 'content', 'This page may have been moved or deleted.<br/>Be sure to check your spelling' );

    parse_template( 'static-block', 's-block', false );

    return return_template( 's-template' );
}

function init_availability_result()
{
    set_template( TEMPLATE_PATH . '/template/result.html', 'r-template' );

    add_block( 'av-result-block', 'ar-block', 'r-template' );

    //-- Meta Data
    init_metadata();

    add_variable( 'result', ticket_check_availability() );
    add_variable( 'slide-class', 'c-widget-search bg-cover' );

    add_actions( 'include-css', 'get_custom_css', get_plugin_url( 'freebird/css/front.css' ) );

    parse_template( 'av-result-block', 'ar-block', false );

    return return_template( 'r-template' );
}

function init_payment()
{
    set_template( TEMPLATE_PATH . '/template/payment.html', 'p-template' );

    add_block( 'payment-detail-transport-2-block', 'dt2-block', 'p-template' );
    add_block( 'payment-detail-transport-block', 'dt-block', 'p-template' );
    add_block( 'payment-detail-passenger-block', 'dp-block', 'p-template' );
    add_block( 'payment-detail-loop-block', 'dl-block', 'p-template' );
    add_block( 'payment-detail-block', 'd-block', 'p-template' );

    add_block( 'empty-block', 'e-block', 'p-template' );

    //-- Meta Data
    init_metadata();

    add_variable( 'slide-class', 'c-widget-search bg-cover' );

    add_actions( 'include-css', 'get_custom_css', get_plugin_url( 'freebird/css/front.css' ) );

    if( isset( $_GET[ 'id' ] ) && !empty( $_GET[ 'id' ] ) )
    {
        $d = fetch_booking( array( 'bid' => $_GET[ 'id' ] ), 'AND', '*', true );

        if( empty( $d ) )
        {
            parse_template( 'empty-block', 'e-block', false );
        }
        else
        {
            if( $d[ 'status' ] == 'wp' )
            {
                add_variable( 'bid', $d[ 'bid' ] );
                add_variable( 'no_ticket', $d[ 'no_ticket' ] );
                add_variable( 'booked_by', $d[ 'booked_by' ] );
                add_variable( 'template_url', get_theme_url() );
                add_variable( 'payment_option', get_payment_option() );
                add_variable( 'created_date', date( 'd F Y', $d[ 'created_date' ] ) );

                add_variable( 'tandc_link', site_url( 'terms-conditions' ) );
                add_variable( 'action_doku', site_url( 'request-doku-payment-url' ) );
                add_variable( 'action_paypal', site_url( 'request-paypal-payment-url' ) );

                if( isset( $_GET[ 'type' ] ) && $_GET[ 'type' ] == 'doku' )
                {
                    add_variable( 'action_doku_css', '' );
                }
                else
                {
                    add_variable( 'action_doku_css', 'hidden' );
                }

                if( isset( $_GET[ 'type' ] ) && $_GET[ 'type' ] == 'paypal' )
                {
                    add_variable( 'action_paypal_css', '' );
                }
                else
                {
                    add_variable( 'action_paypal_css', 'hidden' );
                }

                if( isset( $d[ 'booking_by' ] ) )
                {
                    add_variable( 'bname', $d[ 'booking_by' ][ 'fname' ] );
                    add_variable( 'bphone', $d[ 'booking_by' ][ 'phone' ] );
                    add_variable( 'bemail', $d[ 'booking_by' ][ 'email' ] );
                    add_variable( 'btitle', $d[ 'booking_by' ][ 'title' ] );
                    add_variable( 'baddress', $d[ 'booking_by' ][ 'address' ] );
                    add_variable( 'bcountry', $d[ 'booking_by' ][ 'country' ] );
                }

                if( isset( $d[ 'booking_detail' ] ) && !empty( $d[ 'booking_detail' ] ) )
                {
                    foreach( $d[ 'booking_detail' ] as $dt )
                    {
                        add_variable( 'rto', $dt[ 'rto' ] );
                        add_variable( 'rfrom', $dt[ 'rfrom' ] );

                        add_variable( 'bdid', $dt[ 'bdid' ] );
                        add_variable( 'total', $dt[ 'total' ] );

                        if( $d[ 'booking_detail' ][0][ 'num_adult' ] > 0 && $dt[ 'price_per_adult' ] != 0 )
                        {
                            add_variable( 'adult_price', sprintf( '%s x <span class="text-number" data-a-sep="." data-a-dec="," data-m-dec="0">%s</span>', $d[ 'booking_detail' ][0][ 'num_adult' ], $dt[ 'price_per_adult' ] ) );
                        }
                        else
                        {
                            add_variable( 'adult_price', '-' );
                        }

                        if( $d[ 'booking_detail' ][0][ 'num_child' ] > 0 && $dt[ 'price_per_child' ] != 0 )
                        {
                            add_variable( 'child_price', sprintf( '%s x <span class="text-number" data-a-sep="." data-a-dec="," data-m-dec="0">%s</span>', $d[ 'booking_detail' ][0][ 'num_child' ], $dt[ 'price_per_child' ] ) );
                        }
                        else
                        {
                            add_variable( 'child_price', '-' );
                        }

                        if( $d[ 'booking_detail' ][0][ 'num_infant' ] > 0 && $dt[ 'price_per_infant' ] != 0 )
                        {
                            add_variable( 'infant_price', sprintf( '%s x <span class="text-number" data-a-sep="." data-a-dec="," data-m-dec="0">%s</span>', $d[ 'booking_detail' ][0][ 'num_infant' ], $dt[ 'price_per_infant' ] ) );
                        }
                        else
                        {
                            add_variable( 'infant_price', '-' );
                        }

                        add_variable( 'date', date( 'd F Y', strtotime( $dt[ 'date' ] ) ) );
                        add_variable( 'stime_arrive', date( 'H:i', strtotime( $dt[ 'stime_arrive' ] ) ) );
                        add_variable( 'stime_departure', date( 'H:i', strtotime( $dt[ 'stime_departure' ] ) ) );

                        parse_template( 'payment-detail-loop-block', 'dl-block', true );
                    }

                    if( $d[ 'booking_detail' ][0][ 'pickup_destination' ] != '' || $d[ 'booking_detail' ][0][ 'transfer_destination' ] )
                    {
                        add_variable( 'pickup_cost', $d[ 'booking_detail' ][0][ 'pickup_cost' ] );
                        add_variable( 'transfer_cost', $d[ 'booking_detail' ][0][ 'transfer_cost' ] );

                        add_variable( 'address_pickup', $d[ 'booking_detail' ][0][ 'address_pickup' ] );
                        add_variable( 'address_transfer', $d[ 'booking_detail' ][0][ 'address_transfer' ] );

                        add_variable( 'pickup_destination', $d[ 'booking_detail' ][0][ 'pickup_destination' ] );
                        add_variable( 'transfer_destination', $d[ 'booking_detail' ][0][ 'transfer_destination' ] );

                        parse_template( 'payment-detail-transport-block', 'dt-block' );
                        parse_template( 'payment-detail-transport-2-block', 'dt2-block' );
                    }

                    add_variable( 'num_adult', $d[ 'booking_detail' ][0][ 'num_adult' ] );
                    add_variable( 'num_child', $d[ 'booking_detail' ][0][ 'num_child' ] );
                    add_variable( 'num_infant', $d[ 'booking_detail' ][0][ 'num_infant' ] );
                    add_variable( 'total_summary', $d[ 'booking_detail' ][0][ 'total_summary' ] );
                }

                if( isset( $d[ 'booking_passenger' ] ) && !empty( $d[ 'booking_passenger' ] ) )
                {
                    foreach( $d[ 'booking_passenger' ] as $dp )
                    {
                        add_variable( 'pname', $dp[ 'fname' ] );
                        add_variable( 'pcountry', $dp[ 'country' ] );
                        add_variable( 'ptype', ucfirst( $dp[ 'type' ] ) );

                        parse_template( 'payment-detail-passenger-block', 'dp-block', true );
                    }
                }

                add_actions( 'include-js', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' );
                add_actions( 'include-js', 'get_custom_javascript', ticket_doku_js() );

                parse_template( 'payment-detail-block', 'd-block', false );
            }
            else
            {
                parse_template( 'empty-block', 'e-block', false );
            }
        }
    }
    else
    {
        parse_template( 'empty-block', 'e-block', false );
    }

    return return_template( 'p-template' );
}

function init_thank_you_page()
{
    set_template( TEMPLATE_PATH . '/template/static.html', 's-template' );

    add_block( 'static-block', 's-block', 's-template' );

    //-- Meta Data
    init_metadata();

    $content = '
	Your order has been received. We also have emailed the order receipt to you.<br/>
	If you have any questions regarding payment or anything else, please contact us at<br/>
    <a href="mailto:info@freebird-express.com?subject=Reporting-Error">info@freebird-express.com</a>';

    add_variable( 'title', 'Thank You' );
    add_variable( 'content', $content );

    parse_template( 'static-block', 's-block', false );

    return return_template( 's-template' );
}

function init_payment_success()
{
    if( isset( $_GET[ 'id' ] ) )
    {
        set_template( TEMPLATE_PATH . '/template/static.html', 's-template' );

        add_block( 'static-block', 's-block', 's-template' );

        //-- Meta Data
        init_metadata();

        //-- Get company email
        $email = get_meta_data( 'email' );

        //-- Fetch booking data
        $d = fetch_booking( array( 'bid' => $_GET[ 'id' ] ), 'AND', '*', true );

        if( empty( $d ) )
        {
            $content = sprintf( 'Sorry, we can\'t find this booking data, it seems is not valid anymore<br/>If you have any questions regarding payment or anything else, please contact us at<br/><a href="mailto:%s?subject=Reporting-Error">%s</a>', $email, $email );
            $title   = 'Not Found';
        }
        else
        {
            if( $d[ 'status' ] == 'pd' )
            {
                $dwnlink = site_url( 'download-ticket/' . base64_encode( json_encode( array( 'bid' => $d[ 'bid' ] ) ) ) );
                $content = sprintf( 'Your payment proccesed succesfully. Please download your ticket by click <a href="%s">this link</a><br/>If you have any questions or anything else, please contact us at<br/><a href="mailto:%s?subject=Reporting-Error">%s</a>', $dwnlink, $email, $email );
                $title   = 'Payment Done';
            }
            else
            {
                $content = sprintf( 'We are still waiting for verification of your payment,<br/>please wait a few moments and refresh this page to get a link to download your ticket<br/>If you have any questions regarding payment or anything else, please contact us at<br/><a href="mailto:%s?subject=Reporting-Error">%s</a>', $email, $email );
                $title   = 'Waiting Verification';
            }
        }

        add_variable( 'title', $title );
        add_variable( 'content', $content );

        parse_template( 'static-block', 's-block', false );

        return return_template( 's-template' );
    }
    else
    {
        return init_404();
    }
}

function init_payment_failed()
{
    set_template( TEMPLATE_PATH . '/template/static.html', 's-template' );

    add_block( 'static-block', 's-block', 's-template' );

    //-- Meta Data
    init_metadata();

    $content = '
	It seems that you failed to make a payment<br/>
	If you have any questions regarding payment or anything else, please contact us at<br/>
    <a href="mailto:info@freebird-express.com?subject=Reporting-Error">info@freebird-express.com</a> ';

    add_variable( 'title', 'Payment Failed' );
    add_variable( 'content', $content );

    parse_template( 'static-block', 's-block', false );

    return return_template( 's-template' );
}

function init_body_class()
{
    $class = array( 'first-load', 'body-' . get_page_name() );

    if( is_home() === false )
    {
        array_push( $class, 'body-other-page' );
    }

    return implode( ' ', $class );
}

function init_container_class()
{
    $class = array( 'wrap', 'page', get_page_name() );

    return implode( ' ', $class );
}

function init_popup()
{
    if( !isset( $_SESSION[ 'is_close_popup_static' ] ) || ( isset( $_SESSION[ 'is_close_popup_static' ] ) && $_SESSION[ 'is_close_popup_static' ] != 'yes' ) )
    {
        $content   = get_meta_data( 'gbs_popup_content' );
        $publish   = strtotime( get_meta_data( 'gbs_publish_popup_content' ) . ' 00:00:00' );
        $unpublish = strtotime( get_meta_data( 'gbs_unpublish_popup_content' ) . ' 23:59:59' );

        if( trim( $content ) != '' && time() >= $publish && time() <= $unpublish )
        {
            set_template( TEMPLATE_PATH . '/template/popup.html', 'p-template' );

            add_block( 'popup-block', 'p-block', 'p-template' );

            add_variable( 'content', $content );
            add_variable( 'theme-url', get_theme_url() );
            add_variable( 'content-type', get_meta_data( 'gbs_type_popup_content' ) );

            parse_template( 'popup-block', 'p-block', false );

            return return_template( 'p-template' );
        }
    }
}

function init_metadata( $data = array(), $is_rule = false )
{
    if( empty( $data ) === false )
    {
        extract( $data );

        if( $is_rule )
        {
            $mtitle       = get_rule_additional_field( $lrule_id, 'meta_title', $lgroup );
            $mkeywords    = get_rule_additional_field( $lrule_id, 'meta_keywords', $lgroup );
            $mdescription = get_rule_additional_field( $lrule_id, 'meta_description', $lgroup );

            $mtitle = empty( $mtitle ) ? $lname : $mtitle;
        }
        else
        {
            $mtitle       = get_additional_field( $larticle_id, 'meta_title', $larticle_type );
            $mkeywords    = get_additional_field( $larticle_id, 'meta_keywords', $larticle_type );
            $mdescription = get_additional_field( $larticle_id, 'meta_description', $larticle_type );

            $ogurl         = get_additional_field( $larticle_id, 'og_url', $larticle_type );
            $ogtitle       = get_additional_field( $larticle_id, 'og_title', $larticle_type );
            $ogdescription = get_additional_field( $larticle_id, 'og_description', $larticle_type );

            $mtitle = empty( $mtitle ) ? $larticle_title : $mtitle;
        }

        add_actions( 'meta-title', $mtitle );
        add_actions( 'meta-keywords', empty( $mkeywords ) ? '' : '<meta name="keywords" content="' . $mkeywords . '" />' );
        add_actions( 'meta-description', empty( $mdescription ) ? '' : '<meta name="description" content="' . $mdescription . '" />' );
    }
    else
    {
        $sef = get_uri_sef();

        if( $sef == 'check-ticket' )
        {
            add_actions( 'meta-title', sprintf( 'Availability Result - %s', trim( web_title() ) ) );
        }
        elseif( $sef == 'pay-ticket' )
        {
            add_actions( 'meta-title', sprintf( 'Booking Payment - %s', trim( web_title() ) ) );
        }
        elseif( $sef == 'payment-success' )
        {
            if( isset( $_GET[ 'id' ] ) )
            {
                $d = fetch_booking( array( 'bid' => $_GET[ 'id' ] ), 'AND', '*', true );

                if( empty( $d ) )
                {
                    add_actions( 'meta-title', sprintf( 'Booking Not Found - %s', trim( web_title() ) ) );
                }
                else
                {
                    if( $d[ 'status' ] == 'pd' )
                    {
                        add_actions( 'meta-title', sprintf( 'Payment Done - %s', trim( web_title() ) ) );
                    }
                    else
                    {
                        add_actions( 'meta-title', sprintf( 'Waiting Verification - %s', trim( web_title() ) ) );
                    }
                }
            }
            else
            {
                add_actions( 'meta-title', sprintf( 'Page Not Found - %s', trim( web_title() ) ) );
            }
        }
        elseif( $sef == 'payment-failed' )
        {
            add_actions( 'meta-title', sprintf( 'Payment Failed - %s', trim( web_title() ) ) );
        }
        elseif( $sef == 'thank-you' )
        {
            add_actions( 'meta-title', sprintf( 'Thank You - %s', trim( web_title() ) ) );
        }
        else
        {
            add_actions( 'meta-title', sprintf( 'Page Not Found - %s', trim( web_title() ) ) );
        }
    }
}

function optimize_img( $url, $width = null, $height = null, $format = 'jpg' )
{
    if( !is_null( $width ) && !is_null( $height ) )
    {
        $url = site_url( 'lumonata-functions/mthumb.php?src=' . $url . '&w=' . $width . '&h=' . $height );
    }
    elseif( !is_null( $width ) && is_null( $height ) )
    {
        $url = site_url( 'lumonata-functions/mthumb.php?src=' . $url . '&w=' . $width );
    }
    elseif( is_null( $width ) && !is_null( $height ) )
    {
        $url = site_url( 'lumonata-functions/mthumb.php?src=' . $url . '&h=' . $height );
    }

    return $url;
}

function is_front_ajax( $appname = '' )
{
    global $actions;

    if( empty( $appname ) )
    {
        $appname = trim( get_appname() );
    }

    if( isset( $actions->action[ $appname . '_ajax' ] ) )
    {
        run_actions( $appname . '_ajax' );

        return true;
    }
    else
    {
        return false;
    }
}

function is_ticket_check_availability()
{
    $sef = trim( get_uri_sef() );

    if( $sef == 'check-ticket' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_ticket_payment()
{
    $sef = trim( get_uri_sef() );

    if( $sef == 'pay-ticket' )
    {
        return true;
    }
    else
    {
        return false;
    }

    return false;
}

function is_ticket_payment_failed()
{
    $sef = trim( get_uri_sef() );

    if( $sef == 'payment-failed' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_ticket_payment_success()
{
    $sef = trim( get_uri_sef() );

    if( $sef == 'payment-success' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_thank_you_page()
{
    $sef = trim( get_uri_sef() );

    if( $sef == 'thank-you' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_debug_script()
{
    $sef = trim( get_uri_sef() );

    if( $sef == 'debug' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_contact_and_inquiry()
{
    $sef = trim( get_uri_sef() );

    if( in_array( $sef, array( 'contact-us', 'inquiry' ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function get_page_name()
{
    if( is_home() )
    {
        $page_name = 'home';
    }
    else
    {
        $sef = trim( get_uri_sef() );

        if( $sef == 'about-us' )
        {
            $page_name = 'about';
        }
        elseif( $sef == 'contact-us' )
        {
            $page_name = 'contact';
        }
        elseif( in_array( $sef, array( 'routes', 'routes-schedules' ) ) )
        {
            $page_name = 'route';
        }
        elseif( $sef == 'packages-offers' )
        {
            $page_name = 'packages-offers';
        }
        elseif( $sef == 'inquiry' )
        {
            $page_name = 'contact';
        }
        elseif( in_array( $sef, array( 'why-amed', 'the-gilis', 'transport', 'general-tips', 'accommodation', 'testimonial', 'gallery' ) ) )
        {
            $page_name = $sef;
        }
        else
        {
            $page_name = 'page';
        }
    }

    return $page_name;
}

function get_payment_option()
{
    return '
    <option value="">Select Payment Method</option>
    <option value="1" ' . ( isset( $_GET[ 'type' ] ) && $_GET[ 'type' ] == 'doku' ? 'selected' : '' ) . '>DOKU</option>
    <option value="2" ' . ( isset( $_GET[ 'type' ] ) && $_GET[ 'type' ] == 'paypal' ? 'selected' : '' ) . '>PayPal</option>';
}

function the_footer_menus()
{
    $menu_items = get_meta_data( 'menu_items_footer-menu', 'menus' );
    $menu_items = json_decode( $menu_items, true );
    $menu_items = array_chunk( $menu_items, 5 );

    $menu_order = get_meta_data( 'menu_order_footer-menu', 'menus' );
    $menu_order = json_decode( $menu_order, true );

    $result = '';

    foreach( $menu_items as $i => $item )
    {
        $result .= fetch_menu_set_items( $item, $menu_order, 'li' );
    }

    return $result;
}

function init_inquiry_subject( $post = array(), $name = '' )
{
    if( isset( $post[ 'subject' ] ) && !empty( $post[ 'subject' ] ) )
    {
        return $post[ 'subject' ];
    }
    else
    {
        return $name . ' Contact From ' . $post[ 'name' ];
    }
}

function init_inquiry_content( $post = array(), $name = '' )
{
    set_template( TEMPLATE_PATH . '/template/inquiry.html', 'i-template' );

    add_block( 'inquiry-block', 'i-block', 'i-template' );

    add_variable( 'web-name', $name );
    add_variable( 'name', $post[ 'name' ] );
    add_variable( 'email', $post[ 'email' ] );
    add_variable( 'phone', $post[ 'phone' ] );
    add_variable( 'message', nl2br( $post[ 'message' ] ) );

    parse_template( 'inquiry-block', 'i-block', false );

    return return_template( 'i-template' );
}

function init_session_popup()
{
    $_SESSION[ 'is_close_popup_static' ] = 'yes';

    exit;
}

function init_inquiry()
{
    if( isset( $_POST[ 'rresponse' ] ) && empty( $_POST[ 'rresponse' ] ) === false )
    {
        $curl  = curl_init();
        $param = http_build_query( array(
            'response' => urlencode( $_POST[ 'rresponse' ] ),
            'secret'   => get_meta_data( 'gbs_capcha_secret_key' )
        ) );

        curl_setopt_array( $curl, array(
            CURLOPT_URL            => 'https://www.google.com/recaptcha/api/siteverify?' . $param,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => 1,
        ) );

        $data = curl_exec( $curl );

        curl_close( $curl );

        $response = json_decode( $data );

        if( $response === null && json_last_error() !== JSON_ERROR_NONE )
        {
            echo json_encode( array( 'result' => 'failed', 'message' => 'Something wrong, please try again later' ) );
        }
        else
        {
            if( $response->score >= 0.5 )
            {
                $mail = new PHPMailer( true );

                try
                {
                    $to   = trim( get_meta_data( 'email' ) );
                    $name = trim( get_meta_data( 'web_name' ) );

                    $smtp_server = get_meta_data( 'smtp' );
                    $smtp_port   = get_meta_data( 'gbs_port_smpt' );
                    $smtp_email  = get_meta_data( 'gbs_email_smpt' );
                    $smtp_user   = get_meta_data( 'gbs_username_smpt' );
                    $smtp_pass   = get_meta_data( 'gbs_pass_email_smpt' );

                    $mail->isSMTP();
                    $mail->isHTML( true );

                    $mail->Host     = $smtp_server;
                    $mail->Username = $smtp_user;
                    $mail->Port     = $smtp_port;
                    $mail->Password = $smtp_pass;
                    $mail->SMTPAuth = true;
                    $mail->Timeout  = 60;

                    $mail->setFrom( $smtp_email, 'Freebird Express Automail' );
                    $mail->addReplyTo( $_POST[ 'email' ], $_POST[ 'name' ] );
                    $mail->addAddress( $to, $name );

                    $mail->Subject = init_inquiry_subject( $_POST, $name );
                    $mail->Body    = init_inquiry_content( $_POST, $name );

                    $mail->send();

                    echo json_encode( array( 'result' => 'success', 'message' => 'Your inquiry has been sent' ) );
                }
                catch( Exception $e )
                {
                    echo json_encode( array( 'result' => 'failed', 'message' => 'Message could not be sent. Mailer Error: ' . $e->errorMessage() ) );
                }
            }
            else
            {
                echo json_encode( array( 'result' => 'failed', 'message' => 'Invalid captcha code' ) );
            }
        }
    }
    else
    {
        echo json_encode( array( 'result' => 'failed', 'message' => 'Please check captcha code' ) );
    }

    exit;
}

function init_debug_script()
{
    exit;
}
