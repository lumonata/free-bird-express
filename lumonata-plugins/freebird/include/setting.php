<?php

add_actions( 'setting', 'init_setting' );
add_actions( 'setting_admin_page', 'setting_ajax' );

add_privileges( 'administrator', 'setting', 'insert' );
add_privileges( 'administrator', 'setting', 'update' );
add_privileges( 'administrator', 'setting', 'delete' );

/*
| -----------------------------------------------------------------------------
| Admin Setting
| -----------------------------------------------------------------------------
*/
function init_setting()
{
    run_setting_actions();
    
    //-- Display add new form
    if( is_add_new() )
    {
        return add_setting();
    }
    elseif( is_edit() )
    {
        if( is_contributor() || is_author() )
        {
            if( is_num_setting( array( 'id' => $_GET[ 'id' ] ) ) > 0 )
            {
                return edit_setting( $_GET[ 'id' ] );
            }
            else
            {
                return '
                <div class="alert_red_form">
                    You don\'t have an authorization to access this page
                </div>';
            }
        }
        else
        {
            return edit_setting( $_GET[ 'id' ] );
        }
    }
    elseif( is_delete_all() )
    {
        return delete_batch_setting();
    }
    
    //-- Automatic to display add new when there is no records on database
    if( is_num_setting() == 0 )
    {
        header( 'location:' . get_state_url( 'ticket&sub=setting&prc=add_new' ) );
    }
    elseif( is_num_setting() > 0 )
    {
        return get_setting_list();
    }
}

function get_setting_list()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/setting_list.html', 'setting' );

    add_block( 'list-block', 'l-block', 'setting' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'alert', message_block() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_setting_ajax_url() );
    add_variable( 'button', get_setting_admin_button( get_state_url( 'ticket&sub=setting' ) ) );
    add_variable( 'title', 'Setting List' );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );
    
    add_actions( 'section_title', 'Setting' );

    parse_template( 'list-block', 'l-block', 'setting' );

    return return_template( 'setting' );
}

/*
| -----------------------------------------------------------------------------
| Admin Setting - Add New Setting
| -----------------------------------------------------------------------------
*/
function add_setting()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/setting_new.html', 'setting' );

    add_block( 'form-block', 'f-block', 'setting' );

    add_variable( 'alert', message_block() );
    add_variable( 'ajax-url', get_setting_ajax_url() );
    add_variable( 'button', get_setting_admin_button( get_state_url( 'ticket&sub=setting' ), true ) );
    add_variable( 'ftitle', 'Add New Setting' );
    
    add_actions( 'section_title', 'Setting - Add New' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'setting' );
}

/*
| -----------------------------------------------------------------------------
| Admin Setting - Edit Setting
| -----------------------------------------------------------------------------
*/
function edit_setting( $id )
{
    $d = fetch_setting( array( 'id' => $id ) );

    set_template( PLUGINS_PATH . '/freebird/tpl/setting_new.html', 'setting' );

    add_block( 'form-block', 'f-block', 'setting' );

    add_variable( 'id', $d[ 'id' ] );
    add_variable( 'sef', $d[ 'sef' ] );
    add_variable( 'label', $d[ 'label' ] );
    add_variable( 'value', $d[ 'value' ] );

    add_variable( 'alert', message_block() );
    add_variable( 'ajax-url', get_setting_ajax_url() );
    add_variable( 'button', get_setting_admin_button( get_state_url( 'ticket&sub=setting' ), true ) );
    add_variable( 'ftitle', 'Edit Setting' );
    
    add_actions( 'section_title', 'Setting - Edit' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'setting' );
}

/*
| -----------------------------------------------------------------------------
| Admin Setting - Batch Delete Setting
| -----------------------------------------------------------------------------
*/
function delete_batch_setting()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/setting_batch_delete.html', 'setting-batch-delete' );

    add_block( 'delete-loop-block', 'dl-block', 'setting-batch-delete' );
    add_block( 'delete-block', 'd-block', 'setting-batch-delete' );

    foreach( $_POST[ 'select' ] as $key => $val )
    {
        $d = fetch_setting( array( 'id' => $val ) );

        add_variable( 'label', $d[ 'label' ] );
        add_variable( 'id', $d[ 'id' ] );

        parse_template( 'delete-loop-block', 'dl-block', true );
    }

    add_variable( 'title', 'Batch Delete Setting' );
    add_variable( 'backlink', get_state_url( 'ticket&sub=setting' ) );
    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST[ 'select' ] ) == 1 ? 'this data?' : 'these data?' ) );
    
    add_actions( 'section_title', 'Setting - Batch Delete' );

    parse_template( 'delete-block', 'd-block', 'setting-batch-delete' );

    return return_template( 'setting-batch-delete' );
}

/*
| -----------------------------------------------------------------------------
| Admin Setting - Table Query
| -----------------------------------------------------------------------------
*/
function get_setting_list_query()
{
    global $db;
    
    extract( $_POST );
    
    $post = $_REQUEST;
    $cols  = array(
        1 => 'a.label',
        2 => 'a.value', 
        3 => 'a.created_by',
        4 => 'a.created_date'
    );
    
    //-- Set Order Column
    if ( isset( $post[ 'order' ] ) && !empty( $post[ 'order' ] ) )
    {
        $o = array();
        
        foreach ( $post[ 'order' ] as $i => $od )
        {
            if ( isset( $cols[ $post[ 'order' ][ $i ][ 'column' ] ] ) )
            {
                $o[] = $cols[ $post[ 'order' ][ $i ][ 'column' ] ] . ' ' . $post[ 'order' ][ $i ][ 'dir' ];
            }
        }
        
        $order = !empty( $o ) ? ' ORDER BY ' . implode( ', ', $o ) : '';
    }
    else
    {
        $order = ' ORDER BY a.id ASC';
    }

    //-- Set Condition
    $w = array();

    if( empty( $post[ 'search' ][ 'value' ] ) === false )
    {        
        $s = array();

        foreach ( $cols as $col )
        {
            $s[] = $db->prepare_query( $col . ' LIKE %s', '%' . $post[ 'search' ][ 'value' ] . '%' );
        }

        $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE ' . implode( ' AND ', $w );
    }
    else
    {
        $where = '';
    }

    $q = 'SELECT * FROM ticket_setting AS a' . $where . $order;
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );
    
    $q2 = $q . ' LIMIT ' . $post[ 'start' ] . ', ' . $post[ 'length' ];
    $r2 = $db->do_query( $q2 );
    $n2 = $db->num_rows( $r2 );
    
    $data = array();
    
    if ( $n2 > 0 )
    {
        $url = get_state_url( 'ticket&sub=setting' );
        
        while ( $d2 = $db->fetch_array( $r2 ) )
        {
            $uid = fetch_user_id_by_username( $d2[ 'created_by' ] );

            $data[] = array(
                'created_by'   => $d2[ 'created_by' ],
                'value'        => $d2[ 'value' ],
                'label'        => $d2[ 'label' ],
                'id'           => $d2[ 'id' ],
                'user_link'    => user_url( $uid ),
                'avatar'       => get_avatar( $uid, 3 ),
                'ajax_link'    => get_setting_ajax_url(),
                'created_date' => date( 'd F Y', $d2[ 'created_date' ] ),
                'edit_link'    => get_state_url( 'ticket&sub=setting&prc=edit&id=' . $d2[ 'id' ] )
            );
        }
    }
    else
    {
        $n = 0;
    }
    
    return array(
        'draw' => intval( $post[ 'draw' ] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data 
    );
}

/*
| -----------------------------------------------------------------------------
| Admin Setting - Action Button
| -----------------------------------------------------------------------------
*/
function get_setting_admin_button( $new_url = '', $is_form = false )
{
    if( $is_form )
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=cancel', $new_url ) . '</li>
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=publish' ) . '</li>                 
            <li>' . button( 'button=cancel', $new_url ) . '</li>  
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
    }
    else
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Setting - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_setting_ajax_url()
{
    return get_state_url( 'ajax&apps=setting' );
}

/*
| -----------------------------------------------------------------------------
| Admin Setting Actions
| -----------------------------------------------------------------------------
*/
function run_setting_actions()
{
    global $db;
    global $flash;

    if( is_confirm_delete() )
    {
        $count  = count( $_POST[ 'id' ] );
        $error = 0;

        foreach( $_POST[ 'id' ] as $key => $val )
        {
            if( delete_setting( $val ) === false )
            {
                $error++;
            }
        }

        if( $error > 0 )
        {
            if( $error == $count )
            {
                $flash->add( array( 'type' => 'error', 'content' => 'Failed to delete all selected setting' ) );
            }
            else
            {
                $flash->add( array( 'type' => 'error', 'content' => 'Failed to update some of selected setting' ) );
            }

            header( 'location: ' . get_state_url( 'ticket&sub=setting' ) );

            exit;
        }
        else
        {
            $flash->add( array( 'type' => 'success', 'content' => 'Successfully delete all selected setting' ) );
            
            header( 'location: ' . get_state_url( 'ticket&sub=setting' ) );

            exit;
        }
    }
    else
    {
        if( is_save_draft() || is_publish() )
        {
            if( is_add_new() )
            {
                if( save_setting() )
                {
                    $flash->add( array( 'type' => 'success', 'content' => 'Successfully add new setting' ) );
                    
                    header( 'location: ' . get_state_url( 'ticket&sub=setting&prc=add_new' ) );

                    exit;
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to add new setting' ) );
                }
            }
            elseif( is_edit() )
            {
                if( update_setting() )
                {
                    $flash->add( array( 'type' => 'success', 'content' => 'Successfully updated setting data' ) );
                    
                    header( 'location: ' . get_state_url( 'ticket&sub=setting&prc=edit&id=' . $_GET[ 'id' ] ) );

                    exit;
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to update setting data' ) );
                }
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Setting - Save Setting
| -----------------------------------------------------------------------------
*/
function fetch_setting( $conditions = array(), $operator = 'AND',  $fields = '*' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( is_array( $fields ) && empty( $fields ) === false )
    {
        $param = implode( ', ', $fields );
    }
    else
    {
        $param = $fields;
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT ' . $param . ' FROM ticket_setting' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );

        if( $n > 1 )
        {
            while( $d = $db->fetch_assoc( $r ) )
            {
                $data[] = $d;
            }
        }
        elseif( $n > 0 )
        {
            $data = $db->fetch_assoc( $r );

            if( $fields != '*' && is_string( $fields ) && isset( $data[ $fields ] ) )
            {
                $data = $data[ $fields ];
            }
        }

        return $data;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Setting - Save Setting
| -----------------------------------------------------------------------------
*/
function save_setting( $params = array() )
{
    global $db;

    $fields = array(
        'created_date',
        'created_by',
        'value',
        'label',
        'sef',
        'dlu'
    );

    if( empty( $params ) )
    {
        $params = $_POST;
    }

    if( empty( $params ) )
    {
        return false;
    }
    else
    {
        //-- Insert special rate data
        $data = array( 
            'created_by'   => $_COOKIE[ 'username' ],
            'created_date' => time(), 
            'dlu'          => time()
        );

        foreach( $params as $f => $d )
        {
            if( in_array( $f, $fields ) )
            {
                $data[ $f ] = $d;
            }
        }

        $r = $db->insert( 'ticket_setting', $data );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Setting - Update Setting
| -----------------------------------------------------------------------------
*/
function update_setting( $params = array(), $where = array() )
{
    global $db;

    $fields = array(
        'created_date',
        'created_by',
        'value',
        'label',
        'sef',
        'dlu'
    );

    if( empty( $params ) )
    {
        $params = $_POST;
    }

    if( empty( $where ) )
    {
        $where = array( 'id' => $_GET[ 'id' ] );
    }

    if( empty( $params ) )
    {
        return false;
    }
    else
    {
        //-- Update special rate data
        $data = array( 'dlu' => time() );

        foreach( $params as $f => $d )
        {
            if( in_array( $f, $fields ) )
            {
                $data[ $f ] = $d;
            }
        }

        $r = $db->update( 'ticket_setting', $data, $where );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Setting - Delete Setting
| -----------------------------------------------------------------------------
*/
function delete_setting( $id )
{
    global $db;

    $r = $db->delete( 'ticket_setting', array( 'id' => $id ) );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Setting - Check Number of Setting
| -----------------------------------------------------------------------------
*/
function is_num_setting( $conditions = array(), $operator = 'AND' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();
    $n = 0;

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT * FROM ticket_setting' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );
    }

    return $n;
}

/*
| -----------------------------------------------------------------------------
| Admin Setting - Ajax Function
| -----------------------------------------------------------------------------
*/
function setting_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'load-setting-data' )
    {
        echo json_encode( get_setting_list_query() );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'delete-setting' )
    {
        if( delete_setting( $_POST[ 'id' ] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }
}

?>