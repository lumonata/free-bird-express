<?php

use Dompdf\Dompdf;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

function is_get_special_rate( $rid, $sid, $date )
{
    global $db;

    //-- Apply to selected route and selected schedule [rid!=0,sid!=0]
    $s1 = 'SELECT * FROM ticket_special_rate AS a WHERE %d >= a.srdate_start AND %d <= a.srdate_end AND a.status = %d AND a.rid = %d AND a.sid = %d ORDER BY a.sort_id LIMIT %d';
    $q1 = $db->prepare_query( $s1, strtotime( $date ), strtotime( $date ), 1, $rid, $sid, 1 );

    //-- Apply to selected route and all schedule of the selected route [rid!=0,sid==0]
    $s2 = 'SELECT * FROM ticket_special_rate AS a WHERE %d >= a.srdate_start AND %d <= a.srdate_end AND a.status = %d AND a.rid = %d AND a.sid = %d ORDER BY a.sort_id LIMIT %d';
    $q2 = $db->prepare_query( $s2, strtotime( $date ), strtotime( $date ), 1, $rid, 0, 1 );

    //-- Apply to all routes and selected schedule [rid==0,sid!=0]
    $s3 = 'SELECT * FROM ticket_special_rate AS a WHERE %d >= a.srdate_start AND %d <= a.srdate_end AND a.status = %d AND a.rid = %d AND a.sid = %d ORDER BY a.sort_id LIMIT %d';
    $q3 = $db->prepare_query( $s3, strtotime( $date ), strtotime( $date ), 1, 0, $sid, 1 );

    //-- Apply to all routes and all schedules [rid==0,sid==0]
    $s4 = 'SELECT * FROM ticket_special_rate AS a WHERE %d >= a.srdate_start AND %d <= a.srdate_end AND a.status = %d AND a.rid = %d AND a.sid = %d ORDER BY a.sort_id LIMIT %d';
    $q4 = $db->prepare_query( $s4, strtotime( $date ), strtotime( $date ), 1, 0, 0, 1 );

    if( ticket_is_num_rows( $q1 ) )
    {
        return $q1;
    }
    elseif( ticket_is_num_rows( $q2 ) )
    {
        return $q2;
    }
    elseif( ticket_is_num_rows( $q3 ) )
    {
        return $q3;
    }
    elseif( ticket_is_num_rows( $q4 ) )
    {
        return $q4;
    }
    else
    {
        return false;
    }
}

function is_ticket_over_quota( $rid, $sid, $date, $num_adult, $num_child = 0, $num_infant = 0 )
{
    $ticket = new ticketAvailability( $rid, $sid, $date, $num_adult, $num_child );

    if( $ticket->is_available() )
    {
        return false;
    }
    else
    {
        return true;
    }
}

function ticket_is_num_rows( $sql )
{
    global $db;

    $r = $db->do_query( $sql );
    $n = $db->num_rows( $r );

    if( $n == 0 )
    {
        return false;
    }
    else
    {
        return true;
    }
}

function ticket_get_data_rows( $sql )
{
    global $db;

    $r = $db->do_query( $sql );

    return $db->fetch_array( $r );
}

function ticket_get_allotment( $sid )
{
    global $db;

    $q = $db->prepare_query( 'SELECT * FROM ticket_schedule AS a WHERE a.sid = %d', $sid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    if( $d[ 'sallotment' ] == 0 )
    {
        return ticket_get_allotment_from_route( $d[ 'rid' ] );
    }
    else
    {
        return $d[ 'sallotment' ];
    }
}

function ticket_get_allotment_from_route( $rid )
{
    global $db;

    $q = $db->prepare_query( 'SELECT * FROM ticket_route AS a WHERE a.rid = %d', $rid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d[ 'rallotment' ];
}

function ticket_get_bank_account_info()
{
    global $db;

    $content = '';

    $q = $db->prepare_query( 'SELECT * FROM ticket_bank_account AS a ORDER BY a.sort_id ASC' );
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    if( $n > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $content .= sprintf( '<b>%s %s</b><br />Rek. No : %s<br />Nama : %s<br />Cabang: %s<br />Swift code: %s<br />Alamat : %s<br /><br />', $d[ 'bank_name' ], $d[ 'currency' ], $d[ 'account_no' ], $d[ 'name' ], $d[ 'branch' ], $d[ 'swift_code' ], $d[ 'address' ] );
        }

    }

    return $content;
}

function ticket_paypal_account( $field = '' )
{
    $server  = get_meta_data( 'gbs_paypal_server' );
    $account = array(
        'mode'          => '',
        'username'      => '',
        'client_id'     => '',
        'client_secret' => ''
    );

    if( empty( $server ) === false )
    {
        $mode    = $server == 'staging' ? 'sandbox' : $server;
        $account = array(
            'mode'          => $mode,
            'username'      => get_meta_data( 'gbs_paypal_' . $server . '_account' ),
            'client_id'     => get_meta_data( 'gbs_paypal_' . $server . '_client_id' ),
            'client_secret' => get_meta_data( 'gbs_paypal_' . $server . '_client_secret' )
        );
    }

    if( empty( $field ) === false && isset( $account[ $field ] ) )
    {
        return $account[ $field ];
    }
    else
    {
        return $account;
    }
}

function send_email( $options = array() )
{
    $mail = new PHPMailer( true );

    try
    {
        $mail->isSMTP();
        $mail->isHTML( true );

        $default = array(
            'from'          => array(),
            'to'            => array(),
            'replyto'       => array(),
            'attachment'    => array(),
            'strattachment' => array(),
            'subject'       => '',
            'altbody'       => '',
            'message'       => ''
        );

        $prm = array_merge( $default, $options );

        if( empty( $prm[ 'to' ] ) === false && empty( $prm[ 'message' ] ) === false )
        {
            $mail->Host     = get_meta_data( 'smtp' );
            $mail->Port     = get_meta_data( 'gbs_port_smpt' );
            $mail->Username = get_meta_data( 'gbs_username_smpt' );
            $mail->Password = get_meta_data( 'gbs_pass_email_smpt' );

            $mail->CharSet    = PHPMailer::CHARSET_UTF8;
            $mail->SMTPSecure = 'tls';
            $mail->SMTPAuth   = true;
            $mail->Timeout    = 60;

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer'       => false,
                    'verify_peer_name'  => false,
                    'allow_self_signed' => true
                )
            );

            //-- SET from where the message is send
            if( empty( $prm[ 'from' ] ) === false )
            {
                if( isset( $prm[ 'from' ][ 'email' ] ) && isset( $prm[ 'from' ][ 'label' ] ) )
                {
                    $mail->setFrom( $prm[ 'from' ][ 'email' ], $prm[ 'from' ][ 'label' ] );
                }
                else
                {
                    $mail->setFrom( $prm[ 'from' ] );
                }
            }
            else
            {
                $flabel = sprintf( '%s Automail', trim( get_meta_data( 'web_name' ) ) );

                $mail->setFrom( get_meta_data( 'gbs_email_smpt' ), $flabel );
            }

            //-- SET who the message is to be sent to
            if( isset( $prm[ 'to' ][ 'email' ] ) && isset( $prm[ 'to' ][ 'label' ] ) )
            {
                $mail->addAddress( $prm[ 'to' ][ 'email' ], $prm[ 'to' ][ 'label' ] );
            }
            else
            {
                $mail->addAddress( $prm[ 'to' ] );
            }

            //-- SET who the message will replay to
            if( empty( $prm[ 'replyto' ] ) === false )
            {
                if( isset( $prm[ 'replyto' ][ 'email' ] ) && isset( $prm[ 'replyto' ][ 'label' ] ) )
                {
                    $mail->addReplyTo( $prm[ 'replyto' ][ 'email' ], $prm[ 'replyto' ][ 'label' ] );
                }
                else
                {
                    $mail->addReplyTo( $prm[ 'replyto' ] );
                }
            }

            //-- SET the subject line
            if( empty( $prm[ 'subject' ] ) === false )
            {
                $mail->Subject = $prm[ 'subject' ];
            }

            //-- SET the message
            if( empty( $prm[ 'message' ] ) === false )
            {
                $mail->Body = $prm[ 'message' ];
            }

            //-- SET the alt body
            if( empty( $prm[ 'altbody' ] ) === false )
            {
                $mail->AltBody = $prm[ 'altbody' ];
            }

            //-- ATTACH file
            if( empty( $prm[ 'attachment' ] ) === false )
            {
                if( isset( $prm[ 'attachment' ][ 'path' ] ) && isset( $prm[ 'attachment' ][ 'name' ] ) )
                {
                    $mail->addAttachment( $prm[ 'attachment' ][ 'path' ], $prm[ 'attachment' ][ 'name' ] );
                }
                else
                {
                    $mail->addAttachment( $prm[ 'attachment' ] );
                }
            }

            //-- ATTACH string
            if( empty( $prm[ 'strattachment' ] ) === false )
            {
                $mail->addStringAttachment( $prm[ 'strattachment' ][ 'string' ], $prm[ 'strattachment' ][ 'filename' ], $prm[ 'strattachment' ][ 'encoding' ], $prm[ 'strattachment' ][ 'type' ] );
            }

            $mail->send();

            return true;
        }
        else
        {
            return false;
        }
    }
    catch( Exception $e )
    {
        return false;
    }
}

function send_booking_notification( $data = array() )
{
    if( empty( $data ) === false )
    {
        extract( $data );

        $idx = rand( 1, 1000 );

        set_template( PLUGINS_PATH . '/freebird/tpl/email_notification.html', 'email-notification-' . $idx );

        add_block( 'return-date-block', 'rd-' . $idx, 'email-notification-' . $idx );
        add_block( 'email-block', 'en-' . $idx, 'email-notification-' . $idx );

        add_variable( 'no_ticket', $no_ticket );
        add_variable( 'theme_url', get_theme_url() );
        add_variable( 'email', get_meta_data( 'email' ) );
        add_variable( 'booking_url', get_state_url( 'ticket&sub=booking' ) );
        add_variable( 'name', sprintf( '%s %s', $booking_by[ 'title' ], $booking_by[ 'fname' ] ) );

        if( isset( $booking_detail[ 0 ][ 'rfrom' ] ) && isset( $booking_detail[ 0 ][ 'rto' ] ) )
        {
            add_variable( 'route', sprintf( '%s - %s', $booking_detail[ 0 ][ 'rfrom' ], $booking_detail[ 0 ][ 'rto' ] ) );
        }

        if( isset( $booking_detail[ 0 ][ 'date' ] ) )
        {
            add_variable( 'dep_date', date( 'd F Y', strtotime( $booking_detail[ 0 ][ 'date' ] ) ) );
        }

        if( $type == 1 )
        {
            add_variable( 'trip_type', 'Return trip' );

            if( isset( $booking_detail[ 1 ][ 'date' ] ) )
            {
                add_variable( 'ret_date', date( 'd F Y', strtotime( $booking_detail[ 1 ][ 'date' ] ) ) );

                parse_template( 'return-date-block', 'rd-' . $idx, false );
            }
        }
        else
        {
            add_variable( 'trip_type', 'One way' );
        }

        parse_template( 'email-block', 'en-' . $idx, false );

        return send_email( array(
            'subject' => sprintf( 'New Booking From "%s" #%s', $booking_by[ 'fname' ], $no_ticket ),
            'message' => return_template( 'email-notification-' . $idx ),
            'to'      => array(
                'email' => get_meta_data( 'email' ),
                'label' => get_meta_data( 'web_name' )
            )
        ) );
    }
}

function send_invoice( $data = array() )
{
    if( empty( $data ) === false )
    {
        extract( $data );

        $idx = rand( 1, 1000 );

        set_template( PLUGINS_PATH . '/freebird/tpl/email_invoice.html', 'email-invoice-' . $idx );

        add_block( 'email-passenger-block', 'ep-' . $idx, 'email-invoice-' . $idx );
        add_block( 'email-detail-block', 'ed-' . $idx, 'email-invoice-' . $idx );
        add_block( 'email-block', 'ei-' . $idx, 'email-invoice-' . $idx );

        add_variable( 'no_ticket', $no_ticket );
        add_variable( 'theme_url', get_theme_url() );
        add_variable( 'email', get_meta_data( 'email' ) );
        add_variable( 'bank_info', ticket_get_bank_account_info() );
        add_variable( 'paypal_account', ticket_paypal_account( 'username' ) );
        add_variable( 'doku_link', site_url( 'pay-ticket/?type=doku&id=' . $bid ) );
        add_variable( 'paypal_link', site_url( 'pay-ticket/?type=paypal&id=' . $bid ) );

        add_variable( 'bemail', $booking_by[ 'email' ] );
        add_variable( 'bphone', $booking_by[ 'phone' ] );
        add_variable( 'baddress', $booking_by[ 'address' ] );
        add_variable( 'bdate', date( 'd F Y', $created_date ) );
        add_variable( 'bname', sprintf( '%s %s', $booking_by[ 'title' ], $booking_by[ 'fname' ] ) );

        foreach( $booking_detail as $dt )
        {
            if( $dt[ 'bdtype' ] == 'departure' )
            {
                add_variable( 'num_adult', $dt[ 'num_adult' ] );
                add_variable( 'num_child', $dt[ 'num_child' ] );
                add_variable( 'num_infant', $dt[ 'num_infant' ] );

                add_variable( 'pickup_address', $dt[ 'address_pickup' ] );
                add_variable( 'transfer_address', $dt[ 'address_transfer' ] );
                add_variable( 'pickup_destination', $dt[ 'pickup_destination' ] );
                add_variable( 'transfer_destination', $dt[ 'transfer_destination' ] );

                add_variable( 'price_per_adult', number_format( $dt[ 'price_per_adult' ], 0, ',', '.' ) );
                add_variable( 'price_per_child', number_format( $dt[ 'price_per_child' ], 0, ',', '.' ) );
                add_variable( 'price_per_infant', number_format( $dt[ 'price_per_infant' ], 0, ',', '.' ) );

                add_variable( 'total_adult', number_format( $dt[ 'total_adult' ], 0, ',', '.' ) );
                add_variable( 'total_child', number_format( $dt[ 'total_child' ], 0, ',', '.' ) );
                add_variable( 'total_infant', number_format( $dt[ 'total_infant' ], 0, ',', '.' ) );

                add_variable( 'total', number_format( $dt[ 'total' ], 0, ',', '.' ) );
                add_variable( 'pickup_cost', number_format( $dt[ 'pickup_cost' ], 0, ',', '.' ) );
                add_variable( 'transfer_cost', number_format( $dt[ 'transfer_cost' ], 0, ',', '.' ) );
                add_variable( 'total_summary', number_format( $dt[ 'total_summary' ], 0, ',', '.' ) );
            }

            add_variable( 'to', $dt[ 'rto' ] );
            add_variable( 'from', $dt[ 'rfrom' ] );
            add_variable( 'date', date( 'd F Y', strtotime( $dt[ 'date' ] ) ) );
            add_variable( 'stime_departure', date( 'H:i', strtotime( $dt[ 'stime_departure' ] ) ) );

            parse_template( 'email-detail-block', 'ed-' . $idx, true );
        }

        $no = 1;

        foreach( $booking_passenger as $dp )
        {
            add_variable( 'no', $no );
            add_variable( 'type', $dp[ 'type' ] );
            add_variable( 'fname', $dp[ 'fname' ] );
            add_variable( 'country', $dp[ 'country' ] );

            parse_template( 'email-passenger-block', 'ep-' . $idx, true );

            $no++;
        }

        parse_template( 'email-block', 'ei-' . $idx, false );

        return send_email( array(
            'subject' => sprintf( 'Booking Invoice "%s" #%s', $booking_by[ 'fname' ], $no_ticket ),
            'message' => return_template( 'email-invoice-' . $idx ),
            'to'      => array(
                'email' => $booking_by[ 'email' ],
                'label' => $booking_by[ 'fname' ]
            )
        ) );
    }
}

function send_payment_notification( $data = array(), $is_admin = true )
{
    if( empty( $data ) === false )
    {
        extract( $data );

        $idx = rand( 1, 1000 );

        set_template( PLUGINS_PATH . '/freebird/tpl/email_payment_notification.html', 'email-payment-notification-' . $idx );

        add_block( 'admin-email-block', 'ae-' . $idx, 'email-payment-notification-' . $idx );
        add_block( 'client-email-block', 'ce-' . $idx, 'email-payment-notification-' . $idx );

        add_variable( 'no_ticket', $no_ticket );
        add_variable( 'theme_url', get_theme_url() );
        add_variable( 'fname', $booking_by[ 'fname' ] );
        add_variable( 'email', get_meta_data( 'email' ) );

        if( isset( $booking_payment[ 'ptotal' ] ) )
        {
            add_variable( 'amount', number_format( $booking_payment[ 'ptotal' ], 0, ',', '.' ) );
        }
        else
        {
            add_variable( 'amount', number_format( $total, 0, ',', '.' ) );
        }

        if( $is_admin )
        {
            parse_template( 'admin-email-block', 'ae-' . $idx, false );

            return send_email( array(
                'subject' => sprintf( 'Payment Confirmation From "%s" #%s', $booking_by[ 'fname' ], $no_ticket ),
                'message' => return_template( 'email-payment-notification-' . $idx ),
                'to'      => array(
                    'email' => get_meta_data( 'email' ),
                    'label' => get_meta_data( 'web_name' )
                )
            ) );
        }
        else
        {
            parse_template( 'client-email-block', 'ce-' . $idx, false );

            return send_email( array(
                'subject' => sprintf( 'Payment Confirmation For Ticket Number #%s', $booking_by[ 'fname' ], $no_ticket ),
                'message' => return_template( 'email-payment-notification-' . $idx ),
                'to'      => array(
                    'email' => $booking_by[ 'email' ],
                    'label' => $booking_by[ 'fname' ]
                )
            ) );
        }
    }
}

function send_ticket( $data = array() )
{
    if( empty( $data ) === false )
    {
        extract( $data );

        set_template( PLUGINS_PATH . '/freebird/tpl/email_ticket.html', 'email-ticket' );

        add_block( 'email-block', 'em-block', 'email-ticket' );

        add_variable( 'theme_url', get_theme_url() );
        add_variable( 'fname', $booking_by[ 'fname' ] );
        add_variable( 'email', get_meta_data( 'email' ) );

        parse_template( 'email-block', 'em-block', false );

        return send_email( array(
            'subject' => sprintf( '%s Ticket #%s', trim( get_meta_data( 'web_name' ) ), $no_ticket ),
            'message' => return_template( 'email-ticket' ),
            'to'      => array(
                'email' => $data[ 'booking_by' ][ 'email' ],
                'label' => $data[ 'booking_by' ][ 'fname' ]
            ),
            'strattachment' => array(
                'filename' => sprintf( '%s.pdf', $no_ticket ),
                'string'   => generate_ticket( $data ),
                'type'     => 'application/pdf',
                'encoding' => 'base64'
            )
        ) );
    }
}

function generate_ticket( $data, $is_stream = false )
{
    if( empty( $data ) === false )
    {
        extract( $data );

        $d = new Dompdf( array( 'enable_remote' => true ) );

        $d->setPaper( 'A4', 'landscape' );

        set_template( PLUGINS_PATH . '/freebird/tpl/ticket.html', 'pdf-ticket' );

        add_block( 'ticket-passenger-block', 'tcp-block', 'pdf-ticket' );
        add_block( 'ticket-detail-block', 'tcl-block', 'pdf-ticket' );
        add_block( 'ticket-block', 'tc-block', 'pdf-ticket' );

        add_variable( 'theme_url', get_theme_url() );

        add_variable( 'no_ticket', $no_ticket );
        add_variable( 'booking_date', date( 'd F Y', $created_date ) );

        add_variable( 'name', $booking_by[ 'fname' ] );
        add_variable( 'phone', $booking_by[ 'phone' ] );
        add_variable( 'email', $booking_by[ 'email' ] );
        add_variable( 'address', $booking_by[ 'address' ] );

        foreach( $booking_detail as $dt )
        {
            if( $dt[ 'bdtype' ] == 'departure' )
            {
                add_variable( 'pickup_address', $dt[ 'address_pickup' ] );
                add_variable( 'transfer_address', $dt[ 'address_transfer' ] );
                add_variable( 'pickup_destination', $dt[ 'pickup_destination' ] );
                add_variable( 'transfer_destination', $dt[ 'transfer_destination' ] );
            }

            add_variable( 'to', $dt[ 'rto' ] );
            add_variable( 'from', $dt[ 'rfrom' ] );
            add_variable( 'date', date( 'd F Y', strtotime( $dt[ 'date' ] ) ) );
            add_variable( 'stime_departure', date( 'H:i', strtotime( $dt[ 'stime_departure' ] ) ) );

            parse_template( 'ticket-detail-block', 'tcl-block', true );
        }

        $no = 1;

        foreach( $booking_passenger as $dp )
        {
            add_variable( 'no', $no );
            add_variable( 'type', $dp[ 'type' ] );
            add_variable( 'fname', $dp[ 'fname' ] );
            add_variable( 'country', $dp[ 'country' ] );

            parse_template( 'ticket-passenger-block', 'tcp-block', true );

            $no++;
        }

        parse_template( 'ticket-block', 'tc-block', false );

        $output = return_template( 'pdf-ticket' );

        $d->loadHtml( $output );

        $d->render();

        if( $is_stream )
        {
            ob_end_clean();

            $d->stream( $data[ 'no_ticket' ] . '.pdf' );
        }
        else
        {
            return $d->output();
        }
    }
}

function init_ticket()
{
    header( 'location: ' . get_state_url( 'ticket&sub=booking' ) );

    exit;
}

function init_dashboard_content()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/dashboard.html', 'dashboard-2' );

    add_block( 'list-block', 'l-block', 'dashboard-2' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_booking_ajax_url() );

    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' );

    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' );

    parse_template( 'list-block', 'l-block', false );

    return return_template( 'dashboard-2' );
}

function init_additional_setting()
{
    run_additional_setting_actions();

    set_template( PLUGINS_PATH . '/freebird/tpl/setting.html', 'static' );

    add_block( 'form-block', 'f-block', 'static' );

    add_variable( 'alert', message_block() );
    add_variable( 'admin_tab', setting_tabs() );
    add_variable( 'themes_url', get_theme_url() );
    add_variable( 'state_url', get_state_url( 'global_settings&tab=static' ) );

    //-- Static Content
    add_variable( 'gbs_why_option_1', get_meta_data( 'gbs_why_option_1' ) );
    add_variable( 'gbs_why_option_2', get_meta_data( 'gbs_why_option_2' ) );
    add_variable( 'gbs_why_option_3', get_meta_data( 'gbs_why_option_3' ) );
    add_variable( 'gbs_why_option_4', get_meta_data( 'gbs_why_option_4' ) );
    add_variable( 'gbs_homepage', post_list_option( get_meta_data( 'gbs_homepage' ), array( 'pages' ) ) );

    //-- Popup Content
    add_variable( 'gbs_popup_content', get_meta_data( 'gbs_popup_content' ) );
    add_variable( 'gbs_publish_popup_content', get_meta_data( 'gbs_publish_popup_content' ) );
    add_variable( 'gbs_unpublish_popup_content', get_meta_data( 'gbs_unpublish_popup_content' ) );
    add_variable( 'gbs_type_popup_content', set_static_option( array( 'image-only' => 'Image Only', 'text' => 'Text' ), get_meta_data( 'gbs_type_popup_content' ), false ) );

    //-- Contact Information
    add_variable( 'gbs_email', get_meta_data( 'gbs_email' ) );
    add_variable( 'gbs_web_tag', get_meta_data( 'gbs_web_tag' ) );
    add_variable( 'gbs_address', get_meta_data( 'gbs_address' ) );
    add_variable( 'gbs_google_maps', get_meta_data( 'gbs_google_maps' ) );
    add_variable( 'gbs_phone_number', get_meta_data( 'gbs_phone_number' ) );

    //-- DOKU
    add_variable( 'gbs_doku_expired_time', get_meta_data( 'gbs_doku_expired_time' ) );
    add_variable( 'gbs_doku_live_client_id', get_meta_data( 'gbs_doku_live_client_id' ) );
    add_variable( 'gbs_doku_staging_client_id', get_meta_data( 'gbs_doku_staging_client_id' ) );
    add_variable( 'gbs_doku_live_client_secret', get_meta_data( 'gbs_doku_live_client_secret' ) );
    add_variable( 'gbs_doku_staging_client_secret', get_meta_data( 'gbs_doku_staging_client_secret' ) );
    add_variable( 'gbs_doku_server_option', set_static_option( array( 'staging' => 'Staging', 'live' => 'Production' ), get_meta_data( 'gbs_doku_server' ), false ) );

    //-- PayPal
    add_variable( 'gbs_paypal_live_account', get_meta_data( 'gbs_paypal_live_account' ) );
    add_variable( 'gbs_paypal_staging_account', get_meta_data( 'gbs_paypal_staging_account' ) );

    add_variable( 'gbs_paypal_live_client_id', get_meta_data( 'gbs_paypal_live_client_id' ) );
    add_variable( 'gbs_paypal_staging_client_id', get_meta_data( 'gbs_paypal_staging_client_id' ) );

    add_variable( 'gbs_paypal_live_client_secret', get_meta_data( 'gbs_paypal_live_client_secret' ) );
    add_variable( 'gbs_paypal_staging_client_secret', get_meta_data( 'gbs_paypal_staging_client_secret' ) );

    add_variable( 'gbs_paypal_usd_rate', get_meta_data( 'gbs_paypal_usd_rate' ) );
    add_variable( 'gbs_paypal_server_option', set_static_option( array( 'staging' => 'Staging', 'live' => 'Production' ), get_meta_data( 'gbs_paypal_server' ), false ) );

    //-- SMTP Email
    add_variable( 'gbs_port_smpt', get_meta_data( 'gbs_port_smpt' ) );
    add_variable( 'gbs_email_smpt', get_meta_data( 'gbs_email_smpt' ) );
    add_variable( 'gbs_username_smpt', get_meta_data( 'gbs_username_smpt' ) );
    add_variable( 'gbs_pass_email_smpt', get_meta_data( 'gbs_pass_email_smpt' ) );

    //-- reCAPTCHA
    add_variable( 'gbs_capcha_site_key', get_meta_data( 'gbs_capcha_site_key' ) );
    add_variable( 'gbs_capcha_secret_key', get_meta_data( 'gbs_capcha_secret_key' ) );

    //-- Social Media
    add_variable( 'gbs_link_fb', get_meta_data( 'gbs_link_fb' ) );
    add_variable( 'gbs_link_skype', get_meta_data( 'gbs_link_skype' ) );
    add_variable( 'gbs_link_yahoo', get_meta_data( 'gbs_link_yahoo' ) );
    add_variable( 'gbs_link_twitter', get_meta_data( 'gbs_link_twitter' ) );
    add_variable( 'gbs_link_whatsapp', get_meta_data( 'gbs_link_whatsapp' ) );
    add_variable( 'gbs_link_pinterest', get_meta_data( 'gbs_link_pinterest' ) );
    add_variable( 'gbs_link_tripadvisors', get_meta_data( 'gbs_link_tripadvisors' ) );

    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/css/select2.min.css' );

    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/js/select2.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/tinymce@4.9.11/tinymce.min.js' );
    add_actions( 'js_elements', 'get_javascript', 'tinymce' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'static' );
}

function run_additional_setting_actions()
{
    global $flash;

    if( isset( $_POST[ 'save_changes' ] ) )
    {
        $use_ajax = isset( $_POST[ 'use_ajax' ] ) ? true : false;
        $success  = 0;

        unset( $_POST[ 'save_changes' ] );
        unset( $_POST[ 'use_ajax' ] );

        foreach( $_POST as $key => $val )
        {
            if( update_meta_data( $key, $val, 'global_setting' ) )
            {
                $success++;
            }
        }

        if( $success == count( $_POST ) )
        {
            if( $use_ajax )
            {
                echo json_encode( array(
                    'type'    => 'success',
                    'content' => array(
                        'Your setting has been updated.'
                    )
                ) );
            }
            else
            {
                $flash->add( array(
                    'type'    => 'success',
                    'content' => array(
                        'Your setting has been updated.'
                    )
                ) );

                header( 'location:' . get_state_url( 'global_settings&tab=static' ) );
            }

            exit;
        }
        else
        {
            if( $use_ajax )
            {
                echo json_encode( array(
                    'type'    => 'error',
                    'content' => array(
                        'Update process failed.'
                    )
                ) );

                exit;
            }
            else
            {
                $flash->add( array(
                    'type'    => 'error',
                    'content' => array(
                        'Update process failed.'
                    )
                ) );
            }
        }
    }
}
