<?php

add_actions( 'boat', 'init_boat' );
add_actions( 'boat_admin_page', 'boat_ajax' );

add_privileges( 'administrator', 'boat', 'insert' );
add_privileges( 'administrator', 'boat', 'update' );
add_privileges( 'administrator', 'boat', 'delete' );

/*
| -----------------------------------------------------------------------------
| Admin Boat
| -----------------------------------------------------------------------------
*/
function init_boat()
{
    run_boat_actions();
    
    //-- Display add new form
    if( is_add_new() )
    {
        return add_boat();
    }
    elseif( is_edit() )
    {
        if( is_contributor() || is_author() )
        {
            if( is_num_boat( array( 'boat_id' => $_GET[ 'id' ] ) ) > 0 )
            {
                return edit_boat( $_GET[ 'id' ] );
            }
            else
            {
                return '
                <div class="alert_red_form">
                    You don\'t have an authorization to access this page
                </div>';
            }
        }
        else
        {
            return edit_boat( $_GET[ 'id' ] );
        }
    }
    elseif( is_delete_all() )
    {
        return delete_batch_boat();
    }
    
    //-- Automatic to display add new when there is no records on database
    if( is_num_boat() == 0 )
    {
        header( 'location:' . get_state_url( 'ticket&sub=boat&prc=add_new' ) );
    }
    elseif( is_num_boat() > 0 )
    {
        return get_boat_list();
    }
}

function get_boat_list()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/boat_list.html', 'boat' );

    add_block( 'list-block', 'l-block', 'boat' );

    add_variable( 'title', 'Boat List' );
    add_variable( 'limit', post_viewed() );
    add_variable( 'alert', message_block() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_boat_ajax_url() );
    add_variable( 'view-option', get_boat_view_option() );
    add_variable( 'button', get_boat_admin_button( get_state_url( 'ticket&sub=boat' ) ) );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );
    
    add_actions( 'section_title', 'Boats' );

    parse_template( 'list-block', 'l-block', 'boat' );

    return return_template( 'boat' );
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - Add New Boat
| -----------------------------------------------------------------------------
*/
function add_boat()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/boat_new.html', 'boat' );

    add_block( 'form-block', 'f-block', 'boat' );

    add_variable( 'update_all_css', 'style="display:none;"' );
    add_variable( 'checked_publish', 'checked' );  
    add_variable( 'checked_unpublish', '' );    
    
    add_variable( 'ftitle', 'Add New Boat' );
    add_variable( 'alert', message_block() );
    add_variable( 'ajax-url', get_boat_ajax_url() );
    add_variable( 'button', get_boat_admin_button( get_state_url( 'ticket&sub=boat' ), true ) );
    
    add_actions( 'section_title', 'Boat - Add New' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'boat' );
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - Edit Boat
| -----------------------------------------------------------------------------
*/
function edit_boat( $id )
{
    $d = fetch_boat( array( 'boat_id' => $id ) );

    set_template( PLUGINS_PATH . '/freebird/tpl/boat_new.html', 'boat' );

    add_block( 'form-block', 'f-block', 'boat' );

    add_variable( 'bname', $d[ 'bname' ] );
    add_variable( 'bcode', $d[ 'bcode' ] );
    add_variable( 'boat_id', $d[ 'boat_id' ] );
    add_variable( 'ballotment', $d[ 'ballotment' ] );

    if( $d[ 'status' ] == '1' )
    {
        add_variable( 'checked_publish', 'checked' );  
        add_variable( 'checked_unpublish', '' );    
    }
    else
    {
        add_variable( 'checked_publish', '' ); 
        add_variable( 'checked_unpublish', 'checked' );  
    }

    add_variable( 'update_all_css', '' );
    
    add_variable( 'ftitle', 'Edit Boat' );
    add_variable( 'alert', message_block() );
    add_variable( 'ajax-url', get_boat_ajax_url() );
    add_variable( 'button', get_boat_admin_button( get_state_url( 'ticket&sub=boat' ), true ) );
    
    add_actions( 'section_title', 'Boat - Edit' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'boat' );
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - Batch Delete Boat
| -----------------------------------------------------------------------------
*/
function delete_batch_boat()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/boat_batch_delete.html', 'boat-batch-delete' );

    add_block( 'delete-loop-block', 'dl-block', 'boat-batch-delete' );
    add_block( 'delete-block', 'd-block', 'boat-batch-delete' );

    foreach( $_POST[ 'select' ] as $key => $val )
    {
        $d = fetch_boat( array( 'boat_id' => $val ) );

        add_variable( 'bname', $d[ 'bname' ] );
        add_variable( 'boat_id', $d[ 'boat_id' ] );

        parse_template( 'delete-loop-block', 'dl-block', true );
    }

    add_variable( 'title', 'Batch Delete Boat' );
    add_variable( 'backlink', get_state_url( 'ticket&sub=boat' ) );
    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST[ 'select' ] ) == 1 ? 'this boat?' : 'these boats?' ) );
    
    add_actions( 'section_title', 'Boat - Batch Delete' );

    parse_template( 'delete-block', 'd-block', 'boat-batch-delete' );

    return return_template( 'boat-batch-delete' );
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - Table Query
| -----------------------------------------------------------------------------
*/
function get_boat_list_query()
{
    global $db;
    
    extract( $_POST );
    
    $post = $_REQUEST;
    $cols  = array(
        1 => 'a.bname',
        2 => 'a.bcode',
        3 => 'a.ballotment',
        4 => 'a.created_by',
        5 => 'a.created_date',
        6 => 'a.status' 
    );
    
    //-- Set Order Column
    if ( isset( $post[ 'order' ] ) && !empty( $post[ 'order' ] ) )
    {
        $o = array();
        
        foreach ( $post[ 'order' ] as $i => $od )
        {
            if ( isset( $cols[ $post[ 'order' ][ $i ][ 'column' ] ] ) )
            {
                $o[] = $cols[ $post[ 'order' ][ $i ][ 'column' ] ] . ' ' . $post[ 'order' ][ $i ][ 'dir' ];
            }
        }
        
        $order = !empty( $o ) ? ' ORDER BY ' . implode( ', ', $o ) : '';
    }
    else
    {
        $order = ' ORDER BY a.boat_id ASC';
    }

    //-- Set Condition
    $w = array();

    if( isset( $post[ 'show' ] ) && $post[ 'show' ] != '' && $post[ 'show' ] != 'all' )
    {
        $w[] = $db->prepare_query( 'status = %s', $show );
    }

    if( empty( $post[ 'search' ][ 'value' ] ) === false )
    {        
        $s = array();

        foreach ( $cols as $col )
        {
            $s[] = $db->prepare_query( $col . ' LIKE %s', '%' . $post[ 'search' ][ 'value' ] . '%' );
        }

        $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE ' . implode( ' AND ', $w );
    }
    else
    {
        $where = '';
    }

    $q = 'SELECT * FROM ticket_master_boat AS a' . $where . $order;
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );
    
    $q2 = $q . ' LIMIT ' . $post[ 'start' ] . ', ' . $post[ 'length' ];
    $r2 = $db->do_query( $q2 );
    $n2 = $db->num_rows( $r2 );
    
    $data = array();
    
    if ( $n2 > 0 )
    {
        $url = get_state_url( 'ticket&sub=boat' );
        
        while ( $d2 = $db->fetch_array( $r2 ) )
        {
            $uid = fetch_user_id_by_username( $d2[ 'created_by' ] );

            $data[] = array(
                'ballotment'   => $d2[ 'ballotment' ],
                'created_by'   => $d2[ 'created_by' ],
                'boat_id'      => $d2[ 'boat_id' ],
                'status'       => $d2[ 'status' ],
                'bname'        => $d2[ 'bname' ],
                'bcode'        => $d2[ 'bcode' ],
                'user_link'    => user_url( $uid ),
                'ajax_link'    => get_boat_ajax_url(),
                'avatar'       => get_avatar( $uid, 3 ),
                'created_date' => date( 'd F Y', $d2[ 'created_date' ] ),
                'edit_link'    => get_state_url( 'ticket&sub=boat&prc=edit&id=' . $d2[ 'boat_id' ] )
            );
        }
    }
    else
    {
        $n = 0;
    }
    
    return array(
        'draw' => intval( $post[ 'draw' ] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data 
    );
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - Action Button
| -----------------------------------------------------------------------------
*/
function get_boat_admin_button( $new_url = '', $is_form = false )
{
    if( $is_form )
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=cancel', $new_url ) . '</li>
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=publish' ) . '</li>                 
            <li>' . button( 'button=cancel', $new_url ) . '</li>  
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
    }
    else
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>
            <li>' . button( 'button=publish&type=submit&enable=false' ) . '</li>
            <li>' . button( 'button=unpublish&type=submit&enable=false' ) . '</li>';
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - View Option
| -----------------------------------------------------------------------------
*/
function get_boat_view_option()
{
    $opt_viewed   = '';    
    $show_data    = isset( $_POST[ 'data_to_show' ] ) ? $_POST[ 'data_to_show' ] : ( isset( $_GET[ 'data_to_show' ] ) ? $_GET[ 'data_to_show' ] : 'all' );
    $data_to_show = array( 'all' => 'All', '1' => 'Publish', '0' => 'Unpublish' );
    
    foreach( $data_to_show as $key => $val )
    {
        if( isset( $show_data ) && !empty( $show_data ) )
        {
            if( $show_data === $key )
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" checked="checked" autocomplete="off" />
                <label>' . $val . '</label>';
            }
            else
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" autocomplete="off" />
                <label>' . $val . '</label>';
            }
        }
        else
        {
            $opt_viewed .= '
            <input type="radio" name="data_to_show" value="' . $key . '" autocomplete="off" />
            <label>' . $val . '</label>';
        }
    }

    return $opt_viewed;
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_boat_ajax_url()
{
    return get_state_url( 'ajax&apps=boat' );
}

/*
| -----------------------------------------------------------------------------
| Admin Boat Actions
| -----------------------------------------------------------------------------
*/
function run_boat_actions()
{
    global $db;
    global $flash;

    //-- Publish, Unpublish
    //-- Actions From List Boat
    if( isset( $_POST[ 'select' ] ) )
    {
        if( is_publish() || is_unpublish() )
        {
            $count  = count( $_POST[ 'select' ] );
            $status = is_unpublish() ? '0' : '1';
            $error  = 0;

            foreach( $_POST[ 'select' ] as $key => $val )
            {
                if( update_boat( array( 'status' => $status ), array( 'boat_id' => $val ) ) === false )
                {
                    $error++;
                }
            }

            if( $error > 0 )
            {
                if( $error == $count )
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to update all boat status' ) );
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to update some of boat status' ) );
                }

                header( 'location: ' . get_state_url( 'ticket&sub=boat' ) );

                exit;
            }
            else
            {
                $flash->add( array( 'type' => 'success', 'content' => 'Successfully update all selected boat status' ) );
                
                header( 'location: ' . get_state_url( 'ticket&sub=boat' ) );

                exit;
            }
        }
    }
    elseif( is_confirm_delete() )
    {
        $count  = count( $_POST[ 'id' ] );
        $error = 0;

        foreach( $_POST[ 'id' ] as $key => $val )
        {
            if( delete_boat( $val ) === false )
            {
                $error++;
            }
        }

        if( $error > 0 )
        {
            if( $error == $count )
            {
                $flash->add( array( 'type' => 'error', 'content' => 'Failed to delete all selected boat' ) );
            }
            else
            {
                $flash->add( array( 'type' => 'error', 'content' => 'Failed to update some of selected boat' ) );
            }

            header( 'location: ' . get_state_url( 'ticket&sub=boat' ) );

            exit;
        }
        else
        {
            $flash->add( array( 'type' => 'success', 'content' => 'Successfully delete all selected boat' ) );
            
            header( 'location: ' . get_state_url( 'ticket&sub=boat' ) );

            exit;
        }
    }
    else
    {
        if( is_save_draft() || is_publish() )
        {
            if( is_add_new() )
            {
                if( save_boat() )
                {
                    $flash->add( array( 'type' => 'success', 'content' => 'Successfully add new boat' ) );
                    
                    header( 'location: ' . get_state_url( 'ticket&sub=boat&prc=add_new' ) );

                    exit;
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to add new boat' ) );
                }
            }
            elseif( is_edit() )
            {
                if( update_boat() )
                {
                    $flash->add( array( 'type' => 'success', 'content' => 'Successfully updated boat data' ) );
                    
                    header( 'location: ' . get_state_url( 'ticket&sub=boat&prc=edit&id=' . $_GET[ 'id' ] ) );

                    exit;
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to update boat data' ) );
                }
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - Save Boat
| -----------------------------------------------------------------------------
*/
function fetch_boat( $conditions = array(), $operator = 'AND',  $fields = '*' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( is_array( $fields ) && empty( $fields ) === false )
    {
        $param = implode( ', ', $fields );
    }
    else
    {
        $param = $fields;
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT ' . $param . ' FROM ticket_master_boat' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );

        if( $n > 1 )
        {
            while( $d = $db->fetch_assoc( $r ) )
            {
                $data[] = $d;
            }
        }
        elseif( $n > 0 )
        {
            $data = $db->fetch_assoc( $r );

            if( $fields != '*' && is_string( $fields ) && isset( $data[ $fields ] ) )
            {
                $data = $data[ $fields ];
            }
        }

        return $data;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - Save Boat
| -----------------------------------------------------------------------------
*/
function save_boat( $params = array() )
{
    global $db;

    $fields = array(
        'created_date',
        'created_by',
        'ballotment',
        'sort_id',
        'status',
        'bname',
        'bcode',
        'dlu',
    );

    if( empty( $params ) )
    {
        $params = $_POST;
    }

    if( empty( $params ) )
    {
        return false;
    }
    else
    {
        //-- Insert boat data
        $data = array( 
            'created_by'   => $_COOKIE[ 'username' ],
            'created_date' => time(), 
            'dlu'          => time()
        );

        foreach( $params as $f => $d )
        {
            if( in_array( $f, $fields ) )
            {
                $data[ $f ] = $d;
            }
        }

        $r = $db->insert( 'ticket_master_boat', $data );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - Update Boat
| -----------------------------------------------------------------------------
*/
function update_boat( $params = array(), $where = array() )
{
    global $db;

    $fields = array(
        'created_date',
        'created_by',
        'ballotment',
        'sort_id',
        'status',
        'bname',
        'bcode',
        'dlu',
    );

    if( empty( $params ) )
    {
        $params = $_POST;
    }

    if( empty( $where ) )
    {
        $where = array( 'boat_id' => $_GET[ 'id' ] );
    }

    if( empty( $params ) )
    {
        return false;
    }
    else
    {
        $db->begin();

        $commit = 1;

        //-- Update boat data
        $data = array( 'dlu' => time() );

        foreach( $params as $f => $d )
        {
            if( in_array( $f, $fields ) )
            {
                $data[ $f ] = $d;
            }
        }

        $r = $db->update( 'ticket_master_boat', $data, $where );

        if( is_array( $r ) )
        {
            $commit = 0;
        }

        //-- Update allotment data for all related schedule
        if( isset( $params[ 'update_allotment_all_schedule' ] ) )
        {
            $r = $db->update( 'ticket_schedule', array(
                'sallotment' => $params[ 'ballotment' ],
                'dlu'        => time()
            ), array( 'sboat' => $_GET[ 'id' ] ) );

            if( is_array( $r ) )
            {
                $commit = 0;
            }
        }
                
        if( $commit == 0 )
        {
            $db->rollback();

            return false;
        }
        else
        {
            $db->commit();

            return true;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - Delete Boat
| -----------------------------------------------------------------------------
*/
function delete_boat( $id )
{
    global $db;

    $r = $db->delete( 'ticket_master_boat', array( 'boat_id' => $id ) );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - Check Number of Boat
| -----------------------------------------------------------------------------
*/
function is_num_boat( $conditions = array(), $operator = 'AND' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();
    $n = 0;

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT * FROM ticket_master_boat' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );
    }

    return $n;
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - Boat List Option
| -----------------------------------------------------------------------------
*/
function ticket_boat_option( $selected = '' )
{
    $data = fetch_boat( array( 'status' => 1 ) );
    $html = '';

    if( empty( $data ) === false )
    {
        foreach( $data as $d )
        {
            if( is_array( $d ) === false )
            {
                $d = $data;

                if( $d[ 'boat_id' ] == $selected )
                {
                    $html .= '<option value="' . $d[ 'boat_id' ] . '" selected>' . $d[ 'bname' ] . '</option>';
                }
                else
                {
                    $html .= '<option value="' . $d[ 'boat_id' ] . '">' . $d[ 'bname' ] . '</option>';
                }

                break;
            }
            else
            {
                if( $d[ 'boat_id' ] == $selected )
                {
                    $html .= '<option value="' . $d[ 'boat_id' ] . '" selected>' . $d[ 'bname' ] . '</option>';
                }
                else
                {
                    $html .= '<option value="' . $d[ 'boat_id' ] . '">' . $d[ 'bname' ] . '</option>';
                }
            }
        }
    }

    return $html;
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - Get Detail Boat
| -----------------------------------------------------------------------------
*/
function ticket_boat_get_detail_by_id( $boat_id, $field = '' )
{
    global $db;

    $s = "SELECT * FROM ticket_master_boat AS a WHERE a.boat_id = %d";
    $q = $db->prepare_query( $s, $boat_id );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    if( $field != '' && isset( $d[ $field ] ) )
	{
		return $d[ $field ];
	}
	else
	{
		return $d;
	}
}

/*
| -----------------------------------------------------------------------------
| Admin Boat - Ajax Function
| -----------------------------------------------------------------------------
*/
function boat_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'load-boat-data' )
    {
        echo json_encode( get_boat_list_query() );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'delete-boat' )
    {
        if( delete_boat( $_POST[ 'id' ] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }
}

?> 