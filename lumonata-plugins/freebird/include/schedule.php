<?php

add_actions( 'schedule', 'init_schedule' );
add_actions( 'schedule_admin_page', 'schedule_ajax' );

add_privileges( 'administrator', 'schedule', 'insert' );
add_privileges( 'administrator', 'schedule', 'update' );
add_privileges( 'administrator', 'schedule', 'delete' );

/*
| -----------------------------------------------------------------------------
| Admin Schedule
| -----------------------------------------------------------------------------
*/
function init_schedule()
{
    run_schedule_actions();
    
    //-- Display add new form
    if( is_add_new() )
    {
        return add_schedule();
    }
    elseif( is_edit() )
    {
        if( is_contributor() || is_author() )
        {
            if( is_num_schedule( array( 'sid' => $_GET[ 'id' ] ) ) > 0 )
            {
                return edit_schedule( $_GET[ 'id' ] );
            }
            else
            {
                return '
                <div class="alert_red_form">
                    You don\'t have an authorization to access this page
                </div>';
            }
        }
        else
        {
            return edit_schedule( $_GET[ 'id' ] );
        }
    }
    elseif( is_delete_all() )
    {
        return delete_batch_schedule();
    }
    
    //-- Automatic to display add new when there is no records on database
    if( is_num_schedule() == 0 )
    {
        header( 'location:' . get_state_url( 'ticket&sub=schedule&prc=add_new' ) );
    }
    elseif( is_num_schedule() > 0 )
    {
        return get_schedule_list();
    }
}

function get_schedule_list()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/schedule_list.html', 'schedule' );

    add_block( 'list-block', 'l-block', 'schedule' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'alert', message_block() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_schedule_ajax_url() );
    add_variable( 'view-option', get_schedule_view_option() );
    add_variable( 'button', get_schedule_admin_button( get_state_url( 'ticket&sub=schedule' ) ) );
    add_variable( 'title', 'Schedule List' );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );
    
    add_actions( 'section_title', 'Schedule' );

    parse_template( 'list-block', 'l-block', 'schedule' );

    return return_template( 'schedule' );
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule - Add New Schedule
| -----------------------------------------------------------------------------
*/
function add_schedule()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/schedule_new.html', 'schedule' );

    add_block( 'form-block', 'f-block', 'schedule' );

    add_variable( 'ftitle', 'Add New Schedule' );

    add_variable( 'checked_publish', 'checked' );  
    add_variable( 'checked_unpublish', '' );

    add_variable( 'checked_sunrise', 'checked' );  
    add_variable( 'checked_sunset', '' );

    add_variable( 'rid', ticket_route_option() );
    add_variable( 'sboat', ticket_boat_option() );

    add_variable( 'alert', message_block() );
    add_variable( 'ajax-url', get_schedule_ajax_url() );
    add_variable( 'button', get_schedule_admin_button( get_state_url( 'ticket&sub=schedule' ), true ) );

    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/css/select2.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/js/select2.min.js' );

    add_actions( 'section_title', 'Schedule - Add New' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'schedule' );
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule - Edit Schedule
| -----------------------------------------------------------------------------
*/
function edit_schedule( $id )
{
    $d = fetch_schedule( array( 'sid' => $id ) );

    set_template( PLUGINS_PATH . '/freebird/tpl/schedule_new.html', 'schedule' );

    add_block( 'form-block', 'f-block', 'schedule' );

    add_variable( 'sid', $d[ 'sid' ] );
    add_variable( 'sname', $d[ 'sname' ] );
    add_variable( 'stime_arrive', $d[ 'stime_arrive' ] );
    add_variable( 'stime_departure', $d[ 'stime_departure' ] );

    add_variable( 'rid', ticket_route_option( $d[ 'rid' ] ) );
    add_variable( 'sboat', ticket_boat_option( $d[ 'sboat' ] ) );

    add_variable( 'ftitle', 'Edit Schedule' );

    if( $d[ 'status' ] == '1' )
    {
        add_variable( 'checked_publish', 'checked' );  
        add_variable( 'checked_unpublish', '' );    
    }
    else
    {
        add_variable( 'checked_publish', '' ); 
        add_variable( 'checked_unpublish', 'checked' );  
    }

    if( $d[ 'sparent' ] == '0' )
    {
        add_variable( 'checked_sunrise', 'checked' );  
        add_variable( 'checked_sunset', '' );    
    }
    else
    {
        add_variable( 'checked_sunrise', '' ); 
        add_variable( 'checked_sunset', 'checked' );  
    }
    
    add_variable( 'alert', message_block() );
    add_variable( 'ajax-url', get_schedule_ajax_url() );
    add_variable( 'button', get_schedule_admin_button( get_state_url( 'ticket&sub=schedule' ), true ) );

    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/css/select2.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/js/select2.min.js' );
    
    add_actions( 'section_title', 'Schedule - Edit' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'schedule' );
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule - Batch Delete Schedule
| -----------------------------------------------------------------------------
*/
function delete_batch_schedule()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/schedule_batch_delete.html', 'schedule-batch-delete' );

    add_block( 'delete-loop-block', 'dl-block', 'schedule-batch-delete' );
    add_block( 'delete-block', 'd-block', 'schedule-batch-delete' );

    foreach( $_POST[ 'select' ] as $key => $val )
    {
        $d = fetch_schedule( array( 'sid' => $val ) );

        add_variable( 'sname', $d[ 'sname' ] );
        add_variable( 'sid', $d[ 'sid' ] );

        parse_template( 'delete-loop-block', 'dl-block', true );
    }

    add_variable( 'title', 'Batch Delete Schedule' );
    add_variable( 'backlink', get_state_url( 'ticket&sub=schedule' ) );
    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST[ 'select' ] ) == 1 ? 'this data?' : 'these data?' ) );
    
    add_actions( 'section_title', 'Schedule - Batch Delete' );

    parse_template( 'delete-block', 'd-block', 'schedule-batch-delete' );

    return return_template( 'schedule-batch-delete' );
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule - Table Query
| -----------------------------------------------------------------------------
*/
function get_schedule_list_query()
{
    global $db;
    
    extract( $_POST );
    
    $post = $_REQUEST;
    $cols  = array(
        1 => 'a.sparent',
        2 => 'b.rname',
        3 => 'c.bname', 
        4 => 'a.stime_departure', 
        5 => 'a.stime_arrive', 
        6 => 'a.created_by',
        7 => 'a.created_date',
        8 => 'a.status'
    );
    
    //-- Set Order Column
    if ( isset( $post[ 'order' ] ) && !empty( $post[ 'order' ] ) )
    {
        $o = array();
        
        foreach ( $post[ 'order' ] as $i => $od )
        {
            if ( isset( $cols[ $post[ 'order' ][ $i ][ 'column' ] ] ) )
            {
                $o[] = $cols[ $post[ 'order' ][ $i ][ 'column' ] ] . ' ' . $post[ 'order' ][ $i ][ 'dir' ];
            }
        }
        
        $order = !empty( $o ) ? ' ORDER BY ' . implode( ', ', $o ) : '';
    }
    else
    {
        $order = ' ORDER BY a.sid ASC';
    }

    //-- Set Condition
    $w = array();

    if( isset( $post[ 'show' ] ) && $post[ 'show' ] != '' && $post[ 'show' ] != 'all' )
    {
        $w[] = $db->prepare_query( 'a.status = %s', $show );
    }

    if( empty( $post[ 'search' ][ 'value' ] ) === false )
    {        
        $s = array();

        foreach ( $cols as $col )
        {
            $s[] = $db->prepare_query( $col . ' LIKE %s', '%' . $post[ 'search' ][ 'value' ] . '%' );
        }

        $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE ' . implode( ' AND ', $w );
    }
    else
    {
        $where = '';
    }

    $q = 'SELECT
			a.sid,
			b.rname,
			c.bname,
			a.status,
			a.sparent,
			a.created_by,
			a.created_date,
			TIME_FORMAT( a.stime_arrive, "%H:%i" ) AS stime_arrive,
			TIME_FORMAT( a.stime_departure, "%H:%i" ) AS stime_departure
		 FROM ticket_schedule AS a
		 INNER JOIN ticket_route AS b ON a.rid = b.rid
		 INNER JOIN ticket_master_boat AS c ON a.sboat = c.boat_id' . $where . $order;
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );
    
    $q2 = $q . ' LIMIT ' . $post[ 'start' ] . ', ' . $post[ 'length' ];
    $r2 = $db->do_query( $q2 );
    $n2 = $db->num_rows( $r2 );
    
    $data = array();
    
    if ( $n2 > 0 )
    {
        $url = get_state_url( 'ticket&sub=schedule' );
        
        while ( $d2 = $db->fetch_array( $r2 ) )
        {
            $uid = fetch_user_id_by_username( $d2[ 'created_by' ] );

            $data[] = array(
                'stime_departure'  => $d2[ 'stime_departure' ],
                'stime_arrive'     => $d2[ 'stime_arrive' ],
                'created_by'       => $d2[ 'created_by' ],
                'sparent'          => $d2[ 'sparent' ],
                'status'           => $d2[ 'status' ],
                'rname'            => $d2[ 'rname' ],
                'bname'            => $d2[ 'bname' ],
                'sid'              => $d2[ 'sid' ],
                'user_link'        => user_url( $uid ),
                'avatar'           => get_avatar( $uid, 3 ),
                'ajax_link'        => get_schedule_ajax_url(),
                'created_date'     => date( 'd F Y', $d2[ 'created_date' ] ),
                'edit_link'        => get_state_url( 'ticket&sub=schedule&prc=edit&id=' . $d2[ 'sid' ] )
            );
        }
    }
    else
    {
        $n = 0;
    }
    
    return array(
        'draw' => intval( $post[ 'draw' ] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data 
    );
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule - Action Button
| -----------------------------------------------------------------------------
*/
function get_schedule_admin_button( $new_url = '', $is_form = false )
{
    if( $is_form )
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=cancel', $new_url ) . '</li>
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=publish' ) . '</li>                 
            <li>' . button( 'button=cancel', $new_url ) . '</li>  
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
    }
    else
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>
            <li>' . button( 'button=publish&type=submit&enable=false' ) . '</li>
            <li>' . button( 'button=unpublish&type=submit&enable=false' ) . '</li>';
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule - View Option
| -----------------------------------------------------------------------------
*/
function get_schedule_view_option()
{
    $opt_viewed   = '';    
    $show_data    = isset( $_POST[ 'data_to_show' ] ) ? $_POST[ 'data_to_show' ] : ( isset( $_GET[ 'data_to_show' ] ) ? $_GET[ 'data_to_show' ] : 'all' );
    $data_to_show = array( 'all' => 'All', '1' => 'Publish', '0' => 'Unpublish' );
    
    foreach( $data_to_show as $key => $val )
    {
        if( isset( $show_data ) && !empty( $show_data ) )
        {
            if( $show_data === $key )
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" checked="checked" autocomplete="off" />
                <label>' . $val . '</label>';
            }
            else
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" autocomplete="off" />
                <label>' . $val . '</label>';
            }
        }
        else
        {
            $opt_viewed .= '
            <input type="radio" name="data_to_show" value="' . $key . '" autocomplete="off" />
            <label>' . $val . '</label>';
        }
    }

    return $opt_viewed;
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_schedule_ajax_url()
{
    return get_state_url( 'ajax&apps=schedule' );
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule Actions
| -----------------------------------------------------------------------------
*/
function run_schedule_actions()
{
    global $db;
    global $flash;

    //-- Publish, Unpublish
    //-- Actions From List Schedule
    if( isset( $_POST[ 'select' ] ) )
    {
        if( is_publish() || is_unpublish() )
        {
            $count  = count( $_POST[ 'select' ] );
            $status = is_unpublish() ? '0' : '1';
            $error  = 0;

            foreach( $_POST[ 'select' ] as $key => $val )
            {
                if( update_schedule( array( 'status' => $status ), array( 'sid' => $val ) ) === false )
                {
                    $error++;
                }
            }

            if( $error > 0 )
            {
                if( $error == $count )
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to update all schedule status' ) );
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to update some of schedule status' ) );
                }

                header( 'location: ' . get_state_url( 'ticket&sub=schedule' ) );

                exit;
            }
            else
            {
                $flash->add( array( 'type' => 'success', 'content' => 'Successfully update all selected schedule status' ) );
                
                header( 'location: ' . get_state_url( 'ticket&sub=schedule' ) );

                exit;
            }
        }
    }
    elseif( is_confirm_delete() )
    {
        $count  = count( $_POST[ 'id' ] );
        $error = 0;

        foreach( $_POST[ 'id' ] as $key => $val )
        {
            if( delete_schedule( $val ) === false )
            {
                $error++;
            }
        }

        if( $error > 0 )
        {
            if( $error == $count )
            {
                $flash->add( array( 'type' => 'error', 'content' => 'Failed to delete all selected schedule' ) );
            }
            else
            {
                $flash->add( array( 'type' => 'error', 'content' => 'Failed to update some of selected schedule' ) );
            }

            header( 'location: ' . get_state_url( 'ticket&sub=schedule' ) );

            exit;
        }
        else
        {
            $flash->add( array( 'type' => 'success', 'content' => 'Successfully delete all selected schedule' ) );
            
            header( 'location: ' . get_state_url( 'ticket&sub=schedule' ) );

            exit;
        }
    }
    else
    {
        if( is_save_draft() || is_publish() )
        {
            if( is_add_new() )
            {
                if( save_schedule() )
                {
                    $flash->add( array( 'type' => 'success', 'content' => 'Successfully add new schedule' ) );
                    
                    header( 'location: ' . get_state_url( 'ticket&sub=schedule&prc=add_new' ) );

                    exit;
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to add new schedule' ) );
                }
            }
            elseif( is_edit() )
            {
                if( update_schedule() )
                {
                    $flash->add( array( 'type' => 'success', 'content' => 'Successfully updated schedule data' ) );
                    
                    header( 'location: ' . get_state_url( 'ticket&sub=schedule&prc=edit&id=' . $_GET[ 'id' ] ) );

                    exit;
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to update schedule data' ) );
                }
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule - Save Schedule
| -----------------------------------------------------------------------------
*/
function fetch_schedule( $conditions = array(), $operator = 'AND',  $fields = '*' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( is_array( $fields ) && empty( $fields ) === false )
    {
        $param = implode( ', ', $fields );
    }
    else
    {
        $param = $fields;
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT ' . $param . ' FROM ticket_schedule' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );

        if( $n > 1 )
        {
            while( $d = $db->fetch_assoc( $r ) )
            {
                $data[] = $d;
            }
        }
        elseif( $n > 0 )
        {
            $data = $db->fetch_assoc( $r );

            if( $fields != '*' && is_string( $fields ) && isset( $data[ $fields ] ) )
            {
                $data = $data[ $fields ];
            }
        }

        return $data;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule - Save Schedule
| -----------------------------------------------------------------------------
*/
function save_schedule( $params = array() )
{
    global $db;

    $fields = array(
        'stime_departure',
        'stime_arrive',
        'created_date',
        'created_by',
        'sparent',
        'status',
        'sboat',
        'sname',
        'rid',
        'dlu',
    );

    if( empty( $params ) )
    {
        $params = $_POST;
    }

    if( empty( $params ) )
    {
        return false;
    }
    else
    {
        //-- Insert schedule data
        $data = array( 
            'created_by'   => $_COOKIE[ 'username' ],
            'created_date' => time(), 
            'dlu'          => time()
        );

        foreach( $params as $f => $d )
        {
            if( in_array( $f, $fields ) )
            {
                $data[ $f ] = $d;
            }
        }

        $r = $db->insert( 'ticket_schedule', $data );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule - Update Schedule
| -----------------------------------------------------------------------------
*/
function update_schedule( $params = array(), $where = array() )
{
    global $db;

    $fields = array(
        'stime_departure',
        'stime_arrive',
        'created_date',
        'created_by',
        'sparent',
        'status',
        'sboat',
        'sname',
        'rid',
        'dlu',
    );

    if( empty( $params ) )
    {
        $params = $_POST;
    }

    if( empty( $where ) )
    {
        $where = array( 'sid' => $_GET[ 'id' ] );
    }

    if( empty( $params ) )
    {
        return false;
    }
    else
    {
        //-- Update schedule data
        $data = array( 'dlu' => time() );

        foreach( $params as $f => $d )
        {
            if( in_array( $f, $fields ) )
            {
                $data[ $f ] = $d;
            }
        }

        $r = $db->update( 'ticket_schedule', $data, $where );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule - Delete Schedule
| -----------------------------------------------------------------------------
*/
function delete_schedule( $id )
{
    global $db;

    $r = $db->delete( 'ticket_schedule', array( 'sid' => $id ) );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule - Check Number of Schedule
| -----------------------------------------------------------------------------
*/
function is_num_schedule( $conditions = array(), $operator = 'AND' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();
    $n = 0;

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT * FROM ticket_schedule' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );
    }

    return $n;
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule - Schedule List Option
| -----------------------------------------------------------------------------
*/
function ticket_schedule_option( $selected = '' )
{
	$data = fetch_schedule( array( 'status' => 1 ) );
	$html = '';

	if( empty( $data ) === false )
	{
        if( !is_array( $data ) )
        {
            $data = array( $data );
        }

        foreach( $data as $d )
        {
            if( is_array( $d ) === false )
            {
                $d = $data;

			    if( $d[ 'sid' ] == $selected )
			    {
			    	$html .= '<option value="' . $d[ 'sid' ] . '" data-rid="' . $d[ 'rid' ] . '" data-sboat="' . $d[ 'sboat' ] . '" selected>' . $d[ 'sname' ] . '</option>';
			    }
			    else
			    {
			    	$html .= '<option value="' . $d[ 'sid' ] . '" data-rid="' . $d[ 'rid' ] . '" data-sboat="' . $d[ 'sboat' ] . '">' . $d[ 'sname' ] . '</option>';
			    }

                break;
            }
            else
            {
			    if( $d[ 'sid' ] == $selected )
			    {
			    	$html .= '<option value="' . $d[ 'sid' ] . '" data-rid="' . $d[ 'rid' ] . '" data-sboat="' . $d[ 'sboat' ] . '" selected>' . $d[ 'sname' ] . '</option>';
			    }
			    else
			    {
			    	$html .= '<option value="' . $d[ 'sid' ] . '" data-rid="' . $d[ 'rid' ] . '" data-sboat="' . $d[ 'sboat' ] . '">' . $d[ 'sname' ] . '</option>';
			    }
            }
		}
	}

    return $html;
}

/*
| -----------------------------------------------------------------------------
| Admin Schedule - Ajax Function
| -----------------------------------------------------------------------------
*/
function schedule_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'load-schedule-data' )
    {
        echo json_encode( get_schedule_list_query() );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'delete-schedule' )
    {
        if( delete_schedule( $_POST[ 'id' ] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }
}

?> 