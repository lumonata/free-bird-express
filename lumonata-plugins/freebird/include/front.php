<?php

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\ExecutePayment;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Exception\PayPalConnectionException;

require_once( PLUGINS_PATH . '/freebird/class/ticket.php' );

//-- AJAX Request
add_actions( 'get-pickup-transfer-cost_ajax', 'ticket_get_pickup_transfer_cost' );
add_actions( 'request-paypal-payment-url_ajax', 'ticket_paypal_payment_url' );
add_actions( 'request-doku-payment-url_ajax', 'ticket_doku_payment_url' );
add_actions( 'download-ticket_ajax', 'ticket_download_printout' );
add_actions( 'submit-ticket_ajax', 'ticket_submit_booking' );
add_actions( 'paypal-notify_ajax', 'ticket_paypal_notify' );
add_actions( 'doku-notify_ajax', 'ticket_doku_notify' );

function is_ticket_check_valid_parameter( $prm = array() )
{
	$fields = array( 'dep_date', 'adult', 'from', 'to' );

	if( isset( $prm[ 'trip_type' ] ) && $prm[ 'trip_type' ] == 1 )
	{
		array_push( $fields, 'ret_date' );
	}

	$error = 0;

	foreach( $fields as $field )
	{
		if( isset( $prm[ $field ] ) && $prm[ $field ] == '' )
		{
			$error++;
		}
	}

	if( isset( $prm[ 'trip_type' ] ) && $prm[ 'trip_type' ] == 1 )
	{
		if( strtotime( str_replace( '/', '-', $prm[ 'dep_date' ] ) ) > strtotime( str_replace( '/', '-', $prm[ 'ret_date' ] ) ) )
		{
			$error++;
		}
	}

	if( $error == 0 )
	{
		return true;
	}
	else
	{
		return false;
	}
}

function is_ticket_check_close_booking_date( $prm = array() )
{
	global $db;

	extract( $prm );

	if( isset( $ret_rid ) )
	{
		$s = 'SELECT * FROM ticket_booking_close AS a WHERE a.rid = %d OR a.rid = %d';
		$q = $db->prepare_query( $s, $dep_rid, $ret_rid ); 
	}
	else
	{
		$s = 'SELECT * FROM ticket_booking_close AS a WHERE a.rid = %d';
		$q = $db->prepare_query( $s, $dep_rid ); 
	}

	
	//-- Compare Date
	if( isset( $ret_date ) )
	{
		$date_input = array( strtotime( $dep_date ), strtotime( $ret_date ) );
	}
	else
	{
		$date_input = array( strtotime( $dep_date ) );
	}
	
	$r = $db->do_query( $q );
	$n = $db->num_rows( $r );

	if( $n > 0 )
	{
		$date_list = array();

		while( $d = $db->fetch_array( $r ) )
		{
			$date_list = json_decode( $d[ 'date_list' ], true );

			foreach( $date_input as $di )
			{
				if( in_array( $di, $date_list ) )
				{
					return false;

					break;
				}
			}
		}
	}
	
	return true;	
}

function is_ticket_check_available_seat( $prm = array(), $type )
{
	global $db;

	extract( $prm );

	if( $type == 'departure' )
	{
		$date = $dep_date;
		$rid  = $dep_rid;
	}
	else
	{
		$date = $ret_date;
		$rid  = $ret_rid;
	}
	
	//-- Jadwal sore hanya berlaku 1 juni - 15 september
	$start_date	= date( 'Y', strtotime( $date ) ) . '-06-01';
	$end_date	= date( 'Y', strtotime( $date ) ) . '-09-15';

	if( ticket_check_date_in_range( $start_date, $end_date, $date ) )
	{
		//-- Ambil jadwal seluruhnya, pagi dan sore
		$q = $db->prepare_query( 'SELECT * FROM ticket_schedule AS a WHERE a.rid = %d AND a.status = %d', $rid, 1 );
	}
	else
	{		
		//-- Ambil jadwal pagi saja
		$q = $db->prepare_query( 'SELECT * FROM ticket_schedule AS a WHERE a.rid = %d AND a.status = %d AND a.sparent = %d', $rid, 1, 0 );
	}
	 
	$r = $db->do_query( $q );
	$n = $db->num_rows( $r );

	if( $n > 0 )
	{
		while( $d = $db->fetch_array( $r ) )
		{
			$ticket = new ticketAvailability( $rid, $d[ 'sid' ], $date, $adult, $child );

			if( $ticket->is_available() )
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		return false;
	}
	else
	{
		return false;
	}
}

function ticket_param( $is_admin =  false )
{
	if( $is_admin )
	{
		$param = $_POST;
	}
	else
	{
		if( isset( $_SESSION[ 'ticket_booking' ] ) )
		{
			$param = $_SESSION[ 'ticket_booking' ];
		}
		else
		{
			$param = $_POST;
		}
	}

	return array_merge( array(
		'dep_date'  => '',
		'dep_sid'   => '',
		'dep_rid'   => '', 
		'ret_date'  => '',
		'ret_sid'   => '',
		'ret_rid'   => '', 
		'from'      => '',
		'to'        => '',
		'adult'     => 0,
		'child'     => 0,
		'infant'    => 0,
		'trip_type' => 0
	), array_filter( $param ) );
}

function ticket_check_available_schedule( $prm = array(), $is_admin = false, $type )
{
	global $db;

	extract( $prm );

	if( $type == 'departure' )
	{
		$sname = 'dep_sid';
		$date  = $dep_date;
		$rid   = $dep_rid;
		$sid   = $dep_sid;
	}
	else
	{
		$sname = 'ret_sid';
		$date  = $ret_date;
		$rid   = $ret_rid;
		$sid   = $ret_sid;
	}
	
	//-- Jadwal sore hanya berlaku 1 juni - 15 september
	$start_date	= date( 'Y', strtotime( $date ) ) . '-06-01';
	$end_date	= date( 'Y', strtotime( $date ) ) . '-09-15';

	if( ticket_check_date_in_range( $start_date, $end_date, $date ) )
	{
		//-- Ambil jadwal seluruhnya, pagi dan sore
		if( $is_admin && $sid != '' )
		{
			$s = 'SELECT
					a.sid,
					a.rid,
					a.sparent,
					a.sname,
					a.stime_departure,
					a.stime_arrive,
					b.rname,
					b.rfrom,
					b.rto,
					b.rprice_2way,
					b.rprice_2way_child,
					b.rprice_2way_infant,
					c.bname
				  FROM ticket_schedule AS a
				  INNER JOIN ticket_route AS b ON a.rid = b.rid
				  INNER JOIN ticket_master_boat AS c ON a.sboat = c.boat_id
				  WHERE a.sid = %d AND a.status = %d';
			$q = $db->prepare_query( $s, $sid, 1 );
		}
		else
		{
			$s = 'SELECT
					a.sid,
					a.rid,
					a.sparent,
					a.sname,
					a.stime_departure,
					a.stime_arrive,
					b.rname,
					b.rfrom,
					b.rto,
					b.rprice_2way,
					b.rprice_2way_child,
					b.rprice_2way_infant,
					c.bname
				  FROM ticket_schedule AS a
				  INNER JOIN ticket_route AS b ON a.rid = b.rid
				  INNER JOIN ticket_master_boat AS c ON a.sboat = c.boat_id
				  WHERE a.rid = %d AND a.status = %d';
			$q = $db->prepare_query( $s, $rid, 1 );
		}
	}
	else
	{		
		//-- Ambil jadwal pagi saja
		if( $is_admin && $sid != '' )
		{
			$s = 'SELECT
					a.sid,
					a.rid,
					a.sparent,
					a.sname,
					a.stime_departure,
					a.stime_arrive,
					b.rname,
					b.rfrom,
					b.rto,
					b.rprice_2way,
					b.rprice_2way_child,
					b.rprice_2way_infant,
					c.bname
				  FROM ticket_schedule AS a
				  INNER JOIN ticket_route AS b ON a.rid = b.rid
				  INNER JOIN ticket_master_boat AS c ON a.sboat = c.boat_id
				  WHERE a.sid = %d AND a.status = %d AND a.sparent = %d';
			$q = $db->prepare_query( $s, $sid, 1, 0 );
		}
		else
		{
			$s = 'SELECT
					a.sid,
					a.rid,
					a.sparent,
					a.sname,
					a.stime_departure,
					a.stime_arrive,
					b.rname,
					b.rfrom,
					b.rto,
					b.rprice_2way,
					b.rprice_2way_child,
					b.rprice_2way_infant,
					c.bname
				  FROM ticket_schedule AS a
				  INNER JOIN ticket_route AS b ON a.rid = b.rid
				  INNER JOIN ticket_master_boat AS c ON a.sboat = c.boat_id
				  WHERE a.rid = %d AND a.status = %d AND a.sparent = %d';
			$q = $db->prepare_query( $s, $rid, 1, 0 );
		}
	}
	 
	$r = $db->do_query( $q );
	$n = $db->num_rows( $r );

	if( $n > 0 )
	{
		$list   = '';
		$dcheck = false;

		while( $d = $db->fetch_assoc( $r ) )
		{
			$ticket = new ticketAvailability( $rid, $d[ 'sid' ], $date, $adult, $child );

			if( $ticket->is_available() )
			{
				$srate = is_get_special_rate( $rid, $d[ 'sid' ], $date );

				if( $srate )
				{
					$dt = ticket_get_data_rows( $srate );

					$total_infant = $dt['srprice_2way_infant'] * $infant;
					$total_child  = $dt['srprice_2way_child'] * $child;
					$total_adult  = $dt['srprice_2way'] * $adult;

					$total = $total_adult + $total_child + $total_infant;

					$prm = array(
						'sid'              => $d[ 'sid' ],
						'rid'              => $d[ 'rid' ],
						'rto'              => $d[ 'rto' ],
						'bname'            => $d[ 'bname' ],
						'rname'            => $d[ 'rname' ],
						'rfrom'            => $d[ 'rfrom' ],
						'sname'            => $d[ 'sname' ],
						'sparent'          => $d[ 'sparent' ],
						'stime_arrive'     => $d[ 'stime_arrive' ],
						'stime_departure'  => $d[ 'stime_departure' ],
						'price_per_infant' => $dt[ 'srprice_2way_infant' ],
						'price_per_child'  => $dt[ 'srprice_2way_child' ],
						'price_per_adult'  => $dt[ 'srprice_2way' ],
						'srid'             => $dt[ 'srid' ],
						'total_infant'     => $total_infant,
						'total_child'      => $total_child,
						'total_adult'      => $total_adult,
						'total'            => $total
					);
				}
				else
				{
					$total_infant = $d['rprice_2way_infant'] * $infant;
					$total_child  = $d['rprice_2way_child'] * $child;
					$total_adult  = $d['rprice_2way'] * $adult;

					$total = $total_adult + $total_child + $total_infant;

					$prm = array(
						'sid'              => $d[ 'sid' ],
						'rid'              => $d[ 'rid' ],
						'rto'              => $d[ 'rto' ],
						'bname'            => $d[ 'bname' ],
						'rname'            => $d[ 'rname' ],
						'rfrom'            => $d[ 'rfrom' ],
						'sname'            => $d[ 'sname' ],
						'sparent'          => $d[ 'sparent' ],
						'stime_arrive'     => $d[ 'stime_arrive' ],
						'stime_departure'  => $d[ 'stime_departure' ],
						'price_per_infant' => $d[ 'rprice_2way_infant' ],
						'price_per_child'  => $d[ 'rprice_2way_child' ],
						'price_per_adult'  => $d[ 'rprice_2way' ],
						'srid'             => 0,
						'total_infant'     => $total_infant,
						'total_child'      => $total_child,
						'total_adult'      => $total_adult,
						'total'            => $total
					);
				}

				if( $dcheck === false && $d[ 'sparent' ] == 0 )
				{
					$cstatus = 'checked';
				}
				else
				{
					$cstatus = '';
				}

				$list .= '
				<tr>
					<td class="text-center">
						<div class="check">
							<input id="arsid-' . $d['sid'] . '" type="radio" class="check__check" name="' . $sname . '" value="' . base64_encode( json_encode( $prm ) ) . '" ' . $cstatus . ' autocomplete="off" required>
							<label for="arsid-' . $d['sid'] . '" class="check__indicator"></label>
						</div>
					</td>
					<td class="text-left">' . $d[ 'sname' ] . '</td>
					<td class="text-center">' . date( 'd/m/Y', strtotime( $date ) ) . '</td>
					<td class="text-center">' . $d[ 'stime_departure' ] . '</td>
				 </tr>';
			}
		}

		return $list;
	}
	else
	{	
		return '';
	}
}

function ticket_check_trip_price( $rid, $sid, $date, $adult = 0, $child = 0, $infant = 0, $is_admin = false )
{
	global $db;
	
	//-- Jadwal sore hanya berlaku 1 juni - 15 september
	$start_date = date( 'Y', strtotime( $date ) ) . '-06-01';
	$end_date	= date( 'Y', strtotime( $date ) ) . '-09-15';

	if( ticket_check_date_in_range( $start_date, $end_date, $date ) )
	{
		//-- Ambil jadwal seluruhnya, pagi dan sore
		if( $is_admin && $sid != '' )
		{
			$q = $db->prepare_query( 'SELECT * FROM ticket_schedule AS a INNER JOIN ticket_route AS b ON a.rid = b.rid WHERE a.sid = %d AND a.status = %d', $sid, 1 );
		}
		else
		{
			$q = $db->prepare_query( 'SELECT * FROM ticket_schedule AS a INNER JOIN ticket_route AS b ON a.rid = b.rid WHERE a.rid = %d AND a.status = %d', $rid, 1 );
		}
	}
	else
	{		
		//-- Ambil jadwal pagi saja
		if( $is_admin && $sid != '' )
		{
			$q = $db->prepare_query( 'SELECT * FROM ticket_schedule AS a INNER JOIN ticket_route AS b ON a.rid = b.rid WHERE a.sid = %d AND a.status = %d AND a.sparent = %d', $sid, 1, 0 );
		}
		else
		{
			$q = $db->prepare_query( 'SELECT * FROM ticket_schedule AS a INNER JOIN ticket_route AS b ON a.rid = b.rid WHERE a.rid = %d AND a.status = %d AND a.sparent = %d', $rid, 1, 0 );
		}
	}
	 
	$r = $db->do_query( $q );
	$n = $db->num_rows( $r );

	if( $n > 0 )
	{
		while( $d = $db->fetch_array( $r ) )
		{
			$ticket = new ticketAvailability( $rid, $d[ 'sid' ], $date, $adult, $child );

			if( $ticket->is_available() )
			{
				$srate = is_get_special_rate( $rid, $d[ 'sid' ], $date );

				if( $srate )
				{
					$dt = ticket_get_data_rows( $srate );

					$total_infant = $dt['srprice_2way_infant'] * $infant;
					$total_child  = $dt['srprice_2way_child'] * $child;
					$total_adult  = $dt['srprice_2way'] * $adult;

					$total = $total_adult + $total_child + $total_infant;

					return array(
						'price_per_infant' => $dt['srprice_2way_infant'],
						'price_per_child'  => $dt['srprice_2way_child'],
						'price_per_adult'  => $dt['srprice_2way'],
						'total_infant'     => $total_infant,
						'total_child'      => $total_child,
						'total_adult'      => $total_adult,
						'total'            => $total
					);
				}
				else
				{
					$total_infant = $d['rprice_2way_infant'] * $infant;
					$total_child  = $d['rprice_2way_child'] * $child;
					$total_adult  = $d['rprice_2way'] * $adult;

					$total = $total_adult + $total_child + $total_infant;

					return array(
						'price_per_infant' => $d['rprice_2way_infant'],
						'price_per_child'  => $d['rprice_2way_child'],
						'price_per_adult'  => $d['rprice_2way'],
						'total_infant'     => $total_infant,
						'total_child'      => $total_child,
						'total_adult'      => $total_adult,
						'total'            => $total
					);
				}
			}
			else
			{
				return array(
					'price_per_infant' => 0,
					'price_per_child'  => 0,
					'price_per_adult'  => 0,
					'total_infant'     => 0,
					'total_child'      => 0,
					'total_adult'      => 0,
					'total'            => 0
				);
			}
		}
	}
	else
	{
		return array(
			'price_per_infant' => 0,
			'price_per_child'  => 0,
			'price_per_adult'  => 0,
			'total_infant'     => 0,
			'total_child'      => 0,
			'total_adult'      => 0,
			'total'            => 0
		);
	}
}

function ticket_check_schedule_list( $rid, $sid, $date, $adult = 0, $child = 0, $infant = 0, $is_admin = false )
{
	global $db;
	
	//-- Jadwal sore hanya berlaku 1 juni - 15 september
	$start_date = date( 'Y', strtotime( $date ) ) . '-06-01';
	$end_date	= date( 'Y', strtotime( $date ) ) . '-09-15';

	if( ticket_check_date_in_range( $start_date, $end_date, $date ) )
	{
		//-- Ambil jadwal seluruhnya, pagi dan sore
		if( $is_admin && $sid != '' )
		{
			$q = $db->prepare_query( 'SELECT * FROM ticket_schedule AS a INNER JOIN ticket_route AS b ON a.rid = b.rid WHERE a.sid = %d AND a.status = %d', $sid, 1 );
		}
		else
		{
			$q = $db->prepare_query( 'SELECT * FROM ticket_schedule AS a INNER JOIN ticket_route AS b ON a.rid = b.rid WHERE a.rid = %d AND a.status = %d', $rid, 1 );
		}
	}
	else
	{		
		//-- Ambil jadwal pagi saja
		if( $is_admin && $sid != '' )
		{
			$q = $db->prepare_query( 'SELECT * FROM ticket_schedule AS a INNER JOIN ticket_route AS b ON a.rid = b.rid WHERE a.sid = %d AND a.status = %d AND a.sparent = %d', $sid, 1, 0 );
		}
		else
		{
			$q = $db->prepare_query( 'SELECT * FROM ticket_schedule AS a INNER JOIN ticket_route AS b ON a.rid = b.rid WHERE a.rid = %d AND a.status = %d AND a.sparent = %d', $rid, 1, 0 );
		}
	}
	 
	$r = $db->do_query( $q );
	$n = $db->num_rows( $r );

	if( $n > 0 )
	{
		$list   = '';
		$dcheck = false;

		while( $d = $db->fetch_array( $r ) )
		{
			$ticket = new ticketAvailability( $rid, $d[ 'sid' ], $date, $adult, $child );

			if( $ticket->is_available() )
			{
				$srate = is_get_special_rate( $rid, $d[ 'sid' ], $date );

				if( $srate )
				{
					$dt = ticket_get_data_rows( $srate );

					$total_infant = $dt['srprice_1way_infant'] * $infant;
					$total_child  = $dt['srprice_1way_child'] * $child;
					$total_adult  = $dt['srprice_1way'] * $adult;

					$total = $total_adult + $total_child + $total_infant;

					$rate_adult  = number_format( $dt[ 'srprice_1way' ], 0, ',', '.' );					
					$rate_child  = number_format( $dt[ 'srprice_1way_child' ], 0, ',', '.' );	
					$rate_infant = number_format( $dt[ 'srprice_1way_infant' ], 0, ',', '.' );	
					$rate_total	 = number_format( $total, 0, ',', '.' );	

					$prm = array(
						'sid'              => $d[ 'sid' ],
						'rid'              => $d[ 'rid' ],
						'rto'              => $d[ 'rto' ],
						'bname'            => $d[ 'rboat' ],
						'rname'            => $d[ 'rname' ],
						'rfrom'            => $d[ 'rfrom' ],
						'sname'            => $d[ 'sname' ],
						'sparent'          => $d[ 'sparent' ],
						'stime_arrive'     => $d[ 'stime_arrive' ],
						'stime_departure'  => $d[ 'stime_departure' ],
						'price_per_infant' => $dt[ 'srprice_1way_infant' ],
						'price_per_child'  => $dt[ 'srprice_1way_child' ],
						'price_per_adult'  => $dt[ 'srprice_1way' ],
						'srid'             => $dt[ 'srid' ],
						'total_infant'     => $total_infant,
						'total_child'      => $total_child,
						'total_adult'      => $total_adult,
						'total'            => $total
					);
				}
				else
				{
					$total_infant = $d['rprice_1way_infant'] * $infant;
					$total_child  = $d['rprice_1way_child'] * $child;
					$total_adult  = $d['rprice_1way'] * $adult;

					$total = $total_adult + $total_child + $total_infant;

					$rate_adult  = number_format( $d[ 'rprice_1way' ], 0, ',', '.' );					
					$rate_child  = number_format( $d[ 'rprice_1way_child' ], 0, ',', '.' );	
					$rate_infant = number_format( $d[ 'rprice_1way_infant' ], 0, ',', '.' );	
					$rate_total	 = number_format( $total, 0, ',', '.' );

					$prm = array(
						'sid'              => $d[ 'sid' ],
						'rid'              => $d[ 'rid' ],
						'rto'              => $d[ 'rto' ],
						'bname'            => $d[ 'rboat' ],
						'rname'            => $d[ 'rname' ],
						'rfrom'            => $d[ 'rfrom' ],
						'sname'            => $d[ 'sname' ],
						'sparent'          => $d[ 'sparent' ],
						'stime_arrive'     => $d[ 'stime_arrive' ],
						'stime_departure'  => $d[ 'stime_departure' ],
						'price_per_infant' => $d[ 'rprice_1way_infant' ],
						'price_per_child'  => $d[ 'rprice_1way_child' ],
						'price_per_adult'  => $d[ 'rprice_1way' ],
						'srid'             => 0,
						'total_infant'     => $total_infant,
						'total_child'      => $total_child,
						'total_adult'      => $total_adult,
						'total'            => $total
					);
				}

				if( $dcheck === false && $d[ 'sparent' ] == 0 )
				{
					$cstatus = 'checked';
				}
				else
				{
					$cstatus = '';
				}

				$list .= '
				<tr>
				    <td class="text-center">
						<div class="check">
							<input id="arsid-' . $d['sid'] . '" type="radio" class="check__check" name="dep_sid" value="' . base64_encode( json_encode( $prm ) ) . '" ' . $cstatus . ' autocomplete="off" required>
							<label for="arsid-' . $d['sid'] . '" class="check__indicator"></label>
						</div>
				    </td>
				    <td class="text-left">' . $d[ 'sname' ] . '</td>
				    <td class="text-center">' . $d[ 'stime_departure' ] . '</td>
				    <td class="text-center">' . $d[ 'stime_arrive' ] . '</td>   
				    <td class="text-center">' . $rate_adult . '</td>
				    <td class="text-center">' . $rate_child . '</td>
				    <td class="text-center">' . $rate_infant . '</td>
				    <td class="text-center">' . $rate_total . '</td>		    
				</tr>';
			}
		}

		return $list;
	}
}

function ticket_front_form_passenger( $prm = array() )
{
	extract( $prm );

	$country = set_option( 'SELECT * FROM lumonata_country AS a WHERE a.lpublish = "1" ORDER BY a.lcountry', 'lcountry', 'lcountry', null, false );
	$result  = '';
	$index   = 0;

	if( $adult > 0 )
	{
		for( $i = 0; $i < $adult; $i++ )
		{
			$result .= '
			<tr>
				<td>
					<select name="pass_type[' . $index . ']">
						<option value="adult">Adult '. ( $i + 1 ) .'</option>
					</select>
				</td>
				<td><input type="text" name="pass_fname[' . $index . ']" placeholder="Full Name" required></td>
				<td>
					<select name="pass_country[' . $index . ']" required>
						<option value="">Select Country</option>
						' . $country . '						
					</select>
				</td>
			</tr>';

			$index++;
		}
	}

	if( $child > 0 )
	{
		for( $i = 0; $i < $child; $i++ )
		{
			$result.= '
			<tr>
				<td>
					<select name="pass_type[' . $index . ']">
						<option value="child">Child '. ( $i + 1 ) .'</option>
					</select>
				</td>
				<td><input type="text" name="pass_fname[' . $index . ']" placeholder="Full Name" required></td>
				<td>
					<select name="pass_country[' . $index . ']" required>
						<option value="">Select Country</option>
						' . $country . '						
					</select>
				</td>
			</tr>';

			$index++;
		}
	}

	if( $infant > 0 )
	{
		for( $i = 0; $i < $infant; $i++ )
		{
			$result.= '
			<tr>
				<td>
					<select name="pass_type[' . $index . ']">
						<option value="infant">Infant '. ( $i + 1 ) .'</option>
					</select>
				</td>
				<td><input type="text" name="pass_fname[' . $index . ']" placeholder="Full Name" required></td>
				<td>
					<select name="pass_country[' . $index . ']" required>
						<option value="">Select Country</option>
						' . $country . '						
					</select>
				</td>
			</tr>';

			$index++;
		}
	}

	return $result;
}

function ticket_check_date_in_range( $start_date, $end_date, $date )
{
	//-- Convert to timestamp
	$start_ts = strtotime( $start_date );
	$end_ts   = strtotime( $end_date );
	$user_ts  = strtotime( $date );

	//-- Check that user date is between start & end
	return ( ( $user_ts >= $start_ts ) && ( $user_ts <= $end_ts ) );
}

function ticket_check_availability()
{
    $prm = ticket_param();

    if( is_ticket_check_valid_parameter( $prm ) )
    {
    	set_template( PLUGINS_PATH . '/freebird/tpl/search_result.html', 'sr-template' );

    	add_block( 'result-block', 'r-block', 'sr-template' );
    	add_block( 'empty-block', 'e-block', 'sr-template' );

    	add_variable( 'site-url', site_url() );
    	add_variable( 'alert', message_block() );
        add_variable( 'summary', ticket_booking_summary( $prm ) );

        $prm[ 'dep_rid' ]  = fetch_route( array( 'rfrom' => $prm[ 'from' ], 'rto' => $prm[ 'to' ] ), 'AND', 'rid' );
        $prm[ 'dep_date' ] = date( 'Y-m-d', strtotime( str_replace( '/', '-', $prm[ 'dep_date' ] ) ) );

        if( $prm[ 'trip_type' ] == '1' )
        {
            $prm[ 'ret_rid' ]  = fetch_route( array( 'rfrom' => $prm[ 'to' ], 'rto' => $prm[ 'from' ] ), 'AND', 'rid' );
            $prm[ 'ret_date' ] = date( 'Y-m-d', strtotime( str_replace( '/', '-', $prm[ 'ret_date' ] ) ) );
		}

        if( is_ticket_check_close_booking_date( $prm ) )
        {
            $dep_status = is_ticket_check_available_seat( $prm, 'departure' );

            if( $prm[ 'trip_type' ] == '1' )
        	{
            	$ret_status = is_ticket_check_available_seat( $prm, 'return' );
            }
            else
            {
            	$ret_status = true;
            }

            if( $dep_status && $ret_status )
            {
            	add_variable( 'step', ticket_booking_step( $prm ) );

		    	parse_template( 'result-block', 'r-block', false );
            }
            else
            {
		    	parse_template( 'empty-block', 'e-block', false );
            }
        }
        else
        {
		    parse_template( 'empty-block', 'e-block', false );
        }

        add_actions( 'include-js', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js' );
        add_actions( 'include-js', 'get_custom_javascript', get_plugin_url( 'freebird/js/ticket.js' ) );
            
        return return_template( 'sr-template' );
    }
}

function ticket_booking_summary( $prm )
{
    set_template( PLUGINS_PATH . '/freebird/tpl/search_result_summary.html', 'sm-template' );

	add_block( 'result-summary-block', 'rs-block', 'sm-template' );

    add_variable( 'to', $prm[ 'to' ] );
    add_variable( 'from', $prm[ 'from' ] );		    
    add_variable( 'adult', $prm[ 'adult' ] );
    add_variable( 'child', $prm[ 'child' ] );
    add_variable( 'infant', $prm[ 'infant' ] );
    add_variable( 'dep_date', $prm[ 'dep_date' ] );
    add_variable( 'trip_type', $prm[ 'trip_type' ] );
    add_variable( 'route', sprintf( '%s - %s', $prm[ 'from' ], $prm[ 'to' ] ) );
    
    if( $prm[ 'trip_type' ] == 1 )
    {
    	add_variable( 'ret_date', $prm[ 'ret_date' ] );
    }
    else
    {
    	add_variable( 'ret_date', '-' );
    }

    parse_template( 'result-summary-block', 'rs-block' );

    return return_template( 'sm-template' );
}

function ticket_booking_step( $prm, $is_admin = false )
{
	global $db;

	extract( $prm );    
   	
   	set_template( PLUGINS_PATH . '/freebird/tpl/search_result_step.html', 'srs-template' );

	add_block( 'result-oneway-block', 'ro-block', 'srs-template' );
	add_block( 'result-return-block', 'rr-block', 'srs-template' );
	add_block( 'result-content-block', 'rc-block', 'srs-template' );

	$d = ticket_check_trip_price( $dep_rid, $dep_sid, $dep_date, $adult, $child, $infant, $is_admin );

	add_variable( 'num-adult', $adult );
    add_variable( 'num-child', $child ); 
    add_variable( 'num-infant',$infant ); 
    
    add_variable( 'total', number_format( $d[ 'total' ], 0, ',', '.' ) );
    add_variable( 'total-adult', number_format( $d[ 'total_adult' ], 0, ',', '.' ) );
    add_variable( 'total-child', number_format( $d[ 'total_child' ], 0, ',', '.' ) );
    add_variable( 'total-infant', number_format( $d[ 'total_infant' ], 0, ',', '.' ) );
    
    add_variable( 'price-per-adult', number_format( $d[ 'price_per_adult' ], 0, ',', '.' ) );
    add_variable( 'price-per-child', number_format( $d[ 'price_per_child' ], 0, ',', '.' ) );
    add_variable( 'price-per-infant', number_format( $d[ 'price_per_infant' ], 0, ',', '.' ) );

	if( $trip_type == 1 )
	{
		add_variable( 'ret-row-data', ticket_check_available_schedule( $prm, $is_admin, 'return' ) );
		add_variable( 'dep-row-data', ticket_check_available_schedule( $prm, $is_admin, 'departure' ) );

		parse_template( 'result-return-block', 'rr-block' );
	}
	else
	{
		add_variable( 'row-data', ticket_check_schedule_list( $dep_rid, $dep_sid, $dep_date, $adult, $child, $infant, $is_admin ) );
		
		add_variable( 'schedule-date', date( 'd/m/Y', strtotime( $dep_date ) ) );

		parse_template( 'result-oneway-block', 'ro-block' );
	}

	add_variable( 'passenger', ticket_front_form_passenger( $prm ) );

	add_variable( 'option-title', set_static_option( array( 'Mr.' => 'Mr.', 'Mrs.' => 'Mrs.', 'Dr.' => 'Dr.' ), null, false ) );
	add_variable( 'option-country', set_option( 'SELECT * FROM lumonata_country AS a WHERE a.lpublish = "1" ORDER BY a.lcountry', 'lcountry', 'lcountry', null, false ) );
	add_variable( 'option-destination', set_option( 'SELECT * FROM ticket_pick_trans_rate AS a GROUP BY a.destination_name ORDER BY a.destination_name', 'destination_name', 'destination_name', null, false ) );

	parse_template( 'result-content-block', 'rc-block' );

	return return_template( 'srs-template' );
}

function ticket_get_pickup_transfer_cost()
{
	global $db;

	$route = ticket_get_pickup_transfer_route();

	if( !empty( $route ) )
	{
		$s = 'SELECT * FROM ticket_pick_trans_rate AS a 
		      LEFT JOIN ticket_pick_trans_routes AS b ON b.ptid = a.ptid 
			  WHERE a.destination_name = %s AND rid IN(' . implode( ',', $route ) . ') GROUP BY a.destination_name';
		$q = $db->prepare_query( $s, $_POST[ 'dest_loc' ] );
		$r = $db->do_query( $q );
		
		if( !is_array( $r ) )
		{
			$d = $db->fetch_array( $r );

			$num_person = $_POST[ 'adult_num' ] + $_POST[ 'child_num' ];

			$num_car = ceil( $num_person / ( $d[ 'max_person' ] + $d[ 'max_extra_person' ] ) );
			$num_ext = $num_person - ( $d[ 'max_person' ] * $num_car );

			$cost = $num_car * $d[ 'rate' ];

			if( $num_ext > 0 )
			{
				$cost += $num_ext * $d[ 'rate_additional' ];
			}
			
			echo json_encode( array( 'result' => 'success', 'cost' => $cost, 'cost_txt' => number_format( $cost, 0, ',', '.' ) ) );
		}
		else
		{
			echo json_encode( array( 'result' => 'failed', 'cost' => 0, 'cost_txt' => 0 ) );
		}
	}
	else
	{
		echo json_encode( array( 'result' => 'failed', 'cost' => 0, 'cost_txt' => 0 ) );
	}

	exit;
}

function ticket_get_pickup_transfer_route()
{
	$route = array();

	if( isset( $_POST['dep_sid'] ) && !empty( $_POST['dep_sid'] ) )
	{
		$dep = json_decode( base64_decode( $_POST['dep_sid'] ), true );

		if( isset( $dep[ 'rid' ] ) && !empty( $dep[ 'rid' ] ) )
		{
			array_push( $route, $dep[ 'rid' ] );
		}
	}

	if( isset( $_POST['ret_sid'] ) && !empty( $_POST['ret_sid'] ) )
	{
		$ret = json_decode( base64_decode( $_POST['ret_sid'] ), true );

		if( isset( $ret[ 'rid' ] ) && !empty( $ret[ 'rid' ] ) )
		{
			array_push( $route, $ret[ 'rid' ] );
		}
	}

	return $route;
}

function ticket_generate_booking_by_id()
{
	global $db;

	//-- Booking By ID : [yyyy][mm][dd][xxxx]
	//-- Example       : [2013][01][20][9999] = 201301209999

	$code = date( 'Y' ) . date( 'm' ) . date( 'd' );

	$s = 'SELECT MAX( substring( a.bbid, 9, 4 ) ) AS cnum FROM ticket_booking_by AS a WHERE substring( a.bbid, 1, 8 ) = %d';
	$q = $db->prepare_query( $s, $code );
	$r = $db->do_query( $q );
	$d = $db->fetch_array( $r );

	if( $d[ 'cnum' ] == null )
	{
		$num = 1;
	}
	else
	{		
		$num = intval( $d[ 'cnum' ] ) + 1;		
	}	
	
	if( $num > 9999 )
	{
		$num = 1;
	}
	elseif( $num < 10 )
	{
		$num = '000' . $num;
	}
	elseif( $num < 100 )
	{
		$num = '00' . $num;
	}
	elseif( $num < 1000 )
	{
		$num = '0' . $num;	
	}
	else
	{
		$num = $num;
	}

	return $code . $num;
}

function ticket_generate_code_number( $type = 0 )
{
	global $db;

	//-- No ticket : [type][year][month][date][no-urut]
	//-- Example   : [0][2013][01][20][9999] = 0201301209999

	$code = $type . date( 'Y' ) . date( 'm' ) . date( 'd' );

	$s = 'SELECT MAX( substring( a.no_ticket, 10 , 4 ) ) AS cnum FROM ticket_booking AS a WHERE substring( a.no_ticket, 1, 9 ) = %d';
	$q = $db->prepare_query( $s, $code );
	$r = $db->do_query( $q );
	$d = $db->fetch_array( $r );

	if( $d[ 'cnum' ] == null )
	{
		$num = 1;
	}
	else
	{		
		$num = intval( $d[ 'cnum' ] ) + 1;		
	}	
	
	if( $num > 9999 )
	{
		$num = 1;
	}
	elseif( $num < 10 )
	{
		$num = '000' . $num;
	}
	elseif( $num < 100 )
	{
		$num = '00' . $num;
	}
	elseif( $num < 1000 )
	{
		$num = '0' . $num;	
	}
	else
	{
		$num = $num;
	}

	return $code . $num;
}

function ticket_submit_booking()
{
    global $db;
    global $flash;  

    extract( $_POST );

    $error = array();

    $dp = json_decode( base64_decode( $dep_sid ), true );

    //-- Check departure trip
	if( $dp === null && json_last_error() !== JSON_ERROR_NONE  )
	{
		array_push( $error, 'Please select schedule for your departure trip' );
	}

    //-- Check departure date
	if( !isset( $dep_date ) || ( isset( $dep_date ) && empty( $dep_date ) ) )
	{
		array_push( $error, 'Departure date can\'t be empty' );
	}
	else
	{
		$dep_date = date( 'Y-m-d', strtotime( str_replace( '/', '-', $dep_date ) ) );
	}

	//-- Check over quota for trip departure
	if( is_ticket_over_quota( $dp[ 'rid' ], $dp[ 'sid' ], $dep_date, $adult, $child, $infant ) )
	{
		array_push( $error, 'Schedule for departure trip is over quota' );
	}

    if( $trip_type == 1 )
    {
    	$rt = json_decode( base64_decode( $ret_sid ), true );

    	//-- Check return trip
    	if( $rt === null && json_last_error() !== JSON_ERROR_NONE  )
    	{
    		array_push( $error, 'Please select schedule for your return trip' );
    	}

   		//-- Check return date
    	if( !isset( $ret_date ) || ( isset( $ret_date ) && empty( $ret_date ) ) )
    	{
    		array_push( $error, 'Return date can\'t be empty' );
    	}
    	else
    	{
    		$ret_date = date( 'Y-m-d', strtotime( str_replace( '/', '-', $ret_date ) ) );
    	}

   		//-- Check over quota for trip return
    	if( is_ticket_over_quota( $rt[ 'rid' ], $rt[ 'sid' ], $ret_date, $adult, $child, $infant ) )
    	{
    		array_push( $error, 'Schedule for return trip is over quota' );
    	}
    }

    //-- Set Booking Session
    //-- If booking from frontend
    if( !isset( $is_admin ) )
    {
    	$_SESSION[ 'ticket_booking' ] = $_POST;
    }

    if( empty( $error ) )
    {
    	//-- Start Transaction
	    $db->begin();

	    $commit = 1;

	    //-- Prepare param
    	if( !isset( $pickup_cost ) )
	    {
	    	$pickup_cost = 0;
	    }

	    if( !isset( $trans_cost ) )
	    {
	        $trans_cost = 0;
	    }

	    if( !isset( $dp[ 'srid' ] ) )
	    {
	    	$dp[ 'srid' ] = 0;
	    }

	    if( isset( $is_admin ) )
	    {
	    	$status = $bstatus;
	    }
	    else
	    {
	    	$status = 'wp';
	    }

	    $total     = $dp[ 'total' ] + $pickup_cost + $trans_cost;
	    $no_ticket = ticket_generate_code_number( $trip_type );
	    $booked_by = ticket_generate_booking_by_id();
	    $num_pass  = $adult + $child + $infant;
	    $timestamp = time();

	    //-- Insert ticket_booking
	    $r = $db->insert( 'ticket_booking', array(
	        'boat'          => $dp[ 'bname' ],
	        'srid'          => $dp[ 'srid' ],
	    	'no_ticket'     => $no_ticket,
	    	'type'          => $trip_type,
	        'created_by'    => $booked_by,
	        'booked_by'     => $booked_by,
	        'created_date'  => $timestamp,
	        'dlu'           => $timestamp,
	        'num_passenger' => $num_pass,
	        'status'        => $status,
	        'total'         => $total
	    ));

	    if( is_array( $r ) )
	    {
	    	array_push( $error, 'Failed to save booking data' );

	    	$commit = 0;
	    }
	    else
	    {
	    	$bid = $db->insert_id();

    		//-- Prepare param
		    if( isset( $pickup_dest ) && !empty( $pickup_dest ) )
		    {
		        $pickup_dest = fetch_pick_trans( array( 'ptid' => $pickup_dest ), 'AND', 'destination_name' );
		    }
		    else
		    {
		    	$pickup_dest = '';
		    }

		    if( !isset( $add_pickup ) )
		    {
		        $add_pickup = '';
		    }

		    if( isset( $trans_dest ) && !empty( $trans_dest ) )
		    {
		        $trans_dest = fetch_pick_trans( array( 'ptid' => $trans_dest ), 'AND', 'destination_name' );
		    }
		    else
		    {
		    	$trans_dest  = '';
		    }

		    if( !isset( $add_transfer ) )
		    {
		        $add_transfer = '';
		    }

		    $summary = $dp[ 'total' ] + $pickup_cost + $trans_cost;

		    //-- Insert ticket_booking_detail -> Departure
		    $r = $db->insert( 'ticket_booking_detail', array(
				'price_per_infant'     => $dp[ 'price_per_infant' ],
				'price_per_adult'      => $dp[ 'price_per_adult' ],
				'price_per_child'      => $dp[ 'price_per_child' ],
				'total_infant'         => $dp[ 'total_infant' ],
				'total_adult'          => $dp[ 'total_adult' ],
				'total_child'          => $dp[ 'total_child' ],
				'total'                => $dp[ 'total' ],
				'stime_departure'      => $dp[ 'stime_departure' ],
				'stime_arrive'         => $dp[ 'stime_arrive' ],
				'sparent'              => $dp[ 'sparent' ],
				'srid'                 => $dp[ 'srid' ],
				'boat'                 => $dp[ 'bname' ],
				'rfrom'                => $dp[ 'rfrom' ],
				'rto'                  => $dp[ 'rto' ],
				'sid'                  => $dp[ 'sid' ],
		        'rid'                  => $dp[ 'rid' ],
		        'bdtype'               => 'departure',
				'pickup_cost'          => $pickup_cost,
				'pickup_destination'   => $pickup_dest,
				'transfer_cost'        => $trans_cost,
				'transfer_destination' => $trans_dest,
				'address_transfer'     => $add_transfer,
				'address_pickup'       => $add_pickup,
				'created_by'           => $booked_by,
				'created_date'         => $timestamp,
				'dlu'                  => $timestamp,
				'date'                 => $dep_date,
				'total_summary'        => $summary,
				'status'               => $status,
				'num_infant'           => $infant,
				'num_adult'            => $adult,
				'num_child'            => $child,
		        'bid'                  => $bid
		    ));

		    if( $trip_type == 1 )
		    {
		    	//-- Insert ticket_booking_detail -> Return
		    	//-- Ignore all price, set to 0
		    	$r2 = $db->insert( 'ticket_booking_detail', array(
					'stime_departure'      => $rt[ 'stime_departure' ],
					'stime_arrive'         => $rt[ 'stime_arrive' ],
					'sparent'              => $rt[ 'sparent' ],
					'srid'                 => $rt[ 'srid' ],
					'boat'                 => $rt[ 'bname' ],
					'rfrom'                => $rt[ 'rfrom' ],
					'rto'                  => $rt[ 'rto' ],
					'sid'                  => $rt[ 'sid' ],
			        'rid'                  => $rt[ 'rid' ],
			        'bdtype'               => 'return',
					'pickup_destination'   => $pickup_dest,
					'transfer_destination' => $trans_dest,
					'address_transfer'     => $add_transfer,
					'address_pickup'       => $add_pickup,
					'created_by'           => $booked_by,
					'created_date'         => $timestamp,
					'dlu'                  => $timestamp,
					'date'                 => $ret_date,
					'status'               => $status,
					'num_infant'           => $infant,
					'num_adult'            => $adult,
					'num_child'            => $child,
			        'bid'                  => $bid
			    ));
		    }

		    if( is_array( $r ) || ( isset( $r2 ) && is_array( $r2 ) ) )
		    {
		    	array_push( $error, 'Failed to save booking detail data' );

		    	$commit = 0;
		    }
		    else
		    {
		    	//-- Insert ticket_booking_passenger
		    	if( isset( $pass_type ) && !empty( $pass_type ) )
		    	{
		    		foreach( $pass_type as $i => $ptype )
		    		{
		    			$r = $db->insert( 'ticket_booking_passenger', array(
							'country'      => $pass_country[ $i ],
							'fname'        => $pass_fname[ $i ],
							'created_date' => $timestamp,
							'dlu'          => $timestamp,
							'created_by'   => $booked_by,
							'type'         => $ptype,
							'bid'          => $bid,
							'status'       => 1
					    ));

					    if( is_array( $r ) )
					    {
					    	array_push( $error, 'Failed to save passenger data' );

					    	$commit = 0;

					    	break;
					    }
		    		}
		    	}

		    	if( $commit == 1 )
		    	{
		    		//-- Insert ticket_booking_by
	    			$r = $db->insert( 'ticket_booking_by', array(
						'bbid'         => $booked_by,
						'created_by'   => $booked_by,
						'created_date' => $timestamp,
						'dlu'          => $timestamp,
						'country'      => $country,
						'address'      => $address,
						'email'        => $email,
						'phone'        => $phone,
						'fname'        => $fname,
						'title'        => $title,
				    ));

				    if( is_array( $r ) )
				    {
					    array_push( $error, 'Failed to save applicant data' );

				    	$commit = 0;
					}	
		    	}
		    }
	    }
                
        if( $commit == 0 )
        {
            $db->rollback();

            if( isset( $is_admin ) )
            {
            	echo json_encode( array( 'result' => 'failed' ) );

            	exit;
            }
            else
            {
		    	$flash->add( array( 'type' => 'error', 'content' => $error ) );

		        header( 'location: ' . site_url( 'check-ticket' ) );

		        exit;
            }
        }
        else
        {
            $db->commit();

        	//-- Get booking data
        	$bdata = fetch_booking( array( 'bid' => $bid ), 'AND', '*', true );

	    	//-- Send email notification to admin
	    	send_booking_notification( $bdata );

            if( isset( $is_admin ) )
            {
			    //-- Send invoice to client
            	if( $status == 'wp' && isset( $send_invoice ) && $send_invoice == 1 )
            	{
			    	send_invoice( $bdata );
            	}

            	echo json_encode( array( 'result' => 'success' ) );

            	exit;
            }
            else
            {
            	//-- Clear booking session
            	if( isset( $_SESSION[ 'ticket_booking' ] ) )
            	{
            		unset( $_SESSION[ 'ticket_booking' ] );
            	}

		    	//-- Send invoice to client
		    	send_invoice( $bdata );

		        header( 'location: ' . site_url( 'pay-ticket/?id=' . $bid ) );

		        exit;
		    }
        }
    }
    else
    {
        if( isset( $is_admin ) )
        {
        	echo json_encode( array( 'result' => 'failed' ) );

        	exit;
        }
        else
        {
	    	$flash->add( array( 'type' => 'error', 'content' => $error ) );

	        header( 'location: ' . site_url( 'check-ticket' ) );

	        exit;
	    }
    }
}

function ticket_doku_url()
{
	$server = get_meta_data( 'gbs_doku_server' );

	if( $server === 'live' )
	{
		return 'https://api.doku.com';
	}
	else
	{
		return 'https://api-sandbox.doku.com';
	}
}

function ticket_doku_js()
{
	$server = get_meta_data( 'gbs_doku_server' );

	if( $server === 'live' )
	{
		return 'https://jokul.doku.com/jokul-checkout-js/v1/jokul-checkout-1.0.0.js';
	}
	else
	{
		return 'https://sandbox.doku.com/jokul-checkout-js/v1/jokul-checkout-1.0.0.js';
	}
}

function ticket_doku_payment_url()
{
	extract( $_POST );

	$param = array(
	    'payment' => array(
	        'payment_due_date' => get_meta_data( 'gbs_doku_expired_time' ),
	    ),
	    'order' => array(
	        'invoice_number' => sprintf( 'INV-%s-%s', rand( 1, 1000 ), $ticket ),
	        'callback_url'   => site_url( 'payment-success/?id=' . $id ),
	        'amount'         => $amount,
	        'currency'       => 'IDR',
	        'line_items'     => array(
		        0 => array(
		            'name'     => $ticket,
		            'price'    => $amount,
		            'quantity' => 1,
		        ),
	        ),
	    ),
	    'customer' => array(
	        'id'      => sprintf( 'CUST-%s-%s', rand( 1, 1000 ), $booked_by ),
	        'address' => $address,
	        'email'   => $email,
	        'phone'   => $phone,
	        'name'    => $name,
	    ),
	);

	$server        = get_meta_data( 'gbs_doku_server' );
	$client_id     = get_meta_data( 'gbs_doku_' . $server . '_client_id' );
	$client_secret = get_meta_data( 'gbs_doku_' . $server . '_client_secret' );

	$request_id = rand( 1, 100000 );
	$timestamp  = substr( date('Y-m-d\TH:i:sO', strtotime( gmdate( 'Y-m-d H:i:s' ) ) ), 0, 19 ) . 'Z';

	//-- API URL
	$path = '/checkout/v1/payment';
	$url  = ticket_doku_url() . $path;

	//-- Generate digest
	$digest = base64_encode( hash( 'sha256', json_encode( $param ), true ) );

	//-- Prepare signature component
	$component = "Client-Id:" . $client_id . "\n" . "Request-Id:". $request_id . "\n" . "Request-Timestamp:". $timestamp . "\n" . "Request-Target:" . $path . "\n" . "Digest:" . $digest;

	//-- Generate signature
	$signature = base64_encode( hash_hmac( 'sha256', $component, $client_secret, true ) );

	//-- Execute request
	$ch = curl_init( $url );

	curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Content-Type:application/json' ) );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $param ) );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

	curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
	    'Content-Type: application/json',
	    'Client-Id:' . $client_id,
	    'Request-Id:' . $request_id,
	    'Request-Timestamp:' . $timestamp,
	    'Signature:' . "HMACSHA256=" . $signature,
	));

	//-- Set response json
	$res  = curl_exec( $ch );
	$code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

	curl_close( $ch );

	if( $code == 200 )
	{
		echo $res;
	}
	else
	{
		echo json_encode( array( 'result' => 'failed', 'message' => 'Failed to generate payment link, please try again' ) );
	}

	exit;
}

function ticket_doku_notify()
{
    global $db;

    //-- Mapping the notification received from Jokul
    $post = file_get_contents( 'php://input' );
    $data = json_decode( $post, true );

    //-- Prepare Signature to verify the notification authenticity
    if( isset( $data[ 'transaction' ][ 'status' ] ) && $data[ 'transaction' ][ 'status' ] == 'SUCCESS' )
    {
        list( $prefix, $random_str, $ticket ) = explode( '-', $data[ 'order' ][ 'invoice_number' ] );

        $bdata = fetch_booking( array( 'no_ticket' => $ticket ), 'AND', '*', true );

        if( isset( $bdata[ 'status' ] ) && in_array( $bdata[ 'status' ], array( 'pp', 'wp' ) ) )
        {
	        $pdate = strtotime( $data[ 'transaction' ][ 'date' ] );
	        $time  = time();

		    $db->begin();

		    $commit = 1;

	        $r = $db->insert( 'ticket_booking_payment', array(
	            'ptotal'       => $data[ 'order' ][ 'amount' ],
	            'pchannel'     => $data[ 'channel' ][ 'id' ],
	            'bid'          => $bdata[ 'bid' ],
	            'pdate'        => $pdate,
	            'presponse'    => $post,
	            'created_date' => $time,
	            'ldlu'         => $time
	        ));

	        if( is_array( $r ) )
	        {
	        	$commit = 0;
	        }
	        else
	        {
	            $r = $db->update( 'ticket_booking', array( 'status' => 'pd' ), array( 'bid' => $bdata[ 'bid' ] ) );

	            if( is_array( $r ) )
	            {
	            	$commit = 0;
	            }
	            else
	            {
	            	$r = $db->update( 'ticket_booking_detail', array( 'status' => 'pd' ), array( 'bid' => $bdata[ 'bid' ] ) );

		            if( is_array( $r ) )
		            {
		            	$commit = 0;
		            }
	            }
	        }
                
		    if( $commit == 0 )
		    {
		        $db->rollback();

		        error_log("\n[Time: " . date( 'F d Y, H:i:s' ) . "] - ". $bdata[ 'no_ticket' ] . " => Updated data FAILED", 3, ROOT_PATH . "/log_notify.log" );

		        http_response_code( 400 );
		    }
		    else
		    {
		        $db->commit();
		        
		        error_log("\n[Time: " . date( 'F d Y, H:i:s' ) . "] - ". $bdata[ 'no_ticket' ] . " => Booking Done By Doku", 3, ROOT_PATH . "/log_notify.log" );
		        
		    	//-- Send email confirmation to admin
		    	send_payment_notification( $bdata );

		    	//-- Send email & pdf ticket to client
		    	send_ticket( $bdata );

		        http_response_code( 200 );
		    }
        }
        else
        {
        	error_log("\n[Time: " . date( 'F d Y, H:i:s' ) . "] - ". $bdata[ 'no_ticket' ] . " => Booking not VALID", 3, ROOT_PATH . "/log_notify.log" );

	       	http_response_code( 400 );
        }
    }
    else
    {
    	error_log("\n[Time: " . date( 'F d Y, H:i:s' ) . "] => Transaction return INVALID from DOKU ", 3, ROOT_PATH . "/log_notify.log" );

        http_response_code( 400 );
    }

    header('Content-type:application/json;charset=utf-8');

    exit;
}

function ticket_paypal_invoice_number( $len = 32 )
{
	$bytes = '';

	if( function_exists( 'random_bytes' ) )
	{
		try
		{
		    $bytes = random_bytes( $len );
		}
		catch( \Exception $e ){
		    //-- Do nothing
		}
	}
	elseif( function_exists( 'openssl_random_pseudo_bytes' ) )
	{
		$bytes = openssl_random_pseudo_bytes( $len );
	}

	if( $bytes === '' )
	{
		//-- We failed to produce a proper random string, so make do.
		//-- Use a hash to force the length to the same as the other methods
		$bytes = hash('sha256', uniqid((string) mt_rand(), true), true);
	}

	//-- We don't care about messing up base64 format here, just want a random string
	return str_replace(['=', '+', '/'], '', base64_encode(hash('sha256', $bytes, true)));
}

function ticket_paypal_payment_url()
{
    $name  = sprintf( 'Ticket : %s', $_POST[ 'ticket' ] );
	$rate  = get_meta_data( 'gbs_paypal_usd_rate' );
    $total = round( $_POST[ 'amount' ] / $rate, 2 );

    //-- SET item
    $item = new Item();
    $item->setName( $name )->setCurrency( 'USD' )->setQuantity( 1 )->setPrice( $total );

    //-- SET payer
    $payer = new Payer();
    $payer->setPaymentMethod( 'paypal' );

    //-- SET item to list
    $itemlist = new ItemList();
    $itemlist->setItems( array( $item ) );

    //-- SET details
    $details = new Details();
    $details->setSubtotal( $total );

    //-- SET amount detail
    $amount = new Amount();
    $amount->setCurrency( 'USD' )->setTotal( $total )->setDetails( $details );

    //-- SET transaction
    $transaction = new Transaction();
    $transaction->setAmount( $amount )->setItemList( $itemlist )->setDescription( 'Payment For Ticket #' . $_POST[ 'ticket' ] )->setInvoiceNumber( ticket_paypal_invoice_number() );

    //-- SET redirect url
    $redirecturls = new RedirectUrls();
    $redirecturls->setReturnUrl( site_url( 'paypal-notify/?prm=' . base64_encode( json_encode( array( 'id' => $_POST[ 'id' ], 'total' => $total ) ) ) ) )->setCancelUrl( site_url( 'payment-failed/?id=' . $_POST[ 'id' ] ) );

    //-- SET payment
    $payment = new Payment();
    $payment->setIntent( 'sale' )->setPayer( $payer )->setRedirectUrls( $redirecturls )->setTransactions( array( $transaction ) );

    $request = clone $payment;

    try
    {
    	$account = ticket_paypal_account();

    	$context = new ApiContext( new OAuthTokenCredential( $account[ 'client_id' ], $account[ 'client_secret' ] ) );
	    $context->setConfig( array(
            'mode'           => $account[ 'mode' ],
            'log.FileName'   => '../PayPal.log',
            'log.LogLevel'   => 'DEBUG',
            'log.LogEnabled' => true,
            'cache.enabled'  => true
		));

        $payment->create( $context );

        echo json_encode( array( 'result' => 'success', 'link' => $payment->getApprovalLink() ) );
    }
    catch( Exception $ex )
    {
        echo json_encode( array( 'result' => 'failed', 'message' => 'Failed to generate payment link, please try again' ) );
    }

    exit;
}

function ticket_paypal_notify()
{
    global $db;
    
	if( isset( $_GET[ 'paymentId' ] ) && isset( $_GET[ 'PayerID' ] ) && isset( $_GET[ 'prm' ] ) )
	{
		$prm = json_decode( base64_decode( $_GET[ 'prm' ] ), true );

		if( $prm !== null && json_last_error() === JSON_ERROR_NONE )
		{
			$data = fetch_booking( array( 'bid' => $prm[ 'id' ] ), 'AND', '*', true );

			if( empty( $data ) === false )
			{
		    	$account = ticket_paypal_account();
		    	
		        $context = new ApiContext( new OAuthTokenCredential( $account[ 'client_id' ], $account[ 'client_secret' ] ) );
        	    $context->setConfig( array(
                    'mode'           => $account[ 'mode' ],
                    'log.FileName'   => '../PayPal.log',
                    'log.LogLevel'   => 'DEBUG',
                    'log.LogEnabled' => true,
                    'cache.enabled'  => true
        		));
        		
		    	$payment = Payment::get( $_GET[ 'paymentId' ], $context );

    			//-- SET execution data
		    	$execution = new PaymentExecution();
		    	$execution->setPayerId( $_GET[ 'PayerID' ] );

    			//-- SET details
		    	$details = new Details();
			    $details->setSubtotal( $prm[ 'total' ] );

    			//-- SET amount detail
			    $amount = new Amount();
			    $amount->setCurrency( 'USD' )->setTotal( $prm[ 'total' ] )->setDetails( $details );

    			//-- SET transaction
			    $transaction = new Transaction();
			    $transaction->setAmount( $amount );

			    $execution->addTransaction( $transaction );

			    try
			    {
			        $result = $payment->execute( $execution, $context );
    
			        try
			        {
			            $payment = Payment::get( $_GET[ 'paymentId' ], $context );
			            $object  = json_decode( $result, true );
			            
			            if( strtolower( $payment->getState() ) === 'approved' && $object !== null )
			            {
			                $pdate = new DateTime( $payment->getUpdateTime() );
							$pdate = $pdate->format( 'Y-m-d H:i:s' );
							$pdate = strtotime( $pdate );  
					        $time  = time();

						    $db->begin();

						    $commit = 1;
						    
						    $r = $db->insert( 'ticket_booking_payment', array(
					            'presponse'    => json_encode( $object ),
					            'ptotal'       => $data[ 'total' ],
					            'bid'          => $data[ 'bid' ],
					            'pchannel'     => 'PAYPAL',
					            'pdate'        => $pdate,
					            'created_date' => $time,
					            'ldlu'         => $time
					        ));

					        if( is_array( $r ) )
					        {
					        	$commit = 0;
					        }
					        else
					        {
					            $r = $db->update( 'ticket_booking', array( 'status' => 'pd' ), array( 'bid' => $data[ 'bid' ] ) );

					            if( is_array( $r ) )
					            {
					            	$commit = 0;
					            }
					            else
					            {
					            	$r = $db->update( 'ticket_booking_detail', array( 'status' => 'pd' ), array( 'bid' => $data[ 'bid' ] ) );

						            if( is_array( $r ) )
						            {
						            	$commit = 0;
						            }
					            }
					        }
				                
						    if( $commit == 0 )
						    {
						        $db->rollback();

						    	file_put_contents( ROOT_PATH . '/lumonata-log/log-paypal-' . date( 'Y-m-d' ) . '.txt', 'Failed to update payment status tiket #' . $data[ 'no_ticket' ], FILE_APPEND );

					        	header( 'location: ' . site_url( 'payment-failed/?id=' . $data[ 'bid' ] ) );
					    
					    		exit;
						    }
						    else
						    {
						        $db->commit();
						        
						    	//-- Send email confirmation to admin
						    	send_payment_notification( $data );

						    	//-- Send email & pdf ticket to client
						    	send_ticket( $data );

						    	file_put_contents( ROOT_PATH . '/lumonata-log/log-paypal-' . date( 'Y-m-d' ) . '.txt', 'Payment for tiket #' . $data[ 'no_ticket' ] . ' is done by PayPal', FILE_APPEND );

					        	header( 'location: ' . site_url( 'payment-success/?id=' . $data[ 'bid' ] ) );
					    
					    		exit;
						    }
			            }
			        }
			        catch( Exception $ex )
			        {
				    	file_put_contents( ROOT_PATH . '/lumonata-log/log-paypal-' . date( 'Y-m-d' ) . '.txt', 'Failed to see payment detail for tiket #' . $data[ 'no_ticket' ], FILE_APPEND );

			        	header( 'location: ' . site_url( 'payment-failed' ) );
			    
			    		exit;
			        }
			    }
			    catch( Exception $ex )
			    {
			    	file_put_contents( ROOT_PATH . '/lumonata-log/log-paypal-' . date( 'Y-m-d' ) . '.txt', 'Failed to execute payment tiket #' . $data[ 'no_ticket' ], FILE_APPEND );

		        	header( 'location: ' . site_url( 'payment-failed' ) );
		    
		    		exit;
			    }
			}
			else
			{
				file_put_contents( ROOT_PATH . '/lumonata-log/log-paypal-' . date( 'Y-m-d' ) . '.txt', 'System failed to capture booking data', FILE_APPEND );

			    header( 'location: ' . site_url( 'payment-failed' ) );
			    
			    exit;
			}
		}
		else
		{
			file_put_contents( ROOT_PATH . '/lumonata-log/log-paypal-' . date( 'Y-m-d' ) . '.txt', 'System failed to capture return parameter', FILE_APPEND );

		    header( 'location: ' . site_url( 'payment-failed' ) );
		    
		    exit;
		}
	}
	else
	{
		file_put_contents( ROOT_PATH . '/lumonata-log/log-paypal-' . date( 'Y-m-d' ) . '.txt', 'User Cancelled the Approval', FILE_APPEND );

	    header( 'location: ' . site_url( 'payment-failed' ) );
	    
	    exit;
	}
}

function ticket_download_printout()
{
	$prm = json_decode( base64_decode( get_uri_sef() ), true );

	if( isset( $prm[ 'bid' ] ) )
	{
		$data = fetch_booking( array( 'bid' => $prm[ 'bid' ] ), 'AND', '*', true );

		if( empty( $data ) === false )
		{
			generate_ticket( $data, true );
		}
		else
		{
			http_response_code( 404 );
		}
	}
	else
	{
		http_response_code( 404 );
	}

	exit;
}

?>