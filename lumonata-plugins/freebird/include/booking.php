<?php

add_actions( 'booking', 'init_booking' );
add_actions( 'booking_admin_page', 'booking_ajax' );

add_privileges( 'administrator', 'booking', 'insert' );
add_privileges( 'administrator', 'booking', 'update' );
add_privileges( 'administrator', 'booking', 'delete' );
add_privileges( 'administrator', 'booking', 'view' );

/*
| -----------------------------------------------------------------------------
| Admin Booking
| -----------------------------------------------------------------------------
*/
function init_booking()
{    
    if( is_delete_all() )
    {
        return delete_batch_booking();
    }

    return get_booking_list();
}

function get_booking_list()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/booking_list.html', 'booking' );

    add_block( 'list-block', 'l-block', 'booking' );

    extract( ticket_booking_filter() );

    add_variable( 'limit', post_viewed() );
    add_variable( 'alert', message_block() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_booking_ajax_url() );
    add_variable( 'title', 'Booking List' );

    add_variable( 'end', $end );
    add_variable( 'start', $start );

    add_variable( 'route-option', ticket_route_option( $rid ) );
    add_variable( 'type-option', ticket_departure_type_option() );
    add_variable( 'status-option', ticket_booking_status_option( $status ) );

    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );

    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' );

    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/css/select2.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/js/select2.min.js' );

    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/clipboard@2.0.11/dist/clipboard.min.js' );
    
    add_actions( 'section_title', 'Booking - List' );

    parse_template( 'list-block', 'l-block', 'booking' );

    return return_template( 'booking' );
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - View Booking Detail
| -----------------------------------------------------------------------------
*/
function get_booking_detail()
{
    $d = fetch_booking( array( 'bid' => $_POST[ 'bid' ] ), 'AND', '*', true );

    set_template( PLUGINS_PATH . '/freebird/tpl/booking_detail.html', 'booking' );

    add_block( 'detail-passenger-block', 'dp-block', 'booking' );
    add_block( 'detail-loop-block', 'dl-block', 'booking' );
    add_block( 'detail-block', 'd-block', 'booking' );

    add_variable( 'bid', $d[ 'bid' ] );
    add_variable( 'no_ticket', $d[ 'no_ticket' ] );
    add_variable( 'created_date', date( 'd F Y', $d[ 'created_date' ] ) );

    add_variable( 'status', $d[ 'status' ] );
    add_variable( 'status_label', ticket_booking_status( $d[ 'status' ] ) );
    add_variable( 'status_option', ticket_booking_status_option( $d[ 'status' ], 1 ) );
    add_variable( 'download_ticket_link', site_url( 'download-ticket/' . base64_encode( json_encode( array( 'bid' => $d[ 'bid' ] ) ) ) ) );

    if( isset( $d[ 'booking_by' ] ) )
    {
        add_variable( 'bbid', $d[ 'booking_by' ][ 'bbid' ] );
        add_variable( 'phone', $d[ 'booking_by' ][ 'phone' ] );
        add_variable( 'email', $d[ 'booking_by' ][ 'email' ] );
        add_variable( 'byname', $d[ 'booking_by' ][ 'fname' ] );
        add_variable( 'address', $d[ 'booking_by' ][ 'address' ] );
        add_variable( 'country', set_option( 'SELECT * FROM lumonata_country AS a WHERE a.lpublish = "1" ORDER BY a.lcountry', 'lcountry', 'lcountry', $d[ 'booking_by' ][ 'country' ], false ) );

        add_variable( 'title_option', set_static_option( array( 'Mr.'  => 'Mr.', 'Mrs.' => 'Mrs.', 'Dr.'  => 'Dr.' ), $d[ 'booking_by' ][ 'title' ], false ) );
    }

    if( isset( $d[ 'booking_detail' ] ) && !empty( $d[ 'booking_detail' ] ) )
    {
        foreach( $d[ 'booking_detail' ] as $dt )
        {
            add_variable( 'rto', $dt[ 'rto' ] );
            add_variable( 'rfrom', $dt[ 'rfrom' ] );

            add_variable( 'bdid', $dt[ 'bdid' ] );
            add_variable( 'total', $dt[ 'total' ] );

            add_variable( 'price_per_adult', $dt[ 'price_per_adult' ] );
            add_variable( 'price_per_child', $dt[ 'price_per_child' ] );
            add_variable( 'price_per_infant', $dt[ 'price_per_infant' ] );

            add_variable( 'date', date( 'd F Y', strtotime( $dt[ 'date' ] ) ) );
            add_variable( 'stime_arrive', date( 'H:i', strtotime( $dt[ 'stime_arrive' ] ) ) );
            add_variable( 'stime_departure', date( 'H:i', strtotime( $dt[ 'stime_departure' ] ) ) );

            parse_template( 'detail-loop-block', 'dl-block', true );
        }

        add_variable( 'num_adult', $d[ 'booking_detail' ][0][ 'num_adult' ] );
        add_variable( 'num_child', $d[ 'booking_detail' ][0][ 'num_child' ] );
        add_variable( 'num_infant', $d[ 'booking_detail' ][0][ 'num_infant' ] );

        add_variable( 'pickup_cost', $d[ 'booking_detail' ][0][ 'pickup_cost' ] );
        add_variable( 'transfer_cost', $d[ 'booking_detail' ][0][ 'transfer_cost' ] );
        add_variable( 'total_summary', $d[ 'booking_detail' ][0][ 'total_summary' ] );

        add_variable( 'address_pickup', $d[ 'booking_detail' ][0][ 'address_pickup' ] );
        add_variable( 'address_transfer', $d[ 'booking_detail' ][0][ 'address_transfer' ] );

        add_variable( 'pickup_destination', $d[ 'booking_detail' ][0][ 'pickup_destination' ] );
        add_variable( 'transfer_destination', $d[ 'booking_detail' ][0][ 'transfer_destination' ] );
    }

    if( isset( $d[ 'booking_passenger' ] ) && !empty( $d[ 'booking_passenger' ] ) )
    {
        foreach( $d[ 'booking_passenger' ] as $dp )
        {
            add_variable( 'passname', $dp[ 'fname' ] );
            add_variable( 'type_option', set_static_option( array( 'adult'  => 'Adult', 'child'  => 'Child', 'infant' => 'Infant' ), $dp[ 'type' ], false ) );
            add_variable( 'country_option', set_option( 'SELECT * FROM lumonata_country AS a WHERE a.lpublish = "1" ORDER BY a.lcountry', 'lcountry', 'lcountry', $dp[ 'country' ], false ) );

            parse_template( 'detail-passenger-block', 'dp-block', true );
        }
    }

    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_booking_ajax_url() );

    add_variable( 'ftitle', 'Booking Detail' );

    add_actions( 'section_title', 'Booking - Detail' );

    parse_template( 'detail-block', 'd-block', 'booking' );

    return return_template( 'booking' );
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - View Booking Availability
| -----------------------------------------------------------------------------
*/
function get_booking_availability_form()
{
    global $db;

    set_template( PLUGINS_PATH . '/freebird/tpl/booking_availability.html', 'booking-availability' );

    add_block( 'booking-availability-block', 'ba-block', 'booking-availability' );

    extract( $_POST );

    $a = explode( '-', $date );

    $s = 'SELECT
            a.sid,
            a.rid,
            a.sname,
            b.rname,
            a.stime_departure
         FROM ticket_schedule AS a
         LEFT JOIN ticket_route AS b ON a.rid = b.rid
         WHERE a.sid = %d';
    $q = $db->prepare_query( $s, $sid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    add_variable( 'rid', $d[ 'rid' ] );
    add_variable( 'sid', $d[ 'sid' ] );
    add_variable( 'rname', $d[ 'rname' ] );
    add_variable( 'sname', $d[ 'sname' ] );

    add_variable( 'time', $d[ 'stime_departure' ] );
    add_variable( 'min_date', 'new Date(' . $a[ 0 ] . ', ' . $a[ 2 ] . ', ' . $a[ 1 ] . ')' );

    add_variable( 'date', $date );
    add_variable( 'date_str', date( 'd M Y', strtotime( $date ) ) );
    
    parse_template( 'booking-availability-block', 'ba-block', false );
    
    return return_template( 'booking-availability' );
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Availability Result
| -----------------------------------------------------------------------------
*/
function get_booking_availability_result()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/booking_result.html', 'sr-template' );

    add_block( 'result-block', 'r-block', 'sr-template' );
    add_block( 'empty-block', 'e-block', 'sr-template' );

    $prm = ticket_param( true );
    $dta = fetch_route( array( 'rid' => $prm[ 'rid' ] ) );
    $prm = array_merge( $prm, array( 
        'dep_date' => date( 'Y-m-d', strtotime( str_replace( '/', '-', $prm[ 'dep_date' ] ) ) ),
        'from'     => $dta[ 'rfrom' ], 
        'to'       => $dta[ 'rto' ],
        'dep_rid'  => $dta[ 'rid' ],
        'dep_sid'  => $prm[ 'sid' ]
    ));

    if( $prm[ 'trip_type' ] == '1' )
    {
        $dtr = fetch_route( array( 'rfrom' => $dta[ 'rto' ], 'rto' => $dta[ 'rfrom' ] ) );

        $prm = array_merge( $prm, array( 
            'ret_date' => date( 'Y-m-d', strtotime( str_replace( '/', '-', $prm[ 'ret_date' ] ) ) ),
            'ret_rid'  => $dtr[ 'rid' ]
        ));
    }

    add_variable( 'site-url', site_url() );
    add_variable( 'alert', message_block() );
    add_variable( 'summary', ticket_booking_summary( $prm ) );
    
    if( $prm[ 'trip_type' ] == 1 )
    {
        if( is_ticket_check_available_seat( $prm, 'return' ) )
        {
            $valid = true;
        }
        else
        {
            $valid = false;
        }
    }
    else
    {
        $valid = true;
    }

    if( $valid )
    {
        add_variable( 'step', ticket_booking_step( $prm ) );

        parse_template( 'result-block', 'r-block', false );
    }
    else
    {
        parse_template( 'empty-block', 'e-block', false );
    }

    return return_template( 'sr-template' );
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Batch Delete Booking
| -----------------------------------------------------------------------------
*/
function delete_batch_booking()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/booking_batch_delete.html', 'booking-batch-delete' );

    add_block( 'delete-loop-block', 'dl-block', 'booking-batch-delete' );
    add_block( 'delete-block', 'd-block', 'booking-batch-delete' );

    foreach( $_POST[ 'select' ] as $key => $val )
    {
        $d = fetch_booking( array( 'bid' => $val ) );

        add_variable( 'no_ticket', $d[ 'no_ticket' ] );
        add_variable( 'bid', $d[ 'bid' ] );

        parse_template( 'delete-loop-block', 'dl-block', true );
    }

    add_variable( 'title', 'Batch Delete Booking' );
    add_variable( 'backlink', get_state_url( 'ticket&sub=booking' ) );
    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST[ 'select' ] ) == 1 ? 'this booking?' : 'these bookings?' ) );
    
    add_actions( 'section_title', 'Booking - Batch Delete' );

    parse_template( 'delete-block', 'd-block', 'booking-batch-delete' );

    return return_template( 'booking-batch-delete' );
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Table Query
| -----------------------------------------------------------------------------
*/
function get_booking_list_query()
{
    global $db;
    
    extract( $_POST );
    
    $post = $_REQUEST;
    $cols  = array(
        1 => 'c.fname',
        2 => 'a.no_ticket',
        3 => 'b.rfrom',
        4 => 'b.rto',
        5 => 'b.date',
        7 => 'a.type',
        9 => 'a.total'
    );
    
    //-- Set Order Column
    if ( isset( $post[ 'order' ] ) && !empty( $post[ 'order' ] ) )
    {
        $o = array();
        
        foreach ( $post[ 'order' ] as $i => $od )
        {
            if ( isset( $cols[ $post[ 'order' ][ $i ][ 'column' ] ] ) )
            {
                $o[] = $cols[ $post[ 'order' ][ $i ][ 'column' ] ] . ' ' . $post[ 'order' ][ $i ][ 'dir' ];
            }
        }
        
        $order = !empty( $o ) ? ' ORDER BY ' . implode( ', ', $o ) : '';
    }
    else
    {
        $order = ' ORDER BY a.bid DESC';
    }

    //-- Set Condition
    $w = array();

    if( isset( $post[ 'type' ] ) && $post[ 'type' ] != '' )
    {
        $w[] = $db->prepare_query( 'a.type = %s', $post[ 'type' ] );
    }

    if( isset( $post[ 'rid' ] ) && $post[ 'rid' ] != '' )
    {
        $w[] = $db->prepare_query( 'b.rid = %s', $post[ 'rid' ] );
    }

    if( isset( $post[ 'status' ] ) && $post[ 'status' ] != '' )
    {
        $w[] = $db->prepare_query( 'a.status = %s', $post[ 'status' ] );
    }

    if( isset( $post[ 'dstart' ] ) && $post[ 'dstart' ] != '' && isset( $post[ 'dend' ] ) && $post[ 'dend' ] != '' )
    {
        $w[] = $db->prepare_query( 'b.date BETWEEN %s AND %s', date( 'Y-m-d', strtotime( $post[ 'dstart' ] ) ), date( 'Y-m-d', strtotime( $post[ 'dend' ] ) ) );
    }

    if( empty( $post[ 'search' ][ 'value' ] ) === false )
    {        
        $s = array();

        foreach ( $cols as $col )
        {
            $s[] = $db->prepare_query( $col . ' LIKE %s', '%' . $post[ 'search' ][ 'value' ] . '%' );
        }

        $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
    }

    if( empty( $w ) === false )
    {
        $where = ' AND ' . implode( ' AND ', $w );
    }
    else
    {
        $where = '';
    }

    $q = 'SELECT
            a.bid,
            b.rto,
            a.type,
            b.date,
            a.total,
            b.rfrom,
            c.title,
            c.fname,
            a.no_ticket,
            b.num_adult,
            b.num_child,
            b.num_infant,
            b.stime_departure,
            CASE
                WHEN a.status = "pp" THEN "Unpaid"
                WHEN a.status = "pd" THEN "Confirmed"
                WHEN a.status = "cl" THEN "Canceled"
                WHEN a.status = "ca" THEN "Check Availability"
                WHEN a.status = "wp" THEN "Waiting Payment"
                ELSE "-"
            END AS status
         FROM ticket_booking AS a
         LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
         LEFT JOIN ticket_booking_by AS c ON a.booked_by = c.bbid
         WHERE a.status <> "ar"' . $where . ' GROUP BY b.bid ' . $order;
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    $q2 = $q . ' LIMIT ' . $post[ 'start' ] . ', ' . $post[ 'length' ];
    $r2 = $db->do_query( $q2 );
    $n2 = $db->num_rows( $r2 );
    
    $data = array();
    
    if ( $n2 > 0 )
    {
        $url = get_state_url( 'ticket&sub=booking' );
        
        while ( $d2 = $db->fetch_array( $r2 ) )
        {
       		$pass = sprintf( '<b>Adult :</b> %s<br/><b>Child :</b> %s<br/><b>Infant :</b> %s<br/>', $d2[ 'num_adult' ], $d2[ 'num_child' ], $d2[ 'num_infant' ] );
            $name = sprintf( '%s %s', $d2[ 'title' ], $d2[ 'fname' ] );

            $data[] = array(
                'num_infant' => $d2[ 'num_infant' ],
                'num_adult'  => $d2[ 'num_adult' ],
                'num_child'  => $d2[ 'num_child' ],
                'no_ticket'  => $d2[ 'no_ticket' ],
                'status'     => $d2[ 'status' ],
                'total'      => $d2[ 'total' ],
                'rfrom'      => $d2[ 'rfrom' ],
                'type'       => $d2[ 'type' ],
                'rto'        => $d2[ 'rto' ],
                'bid'        => $d2[ 'bid' ],
                'name'       => $name,
                'pass'       => $pass,
                'ajax_link'  => get_booking_ajax_url(),
                'date'       => date( 'd F Y', strtotime( $d2[ 'date' ] ) ),
                'time'       => date( 'H:i', strtotime( $d2[ 'stime_departure' ] ) ),
                'view_link'  => get_state_url( 'ticket&sub=booking&prc=view&id=' . $d2[ 'bid' ] )
            );
        }
    }
    else
    {
        $n = 0;
    }
    
    return array(
        'draw' => intval( $post[ 'draw' ] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data 
    );
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Latest Booking Query
| -----------------------------------------------------------------------------
*/
function get_lastest_booking_list_query()
{
    global $db;
    
    extract( $_POST );
    
    $post = $_REQUEST;
    $cols  = array(
        1 => 'c.fname',
        2 => 'a.no_ticket',
        3 => 'b.rfrom',
        4 => 'b.rto',
        5 => 'b.date',
        7 => 'a.type',
        9 => 'a.total'
    );

    //-- Set Condition
    $w = array();

    if( empty( $post[ 'search' ][ 'value' ] ) === false )
    {        
        $s = array();

        foreach ( $cols as $col )
        {
            $s[] = $db->prepare_query( $col . ' LIKE %s', '%' . $post[ 'search' ][ 'value' ] . '%' );
        }

        $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
    }

    if( empty( $w ) === false )
    {
        $where = ' AND ' . implode( ' AND ', $w );
    }
    else
    {
        $where = '';
    }

    $q = 'SELECT
            a.bid,
            b.rto,
            a.type,
            b.date,
            a.total,
            b.rfrom,
            c.title,
            c.fname,
            a.no_ticket,
            b.num_adult,
            b.num_child,
            b.num_infant,
            a.created_date,
            b.stime_departure,
            CASE
                WHEN a.status = "pp" THEN "Unpaid"
                WHEN a.status = "pd" THEN "Confirmed"
                WHEN a.status = "cl" THEN "Canceled"
                WHEN a.status = "ca" THEN "Check Availability"
                WHEN a.status = "wp" THEN "Waiting Payment"
                ELSE "-"
            END AS status
         FROM ticket_booking AS a
         LEFT JOIN ticket_booking_detail AS b ON b.bid = a.bid
         LEFT JOIN ticket_booking_by AS c ON a.booked_by = c.bbid
         WHERE a.status <> "ar"' . $where . ' GROUP BY b.bid ORDER BY a.created_date DESC LIMIT 50';
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );
    
    $data = array();
    
    if( $n > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $data[] = array(
                'name'       => sprintf( '%s %s', $d[ 'title' ], $d[ 'fname' ] ),
                'time'       => date( 'H:i', strtotime( $d[ 'stime_departure' ] ) ),
                'date'       => date( 'd F Y', strtotime( $d[ 'date' ] ) ),
                'bdate'      => date( 'd F Y', $d[ 'created_date' ] ),
                'ajax_link'  => get_booking_ajax_url(),
                'num_infant' => $d[ 'num_infant' ],
                'num_adult'  => $d[ 'num_adult' ],
                'num_child'  => $d[ 'num_child' ],
                'no_ticket'  => $d[ 'no_ticket' ],
                'status'     => $d[ 'status' ],
                'total'      => $d[ 'total' ],
                'rfrom'      => $d[ 'rfrom' ],
                'type'       => $d[ 'type' ],
                'rto'        => $d[ 'rto' ],
                'bid'        => $d[ 'bid' ]
            );
        }
    }
    else
    {
        $n = 0;
    }
    
    return array(
        'draw' => intval( $post[ 'draw' ] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data 
    );
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_booking_ajax_url()
{
    return get_state_url( 'ajax&apps=booking' );
}

/*
| -----------------------------------------------------------------------------
| Admin Booking Actions
| -----------------------------------------------------------------------------
*/
function run_booking_actions()
{
    global $db;
    global $flash;

    //-- Publish, Unpublish
    //-- Actions From List Booking
    if( is_confirm_delete() )
    {
        $count  = count( $_POST[ 'id' ] );
        $error = 0;

        foreach( $_POST[ 'id' ] as $key => $val )
        {
            if( delete_booking( $val ) === false )
            {
                $error++;
            }
        }

        if( $error > 0 )
        {
            if( $error == $count )
            {
                $flash->add( array( 'type' => 'error', 'content' => 'Failed to delete all selected booking' ) );
            }
            else
            {
                $flash->add( array( 'type' => 'error', 'content' => 'Failed to update some of selected booking' ) );
            }

            header( 'location: ' . get_state_url( 'ticket&sub=booking' ) );

            exit;
        }
        else
        {
            $flash->add( array( 'type' => 'success', 'content' => 'Successfully delete all selected booking' ) );
            
            header( 'location: ' . get_state_url( 'ticket&sub=booking' ) );

            exit;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Get Booking
| -----------------------------------------------------------------------------
*/
function fetch_booking( $conditions = array(), $operator = 'AND',  $fields = '*', $is_detail = false )
{
    global $db;

    //-- PREPARE parameter
    $w = array();

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( is_array( $fields ) && empty( $fields ) === false )
    {
        $param = implode( ', ', $fields );
    }
    else
    {
        $param = $fields;
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT ' . $param . ' FROM ticket_booking' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );

        if( $n > 1 )
        {
            while( $d = $db->fetch_assoc( $r ) )
            {
                if( $is_detail === true )
                {
                    $r2 = $db->do_query( 'SELECT * FROM ticket_booking_by AS a WHERE a.booked_by = "' . $d[ 'booked_by' ] . '"' );

                    if( $db->num_rows( $r2 ) )
                    {
                        while( $d2 = $db->fetch_assoc( $r2 ) )
                        {
                            $d[ 'booking_by' ] = $d2;
                        }
                    }

                    $r3 = $db->do_query( 'SELECT * FROM ticket_booking_detail AS a WHERE a.bid = "' . $d[ 'bid' ] . '"' );

                    if( $db->num_rows( $r3 ) )
                    {
                        while( $d3 = $db->fetch_assoc( $r3 ) )
                        {
                            $d[ 'booking_detail' ][] = $d3;
                        }
                    }

                    $r4 = $db->do_query( 'SELECT * FROM ticket_booking_passenger AS a WHERE a.bid = "' . $d[ 'bid' ] . '"' );

                    if( $db->num_rows( $r4 ) )
                    {
                        while( $d4 = $db->fetch_assoc( $r4 ) )
                        {
                            $d[ 'booking_passenger' ][] = $d4;
                        }
                    }

                    $r5 = $db->do_query( 'SELECT * FROM ticket_booking_payment AS a WHERE a.bid = "' . $d[ 'bid' ] . '"' );

                    if( $db->num_rows( $r5 ) )
                    {
                        while( $d5 = $db->fetch_assoc( $r5 ) )
                        {
                            $d[ 'booking_payment' ][] = $d5;
                        }
                    }
                }

                $data[] = $d;
            }
        }
        elseif( $n > 0 )
        {
            $data = $db->fetch_assoc( $r );

            if( $is_detail === true )
            {
                $r2 = $db->do_query( 'SELECT * FROM ticket_booking_by AS a WHERE a.bbid = "' . $data[ 'booked_by' ] . '"' );

                if( $db->num_rows( $r2 ) )
                {
                    while( $d2 = $db->fetch_assoc( $r2 ) )
                    {
                        $data[ 'booking_by' ] = $d2;
                    }
                }

                $r3 = $db->do_query( 'SELECT * FROM ticket_booking_detail AS a WHERE a.bid = "' . $data[ 'bid' ] . '"' );

                if( $db->num_rows( $r3 ) )
                {
                    while( $d3 = $db->fetch_assoc( $r3 ) )
                    {
                        $data[ 'booking_detail' ][] = $d3;
                    }
                }

                $r4 = $db->do_query( 'SELECT * FROM ticket_booking_passenger AS a WHERE a.bid = "' . $data[ 'bid' ] . '"' );

                if( $db->num_rows( $r4 ) )
                {
                    while( $d4 = $db->fetch_assoc( $r4 ) )
                    {
                        $data[ 'booking_passenger' ][] = $d4;
                    }
                }
            }

            if( $fields != '*' && is_string( $fields ) && isset( $data[ $fields ] ) )
            {
                $data = $data[ $fields ];
            }
        }
        else
        {
            $data = array();
        }

        return $data;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Edit Booking
| -----------------------------------------------------------------------------
*/
function edit_booking_detail( $id )
{
    global $db;

    extract( $_POST );

    $db->begin();

    $commit = 1;

    $r = $db->update( 'ticket_booking', array( 'status' => $status, 'dlu' => time() ), array( 'bid' => $id ) );

    if( is_array( $r ) )
    {
        $commit = 0;
    }
    else
    {
        $r = $db->update( 'ticket_booking_detail', array( 'status' => $status, 'dlu' => time() ), array( 'bid' => $id ) );

        if( is_array( $r ) )
        {
            $commit = 0;
        }
        else
        {
            $r = $db->update( 'ticket_booking_by', $booking_by, array( 'bbid' => $bbid ) );

            if( is_array( $r ) )
            {
                $commit = 0;
            }
            else
            {
                if( isset( $booking_passenger ) && !empty( $booking_passenger ) )
                {
                    foreach( $booking_passenger as $dp )
                    {
                        $r = $db->update( 'ticket_booking_passenger', $dp, array( 'bid' => $id ) );

                        if( is_array( $r ) )
                        {
                            $commit = 0;

                            break;
                        }
                    }
                }

                if( isset( $booking_detail ) && !empty( $booking_detail ) )
                {
                    foreach( $booking_detail as $bdid => $dt )
                    {
                        $r = $db->update( 'ticket_booking_detail', $dt, array( 'bdid' => $bdid ) );
                    }
                }
            }
        }
    }
                
    if( $commit == 0 )
    {
        $db->rollback();

        return false;
    }
    else
    {
        $db->commit();

        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Delete Booking
| -----------------------------------------------------------------------------
*/
function delete_booking( $id )
{
    global $db;

    $r = $db->update( 'ticket_booking', array( 'status' => 'ar', 'dlu' => time() ), array( 'bid' => $id ) );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Check Number of Booking
| -----------------------------------------------------------------------------
*/
function is_num_booking( $conditions = array(), $operator = 'AND' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();
    $n = 0;

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT * FROM ticket_booking' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );
    }

    return $n;
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Booking Status
| -----------------------------------------------------------------------------
*/
function ticket_booking_status( $status = '' )
{
    $list = array(
        'ca' => 'Check Availability',
        'wp' => 'Waiting Payment',
        'pd' => 'Confirmed',
        'cl' => 'Canceled',
    );

    if( $status != '' && isset( $list[ $status ] ) )
    {
        return $list[ $status ];
    }
    else
    {
        return $list;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Booking Filter Param
| -----------------------------------------------------------------------------
*/
function ticket_booking_filter()
{
    if( isset( $_GET[ 'param' ] ) )
    {
        return json_decode( base64_decode( $_GET[ 'param' ] ), true );
    }
    else
    {
        return array( 'start' => '', 'end' => '', 'rid' => '', 'status' => '' );
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Departure Type List Option
| -----------------------------------------------------------------------------
*/
function ticket_departure_type_option( $selected = '' )
{
    return set_static_option( array( 'Departure Sunrise', 'Departure Sunset' ), $selected, false );
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Booking Status List Option
| -----------------------------------------------------------------------------
*/
function ticket_booking_status_option( $selected = '', $type = 0 )
{
    $status = ticket_booking_status();

    if( $type == 0 )
    {
        return set_static_option( $status, $selected, false );
    }
    else
    {
        $list = '';

        foreach( $status as $key => $dt )
        {
            $list .= '
            <li class="' . generateSefUrl( $dt ) . '">
                <label for="' . $key . '-status">
                    <input id="' . $key . '-status" data-class="' . $key . '-notice notice" data-label="' . $dt . '" type="radio" name="status" value="' . $key . '" ' . ( $key == $selected ? 'checked' : '' ) . ' />' . $dt . '
                </label>
            </li>';
        }

        return $list;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Ajax Send Invoice
| -----------------------------------------------------------------------------
*/
function send_booking_invoice()
{
    $data = fetch_booking( array( 'bid' => $_POST[ 'bid' ] ), 'AND', '*', true );

    if( send_invoice( $data ) === true )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Booking - Ajax Function
| -----------------------------------------------------------------------------
*/
function booking_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'load-booking-data' )
    {
        echo json_encode( get_booking_list_query() );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'load-latest-booking-data' )
    {
        echo json_encode( get_lastest_booking_list_query() );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'delete-booking' )
    {
        if( delete_booking( $_POST[ 'id' ] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'edit-booking-detail' )
    {
        if( edit_booking_detail( $_POST[ 'id' ] ) )
        {
            echo json_encode( array( 'result' => 'success', 'message' => 'Data booking has been updated' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed', 'message' => 'Data booking failed to updated' ) );
        }
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'view-booking-detail' )
    {
        echo get_booking_detail();
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'view-availability-form' )
    {
        echo get_booking_availability_form();
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'view-availability-result' )
    {
        echo get_booking_availability_result();
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'send-invoice' )
    {
        if( send_booking_invoice() )
        {
            echo json_encode( array( 'result' => 'success', 'message' => 'Invoice has been sent' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed', 'message' => 'Invoice failed to send' ) );
        }
    }
}

?> 