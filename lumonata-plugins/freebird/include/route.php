<?php

add_actions( 'route', 'init_route' );
add_actions( 'route_admin_page', 'route_ajax' );

add_privileges( 'administrator', 'route', 'insert' );
add_privileges( 'administrator', 'route', 'update' );
add_privileges( 'administrator', 'route', 'delete' );

/*
| -----------------------------------------------------------------------------
| Admin Route
| -----------------------------------------------------------------------------
*/
function init_route()
{
    run_route_actions();
    
    //-- Display add new form
    if( is_add_new() )
    {
        return add_route();
    }
    elseif( is_edit() )
    {
        if( is_contributor() || is_author() )
        {
            if( is_num_route( array( 'rid' => $_GET[ 'id' ] ) ) > 0 )
            {
                return edit_route( $_GET[ 'id' ] );
            }
            else
            {
                return '
                <div class="alert_red_form">
                    You don\'t have an authorization to access this page
                </div>';
            }
        }
        else
        {
            return edit_route( $_GET[ 'id' ] );
        }
    }
    elseif( is_delete_all() )
    {
        return delete_batch_route();
    }
    
    //-- Automatic to display add new when there is no records on database
    if( is_num_route() == 0 )
    {
        header( 'location:' . get_state_url( 'ticket&sub=route&prc=add_new' ) );
    }
    elseif( is_num_route() > 0 )
    {
        return get_route_list();
    }
}

function get_route_list()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/route_list.html', 'route' );

    add_block( 'list-block', 'l-block', 'route' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'alert', message_block() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'title', 'Route & Price List' );
    add_variable( 'ajax-url', get_route_ajax_url() );
    add_variable( 'view-option', get_route_view_option() );
    add_variable( 'button', get_route_admin_button( get_state_url( 'ticket&sub=route' ) ) );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );
    
    add_actions( 'section_title', 'Route & Price - List' );

    parse_template( 'list-block', 'l-block', 'route' );

    return return_template( 'route' );
}

/*
| -----------------------------------------------------------------------------
| Admin Route - Add New Route
| -----------------------------------------------------------------------------
*/
function add_route()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/route_new.html', 'route' );

    add_block( 'form-block', 'f-block', 'route' );

    add_variable( 'ftitle', 'Add New Route & Price' );
    add_variable( 'checked_publish', 'checked' );  
    add_variable( 'checked_unpublish', '' );    
    
    add_variable( 'alert', message_block() );
    add_variable( 'ajax-url', get_route_ajax_url() );
    add_variable( 'button', get_route_admin_button( get_state_url( 'ticket&sub=route' ), true ) );
    
    add_actions( 'section_title', 'Route & Price - Add New' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'route' );
}

/*
| -----------------------------------------------------------------------------
| Admin Route - Edit Route
| -----------------------------------------------------------------------------
*/
function edit_route( $id )
{
    $d = fetch_route( array( 'rid' => $id ) );

    set_template( PLUGINS_PATH . '/freebird/tpl/route_new.html', 'route' );

    add_block( 'form-block', 'f-block', 'route' );

    add_variable( 'rid', $d[ 'rid' ] );
    add_variable( 'rto', $d[ 'rto' ] );
    add_variable( 'rfrom', $d[ 'rfrom' ] );
    add_variable( 'rname', $d[ 'rname' ] );

    add_variable( 'rprice_1way', $d[ 'rprice_1way' ] );
    add_variable( 'rprice_1way_child', $d[ 'rprice_1way_child' ] );
    add_variable( 'rprice_1way_infant', $d[ 'rprice_1way_infant' ] );
    
    add_variable( 'rprice_2way', $d[ 'rprice_2way' ] );
    add_variable( 'rprice_2way_child', $d[ 'rprice_2way_child' ] );
    add_variable( 'rprice_2way_infant', $d[ 'rprice_2way_infant' ] );

    add_variable( 'ftitle', 'Edit Route & Price' );

    if( $d[ 'status' ] == '1' )
    {
        add_variable( 'checked_publish', 'checked' );  
        add_variable( 'checked_unpublish', '' );    
    }
    else
    {
        add_variable( 'checked_publish', '' ); 
        add_variable( 'checked_unpublish', 'checked' );  
    }
    
    add_variable( 'alert', message_block() );
    add_variable( 'ajax-url', get_route_ajax_url() );
    add_variable( 'button', get_route_admin_button( get_state_url( 'ticket&sub=route' ), true ) );
    
    add_actions( 'section_title', 'Route & Price - Edit' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'route' );
}

/*
| -----------------------------------------------------------------------------
| Admin Route - Batch Delete Route
| -----------------------------------------------------------------------------
*/
function delete_batch_route()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/route_batch_delete.html', 'route-batch-delete' );

    add_block( 'delete-loop-block', 'dl-block', 'route-batch-delete' );
    add_block( 'delete-block', 'd-block', 'route-batch-delete' );

    foreach( $_POST[ 'select' ] as $key => $val )
    {
        $d = fetch_route( array( 'rid' => $val ) );

        add_variable( 'rname', $d[ 'rname' ] );
        add_variable( 'rid', $d[ 'rid' ] );

        parse_template( 'delete-loop-block', 'dl-block', true );
    }

    add_variable( 'title', 'Batch Delete Route & Price' );
    add_variable( 'backlink', get_state_url( 'ticket&sub=route' ) );
    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST[ 'select' ] ) == 1 ? 'this route?' : 'these routes?' ) );
    
    add_actions( 'section_title', 'Route & Price - Batch Delete' );

    parse_template( 'delete-block', 'd-block', 'route-batch-delete' );

    return return_template( 'route-batch-delete' );
}

/*
| -----------------------------------------------------------------------------
| Admin Route - Table Query
| -----------------------------------------------------------------------------
*/
function get_route_list_query()
{
    global $db;
    
    extract( $_POST );
    
    $post = $_REQUEST;
    $cols  = array(
        1 => 'a.rname',
        2 => 'a.rfrom',
        3 => 'a.rto',
        4 => 'a.created_by',
        5 => 'a.created_date',
        6 => 'a.status'
    );
    
    //-- Set Order Column
    if ( isset( $post[ 'order' ] ) && !empty( $post[ 'order' ] ) )
    {
        $o = array();
        
        foreach ( $post[ 'order' ] as $i => $od )
        {
            if ( isset( $cols[ $post[ 'order' ][ $i ][ 'column' ] ] ) )
            {
                $o[] = $cols[ $post[ 'order' ][ $i ][ 'column' ] ] . ' ' . $post[ 'order' ][ $i ][ 'dir' ];
            }
        }
        
        $order = !empty( $o ) ? ' ORDER BY ' . implode( ', ', $o ) : '';
    }
    else
    {
        $order = ' ORDER BY a.rid ASC';
    }

    //-- Set Condition
    $w = array();

    if( isset( $post[ 'show' ] ) && $post[ 'show' ] != '' && $post[ 'show' ] != 'all' )
    {
        $w[] = $db->prepare_query( 'status = %s', $show );
    }

    if( empty( $post[ 'search' ][ 'value' ] ) === false )
    {        
        $s = array();

        foreach ( $cols as $col )
        {
            $s[] = $db->prepare_query( $col . ' LIKE %s', '%' . $post[ 'search' ][ 'value' ] . '%' );
        }

        $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE ' . implode( ' AND ', $w );
    }
    else
    {
        $where = '';
    }

    $q = 'SELECT * FROM ticket_route AS a' . $where . $order;
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );
    
    $q2 = $q . ' LIMIT ' . $post[ 'start' ] . ', ' . $post[ 'length' ];
    $r2 = $db->do_query( $q2 );
    $n2 = $db->num_rows( $r2 );
    
    $data = array();
    
    if ( $n2 > 0 )
    {
        $url = get_state_url( 'ticket&sub=route' );
        
        while ( $d2 = $db->fetch_array( $r2 ) )
        {
            $uid    = fetch_user_id_by_username( $d2[ 'created_by' ] );
       		$oprice = sprintf( '<b>Adult :</b> %s<br/><b>Child :</b> %s<br/><b>Infant :</b> %s<br/>', number_format( $d2[ 'rprice_1way' ], 0, '', ',' ), number_format( $d2[ 'rprice_1way_child' ], 0, '', ',' ), number_format( $d2[ 'rprice_1way_infant' ], 0, '', ',' ) );
       		$rprice = sprintf( '<b>Adult :</b> %s<br/><b>Child :</b> %s<br/><b>Infant :</b> %s<br/>', number_format( $d2[ 'rprice_2way' ], 0, '', ',' ), number_format( $d2[ 'rprice_2way_child' ], 0, '', ',' ), number_format( $d2[ 'rprice_2way_infant' ], 0, '', ',' ) );

            $data[] = array(
                'created_by'   => $d2[ 'created_by' ],
                'status'       => $d2[ 'status' ],
                'rname'        => $d2[ 'rname' ],
                'rfrom'        => $d2[ 'rfrom' ],
                'rto'          => $d2[ 'rto' ],
                'rid'          => $d2[ 'rid' ],
                'oprice'       => $oprice,
                'rprice'       => $rprice,
                'user_link'    => user_url( $uid ),
                'ajax_link'    => get_route_ajax_url(),
                'avatar'       => get_avatar( $uid, 3 ),
                'created_date' => date( 'd F Y', $d2[ 'created_date' ] ),
                'edit_link'    => get_state_url( 'ticket&sub=route&prc=edit&id=' . $d2[ 'rid' ] )
            );
        }
    }
    else
    {
        $n = 0;
    }
    
    return array(
        'draw' => intval( $post[ 'draw' ] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data 
    );
}

/*
| -----------------------------------------------------------------------------
| Admin Route - Action Button
| -----------------------------------------------------------------------------
*/
function get_route_admin_button( $new_url = '', $is_form = false )
{
    if( $is_form )
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=cancel', $new_url ) . '</li>
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=publish' ) . '</li>                 
            <li>' . button( 'button=cancel', $new_url ) . '</li>  
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
    }
    else
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>
            <li>' . button( 'button=publish&type=submit&enable=false' ) . '</li>
            <li>' . button( 'button=unpublish&type=submit&enable=false' ) . '</li>';
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Route - View Option
| -----------------------------------------------------------------------------
*/
function get_route_view_option()
{
    $opt_viewed   = '';    
    $show_data    = isset( $_POST[ 'data_to_show' ] ) ? $_POST[ 'data_to_show' ] : ( isset( $_GET[ 'data_to_show' ] ) ? $_GET[ 'data_to_show' ] : 'all' );
    $data_to_show = array( 'all' => 'All', '1' => 'Publish', '0' => 'Unpublish' );
    
    foreach( $data_to_show as $key => $val )
    {
        if( isset( $show_data ) && !empty( $show_data ) )
        {
            if( $show_data === $key )
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" checked="checked" autocomplete="off" />
                <label>' . $val . '</label>';
            }
            else
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" autocomplete="off" />
                <label>' . $val . '</label>';
            }
        }
        else
        {
            $opt_viewed .= '
            <input type="radio" name="data_to_show" value="' . $key . '" autocomplete="off" />
            <label>' . $val . '</label>';
        }
    }

    return $opt_viewed;
}

/*
| -----------------------------------------------------------------------------
| Admin Route - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_route_ajax_url()
{
    return get_state_url( 'ajax&apps=route' );
}

/*
| -----------------------------------------------------------------------------
| Admin Route Actions
| -----------------------------------------------------------------------------
*/
function run_route_actions()
{
    global $db;
    global $flash;

    //-- Publish, Unpublish
    //-- Actions From List Route
    if( isset( $_POST[ 'select' ] ) )
    {
        if( is_publish() || is_unpublish() )
        {
            $count  = count( $_POST[ 'select' ] );
            $status = is_unpublish() ? '0' : '1';
            $error  = 0;

            foreach( $_POST[ 'select' ] as $key => $val )
            {
                if( update_route( array( 'status' => $status ), array( 'rid' => $val ) ) === false )
                {
                    $error++;
                }
            }

            if( $error > 0 )
            {
                if( $error == $count )
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to update all route status' ) );
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to update some of route status' ) );
                }

                header( 'location: ' . get_state_url( 'ticket&sub=route' ) );

                exit;
            }
            else
            {
                $flash->add( array( 'type' => 'success', 'content' => 'Successfully update all selected route status' ) );
                
                header( 'location: ' . get_state_url( 'ticket&sub=route' ) );

                exit;
            }
        }
    }
    elseif( is_confirm_delete() )
    {
        $count  = count( $_POST[ 'id' ] );
        $error = 0;

        foreach( $_POST[ 'id' ] as $key => $val )
        {
            if( delete_route( $val ) === false )
            {
                $error++;
            }
        }

        if( $error > 0 )
        {
            if( $error == $count )
            {
                $flash->add( array( 'type' => 'error', 'content' => 'Failed to delete all selected route' ) );
            }
            else
            {
                $flash->add( array( 'type' => 'error', 'content' => 'Failed to update some of selected route' ) );
            }

            header( 'location: ' . get_state_url( 'ticket&sub=route' ) );

            exit;
        }
        else
        {
            $flash->add( array( 'type' => 'success', 'content' => 'Successfully delete all selected route' ) );
            
            header( 'location: ' . get_state_url( 'ticket&sub=route' ) );

            exit;
        }
    }
    else
    {
        if( is_save_draft() || is_publish() )
        {
            if( is_add_new() )
            {
                if( save_route() )
                {
                    $flash->add( array( 'type' => 'success', 'content' => 'Successfully add new route' ) );
                    
                    header( 'location: ' . get_state_url( 'ticket&sub=route&prc=add_new' ) );

                    exit;
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to add new route' ) );
                }
            }
            elseif( is_edit() )
            {
                if( update_route() )
                {
                    $flash->add( array( 'type' => 'success', 'content' => 'Successfully updated route data' ) );
                    
                    header( 'location: ' . get_state_url( 'ticket&sub=route&prc=edit&id=' . $_GET[ 'id' ] ) );

                    exit;
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to update route data' ) );
                }
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Route - Save Route
| -----------------------------------------------------------------------------
*/
function fetch_route( $conditions = array(), $operator = 'AND',  $fields = '*' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( is_array( $fields ) && empty( $fields ) === false )
    {
        $param = implode( ', ', $fields );
    }
    else
    {
        $param = $fields;
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT ' . $param . ' FROM ticket_route' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );

        if( $n > 1 )
        {
            while( $d = $db->fetch_assoc( $r ) )
            {
                $data[] = $d;
            }

            return $data;
        }
        elseif( $n > 0 )
        {
            $data = $db->fetch_assoc( $r );

            if( $fields != '*' && is_string( $fields ) && isset( $data[ $fields ] ) )
            {
                $data = $data[ $fields ];
            }
            
            return $data;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Route - Save Route
| -----------------------------------------------------------------------------
*/
function save_route( $params = array() )
{
    global $db;

    $fields = array(
		'rprice_1way_infant',
		'rprice_2way_infant',
		'rprice_1way_child',
		'rprice_2way_child',
        'created_date',
    	'rprice_1way',
		'rprice_2way',
        'created_by',
        'ballotment',
        'sort_id',
        'status',
        'rname',
        'rfrom',
        'rto',
        'dlu',
    );

    if( empty( $params ) )
    {
        $params = $_POST;
    }

    if( empty( $params ) )
    {
        return false;
    }
    else
    {
        //-- Insert route data
        $data = array( 
        	'created_by'   => $_COOKIE[ 'username' ], 
        	'created_date' => time(), 
        	'dlu'          => time()
        );

        foreach( $params as $f => $d )
        {
            if( in_array( $f, $fields ) )
            {
                $data[ $f ] = $d;
            }
        }

        $r = $db->insert( 'ticket_route', $data );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Route - Update Route
| -----------------------------------------------------------------------------
*/
function update_route( $params = array(), $where = array() )
{
    global $db;

    $fields = array(
		'rprice_1way_infant',
		'rprice_2way_infant',
		'rprice_1way_child',
		'rprice_2way_child',
        'created_date',
    	'rprice_1way',
		'rprice_2way',
        'created_by',
        'ballotment',
        'sort_id',
        'status',
        'rname',
        'rfrom',
        'rto',
        'dlu',
    );

    if( empty( $params ) )
    {
        $params = $_POST;
    }

    if( empty( $where ) )
    {
        $where = array( 'rid' => $_GET[ 'id' ] );
    }

    if( empty( $params ) )
    {
        return false;
    }
    else
    {
        //-- Update route data
        $data = array( 'dlu' => time() );

        foreach( $params as $f => $d )
        {
            if( in_array( $f, $fields ) )
            {
                $data[ $f ] = $d;
            }
        }

        $r = $db->update( 'ticket_route', $data, $where );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Route - Delete Route
| -----------------------------------------------------------------------------
*/
function delete_route( $id )
{
    global $db;

    $r = $db->delete( 'ticket_route', array( 'rid' => $id ) );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Route - Check Number of Route
| -----------------------------------------------------------------------------
*/
function is_num_route( $conditions = array(), $operator = 'AND' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();
    $n = 0;

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT * FROM ticket_route' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );
    }

    return $n;
}

/*
| -----------------------------------------------------------------------------
| Admin Route - Get Route Allotment
| -----------------------------------------------------------------------------
*/
function ticket_route_get_allotment( $rid )
{
    global $db;

    $s = 'SELECT * FROM ticket_route AS a WHERE a.rid = %d';
    $q = $db->prepare_query( $s, $rid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d[ 'rallotment' ];
}

/*
| -----------------------------------------------------------------------------
| Admin Route - Route List Option
| -----------------------------------------------------------------------------
*/
function ticket_route_option( $selected = '' )
{
	$data = fetch_route( array( 'status' => 1 ) );
	$html = '';

	if( empty( $data ) === false )
	{
        if( !is_array( $data ) )
        {
            $data = array( $data );
        }

        foreach( $data as $d )
        {
            if( is_array( $d ) === false )
            {
                $d = $data;

			    if( $d[ 'rid' ] == $selected )
			    {
			    	$html .= '<option value="' . $d[ 'rid' ] . '" selected>' . $d[ 'rname' ] . '</option>';
			    }
			    else
			    {
			    	$html .= '<option value="' . $d[ 'rid' ] . '">' . $d[ 'rname' ] . '</option>';
			    }

                break;
            }
            else
            {
			    if( $d[ 'rid' ] == $selected )
			    {
			    	$html .= '<option value="' . $d[ 'rid' ] . '" selected>' . $d[ 'rname' ] . '</option>';
			    }
			    else
			    {
			    	$html .= '<option value="' . $d[ 'rid' ] . '">' . $d[ 'rname' ] . '</option>';
			    }
            }
		}
	}

    return $html;
}

function ticket_multiple_route_option( $selected = array() )
{
	$data = fetch_route( array( 'status' => 1 ) );
	$html = '';

	if( empty( $data ) === false )
	{
        if( !is_array( $data ) )
        {
            $data = array( $data );
        }

        foreach( $data as $d )
        {
            if( is_array( $d ) === false )
            {
                $d = $data;

			    if( in_array( $d[ 'rid' ], $selected ) )
			    {
			    	$html .= '<option value="' . $d[ 'rid' ] . '" selected>' . $d[ 'rname' ] . '</option>';
			    }
			    else
			    {
			    	$html .= '<option value="' . $d[ 'rid' ] . '">' . $d[ 'rname' ] . '</option>';
			    }

                break;
            }
            else
            {
			    if( in_array( $d[ 'rid' ], $selected ) )
			    {
			    	$html .= '<option value="' . $d[ 'rid' ] . '" selected>' . $d[ 'rname' ] . '</option>';
			    }
			    else
			    {
			    	$html .= '<option value="' . $d[ 'rid' ] . '">' . $d[ 'rname' ] . '</option>';
			    }
            }
		}
	}

    return $html;
}

/*
| -----------------------------------------------------------------------------
| Admin Route - Ajax Function
| -----------------------------------------------------------------------------
*/
function route_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'load-route-data' )
    {
        echo json_encode( get_route_list_query() );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'delete-route' )
    {
        if( delete_route( $_POST[ 'id' ] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }
}

?> 