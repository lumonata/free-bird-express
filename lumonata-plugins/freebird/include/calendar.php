<?php

add_actions( 'calendar', 'init_calendar' );
add_actions( 'calendar_admin_page', 'calendar_ajax' );

add_privileges( 'administrator', 'calendar', 'insert' );
add_privileges( 'administrator', 'calendar', 'update' );
add_privileges( 'administrator', 'calendar', 'delete' );

/*
| -----------------------------------------------------------------------------
| Admin Calendar
| -----------------------------------------------------------------------------
*/
function init_calendar()
{
    return get_calendar_data();
}

function get_calendar_data()
{
    global $db;

    set_template( PLUGINS_PATH . '/freebird/tpl/calendar_list.html', 'calendar' );

    add_block( 'calendar-loop-block', 'cl-block', 'calendar' );
    add_block( 'list-block', 'l-block', 'calendar' );

    run_calendar_actions();

    if( isset( $_POST[ 'filter' ] ) )
    {
        $filter = array(
            'rid'   => $_POST[ 'rid' ],
            'year'  => $_POST[ 'year' ],
            'month' => $_POST[ 'month' ]
        );
    }
    elseif( isset( $_GET[ 'filter' ] ) )
    {
        $filter = json_decode( base64_decode( $_GET[ 'filters' ] ), true );
    }
    else
    {
        $filter = array(
            'rid'   => '',
            'year'  => date( 'Y' ),
            'month' => date( 'm' )
        );
    }

    if( $filter[ 'rid' ] == '' )
    {
        $s = 'SELECT * FROM ticket_route AS a WHERE a.status = %s ORDER BY a.sort_id';
        $q = $db->prepare_query( $s, 1 );
    }
    else
    {
        $s = 'SELECT * FROM ticket_route AS a WHERE a.rid = %d AND a.status = %s ORDER BY a.sort_id';
        $q = $db->prepare_query( $s, $filter[ 'rid' ], 1 );
    }

    $r = $db->do_query( $q );
    $i = 0;

    while( $d = $db->fetch_array( $r ) )
    {
        add_variable( 'i', $i );
        add_variable( 'rid', $d[ 'rid' ] );
        add_variable( 'route-name', $d[ 'rname' ] );

        add_variable( 'row-span', ( ticket_calendar_schedule_num( $d[ 'rid' ] ) + 1 ) );
        add_variable( 'tr-schedule-name', ticket_calendar_schedule_name( $d[ 'rid' ], $i ) );
        add_variable( 'tr-calendar-date-data', ticket_calendar_schedule_date( $d[ 'rid' ], $filter ) );

        parse_template( 'calendar-loop-block', 'cl-block', true );

        $i++;
    }

    add_variable( 'alert', message_block() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_booking_ajax_url() );
    add_variable( 'title', 'Calendar List' );

    add_variable( 'year-option', ticket_year_option( $filter[ 'year' ] ) );
    add_variable( 'route-option', ticket_route_option( $filter[ 'rid' ] ) );
    add_variable( 'month-option', ticket_month_option( $filter[ 'month' ] ) );
    add_variable( 'status-option', set_static_option( array( 'Available', 'Not Available' ), null, false ) );

    add_actions( 'section_title', 'Calendar - List' );

    add_actions( 'css_elements', 'get_custom_css', get_plugin_url( 'freebird/css/front.css' ) );

    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/css/select2.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/js/select2.min.js' );

    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' );
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css' )
    ;
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' );

    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js' );

    parse_template( 'list-block', 'l-block', 'calendar' );

    return return_template( 'calendar' );
}

/*
| -----------------------------------------------------------------------------
| Admin Calendar Actions
| -----------------------------------------------------------------------------
*/
function run_calendar_actions()
{
    global $db;
    global $flash;

    if( isset( $_POST[ 'update' ] ) )
    {
        $error = array();

        if( $_POST[ 'start' ] == '' )
        {
            $error[] = 'Start date can\'t be empty on closing the schedule';
        }

        if( $_POST[ 'end' ] == '' )
        {
            $error[] = 'End date can\'t be empty on closing the schedule';
        }

        if( empty( $error ) === false )
        {
            $flash->add( array( 'type' => 'error', 'content' => $error ) );

            header( 'location: ' . get_state_url( 'ticket&sub=calendar' ) );

            exit;
        }

        if( empty( $_POST[ 'rid' ] ) )
        {
            $s = 'SELECT * FROM ticket_route AS a WHERE a.status = %s ORDER BY a.sort_id';
            $q = $db->prepare_query( $s, 1 );
            $r = $db->do_query( $q );
            $n = $db->num_rows( $r );

            if( $n > 0 )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    ticket_close_booking( $_POST, $d[ 'rid' ] );
                }
            }
        }
        else
        {
            ticket_close_booking( $_POST );
        }
    }
}

function ticket_close_booking( $post, $rid = '' )
{
    global $db;

    //-- GET DATE RANGE AND ADD TO DATE LIST
    $period = new DatePeriod( new DateTime( $post[ 'start' ] ), new DateInterval( 'P1D' ), new DateTime( $post[ 'end' ] . ' +1 day' ) );
    $start  = strtotime( $post[ 'start' ] );
    $end    = strtotime( $post[ 'end' ] );
    $date   = array();

    foreach( $period as $key => $value )
    {
        array_push( $date, strtotime( $value->format( 'Y-m-d' ) ) );
    }

    if( $rid == '' )
    {
        $rid = $post[ 'rid' ];
    }

    //-- GET CURRENT CLOSE BOOKING DATE FROM DATABASE
    $cdate = get_closing_date( $rid );

    //-- SET NEW CLOSE BOOKING DATE
    if( $post[ 'status' ] == 0 )
    {
        if( !empty( $cdate ) )
        {
            foreach( $date as $obj )
            {
                if( ( $key = array_search( $obj, $cdate ) ) !== false )
                {
                    unset( $cdate[ $key ] );
                }
            }

            if( !empty( $cdate ) )
            {
                $r = $db->update( 'ticket_booking_close', array(
                    'date_list' => json_encode( array_values( $cdate ) ),
                    'date_from' => $start,
                    'date_to'   => $end,
                ), array( 'rid' => $rid ) );
            }
            else
            {
                $r = $db->delete( 'ticket_booking_close', array( 'rid' => $rid ) );
            }
        }
    }
    elseif( $post[ 'status' ] == 1 )
    {
        if( !empty( $cdate ) )
        {
            foreach( $date as $obj )
            {
                if( !in_array( $obj, $cdate ) )
                {
                    $cdate[] = $obj;
                }
            }

            sort( $cdate );

            $r = $db->update( 'ticket_booking_close', array(
                'date_list' => json_encode( $cdate ),
                'date_from' => $start,
                'date_to'   => $end,
            ), array( 'rid' => $rid ) );
        }
        else
        {
            $r = $db->insert( 'ticket_booking_close', array(
                'date_list' => json_encode( $date ),
                'date_from' => $start,
                'date_to'   => $end,
                'rid'       => $rid
            ) );
        }
    }
}

function ticket_calendar_schedule_num( $rid )
{
    global $db;

    $q = $db->prepare_query( "SELECT * FROM ticket_schedule WHERE rid = %d ORDER BY sort_id ASC", $rid );
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    return $n;
}

function ticket_calendar_schedule_name( $rid, $i = 0 )
{
    global $db;

    $html = '';

    $q = $db->prepare_query( "SELECT * FROM ticket_schedule WHERE rid = %d ORDER BY sparent ASC, sort_id ASC", $rid );
    $r = $db->do_query( $q );

    while( $d = $db->fetch_array( $r ) )
    {
        $html .= '
        <tr>
			<td id="sname' . $d[ 'sid' ] . '">' . $d[ 'sname' ] . '</td>
			<td class="nav nav-prev"><a class="trigger-prev" rel="tb-calendar-data-' . $i . '">&nbsp;&nbsp;</a></td>
			<td class="nav nav-next"><a class="trigger-next" rel="tb-calendar-data-' . $i . '">&nbsp;&nbsp;</a></td>
		</tr>';

    }

    return $html;
}

function ticket_calendar_schedule_date( $rid, $filter )
{
    global $db;

    $year    = $filter[ 'year' ];
    $month   = $filter[ 'month' ];
    $num_day = date( 't', mktime( 0, 0, 0, $month, 1, $year ) );

    $q = $db->prepare_query( 'SELECT * FROM ticket_schedule AS a WHERE a.rid = %d ORDER BY a.sparent ASC, a.sort_id ASC', $rid );
    $r = $db->do_query( $q );

    //-- GET CLOSE SCHEDULE PERIOD
    $q_2 = $db->prepare_query( 'SELECT * FROM ticket_booking_close AS a WHERE a.rid = %d', $rid );
    $r_2 = $db->do_query( $q_2 );

    $date_list = array();

    while( $d_2 = $db->fetch_array( $r_2 ) )
    {
        $date_list = json_decode( $d_2[ 'date_list' ], true );
    }

    $today = date( 'Y-m-d' );
    $html  = '';

    while( $d = $db->fetch_array( $r ) )
    {
        $td = '';

        for( $i = 1; $i <= $num_day; $i++ )
        {
            $day  = $i < 10 ? '0' . $i : $i;
            $date = $year . '-' . $month . '-' . $day;

            //-- CHECK IF DATE IN CLOSE BOOKING RANGE
            $num_booking = ticket_calendar_num_booking_per_schedule( $d[ 'sid' ], $date );
            $in_close    = false;

            if( !empty( $date_list ) )
            {
                $user_tmp = strtotime( $date );

                if( in_array( $user_tmp, $date_list ) )
                {
                    $num_booking = ticket_get_allotment( $d[ 'sid' ] );

                    $in_close = true;
                }
            }

            $num_allotment = ticket_get_allotment( $d[ 'sid' ] );

            $class_allot = array( 'col-allot' );
            $class_col   = array( 'col' );

            if( $num_booking == $num_allotment )
            {
                array_push( $class_allot, 'full' );
            }
            elseif( $num_booking > $num_allotment )
            {
                array_push( $class_allot, 'overload' );
            }
            elseif( $num_booking > 0 )
            {
                array_push( $class_allot, 'some-book' );
            }
            else
            {
                array_push( $class_allot, 'no-book' );
            }

            if( $today == $date )
            {
                array_push( $class_col, 'col-today' );
                array_push( $class_allot, 'today' );
            }

            //-- Begin update use pacha system
            $ta = new ticketAvailability( $rid, $d[ 'sid' ], $date, 0, 0 );

            if( $in_close )
            {
                $num_available_allotment = $num_allotment;
            }
            else
            {
                $num_available_allotment = $ta->get_total_available_ticket_from_x_to_y();
            }

            $td .= '
            <div class="' . implode( ' ', $class_col ) . '">
                <div class="col-date">
                    ' . $i . '<br>' . date( 'F', mktime( 0, 0, 0, $month, 10 ) ) . '
                </div>
                <div class="' . implode( ' ', $class_allot ) . '">
                    <span id="d_' . $d[ 'sid' ] . '_' . $date . '8">' . $num_booking . '/' . $num_available_allotment . '</span>
                    <span class="wrap">
                        <span class="td-option">+</span>
                        <span class="menu">';

            $fdate = date( 'd F Y', strtotime( $date ) );
            $param = base64_encode( json_encode( array(
                'start'  => $fdate,
                'end'    => $fdate,
                'rid'    => $rid,
                'status' => 'pd'
            ) ) );

            if( $num_booking != $num_allotment && $num_booking < $num_allotment )
            {
                if( strtotime( $date ) >= strtotime( $today ) )
                {

                    $td .= '
                                    <span>
                                        <a href="#" class="add-mb add-booking" data-rid="' . $rid . '" data-sid="' . $d[ 'sid' ] . '" data-date="' . $date . '">
                                            Add Booking
                                        </a>
                                    </span>';
                }
            }

            $td .= '
                            <span>
                                <a href="' . get_state_url( 'ticket&sub=booking&param=' . $param ) . '"> 
                                    Booking Detail
                                </a>
                            </span>
                        </span>
                    </span>
                </div>
            </div>';
        }

        $html .= $td;
    }

    return $html;
}

function ticket_calendar_num_booking_per_schedule( $sid, $date )
{
    global $db;

    $q = $db->prepare_query( "SELECT * FROM ticket_booking_detail WHERE sid = %d AND date = %s AND status = %s", $sid, $date, 'pd' );
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    if( $n == 0 )
    {
        return $n;
    }
    else
    {
        $num = 0;

        while( $d = $db->fetch_array( $r ) )
        {
            $num = $num + ( $d[ 'num_adult' ] + $d[ 'num_child' ] );
        }

        return $num;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Calendar - Get List of Close Date By Route ID
| -----------------------------------------------------------------------------
*/
function get_closing_date( $rid )
{
    global $db;

    $s = 'SELECT a.date_list FROM ticket_booking_close AS a WHERE a.rid = %d';
    $q = $db->prepare_query( $s, $rid );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );
    $c = array();

    if( isset( $d[ 'date_list' ] ) && !empty( $d[ 'date_list' ] ) )
    {
        $c = json_decode( $d[ 'date_list' ], true );
    }

    return $c;
}

/*
| -----------------------------------------------------------------------------
| Admin Calendar - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_calendar_ajax_url()
{
    return get_state_url( 'ajax&apps=calendar' );
}

/*
| -----------------------------------------------------------------------------
| Admin Calendar - Calendar Month List Option
| -----------------------------------------------------------------------------
*/
function ticket_month_option( $selected = '' )
{
    return set_static_option( array(
        '01' => 'January',
        '02' => 'February',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December'
    ), $selected, false );
}

/*
| -----------------------------------------------------------------------------
| Admin Calendar - Calendar Year List Option
| -----------------------------------------------------------------------------
*/
function ticket_year_option( $selected = '' )
{
    $ranges = range( date( 'Y', strtotime( '+5 years' ) ), 2013 );

    $list = array();

    foreach( $ranges as $year )
    {
        $list[ $year ] = $year;
    }

    return set_static_option( $list, $selected, false );
}

/*
| -----------------------------------------------------------------------------
| Admin Calendar - Ajax Function
| -----------------------------------------------------------------------------
*/
function calendar_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    exit;
}
