<?php

add_actions( 'special_rate', 'init_special_rate' );
add_actions( 'special_rate_admin_page', 'special_rate_ajax' );

add_privileges( 'administrator', 'special_rate', 'insert' );
add_privileges( 'administrator', 'special_rate', 'update' );
add_privileges( 'administrator', 'special_rate', 'delete' );

/*
| -----------------------------------------------------------------------------
| Admin Special Rate
| -----------------------------------------------------------------------------
*/
function init_special_rate()
{
    run_special_rate_actions();
    
    //-- Display add new form
    if( is_add_new() )
    {
        return add_special_rate();
    }
    elseif( is_edit() )
    {
        if( is_contributor() || is_author() )
        {
            if( is_num_special_rate( array( 'srid' => $_GET[ 'id' ] ) ) > 0 )
            {
                return edit_special_rate( $_GET[ 'id' ] );
            }
            else
            {
                return '
                <div class="alert_red_form">
                    You don\'t have an authorization to access this page
                </div>';
            }
        }
        else
        {
            return edit_special_rate( $_GET[ 'id' ] );
        }
    }
    elseif( is_delete_all() )
    {
        return delete_batch_special_rate();
    }
    
    //-- Automatic to display add new when there is no records on database
    if( is_num_special_rate() == 0 )
    {
        header( 'location:' . get_state_url( 'ticket&sub=special_rate&prc=add_new' ) );
    }
    elseif( is_num_special_rate() > 0 )
    {
        return get_special_rate_list();
    }
}

function get_special_rate_list()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/special_rate_list.html', 'special-rate' );

    add_block( 'list-block', 'l-block', 'special-rate' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'alert', message_block() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_special_rate_ajax_url() );
    add_variable( 'view-option', get_special_rate_view_option() );
    add_variable( 'button', get_special_rate_admin_button( get_state_url( 'ticket&sub=special_rate' ) ) );
    add_variable( 'title', 'Special Rate List' );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );
    
    add_actions( 'section_title', 'Special Rate' );

    parse_template( 'list-block', 'l-block', 'special-rate' );

    return return_template( 'special-rate' );
}

/*
| -----------------------------------------------------------------------------
| Admin Special Rate - Add New Special Rate
| -----------------------------------------------------------------------------
*/
function add_special_rate()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/special_rate_new.html', 'special-rate' );

    add_block( 'form-block', 'f-block', 'special-rate' );

    add_variable( 'ftitle', 'Add New Special Rate' );

    add_variable( 'checked_publish', 'checked' );  
    add_variable( 'checked_unpublish', '' );

    add_variable( 'checked_sunrise', 'checked' );  
    add_variable( 'checked_sunset', '' );

    add_variable( 'rid', ticket_route_option() );
    add_variable( 'sid', ticket_schedule_option() );

    add_variable( 'alert', message_block() );
    add_variable( 'ajax-url', get_special_rate_ajax_url() );
    add_variable( 'button', get_special_rate_admin_button( get_state_url( 'ticket&sub=special_rate' ), true ) );
    
    add_actions( 'section_title', 'Special Rate - Add New' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'special-rate' );
}

/*
| -----------------------------------------------------------------------------
| Admin Special Rate - Edit Special Rate
| -----------------------------------------------------------------------------
*/
function edit_special_rate( $id )
{
    $d = fetch_special_rate( array( 'srid' => $id ) );

    set_template( PLUGINS_PATH . '/freebird/tpl/special_rate_new.html', 'special-rate' );

    add_block( 'form-block', 'f-block', 'special-rate' );

    add_variable( 'srid', $d[ 'srid' ] );
    add_variable( 'srname', $d[ 'srname' ] );
    add_variable( 'srdate_end', date( 'd F Y', $d[ 'srdate_end' ] ) );
    add_variable( 'srdate_start', date( 'd F Y', $d[ 'srdate_start' ] ) );

    add_variable( 'rid', ticket_route_option( $d[ 'rid' ] ) );
    add_variable( 'sid', ticket_schedule_option( $d[ 'sid' ] ) );

    add_variable( 'srprice_1way', $d[ 'srprice_1way' ] );
    add_variable( 'srprice_1way_child', $d[ 'srprice_1way_child' ] );
    add_variable( 'srprice_1way_infant', $d[ 'srprice_1way_infant' ] );
    
    add_variable( 'srprice_2way', $d[ 'srprice_2way' ] );
    add_variable( 'srprice_2way_child', $d[ 'srprice_2way_child' ] );
    add_variable( 'srprice_2way_infant', $d[ 'srprice_2way_infant' ] );

    add_variable( 'ftitle', 'Edit Special Rate' );

    if( $d[ 'status' ] == '1' )
    {
        add_variable( 'checked_publish', 'checked' );  
        add_variable( 'checked_unpublish', '' );    
    }
    else
    {
        add_variable( 'checked_publish', '' ); 
        add_variable( 'checked_unpublish', 'checked' );  
    }
    
    add_variable( 'alert', message_block() );
    add_variable( 'ajax-url', get_special_rate_ajax_url() );
    add_variable( 'button', get_special_rate_admin_button( get_state_url( 'ticket&sub=special_rate' ), true ) );
    
    add_actions( 'section_title', 'Special Rate - Edit' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'special-rate' );
}

/*
| -----------------------------------------------------------------------------
| Admin Special Rate - Batch Delete Special Rate
| -----------------------------------------------------------------------------
*/
function delete_batch_special_rate()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/special_rate_batch_delete.html', 'special-rate-batch-delete' );

    add_block( 'delete-loop-block', 'dl-block', 'special-rate-batch-delete' );
    add_block( 'delete-block', 'd-block', 'special-rate-batch-delete' );

    foreach( $_POST[ 'select' ] as $key => $val )
    {
        $d = fetch_special_rate( array( 'srid' => $val ) );

        add_variable( 'srname', $d[ 'srname' ] );
        add_variable( 'srid', $d[ 'srid' ] );

        parse_template( 'delete-loop-block', 'dl-block', true );
    }

    add_variable( 'title', 'Batch Delete Special Rate' );
    add_variable( 'backlink', get_state_url( 'ticket&sub=special_rate' ) );
    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST[ 'select' ] ) == 1 ? 'this data?' : 'these data?' ) );
    
    add_actions( 'section_title', 'Special Rate - Batch Delete' );

    parse_template( 'delete-block', 'd-block', 'special-rate-batch-delete' );

    return return_template( 'special-rate-batch-delete' );
}

/*
| -----------------------------------------------------------------------------
| Admin Special Rate - Table Query
| -----------------------------------------------------------------------------
*/
function get_special_rate_list_query()
{
    global $db;
    
    extract( $_POST );
    
    $post = $_REQUEST;
    $cols  = array(
        1 => 'a.srname',
        3 => 'a.srdate_start', 
        4 => 'a.srdate_end', 
        5 => 'a.created_by',
        6 => 'a.created_date',
        7 => 'a.status'
    );
    
    //-- Set Order Column
    if ( isset( $post[ 'order' ] ) && !empty( $post[ 'order' ] ) )
    {
        $o = array();
        
        foreach ( $post[ 'order' ] as $i => $od )
        {
            if ( isset( $cols[ $post[ 'order' ][ $i ][ 'column' ] ] ) )
            {
                $o[] = $cols[ $post[ 'order' ][ $i ][ 'column' ] ] . ' ' . $post[ 'order' ][ $i ][ 'dir' ];
            }
        }
        
        $order = !empty( $o ) ? ' ORDER BY ' . implode( ', ', $o ) : '';
    }
    else
    {
        $order = ' ORDER BY a.srid ASC';
    }

    //-- Set Condition
    $w = array();

    if( isset( $post[ 'show' ] ) && $post[ 'show' ] != '' && $post[ 'show' ] != 'all' )
    {
        $w[] = $db->prepare_query( 'a.status = %s', $show );
    }

    if( empty( $post[ 'search' ][ 'value' ] ) === false )
    {        
        $s = array();

        foreach ( $cols as $col )
        {
            $s[] = $db->prepare_query( $col . ' LIKE %s', '%' . $post[ 'search' ][ 'value' ] . '%' );
        }

        $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE ' . implode( ' AND ', $w );
    }
    else
    {
        $where = '';
    }

    $q = 'SELECT
			a.srid,
            b.rname,
            c.sname,
			a.srname,
            a.status,
            a.created_by,
            a.srdate_end,
            a.created_date,
            a.srdate_start,
            a.srprice_1way,
            a.srprice_2way,
            a.srprice_1way_child,
            a.srprice_2way_child,
            a.srprice_1way_infant,
            a.srprice_2way_infant
		 FROM ticket_special_rate AS a
		 LEFT JOIN ticket_route AS b ON a.rid = b.rid
		 LEFT JOIN ticket_schedule AS c ON a.sid = c.sid' . $where . $order;
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );
    
    $q2 = $q . ' LIMIT ' . $post[ 'start' ] . ', ' . $post[ 'length' ];
    $r2 = $db->do_query( $q2 );
    $n2 = $db->num_rows( $r2 );
    
    $data = array();
    
    if ( $n2 > 0 )
    {
        $url = get_state_url( 'ticket&sub=special_rate' );
        
        while ( $d2 = $db->fetch_array( $r2 ) )
        {
            $uid = fetch_user_id_by_username( $d2[ 'created_by' ] );

            if( empty( $d2[ 'rname' ] ) )
            {
                $d2[ 'rname' ] = 'All Routes';
            }

            if( empty( $d2[ 'sname' ] ) )
            {
                $d2[ 'sname' ] = 'All Schedules';
            }

            $oprice  = sprintf( '<b>Adult :</b> %s<br/><b>Child :</b> %s<br/><b>Infant :</b> %s<br/>', number_format( $d2[ 'srprice_1way' ], 0, '', ',' ), number_format( $d2[ 'srprice_1way_child' ], 0, '', ',' ), number_format( $d2[ 'srprice_1way_infant' ], 0, '', ',' ) );
            $rprice  = sprintf( '<b>Adult :</b> %s<br/><b>Child :</b> %s<br/><b>Infant :</b> %s<br/>', number_format( $d2[ 'srprice_2way' ], 0, '', ',' ), number_format( $d2[ 'srprice_2way_child' ], 0, '', ',' ), number_format( $d2[ 'srprice_2way_infant' ], 0, '', ',' ) );
            $applied = sprintf( '%s<br/>%s', $d2[ 'rname' ], $d2[ 'sname' ] );

            $data[] = array(
                'created_by'   => $d2[ 'created_by' ],
                'status'       => $d2[ 'status' ],
                'srname'       => $d2[ 'srname' ],
                'srid'         => $d2[ 'srid' ],
                'applied_to'   => $applied,
                'oprice'       => $oprice,
                'rprice'       => $rprice,
                'user_link'    => user_url( $uid ),
                'avatar'       => get_avatar( $uid, 3 ),
                'ajax_link'    => get_special_rate_ajax_url(),
                'srdate_end'   => date( 'd F Y', $d2[ 'srdate_end' ] ),
                'srdate_start' => date( 'd F Y', $d2[ 'srdate_start' ] ),
                'created_date' => date( 'd F Y', $d2[ 'created_date' ] ),
                'edit_link'    => get_state_url( 'ticket&sub=special_rate&prc=edit&id=' . $d2[ 'srid' ] )
            );
        }
    }
    else
    {
        $n = 0;
    }
    
    return array(
        'draw' => intval( $post[ 'draw' ] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data 
    );
}

/*
| -----------------------------------------------------------------------------
| Admin Special Rate - Action Button
| -----------------------------------------------------------------------------
*/
function get_special_rate_admin_button( $new_url = '', $is_form = false )
{
    if( $is_form )
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=cancel', $new_url ) . '</li>
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=publish' ) . '</li>                 
            <li>' . button( 'button=cancel', $new_url ) . '</li>  
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
    }
    else
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>
            <li>' . button( 'button=publish&type=submit&enable=false' ) . '</li>
            <li>' . button( 'button=unpublish&type=submit&enable=false' ) . '</li>';
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Special Rate - View Option
| -----------------------------------------------------------------------------
*/
function get_special_rate_view_option()
{
    $opt_viewed   = '';    
    $show_data    = isset( $_POST[ 'data_to_show' ] ) ? $_POST[ 'data_to_show' ] : ( isset( $_GET[ 'data_to_show' ] ) ? $_GET[ 'data_to_show' ] : 'all' );
    $data_to_show = array( 'all' => 'All', '1' => 'Publish', '0' => 'Unpublish' );
    
    foreach( $data_to_show as $key => $val )
    {
        if( isset( $show_data ) && !empty( $show_data ) )
        {
            if( $show_data === $key )
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" checked="checked" autocomplete="off" />
                <label>' . $val . '</label>';
            }
            else
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" autocomplete="off" />
                <label>' . $val . '</label>';
            }
        }
        else
        {
            $opt_viewed .= '
            <input type="radio" name="data_to_show" value="' . $key . '" autocomplete="off" />
            <label>' . $val . '</label>';
        }
    }

    return $opt_viewed;
}

/*
| -----------------------------------------------------------------------------
| Admin Special Rate - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_special_rate_ajax_url()
{
    return get_state_url( 'ajax&apps=special_rate' );
}

/*
| -----------------------------------------------------------------------------
| Admin Special Rate Actions
| -----------------------------------------------------------------------------
*/
function run_special_rate_actions()
{
    global $db;
    global $flash;

    //-- Publish, Unpublish
    //-- Actions From List Special Rate
    if( isset( $_POST[ 'select' ] ) )
    {
        if( is_publish() || is_unpublish() )
        {
            $count  = count( $_POST[ 'select' ] );
            $status = is_unpublish() ? '0' : '1';
            $error  = 0;

            foreach( $_POST[ 'select' ] as $key => $val )
            {
                if( update_special_rate( array( 'status' => $status ), array( 'srid' => $val ) ) === false )
                {
                    $error++;
                }
            }

            if( $error > 0 )
            {
                if( $error == $count )
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to update all special rate status' ) );
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to update some of special rate status' ) );
                }

                header( 'location: ' . get_state_url( 'ticket&sub=special_rate' ) );

                exit;
            }
            else
            {
                $flash->add( array( 'type' => 'success', 'content' => 'Successfully update all selected special rate status' ) );
                
                header( 'location: ' . get_state_url( 'ticket&sub=special_rate' ) );

                exit;
            }
        }
    }
    elseif( is_confirm_delete() )
    {
        $count  = count( $_POST[ 'id' ] );
        $error = 0;

        foreach( $_POST[ 'id' ] as $key => $val )
        {
            if( delete_special_rate( $val ) === false )
            {
                $error++;
            }
        }

        if( $error > 0 )
        {
            if( $error == $count )
            {
                $flash->add( array( 'type' => 'error', 'content' => 'Failed to delete all selected special  rate' ) );
            }
            else
            {
                $flash->add( array( 'type' => 'error', 'content' => 'Failed to update some of selected special rate' ) );
            }

            header( 'location: ' . get_state_url( 'ticket&sub=special_rate' ) );

            exit;
        }
        else
        {
            $flash->add( array( 'type' => 'success', 'content' => 'Successfully delete all selected special rate' ) );
            
            header( 'location: ' . get_state_url( 'ticket&sub=special_rate' ) );

            exit;
        }
    }
    else
    {
        if( is_save_draft() || is_publish() )
        {
            if( is_add_new() )
            {
                if( save_special_rate() )
                {
                    $flash->add( array( 'type' => 'success', 'content' => 'Successfully add new special rate' ) );
                    
                    header( 'location: ' . get_state_url( 'ticket&sub=special_rate&prc=add_new' ) );

                    exit;
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to add new special rate' ) );
                }
            }
            elseif( is_edit() )
            {
                if( update_special_rate() )
                {
                    $flash->add( array( 'type' => 'success', 'content' => 'Successfully updated special rate data' ) );
                    
                    header( 'location: ' . get_state_url( 'ticket&sub=special_rate&prc=edit&id=' . $_GET[ 'id' ] ) );

                    exit;
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to update special rate data' ) );
                }
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Special Rate - Save Special Rate
| -----------------------------------------------------------------------------
*/
function fetch_special_rate( $conditions = array(), $operator = 'AND',  $fields = '*' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( is_array( $fields ) && empty( $fields ) === false )
    {
        $param = implode( ', ', $fields );
    }
    else
    {
        $param = $fields;
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT ' . $param . ' FROM ticket_special_rate' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );

        if( $n > 1 )
        {
            while( $d = $db->fetch_assoc( $r ) )
            {
                $data[] = $d;
            }
        }
        elseif( $n > 0 )
        {
            $data = $db->fetch_assoc( $r );

            if( $fields != '*' && is_string( $fields ) && isset( $data[ $fields ] ) )
            {
                $data = $data[ $fields ];
            }
        }

        return $data;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Special Rate - Save Special Rate
| -----------------------------------------------------------------------------
*/
function save_special_rate( $params = array() )
{
    global $db;

    $fields = array(
        'srprice_1way_infant',
        'srprice_2way_infant',
        'srprice_1way_child',
        'srprice_2way_child',
        'srprice_1way',
        'srprice_2way',
        'created_date',
        'srdate_start',
        'created_by',
        'srdate_end',
        'status',
        'srname',
        'rid',
        'sid',
        'dlu'
    );

    if( empty( $params ) )
    {
        $params = $_POST;
    }

    if( empty( $params ) )
    {
        return false;
    }
    else
    {
        //-- Insert special rate data
        $data = array( 
            'created_by'   => $_COOKIE[ 'username' ],
            'created_date' => time(), 
            'dlu'          => time()
        );

        foreach( $params as $f => $d )
        {
            if( in_array( $f, $fields ) )
            {
                if( $f == 'srdate_start' || $f == 'srdate_end' )
                {
                    $d = strtotime( $d );
                }
                
                $data[ $f ] = $d;
            }
        }

        $r = $db->insert( 'ticket_special_rate', $data );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Special Rate - Update Special Rate
| -----------------------------------------------------------------------------
*/
function update_special_rate( $params = array(), $where = array() )
{
    global $db;

    $fields = array(
        'srprice_1way_infant',
        'srprice_2way_infant',
        'srprice_1way_child',
        'srprice_2way_child',
        'srprice_1way',
        'srprice_2way',
        'created_date',
        'srdate_start',
        'created_by',
        'srdate_end',
        'status',
        'srname',
        'rid',
        'sid',
        'dlu'
    );

    if( empty( $params ) )
    {
        $params = $_POST;
    }

    if( empty( $where ) )
    {
        $where = array( 'srid' => $_GET[ 'id' ] );
    }

    if( empty( $params ) )
    {
        return false;
    }
    else
    {
        //-- Update special rate data
        $data = array( 'dlu' => time() );

        foreach( $params as $f => $d )
        {
            if( in_array( $f, $fields ) )
            {
                if( $f == 'srdate_start' || $f == 'srdate_end' )
                {
                    $d = strtotime( $d );
                }
                
                $data[ $f ] = $d;
            }
        }

        $r = $db->update( 'ticket_special_rate', $data, $where );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Special Rate - Delete Special Rate
| -----------------------------------------------------------------------------
*/
function delete_special_rate( $id )
{
    global $db;

    $r = $db->delete( 'ticket_special_rate', array( 'srid' => $id ) );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Special Rate - Check Number of Special Rate
| -----------------------------------------------------------------------------
*/
function is_num_special_rate( $conditions = array(), $operator = 'AND' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();
    $n = 0;

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT * FROM ticket_special_rate' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );
    }

    return $n;
}

/*
| -----------------------------------------------------------------------------
| Admin Special Rate - Ajax Function
| -----------------------------------------------------------------------------
*/
function special_rate_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'load-special-rate-data' )
    {
        echo json_encode( get_special_rate_list_query() );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'delete-special-rate' )
    {
        if( delete_special_rate( $_POST[ 'id' ] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }
}

?>