<?php

add_actions( 'pick_trans', 'init_pick_trans' );
add_actions( 'pick_trans_admin_page', 'pick_trans_ajax' );

add_privileges( 'administrator', 'pick_trans', 'insert' );
add_privileges( 'administrator', 'pick_trans', 'update' );
add_privileges( 'administrator', 'pick_trans', 'delete' );

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate
| -----------------------------------------------------------------------------
*/
function init_pick_trans()
{
    run_pick_trans_actions();
    
    //-- Display add new form
    if( is_add_new() )
    {
        return add_pick_trans();
    }
    elseif( is_edit() )
    {
        if( is_contributor() || is_author() )
        {
            if( is_num_pick_trans( array( 'ptid' => $_GET[ 'id' ] ) ) > 0 )
            {
                return edit_pick_trans( $_GET[ 'id' ] );
            }
            else
            {
                return '
                <div class="alert_red_form">
                    You don\'t have an authorization to access this page
                </div>';
            }
        }
        else
        {
            return edit_pick_trans( $_GET[ 'id' ] );
        }
    }
    elseif( is_delete_all() )
    {
        return delete_batch_pick_trans();
    }
    
    //-- Automatic to display add new when there is no records on database
    if( is_num_pick_trans() == 0 )
    {
        header( 'location:' . get_state_url( 'ticket&sub=pick_trans&prc=add_new' ) );
    }
    elseif( is_num_pick_trans() > 0 )
    {
        return get_pick_trans_list();
    }
}

function get_pick_trans_list()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/pick_trans_list.html', 'pick-trans' );

    add_block( 'list-block', 'l-block', 'pick-trans' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'alert', message_block() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_pick_trans_ajax_url() );
    add_variable( 'title', 'Pickup & Transfer Rate List' );
    add_variable( 'view-option', get_pick_trans_view_option() );
    add_variable( 'button', get_pick_trans_admin_button( get_state_url( 'ticket&sub=pick_trans' ) ) );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );
    
    add_actions( 'section_title', 'Pickup & Transfer Rate' );

    parse_template( 'list-block', 'l-block', 'pick-trans' );

    return return_template( 'pick-trans' );
}

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate - Add New Pickup & Transfer Rate
| -----------------------------------------------------------------------------
*/
function add_pick_trans()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/pick_trans_new.html', 'pick-trans' );

    add_block( 'form-block', 'f-block', 'pick-trans' );

    add_variable( 'ftitle', 'Add New Pickup & Transfer Rate' );
    add_variable( 'checked_publish', 'checked' );  
    add_variable( 'checked_unpublish', '' );
    
    add_variable( 'alert', message_block() );
    add_variable( 'rid', ticket_multiple_route_option() );
    add_variable( 'ajax-url', get_pick_trans_ajax_url() );
    add_variable( 'button', get_pick_trans_admin_button( get_state_url( 'ticket&sub=pick_trans' ), true ) );

    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/css/select2.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/js/select2.min.js' );
    
    add_actions( 'section_title', 'Pickup & Transfer Rate - Add New' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'pick-trans' );
}

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate - Edit Pickup & Transfer Rate
| -----------------------------------------------------------------------------
*/
function edit_pick_trans( $id )
{
    $d = fetch_pick_trans( array( 'ptid' => $id ) );
    $t = fetch_pick_trans_routes( array( 'ptid' => $id ), 'AND', 'rid' );

    set_template( PLUGINS_PATH . '/freebird/tpl/pick_trans_new.html', 'pick-trans' );

    add_block( 'form-block', 'f-block', 'pick-trans' );

    add_variable( 'ptid', $d[ 'ptid' ] );
    add_variable( 'rate', $d[ 'rate' ] );
    add_variable( 'max_person', $d[ 'max_person' ] );
    add_variable( 'rid', ticket_multiple_route_option( $t ) );
    add_variable( 'rate_additional', $d[ 'rate_additional' ] );
    add_variable( 'destination_name', $d[ 'destination_name' ] );
    add_variable( 'max_extra_person', $d[ 'max_extra_person' ] );

    add_variable( 'ftitle', 'Edit Pickup & Transfer Rate' );

    if( $d[ 'status' ] == '1' )
    {
        add_variable( 'checked_publish', 'checked' );  
        add_variable( 'checked_unpublish', '' );    
    }
    else
    {
        add_variable( 'checked_publish', '' ); 
        add_variable( 'checked_unpublish', 'checked' );  
    }
    
    add_variable( 'alert', message_block() );
    add_variable( 'ajax-url', get_pick_trans_ajax_url() );
    add_variable( 'button', get_pick_trans_admin_button( get_state_url( 'ticket&sub=pick_trans' ), true ) );

    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/css/select2.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/select2@4.0.10/dist/js/select2.min.js' );
    
    add_actions( 'section_title', 'Pickup & Transfer Rate - Edit' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'pick-trans' );
}

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate - Batch Delete Pickup & Transfer Rate
| -----------------------------------------------------------------------------
*/
function delete_batch_pick_trans()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/pick_trans_batch_delete.html', 'pick-trans-batch-delete' );

    add_block( 'delete-loop-block', 'dl-block', 'pick-trans-batch-delete' );
    add_block( 'delete-block', 'd-block', 'pick-trans-batch-delete' );

    foreach( $_POST[ 'select' ] as $key => $val )
    {
        $d = fetch_pick_trans( array( 'ptid' => $val ) );

        add_variable( 'destination_name', $d[ 'destination_name' ] );
        add_variable( 'ptid', $d[ 'ptid' ] );

        parse_template( 'delete-loop-block', 'dl-block', true );
    }

    add_variable( 'title', 'Batch Delete Pickup & Transfer Rate' );
    add_variable( 'backlink', get_state_url( 'ticket&sub=pick_trans' ) );
    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST[ 'select' ] ) == 1 ? 'this data?' : 'these data?' ) );
    
    add_actions( 'section_title', 'Pickup & Transfer Rate - Batch Delete' );

    parse_template( 'delete-block', 'd-block', 'pick-trans-batch-delete' );

    return return_template( 'pick-trans-batch-delete' );
}

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate - Table Query
| -----------------------------------------------------------------------------
*/
function get_pick_trans_list_query()
{
    global $db;
    
    extract( $_POST );
    
    $post = $_REQUEST;
    $cols  = array(
        1 => 'a.destination_name',
        2 => 'a.created_by',
        3 => 'a.created_date',
        4 => 'a.status', 
        5 => 'a.max_person', 
        6 => 'a.max_extra_person', 
        7 => 'a.rate',  
        8 => 'a.rate_additional', 
    );
    
    //-- Set Order Column
    if ( isset( $post[ 'order' ] ) && !empty( $post[ 'order' ] ) )
    {
        $o = array();
        
        foreach ( $post[ 'order' ] as $i => $od )
        {
            if ( isset( $cols[ $post[ 'order' ][ $i ][ 'column' ] ] ) )
            {
                $o[] = $cols[ $post[ 'order' ][ $i ][ 'column' ] ] . ' ' . $post[ 'order' ][ $i ][ 'dir' ];
            }
        }
        
        $order = !empty( $o ) ? ' ORDER BY ' . implode( ', ', $o ) : '';
    }
    else
    {
        $order = ' ORDER BY a.destination_name ASC';
    }

    //-- Set Condition
    $w = array();

    if( isset( $post[ 'show' ] ) && $post[ 'show' ] != '' && $post[ 'show' ] != 'all' )
    {
        $w[] = $db->prepare_query( 'status = %s', $show );
    }

    if( empty( $post[ 'search' ][ 'value' ] ) === false )
    {        
        $s = array();

        foreach ( $cols as $col )
        {
            $s[] = $db->prepare_query( $col . ' LIKE %s', '%' . $post[ 'search' ][ 'value' ] . '%' );
        }

        $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE ' . implode( ' AND ', $w );
    }
    else
    {
        $where = '';
    }

    $q = 'SELECT a.*, ( 
            SELECT GROUP_CONCAT(b2.rname SEPARATOR  "<br/>")
            FROM ticket_pick_trans_routes AS a2
            LEFT JOIN ticket_route AS b2 ON a2.rid = b2.rid
            WHERE a2.ptid = a.ptid
            GROUP BY a2.ptid
         ) AS routes 
         FROM ticket_pick_trans_rate AS a' . $where . $order;
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );
    
    $q2 = $q . ' LIMIT ' . $post[ 'start' ] . ', ' . $post[ 'length' ];
    $r2 = $db->do_query( $q2 );
    $n2 = $db->num_rows( $r2 );
    
    $data = array();
    
    if ( $n2 > 0 )
    {
        while ( $d2 = $db->fetch_array( $r2 ) )
        {
            $uid = fetch_user_id_by_username( $d2[ 'created_by' ] );

            $data[] = array(
                'destination_name' => $d2[ 'destination_name' ],
                'max_extra_person' => $d2[ 'max_extra_person' ],
                'rate_additional'  => $d2[ 'rate_additional' ],
                'created_by'       => $d2[ 'created_by' ],
                'max_person'       => $d2[ 'max_person' ],
                'status'           => $d2[ 'status' ],
                'routes'           => $d2[ 'routes' ],
                'rate'             => $d2[ 'rate' ],
                'ptid'             => $d2[ 'ptid' ],
                'user_link'        => user_url( $uid ),
                'avatar'           => get_avatar( $uid, 3 ),
                'ajax_link'        => get_pick_trans_ajax_url(),
                'created_date'     => date( 'd F Y', $d2[ 'created_date' ] ),
                'edit_link'        => get_state_url( 'ticket&sub=pick_trans&prc=edit&id=' . $d2[ 'ptid' ] )
            );
        }
    }
    else
    {
        $n = 0;
    }
    
    return array(
        'draw' => intval( $post[ 'draw' ] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data 
    );
}

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate - Action Button
| -----------------------------------------------------------------------------
*/
function get_pick_trans_admin_button( $new_url = '', $is_form = false )
{
    if( $is_form )
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=cancel', $new_url ) . '</li>
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=publish' ) . '</li>                 
            <li>' . button( 'button=cancel', $new_url ) . '</li>  
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
    }
    else
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>
            <li>' . button( 'button=publish&type=submit&enable=false' ) . '</li>
            <li>' . button( 'button=unpublish&type=submit&enable=false' ) . '</li>';
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate - View Option
| -----------------------------------------------------------------------------
*/
function get_pick_trans_view_option()
{
    $opt_viewed   = '';    
    $show_data    = isset( $_POST[ 'data_to_show' ] ) ? $_POST[ 'data_to_show' ] : ( isset( $_GET[ 'data_to_show' ] ) ? $_GET[ 'data_to_show' ] : 'all' );
    $data_to_show = array( 'all' => 'All', '1' => 'Publish', '0' => 'Unpublish' );
    
    foreach( $data_to_show as $key => $val )
    {
        if( isset( $show_data ) && !empty( $show_data ) )
        {
            if( $show_data === $key )
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" checked="checked" autocomplete="off" />
                <label>' . $val . '</label>';
            }
            else
            {
                $opt_viewed .= '
                <input type="radio" name="data_to_show" value="' . $key . '" autocomplete="off" />
                <label>' . $val . '</label>';
            }
        }
        else
        {
            $opt_viewed .= '
            <input type="radio" name="data_to_show" value="' . $key . '" autocomplete="off" />
            <label>' . $val . '</label>';
        }
    }

    return $opt_viewed;
}

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_pick_trans_ajax_url()
{
    return get_state_url( 'ajax&apps=pick_trans' );
}

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate Actions
| -----------------------------------------------------------------------------
*/
function run_pick_trans_actions()
{
    global $db;
    global $flash;

    //-- Publish, Unpublish
    //-- Actions From List Pickup & Transfer Rate
    if( isset( $_POST[ 'select' ] ) )
    {
        if( is_publish() || is_unpublish() )
        {
            $count  = count( $_POST[ 'select' ] );
            $status = is_unpublish() ? '0' : '1';
            $error  = 0;

            foreach( $_POST[ 'select' ] as $key => $val )
            {
                if( update_pick_trans( array( 'status' => $status ), array( 'ptid' => $val ) ) === false )
                {
                    $error++;
                }
            }

            if( $error > 0 )
            {
                if( $error == $count )
                {
                    $flash->add( array( 'type' => 'error', 'content' => array( 'Failed to update all pickup & transfer rate status' ) ) );
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => array( 'Failed to update some of pickup & transfer rate status' ) ) );
                }

                header( 'location: ' . get_state_url( 'ticket&sub=pick_trans' ) );

                exit;
            }
            else
            {
                $flash->add( array( 'type' => 'success', 'content' => array( 'Successfully update all selected pickup & transfer rate status' ) ) );
                
                header( 'location: ' . get_state_url( 'ticket&sub=pick_trans' ) );

                exit;
            }
        }
    }
    elseif( is_confirm_delete() )
    {
        $count  = count( $_POST[ 'id' ] );
        $error = 0;

        foreach( $_POST[ 'id' ] as $key => $val )
        {
            if( delete_pick_trans( $val ) === false )
            {
                $error++;
            }
        }

        if( $error > 0 )
        {
            if( $error == $count )
            {
                $flash->add( array( 'type' => 'error', 'content' => array( 'Failed to delete all selected pickup and transfer rate' ) ) );
            }
            else
            {
                $flash->add( array( 'type' => 'error', 'content' => array( 'Failed to update some of selected pickup and transfer rate' ) ) );
            }

            header( 'location: ' . get_state_url( 'ticket&sub=pick_trans' ) );

            exit;
        }
        else
        {
            $flash->add( array( 'type' => 'success', 'content' => array( 'Successfully delete all selected pickup and transfer rate' ) ) );
            
            header( 'location: ' . get_state_url( 'ticket&sub=pick_trans' ) );

            exit;
        }
    }
    else
    {
        if( is_save_draft() || is_publish() )
        {
            if( is_add_new() )
            {
                if( save_pick_trans() )
                {
                    $flash->add( array( 'type' => 'success', 'content' => array( 'Successfully add new pickup and transfer rate' ) ) );
                    
                    header( 'location: ' . get_state_url( 'ticket&sub=pick_trans&prc=add_new' ) );

                    exit;
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => array( 'Failed to add new pickup and transfer rate' ) ) );
                }
            }
            elseif( is_edit() )
            {
                if( update_pick_trans() )
                {
                    $flash->add( array( 'type' => 'success', 'content' => array( 'Successfully updated pickup and transfer rate data' ) ) );
                    
                    header( 'location: ' . get_state_url( 'ticket&sub=pick_trans&prc=edit&id=' . $_GET[ 'id' ] ) );

                    exit;
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => array( 'Failed to update pickup and transfer rate data' ) ) );
                }
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate - Save Pickup & Transfer Rate
| -----------------------------------------------------------------------------
*/
function fetch_pick_trans( $conditions = array(), $operator = 'AND',  $fields = '*' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( is_array( $fields ) && empty( $fields ) === false )
    {
        $param = implode( ', ', $fields );
    }
    else
    {
        $param = $fields;
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT ' . $param . ' FROM ticket_pick_trans_rate' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );

        if( $n > 1 )
        {
            while( $d = $db->fetch_assoc( $r ) )
            {
                $data[] = $d;
            }
        }
        elseif( $n > 0 )
        {
            $data = $db->fetch_assoc( $r );

            if( $fields != '*' && is_string( $fields ) && isset( $data[ $fields ] ) )
            {
                $data = $data[ $fields ];
            }
        }

        return $data;
    }
}

function fetch_pick_trans_routes( $conditions = array(), $operator = 'AND',  $fields = '*' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( is_array( $fields ) && empty( $fields ) === false )
    {
        $param = implode( ', ', $fields );
    }
    else
    {
        $param = $fields;
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT ' . $param . ' FROM ticket_pick_trans_routes' . $where );

    if( is_array( $r ) === false )
    {
        $data = array();

        while( $d = $db->fetch_assoc( $r ) )
        {
            if( $fields != '*' && is_string( $fields ) && isset( $d[ $fields ] ) )
            {
                $d = $d[ $fields ];
            }

            $data[] = $d;
        }

        return $data;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate - Save Pickup & Transfer Rate
| -----------------------------------------------------------------------------
*/
function save_pick_trans( $params = array() )
{
    global $db;

    $fields = array(
        'destination_name',
        'rate_additional',
        'max_extra_person',
        'created_date',
        'created_by',
        'max_person',
        'status',
        'rate',
        'dlu',
    );

    if( empty( $params ) )
    {
        $params = $_POST;
    }

    if( empty( $params ) || !isset( $params[ 'rid' ] ) || ( isset( $params[ 'rid' ] ) && empty( $params[ 'rid' ] ) ) )
    {
        return false;
    }
    else
    {
        $db->begin();
    
        $res = 1;

        //-- Insert pickup and transfer rate data
        $data = array( 
            'created_by'   => $_COOKIE[ 'username' ],
            'created_date' => time(), 
            'dlu'          => time()
        );

        foreach( $params as $f => $d )
        {
            if( in_array( $f, $fields ) )
            {
                $data[ $f ] = $d;
            }
        }

        $r = $db->insert( 'ticket_pick_trans_rate', $data );

        if( is_array( $r ) )
        {
            $res = 0;
        }
        else
        {
            $ptid = $db->insert_id();

            foreach( $params[ 'rid' ] as $rid )
            {
                //-- Add new routes on pickup trans route
                $r = $db->insert( 'ticket_pick_trans_routes', array( 'ptid' => $ptid, 'rid' => $rid ) );

                if( is_array( $r ) )
                {
                    $res = 0;

                    break;
                }
            }
        }

        if( $res == 0 )
        {
            $db->rollback();

            return false;
        }
        else
        {
            $db->commit();

            return true;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate - Update Pickup & Transfer Rate
| -----------------------------------------------------------------------------
*/
function update_pick_trans( $params = array(), $where = array() )
{
    global $db;

    $fields = array(
        'destination_name',
        'rate_additional',
        'max_extra_person',
        'created_date',
        'created_by',
        'max_person',
        'status',
        'rate',
        'dlu',
    );

    if( empty( $params ) )
    {
        $params = $_POST;
    }

    if( empty( $where ) )
    {
        $where = array( 'ptid' => $_GET[ 'id' ] );
    }

    if( empty( $params ) || !isset( $params[ 'rid' ] ) || ( isset( $params[ 'rid' ] ) && empty( $params[ 'rid' ] ) ) )
    {
        return false;
    }
    else
    {
        $db->begin();
    
        $res = 1;

        //-- Update pickup and transfer rate data
        $data = array( 'dlu' => time() );

        foreach( $params as $f => $d )
        {
            if( in_array( $f, $fields ) )
            {
                $data[ $f ] = $d;
            }
        }

        $r = $db->update( 'ticket_pick_trans_rate', $data, $where );

        if( is_array( $r ) )
        {
            $res = 0;
        }
        else
        {
            //-- Delete existing routes on pickup trans route
            $r = $db->delete( 'ticket_pick_trans_routes', $where );

            if( is_array( $r ) )
            {
                $res = 0;
            }
            else
            {
                foreach( $params[ 'rid' ] as $rid )
                {
                    //-- Add new routes on pickup trans route
                    $r = $db->insert( 'ticket_pick_trans_routes', array( 'ptid' => $_GET[ 'id' ], 'rid' => $rid ) );

                    if( is_array( $r ) )
                    {
                        $res = 0;

                        break;
                    }
                }
            }
        }

        if( $res == 0 )
        {
            $db->rollback();

            return false;
        }
        else
        {
            $db->commit();

            return true;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate - Delete Pickup & Transfer Rate
| -----------------------------------------------------------------------------
*/
function delete_pick_trans( $id )
{
    global $db;

    $r = $db->delete( 'ticket_pick_trans_rate', array( 'ptid' => $id ) );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate - Check Number of Pickup & Transfer Rate
| -----------------------------------------------------------------------------
*/
function is_num_pick_trans( $conditions = array(), $operator = 'AND' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();
    $n = 0;

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT * FROM ticket_pick_trans_rate' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );
    }

    return $n;
}

/*
| -----------------------------------------------------------------------------
| Admin Pickup & Transfer Rate - Ajax Function
| -----------------------------------------------------------------------------
*/
function pick_trans_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'load-pick-trans-data' )
    {
        echo json_encode( get_pick_trans_list_query() );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'delete-pick-trans' )
    {
        if( delete_pick_trans( $_POST[ 'id' ] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }
}

?> 