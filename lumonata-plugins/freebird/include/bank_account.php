<?php

add_actions( 'bank_account', 'init_bank_account' );
add_actions( 'bank_account_admin_page', 'bank_account_ajax' );

add_privileges( 'administrator', 'bank_account', 'insert' );
add_privileges( 'administrator', 'bank_account', 'update' );
add_privileges( 'administrator', 'bank_account', 'delete' );

/*
| -----------------------------------------------------------------------------
| Admin Bank Account
| -----------------------------------------------------------------------------
*/
function init_bank_account()
{
    run_bank_account_actions();
    
    //-- Display add new form
    if( is_add_new() )
    {
        return add_bank_account();
    }
    elseif( is_edit() )
    {
        if( is_contributor() || is_author() )
        {
            if( is_num_bank_account( array( 'account_id' => $_GET[ 'id' ] ) ) > 0 )
            {
                return edit_bank_account( $_GET[ 'id' ] );
            }
            else
            {
                return '
                <div class="alert_red_form">
                    You don\'t have an authorization to access this page
                </div>';
            }
        }
        else
        {
            return edit_bank_account( $_GET[ 'id' ] );
        }
    }
    elseif( is_delete_all() )
    {
        return delete_batch_bank_account();
    }
    
    //-- Automatic to display add new when there is no records on database
    if( is_num_bank_account() == 0 )
    {
        header( 'location:' . get_state_url( 'ticket&sub=bank_account&prc=add_new' ) );
    }
    elseif( is_num_bank_account() > 0 )
    {
        return get_bank_account_list();
    }
}

function get_bank_account_list()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/bank_account_list.html', 'bank-account' );

    add_block( 'list-block', 'l-block', 'bank-account' );

    add_variable( 'limit', post_viewed() );
    add_variable( 'alert', message_block() );
    add_variable( 'img-url', get_theme_img() );
    add_variable( 'ajax-url', get_bank_account_ajax_url() );
    add_variable( 'button', get_bank_account_admin_button( get_state_url( 'ticket&sub=bank_account' ) ) );
    add_variable( 'title', 'Bank Account List' );
    
    add_actions( 'css_elements', 'get_custom_css', '//cdn.jsdelivr.net/npm/datatables.net-dt@1.12.1/css/jquery.dataTables.min.css' );
    add_actions( 'js_elements', 'get_custom_javascript', '//cdn.jsdelivr.net/npm/datatables.net@1.12.1/js/jquery.dataTables.min.js' );
    
    add_actions( 'section_title', 'Bank Accounts' );

    parse_template( 'list-block', 'l-block', 'bank-account' );

    return return_template( 'bank-account' );
}

/*
| -----------------------------------------------------------------------------
| Admin Bank Account - Add New Bank Account
| -----------------------------------------------------------------------------
*/
function add_bank_account()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/bank_account_new.html', 'bank-account' );

    add_block( 'form-block', 'f-block', 'bank-account' );
    
    add_variable( 'alert', message_block() );
    add_variable( 'ajax-url', get_bank_account_ajax_url() );
    add_variable( 'button', get_bank_account_admin_button( get_state_url( 'ticket&sub=bank_account' ), true ) );
    add_variable( 'ftitle', 'Add New Bank Account' );
    
    add_actions( 'section_title', 'Bank Account - Add New' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'bank-account' );
}

/*
| -----------------------------------------------------------------------------
| Admin Bank Account - Edit Bank Account
| -----------------------------------------------------------------------------
*/
function edit_bank_account( $id )
{
    $d = fetch_bank_account( array( 'account_id' => $id ) );

    set_template( PLUGINS_PATH . '/freebird/tpl/bank_account_new.html', 'bank-account' );

    add_block( 'form-block', 'f-block', 'bank-account' );

    add_variable( 'name', $d[ 'name' ] );
    add_variable( 'branch', $d[ 'branch' ] );
    add_variable( 'address', $d[ 'address' ] );
    add_variable( 'bank_name', $d[ 'bank_name' ] );
    add_variable( 'swift_code', $d[ 'swift_code' ] );
    add_variable( 'account_no', $d[ 'account_no' ] );
    add_variable( 'account_id', $d[ 'account_id' ] );

    if( $d[ 'currency' ] == 'USD' )
    {
        add_variable( 'selected_usd', 'selected' );  
        add_variable( 'selected_idr', '' );    
    }
    else
    {
        add_variable( 'selected_usd', '' ); 
        add_variable( 'selected_idr', 'selected' );  
    }
    
    add_variable( 'alert', message_block() );
    add_variable( 'ajax-url', get_bank_account_ajax_url() );
    add_variable( 'button', get_bank_account_admin_button( get_state_url( 'ticket&sub=bank_account' ), true ) );
    add_variable( 'ftitle', 'Edit Bank Account' );
    
    add_actions( 'section_title', 'Bank Account - Edit' );

    parse_template( 'form-block', 'f-block', false );

    return return_template( 'bank-account' );
}

/*
| -----------------------------------------------------------------------------
| Admin Bank Account - Batch Delete Bank Account
| -----------------------------------------------------------------------------
*/
function delete_batch_bank_account()
{
    set_template( PLUGINS_PATH . '/freebird/tpl/bank_account_batch_delete.html', 'bank-account-batch-delete' );

    add_block( 'delete-loop-block', 'dl-block', 'bank-account-batch-delete' );
    add_block( 'delete-block', 'd-block', 'bank-account-batch-delete' );

    foreach( $_POST[ 'select' ] as $key => $val )
    {
        $d = fetch_bank_account( array( 'account_id' => $val ) );

        add_variable( 'account_id', $d[ 'account_id' ] );
        add_variable( 'name', sprintf( '%s - %s', $d[ 'name' ], $d[ 'account_no' ] ) );

        parse_template( 'delete-loop-block', 'dl-block', true );
    }

    add_variable( 'title', 'Batch Delete Bank Account' );
    add_variable( 'backlink', get_state_url( 'ticket&sub=bank_account' ) );
    add_variable( 'message', 'Are you sure want to delete ' . ( count( $_POST[ 'select' ] ) == 1 ? 'this bank account?' : 'these bank accounts?' ) );
    
    add_actions( 'section_title', 'Bank Account - Batch Delete' );

    parse_template( 'delete-block', 'd-block', 'bank-account-batch-delete' );

    return return_template( 'bank-account-batch-delete' );
}

/*
| -----------------------------------------------------------------------------
| Admin Bank Account - Table Query
| -----------------------------------------------------------------------------
*/
function get_bank_account_list_query()
{
    global $db;
    
    extract( $_POST );
    
    $post = $_REQUEST;
    $cols  = array(
        1 => 'a.name',
        2 => 'a.bank_name',
        3 => 'a.branch',
        4 => 'a.account_no',
        5 => 'a.swift_code',
        6 => 'a.currency',
        7 => 'a.created_by',
        8 => 'a.created_date'
    );
    
    //-- Set Order Column
    if ( isset( $post[ 'order' ] ) && !empty( $post[ 'order' ] ) )
    {
        $o = array();
        
        foreach ( $post[ 'order' ] as $i => $od )
        {
            if ( isset( $cols[ $post[ 'order' ][ $i ][ 'column' ] ] ) )
            {
                $o[] = $cols[ $post[ 'order' ][ $i ][ 'column' ] ] . ' ' . $post[ 'order' ][ $i ][ 'dir' ];
            }
        }
        
        $order = !empty( $o ) ? ' ORDER BY ' . implode( ', ', $o ) : '';
    }
    else
    {
        $order = ' ORDER BY a.account_id ASC';
    }

    //-- Set Condition
    $w = array();

    if( empty( $post[ 'search' ][ 'value' ] ) === false )
    {        
        $s = array();

        foreach ( $cols as $col )
        {
            $s[] = $db->prepare_query( $col . ' LIKE %s', '%' . $post[ 'search' ][ 'value' ] . '%' );
        }

        $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE ' . implode( ' AND ', $w );
    }
    else
    {
        $where = '';
    }

    $q = 'SELECT * FROM ticket_bank_account AS a' . $where . $order;
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );
    
    $q2 = $q . ' LIMIT ' . $post[ 'start' ] . ', ' . $post[ 'length' ];
    $r2 = $db->do_query( $q2 );
    $n2 = $db->num_rows( $r2 );
    
    $data = array();
    
    if ( $n2 > 0 )
    {
        $url = get_state_url( 'ticket&sub=bank_account' );
        
        while ( $d2 = $db->fetch_array( $r2 ) )
        {
            $uid = fetch_user_id_by_username( $d2[ 'created_by' ] );

            $data[] = array(
                'created_by'   => $d2[ 'created_by' ],
                'account_id'   => $d2[ 'account_id' ],
                'account_no'   => $d2[ 'account_no' ],
                'swift_code'   => $d2[ 'swift_code' ],
                'bank_name'    => $d2[ 'bank_name' ],
                'currency'     => $d2[ 'currency' ],
                'branch'       => $d2[ 'branch' ],
                'name'         => $d2[ 'name' ],
                'user_link'    => user_url( $uid ),
                'ajax_link'    => get_bank_account_ajax_url(),
                'avatar'       => get_avatar( $uid, 3 ),
                'created_date' => date( 'd F Y', $d2[ 'created_date' ] ),
                'edit_link'    => get_state_url( 'ticket&sub=bank_account&prc=edit&id=' . $d2[ 'account_id' ] )
            );
        }
    }
    else
    {
        $n = 0;
    }
    
    return array(
        'draw' => intval( $post[ 'draw' ] ),
        'recordsTotal' => intval( $n ),
        'recordsFiltered' => intval( $n ),
        'data' => $data 
    );
}

/*
| -----------------------------------------------------------------------------
| Admin Bank Account - Action Button
| -----------------------------------------------------------------------------
*/
function get_bank_account_admin_button( $new_url = '', $is_form = false )
{
    if( $is_form )
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=cancel', $new_url ) . '</li>
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=publish' ) . '</li>                 
            <li>' . button( 'button=cancel', $new_url ) . '</li>  
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new'  ) . '</li>';
        }
    }
    else
    {
        if( is_contributor() )
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
        }
        else
        {
            return '
            <li>' . button( 'button=add_new', $new_url . '&prc=add_new' ) . '</li>
            <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Bank Account - Ajax URL
| -----------------------------------------------------------------------------
*/
function get_bank_account_ajax_url()
{
    return get_state_url( 'ajax&apps=bank_account' );
}

/*
| -----------------------------------------------------------------------------
| Admin Bank Account Actions
| -----------------------------------------------------------------------------
*/
function run_bank_account_actions()
{
    global $db;
    global $flash;

    if( is_confirm_delete() )
    {
        $count  = count( $_POST[ 'id' ] );
        $error = 0;

        foreach( $_POST[ 'id' ] as $key => $val )
        {
            if( delete_bank_account( $val ) === false )
            {
                $error++;
            }
        }

        if( $error > 0 )
        {
            if( $error == $count )
            {
                $flash->add( array( 'type' => 'error', 'content' => 'Failed to delete all selected bank account' ) );
            }
            else
            {
                $flash->add( array( 'type' => 'error', 'content' => 'Failed to update some of selected bank account' ) );
            }

            header( 'location: ' . get_state_url( 'ticket&sub=bank_account' ) );

            exit;
        }
        else
        {
            $flash->add( array( 'type' => 'success', 'content' => 'Successfully delete all selected bank account' ) );
            
            header( 'location: ' . get_state_url( 'ticket&sub=bank_account' ) );

            exit;
        }
    }
    else
    {
        if( is_save_draft() || is_publish() )
        {
            if( is_add_new() )
            {
                if( save_bank_account() )
                {
                    $flash->add( array( 'type' => 'success', 'content' => 'Successfully add new bank account' ) );
                    
                    header( 'location: ' . get_state_url( 'ticket&sub=bank_account&prc=add_new' ) );

                    exit;
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to add new bank account' ) );
                }
            }
            elseif( is_edit() )
            {
                if( update_bank_account() )
                {
                    $flash->add( array( 'type' => 'success', 'content' => 'Successfully updated bank account data' ) );
                    
                    header( 'location: ' . get_state_url( 'ticket&sub=bank_account&prc=edit&id=' . $_GET[ 'id' ] ) );

                    exit;
                }
                else
                {
                    $flash->add( array( 'type' => 'error', 'content' => 'Failed to update bank account data' ) );
                }
            }
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Bank Account - Save Bank Account
| -----------------------------------------------------------------------------
*/
function fetch_bank_account( $conditions = array(), $operator = 'AND',  $fields = '*' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( is_array( $fields ) && empty( $fields ) === false )
    {
        $param = implode( ', ', $fields );
    }
    else
    {
        $param = $fields;
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT ' . $param . ' FROM ticket_bank_account' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );

        if( $n > 1 )
        {
            while( $d = $db->fetch_assoc( $r ) )
            {
                $data[] = $d;
            }
        }
        elseif( $n > 0 )
        {
            $data = $db->fetch_assoc( $r );

            if( $fields != '*' && is_string( $fields ) && isset( $data[ $fields ] ) )
            {
                $data = $data[ $fields ];
            }
        }

        return $data;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Bank Account - Save Bank Account
| -----------------------------------------------------------------------------
*/
function save_bank_account( $params = array() )
{
    global $db;

    $fields = array(
        'created_date',
        'created_by',
        'account_no',
        'swift_code',
        'bank_name',
        'currency',
        'address',
        'branch',
        'name',
        'dlu',
    );

    if( empty( $params ) )
    {
        $params = $_POST;
    }

    if( empty( $params ) )
    {
        return false;
    }
    else
    {
        //-- Insert bank account data
        $data = array( 
            'created_by'   => $_COOKIE[ 'username' ],
            'created_date' => time(), 
            'dlu'          => time()
        );

        foreach( $params as $f => $d )
        {
            if( in_array( $f, $fields ) )
            {
                $data[ $f ] = $d;
            }
        }

        $r = $db->insert( 'ticket_bank_account', $data );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Bank Account - Update Bank Account
| -----------------------------------------------------------------------------
*/
function update_bank_account( $params = array(), $where = array() )
{
    global $db;

    $fields = array(
        'created_date',
        'created_by',
        'account_no',
        'swift_code',
        'bank_name',
        'currency',
        'address',
        'branch',
        'name',
        'dlu',
    );

    if( empty( $params ) )
    {
        $params = $_POST;
    }

    if( empty( $where ) )
    {
        $where = array( 'account_id' => $_GET[ 'id' ] );
    }

    if( empty( $params ) )
    {
        return false;
    }
    else
    {
        //-- Update bank account data
        $data = array( 'dlu' => time() );

        foreach( $params as $f => $d )
        {
            if( in_array( $f, $fields ) )
            {
                $data[ $f ] = $d;
            }
        }

        $r = $db->update( 'ticket_bank_account', $data, $where );

        if( is_array( $r ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Bank Account - Delete Bank Account
| -----------------------------------------------------------------------------
*/
function delete_bank_account( $id )
{
    global $db;

    $r = $db->delete( 'ticket_bank_account', array( 'account_id' => $id ) );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*
| -----------------------------------------------------------------------------
| Admin Bank Account - Check Number of Bank Account
| -----------------------------------------------------------------------------
*/
function is_num_bank_account( $conditions = array(), $operator = 'AND' )
{
    global $db;

    //-- PREPARE parameter
    $w = array();
    $n = 0;

    if( is_array( $conditions ) && empty( $conditions ) === false )
    {
        foreach( $conditions as $field => $value )
        {
            if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
            {
                $field .= ' = ';
            }

            $w[] = $db->prepare_query( $field . '%s', $value );
        }
    }

    if( empty( $w ) === false )
    {
        $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
    }
    else
    {
        $where = '';
    }

    $r = $db->do_query( 'SELECT * FROM ticket_bank_account' . $where );

    if( is_array( $r ) === false )
    {
        $n = $db->num_rows( $r );
    }

    return $n;
}

/*
| -----------------------------------------------------------------------------
| Admin Bank Account - Ajax Function
| -----------------------------------------------------------------------------
*/
function bank_account_ajax()
{
    add_actions( 'is_use_ajax', true );

    if( !is_user_logged() )
    {
        exit( 'You have to login to access this page!' );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'load-bank-account-data' )
    {
        echo json_encode( get_bank_account_list_query() );
    }

    if( isset( $_POST[ 'pkey' ] ) and $_POST[ 'pkey' ] == 'delete-bank-account' )
    {
        if( delete_bank_account( $_POST[ 'id' ] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }
}

?> 