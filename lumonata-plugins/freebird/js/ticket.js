jQuery(document).ready(function(){
    jQuery('[name=use_transport]').on( 'change', function(){
        if( this.checked )
        {
            jQuery('.transport .required-old').each(function(){
                jQuery(this).removeAttr('disabled').removeClass('error');
            });
        }
        else
        {
            jQuery('.transport .required-old').each(function() {
                jQuery(this).attr('disabled', 'disabled').removeClass('error').val('');
                jQuery('.cost-value').val(0);
                jQuery('.cost').text('');
            });
        }
    });

    jQuery('.dest_loc').change(function() {
    	var form = jQuery('[name=f-booking]');
        var url  = form.data('url') + 'get-pickup-transfer-cost';
        var sel  = jQuery(this);

        if( sel.val() == '' )
        {
            sel.parent().parent().parent().find('.cost').text( 0 );
            sel.parent().parent().parent().find('.cost-value').val( 0 );
        }
        else
        {
	        var prm = new Object;
                prm.dep_sid   = form.find('[name=dep_sid]:checked').val();
                prm.ret_sid   = form.find('[name=ret_sid]:checked').val();
		        prm.adult_num = form.find('[name=adult]').val();
		        prm.child_num = form.find('[name=child]').val();
		        prm.from      = form.find('[name=from]').val();
		        prm.dest_loc  = sel.val();

            jQuery.ajax({
                url: url,
                data: prm,
                type: 'POST',
                dataType: 'json',
                beforeSend: function( xhr ){
                    form.find('.send-book-inquiry').prop('disabled', true);
                },
                success: function( e ){
	                if( e.result == 'success' )
	                {
	                    sel.parent().parent().parent().find('.cost').text( e.cost_txt );
	                    sel.parent().parent().parent().find('.cost-value').val( e.cost );
	                }
	                else
	                {
	                    sel.parent().parent().parent().find('.cost').text( 0 );
	                    sel.parent().parent().parent().find('.cost-value').val( 0 );
	                }
                },
                complete: function( e ){
                	form.find('.send-book-inquiry').prop('disabled', false);
                },
                error: function( e ){
                    sel.parent().parent().parent().find('.cost').text( 0 );
                    sel.parent().parent().parent().find('.cost-value').val( 0 );

                	form.find('.send-book-inquiry').prop('disabled', false);
                }
            });
        }
    });

    jQuery('#f-booking').validate({
    	errorClass: 'invalid-field',
		submitHandler: function( form ){
			form.submit();
		},
		errorPlacement: function( error, element ){
			error.insertBefore( element );
		},
		rules: {
			add_pickup: {
				required: {
					depends: function(element) {
						return jQuery('[name=pickup_dest]').val() != '';
					}
				}
			},
			add_transfer: {
				required: {
					depends: function(element) {
						return jQuery('[name=trans_dest]').val() != '';
					}
				}
			}
		}
	});
});