<?php

class ticketAvailability
{
    var $rid, $sid, $sparent, $from, $to, $date;
    var $num_adult, $num_child, $num_passenger;
    var $node, $node_number, $node_code;
    var $node_from, $node_to;
    var $allotment;

    function __construct( $rid, $sid, $date, $num_adult, $num_child )
    {
        $this->rid           = $rid;
        $this->sid           = $sid;
        $this->date          = $date;
        $this->num_adult     = $num_adult;
        $this->num_child     = $num_child;
        $this->num_passenger = $num_adult + $num_child;

		$this->node 		 = $this->get_node();
		$this->node_code	 = $this->get_node_code();
		$this->node_number   = $this->get_node_number();
        $this->allotment     = $this->get_allotment( $sid );
        $this->to            = $this->get_route_by_id( $rid, 'rto' );
        $this->from          = $this->get_route_by_id( $rid, 'rfrom' );
        $this->sparent       = $this->get_schedule_by_id( $sid, 'sparent' );

        $this->node_to       = array_search( strtolower( $this->to ), array_map( 'strtolower', $this->node ) );
        $this->node_from     = array_search( strtolower( $this->from ), array_map( 'strtolower', $this->node ) );
    }

	/**
	 * Get Trip Location
	 * 
	 * 
	 * @since 1.0.0
	 * @return array List of trip location.
	 */
	function get_node()
	{
		return array( 
			'a' => 'Amed', 
			'b' => 'Gili Trawangan', 
			'c' => 'Gili Meno',
			'd' => 'Gili Air',
			'e' => 'Bangsal',
			'f' => 'Padangbai'
		);
	}

	/**
	 * Get Trip Location Number
	 * 
	 * 
	 * @since 1.0.0
	 * @return array List of trip location number.
	 */
	function get_node_number()
	{
		return array(
			0 => 'a',
			1 => 'b',
			2 => 'c',
			3 => 'd',
			4 => 'e',
			5 => 'f'
		);
	}

	/**
	 * Get Trip Location Code
	 * 
	 * 
	 * @since 1.0.0
	 * @return array List of trip location code.
	 */
	function get_node_code()
	{
		return array(
			'a' => 0,
			'b' => 1,
			'c' => 2,
			'd' => 3,
			'e' => 4,
			'f' => 5
		);
	}

	/**
	 * Get Schedule Data
	 * 
	 * 
	 * @since 1.0.0
	 * @return array List of schedule data
	 */
	function get_schedule_by_id( $sid, $field = '' )
	{
		global $db;

		$q = $db->prepare_query( 'SELECT * FROM ticket_schedule AS a WHERE a.sid = %d', $sid );
	 	$r = $db->do_query( $q );
	 	$d = $db->fetch_array( $r );

		if( $field != '' && isset( $d[ $field ] ) )
		{
			return $d[ $field ];
		}
		else
		{
			return $d;
		}
	}

	/**
	 * Get Route Data
	 * 
	 * 
	 * @since 1.0.0
	 * @return array List of route data
	 */
	function get_route_by_id( $rid, $field = '' )
	{
		global $db;

		$q = $db->prepare_query( 'SELECT * FROM ticket_route AS a WHERE a.rid = %d', $rid );
	 	$r = $db->do_query( $q );
	 	$d = $db->fetch_array( $r );

		if( $field != '' && isset( $d[ $field ] ) )
		{
			return $d[ $field ];
		}
		else
		{
			return $d;
		}
	}

	/**
	 * Get Route Data By Location
	 * 
	 * 
	 * @since 1.0.0
	 * @return array List of route data
	 */
	function get_route_by_node( $from, $to, $field = '' )
	{
		global $db;

		$q = $db->prepare_query( 'SELECT * FROM ticket_route AS a WHERE a.rfrom = %s AND a.rto = %s AND a.status = %d LIMIT 1', $from, $to, 1 );
	 	$r = $db->do_query( $q );
	 	$d = $db->fetch_array( $r );

		if( $field != '' && isset( $d[ $field ] ) )
		{
			return $d[ $field ];
		}
		else
		{
			return $d;
		}
	}

	/**
	 * Get Allotment By Schedule
	 * 
	 * 
	 * @since 1.0.0
	 * @return integer Number of allotment
	 */
	function get_allotment( $sid )
	{
		global $db;

		$q = $db->prepare_query( 'SELECT * FROM ticket_schedule AS a WHERE a.sid = %d', $sid );
	 	$r = $db->do_query( $q );
	 	$d = $db->fetch_array( $r );

	 	if( $d[ 'sallotment' ] == 0 )
	 	{
	 		return $this->get_allotment_from_route( $d[ 'rid' ] );
	 	}
	 	else
	 	{
	 		return $d[ 'sallotment' ];
	 	}
	}

	/**
	 * Get Allotment By Route
	 * 
	 * 
	 * @since 1.0.0
	 * @return integer Number of allotment
	 */
	function get_allotment_from_route( $rid )
	{
		global $db;

		$q = $db->prepare_query( 'SELECT * FROM ticket_route AS a WHERE a.rid = %d', $rid );
	 	$r = $db->do_query( $q );
	 	$d = $db->fetch_array( $r );

	 	return $d[ 'rallotment' ];
	}

	/**
	 * Get Total Booking By Destination
	 * 
	 * 
	 * @since 1.0.0
	 * @return integer Total Booking
	 */
    function get_total_booking_to_destination( $node_to )
    {
        global $db;

        $q  = $db->prepare_query( 'SELECT * FROM ticket_booking_detail AS a WHERE a.status = %s AND a.sparent = %d AND a.date = %s AND a.rto = %s', 'pd', $this->sid, $this->sparent, $this->date, $this->node[ $node_to ] );
        $r  = $db->do_query( $q );
        $n  = $db->num_rows( $r );

        if( $n == 0 )
        {
            return $n;
        }
        else
        {
            $n = 0;

            while( $d = $db->fetch_array( $r ) )
            {
                $n = $n + intval( $d[ 'num_adult' ] ) + intval( $d[ 'num_child' ] );
            }

            return $n;
        }
    }

	/**
	 * Get Total Booking From Depart Node to Arrival Node
	 * 
	 * 
	 * @since 1.0.0
	 * @return integer Return Total Booking.
	 */
    function tb( $node_from, $node_to, $debug = 0 )
    {
        global $db;

        $rid = $this->get_route_by_node( $this->node[ $node_from ], $this->node[ $node_to ], 'rid' );

        $q = $db->prepare_query( 'SELECT * FROM ticket_booking_detail AS a WHERE a.status = %s AND a.rid = %d AND a.sparent = %d AND a.date = %s', 'pd', $rid, $this->sparent, $this->date );
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        if( $n == 0 )
        {
            return $n;
        }
        else
        {
            $n = 0;

            while( $d = $db->fetch_array( $r ) )
            {
                $n = $n + intval( $d[ 'num_adult' ] ) + intval( $d[ 'num_child' ] );
            }

            return $n;
        }
    }

	/**
	 * Get Total Booking That Will be Passed
	 * From Depart Node to Arrival Node
	 * 
	 * 
	 * @since 1.0.0
	 * @return integer Return Total Booking.
	 */
    function get_total_booking_the_next_node( $node_from, $node_to )
    {
        $index_from = $this->node_code[ $node_from ];
        $index_to   = $this->node_code[ $node_to ];
        $total      = 0;

        if( $index_from < $index_to )
        {
            $diff = $index_to - $index_from;

            if( $diff > 1 )
            {
                for( $i = 0; $i < $diff; $i++ )
                {
                    if( $i != 0 )
                    {
                        $from = $this->node_number[ $index_from + $i ];

                        for( $j = 0; $j < count( $this->node ); $j++ )
                        {
                            $to = $this->node_number[ $j ];

                            if( $from != $to )
                            {
                                $total = $total + $this->tb( $from, $to );
                            }
                        }
                    }
                }
            }   

            return $total;
        }
        elseif( $index_from > $index_to )
        {
            $diff = $index_from - $index_to;

            switch( $diff )
            {
                case 1:
                    $num_inc = 3;

                    break;
                case 2:
                    $num_inc = 2;

                    break;
                case 3:
                    $num_inc = 1;

                    break;
                case 4:
                    $num_inc = 0;

                    break;
                default:
                    $num_inc = 0;

                    break;
            }

            for( $i = 0; $i < $num_inc; $i++ )
            {
                if( $i != 0 )
                {
                    if( $index_from == 4 )
                    {
                        $start = $i;
                    }
                    else
                    {
                        $start = $index_from + $i;
                    }
                    if( $start > 4 )
                    {
                        $start = $start - 5;
                    }

                    $from = $this->node_number[ $start ];

                    for( $j = 0; $j < count( $this->node ); $j++ )
                    {
                        $to = $this->node_number[ $j ];

                        if( $from != $to )
                        {
                            $total = $total + $this->tb( $from, $to );
                        }
                    }
                }
            }

            return $total;
        }

        return $total;
    }

	/**
	 * Get Total Allotment By Depart Node
	 * 
	 * 
	 * @since 1.0.0
	 * @return integer Return Total Allotment.
	 */
    function get_total_available_allotment_from_node( $node )
    {
        switch( $node )
        {
        	case 'a':
                $tb = ( $this->tb( 'a', 'b' ) + $this->tb( 'a', 'c' ) + $this->tb( 'a', 'd' ) + $this->tb( 'a', 'e' ) ) + ( $this->tb( 'e', 'b' ) + $this->tb( 'e', 'c' ) + $this->tb( 'e', 'd' ) ) + ( $this->tb( 'd', 'b' ) + $this->tb( 'd', 'c' ) ) + $this->tb( 'c', 'b' );
                break;
            case 'b':
                $tb = ( $this->tb( 'b', 'c' ) + $this->tb( 'b', 'd' ) + $this->tb( 'b', 'e' ) + $this->tb( 'b', 'f' ) ) + ( $this->tb( 'f', 'c' ) + $this->tb( 'f', 'd' ) + $this->tb( 'f', 'e' ) ) + ( $this->tb( 'e', 'c' ) + $this->tb( 'e', 'd' ) ) + $this->tb( 'd', 'c' );
                break;
            case 'c':
                $tb = ( $this->tb( 'c', 'd' ) + $this->tb( 'c', 'e' ) + $this->tb( 'c', 'f' ) + $this->tb( 'c', 'a' ) ) + ( $this->tb( 'a', 'd' ) + $this->tb( 'a', 'e' ) + $this->tb( 'a', 'f' ) ) + ( $this->tb( 'f', 'd' ) + $this->tb( 'f', 'e' ) ) + $this->tb( 'e', 'd' );
                break;
            case 'd':
                $tb = ( $this->tb( 'd', 'e' ) + $this->tb( 'd', 'f' ) + $this->tb( 'd', 'a' ) + $this->tb( 'd', 'b' ) ) + ( $this->tb( 'b', 'e' ) + $this->tb( 'b', 'f' ) + $this->tb( 'b', 'a' ) ) + ( $this->tb( 'a', 'e' ) + $this->tb( 'a', 'f' ) ) + $this->tb( 'f', 'e' );
                break;
            case 'e':
                $tb = ( $this->tb( 'e', 'f' ) + $this->tb( 'e', 'a' ) + $this->tb( 'e', 'b' ) + $this->tb( 'e', 'c' ) ) + ( $this->tb( 'c', 'f' ) + $this->tb( 'c', 'a' ) + $this->tb( 'c', 'b' ) ) + ( $this->tb( 'b', 'f' ) + $this->tb( 'b', 'a' ) ) + $this->tb( 'a', 'f' );
                break;
            case 'f':
                $tb = ( $this->tb( 'f', 'a' ) + $this->tb( 'f', 'b' ) + $this->tb( 'f', 'c' ) + $this->tb( 'f', 'd' ) ) + ( $this->tb( 'd', 'a' ) + $this->tb( 'd', 'b' ) + $this->tb( 'd', 'c' ) ) + ( $this->tb( 'c', 'a' ) + $this->tb( 'c', 'b' ) ) + $this->tb( 'b', 'a' );
                break;
        }

        $total = $this->allotment - $tb;

        return $total;
    }

	/**
	 * Get Total Allotment By Arrival Node
	 * 
	 * 
	 * @since 1.0.0
	 * @return integer Return Total Allotment.
	 */
    function get_total_available_allotment_to_node( $node_to )
    {
        $total = $this->allotment - $this->get_total_booking_to_destination( $node_to );

        return $total;
    }

	/**
	 * Get Total Allotment By All Node
	 * 
	 * 
	 * @since 1.0.0
	 * @return integer Return Total Allotment.
	 */
    function get_total_available_ticket_from_x_to_y()
    {
        $available_from = $this->get_total_available_allotment_from_node( $this->node_from ) - $this->get_total_booking_the_next_node( $this->node_from, $this->node_to );
        $available_to   = $this->get_total_available_allotment_to_node( $this->node_to );

        if( $available_from <= $available_to )
        {
            return $available_from;
        }
        else
        {
            return $available_to;
        }
    }

	/**
	 * Get Availability Status
	 * 
	 * 
	 * @since 1.0.0
	 * @return boolean Return Availability Status.
	 */
    function is_available()
    {
        if( $this->num_passenger <= $this->get_total_available_ticket_from_x_to_y( $this->node_from, $this->node_to ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

?>