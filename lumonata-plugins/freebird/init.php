<?php

/*
    Plugin Name: Freebird Ticketing
    Plugin URL: 
    Description: It's plugin is use for Freebird Express ticketing system
    Author: Ngurah
    Author URL: http://lumonata.com/about-us
    Version: 1.0.0
    Created Date : 23 July 2022
*/

date_default_timezone_set( 'UTC' );

add_actions( 'css_elements', 'get_custom_css', SITE_URL . '/lumonata-plugins/freebird/css/admin.css' );
add_actions( 'tab_admin_aditional', 'init_additional_setting', array( 'static' => 'Others' ) );
add_actions( 'dashboard_content', 'init_dashboard_content' );

add_privileges( 'administrator', 'ticket', 'insert' );
add_privileges( 'administrator', 'ticket', 'update' );
add_privileges( 'administrator', 'ticket', 'delete' );

add_main_menu( array( 'accommodation' => 'Accommodation' ) );
add_main_menu( array( 'ticket' => 'Booking Engine' ) );
add_main_menu( array( 'promo' => 'Promo' ) );

add_sub_menu( 'ticket', array(
    'booking'      => 'Booking',
    'calendar'     => 'Calendar',
    'boat'         => 'Boat',
    'route'        => 'Route & Price',
    'schedule'     => 'Schedule',
    'pick_trans'   => 'Pickup & Transfer Rate',
    'special_rate' => 'Special Rate',
    'bank_account' => 'Bank Account'
));

add_actions( 'ticket', 'init_ticket' );

//-- Admin
include( 'include/functions.php' );
include( 'include/setting.php' );
include( 'include/route.php' );
include( 'include/schedule.php' );
include( 'include/special_rate.php' );
include( 'include/boat.php' );
include( 'include/pick_trans.php' );
include( 'include/bank_account.php' );
include( 'include/booking.php' );
include( 'include/calendar.php' );

//-- Frontend
include( 'include/front.php' );