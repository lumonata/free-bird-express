/*
 Navicat Premium Data Transfer

 Source Server         : Server
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : arunna

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 04/08/2019 20:34:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lumonata_additional_fields
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_additional_fields`;
CREATE TABLE `lumonata_additional_fields`  (
  `lapp_id` bigint(20) NOT NULL,
  `lkey` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lvalue` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lapp_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`lapp_id`, `lkey`) USING BTREE,
  INDEX `key`(`lkey`) USING BTREE,
  INDEX `app_name`(`lapp_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for lumonata_additional_rule_fields
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_additional_rule_fields`;
CREATE TABLE `lumonata_additional_rule_fields`  (
  `lrule_id` bigint(20) NOT NULL,
  `lkey` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lvalue` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lapp_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`lrule_id`, `lkey`) USING BTREE,
  INDEX `key`(`lkey`) USING BTREE,
  INDEX `app_name`(`lapp_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for lumonata_articles
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_articles`;
CREATE TABLE `lumonata_articles`  (
  `larticle_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `larticle_title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `larticle_brief` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `larticle_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `larticle_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `larticle_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lcomment_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lcomment_count` bigint(20) NULL DEFAULT 0,
  `lcount_like` bigint(20) NULL DEFAULT 0,
  `lsef` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lorder` bigint(20) NULL DEFAULT 1,
  `lpost_by` bigint(20) NULL DEFAULT 0,
  `lpost_date` datetime(0) NULL DEFAULT NULL,
  `lupdated_by` bigint(20) NULL DEFAULT 0,
  `ldlu` datetime(0) NULL DEFAULT NULL,
  `lshare_to` bigint(20) NULL DEFAULT 0,
  PRIMARY KEY (`larticle_id`) USING BTREE,
  INDEX `article_title`(`larticle_title`(255)) USING BTREE,
  INDEX `type_status_date_by`(`larticle_type`, `larticle_status`, `lpost_date`, `lpost_by`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for lumonata_attachment
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_attachment`;
CREATE TABLE `lumonata_attachment`  (
  `lattach_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `larticle_id` bigint(20) NOT NULL,
  `lattach_loc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lattach_loc_thumb` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lattach_loc_medium` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lattach_loc_large` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lapp_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ltitle` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lcontent` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lalt_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lcaption` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mime_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lorder` bigint(20) NULL DEFAULT 1,
  `upload_date` datetime(0) NULL DEFAULT NULL,
  `date_last_update` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`lattach_id`) USING BTREE,
  INDEX `article_id`(`larticle_id`) USING BTREE,
  INDEX `attachment_title`(`ltitle`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for lumonata_comments
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_comments`;
CREATE TABLE `lumonata_comments`  (
  `lcomment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `larticle_id` bigint(20) NOT NULL,
  `lcomment_parent` bigint(20) NULL DEFAULT 0,
  `lcomentator_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `lcomentator_email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `lcomentator_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `lcomentator_ip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `lcomment_date` datetime(0) NULL DEFAULT NULL,
  `lcomment` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lcomment_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `lcomment_like` bigint(20) NULL DEFAULT 0,
  `luser_id` bigint(20) NOT NULL,
  `lcomment_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'like,comment,like_comment',
  PRIMARY KEY (`lcomment_id`) USING BTREE,
  INDEX `lcomment_status`(`lcomment_status`) USING BTREE,
  INDEX `lcomment_userid`(`luser_id`) USING BTREE,
  INDEX `lcomment_type`(`lcomment_type`) USING BTREE,
  INDEX `larticle_id`(`larticle_id`) USING BTREE,
  CONSTRAINT `lumonata_comments_ibfk_1` FOREIGN KEY (`larticle_id`) REFERENCES `lumonata_articles` (`larticle_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `lumonata_comments_ibfk_2` FOREIGN KEY (`luser_id`) REFERENCES `lumonata_users` (`luser_id`) ON DELETE RESTRICT ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for lumonata_friends_list
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_friends_list`;
CREATE TABLE `lumonata_friends_list`  (
  `lfriends_list_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `luser_id` bigint(20) NOT NULL,
  `llist_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lorder` bigint(20) NULL DEFAULT 1,
  PRIMARY KEY (`lfriends_list_id`) USING BTREE,
  INDEX `luser_id`(`luser_id`) USING BTREE,
  CONSTRAINT `lumonata_friends_list_ibfk_1` FOREIGN KEY (`luser_id`) REFERENCES `lumonata_users` (`luser_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for lumonata_friends_list_rel
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_friends_list_rel`;
CREATE TABLE `lumonata_friends_list_rel`  (
  `lfriendship_id` bigint(20) NOT NULL,
  `lfriends_list_id` bigint(20) NOT NULL,
  PRIMARY KEY (`lfriendship_id`, `lfriends_list_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for lumonata_friendship
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_friendship`;
CREATE TABLE `lumonata_friendship`  (
  `lfriendship_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `luser_id` bigint(20) NOT NULL,
  `lfriend_id` bigint(20) NOT NULL,
  `lstatus` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'connected, onrequest, pending, unfollow',
  PRIMARY KEY (`lfriendship_id`) USING BTREE,
  INDEX `luser_id`(`luser_id`) USING BTREE,
  CONSTRAINT `lumonata_friendship_ibfk_1` FOREIGN KEY (`luser_id`) REFERENCES `lumonata_users` (`luser_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for lumonata_language
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_language`;
CREATE TABLE `lumonata_language`  (
  `llang_id` int(11) NOT NULL AUTO_INCREMENT,
  `llanguage` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `llanguage_code` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lstatus` smallint(3) NOT NULL DEFAULT 0 COMMENT '0=Non Active;1=Active',
  `ldefault` smallint(3) NOT NULL COMMENT '0=Non;1=Yes',
  `lorder` bigint(20) NOT NULL,
  `lpost_by` bigint(20) NOT NULL,
  `lpost_date` datetime(0) NOT NULL,
  `lupdated_by` bigint(20) NOT NULL,
  `ldlu` datetime(0) NOT NULL,
  PRIMARY KEY (`llang_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for lumonata_meta_data
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_meta_data`;
CREATE TABLE `lumonata_meta_data`  (
  `lmeta_id` int(11) NOT NULL AUTO_INCREMENT,
  `lmeta_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lmeta_value` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lapp_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lapp_id` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`lmeta_id`) USING BTREE,
  INDEX `meta_name`(`lmeta_name`) USING BTREE,
  INDEX `app_name`(`lapp_name`) USING BTREE,
  INDEX `app_id`(`lapp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of lumonata_meta_data
-- ----------------------------
INSERT INTO `lumonata_meta_data` VALUES (1, 'front_theme', 'custom', 'themes', 0);
INSERT INTO `lumonata_meta_data` VALUES (2, 'admin_theme', 'default', 'themes', 0);
INSERT INTO `lumonata_meta_data` VALUES (3, 'custome_bg_color', 'FFF', 'themes', 0);
INSERT INTO `lumonata_meta_data` VALUES (4, 'active_plugins', '{\"lumonata-meta-data\":\"\\/metadata\\/metadata.php\"}', 'plugins', 0);
INSERT INTO `lumonata_meta_data` VALUES (5, 'time_zone', 'Asia/Singapore', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (6, 'site_url', 'localhost/arunna', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (7, 'web_title', 'Arunna                                                  ', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (8, 'smtp_server', 'localhost', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (9, 'smtp', 'mail.lumonatalabs.com', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (10, 'email', 'dev@lumonata.com', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (11, 'web_tagline', '', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (12, 'invitation_limit', '10', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (13, 'date_format', 'F j, Y', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (14, 'time_format', 'H:i', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (15, 'post_viewed', '6', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (16, 'rss_viewed', '15', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (17, 'rss_view_format', 'full_text', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (18, 'list_viewed', '50', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (19, 'email_format', 'html', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (20, 'text_editor', 'tiny_mce', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (21, 'thumbnail_image_size', '300:300', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (22, 'large_image_size', '1024:1024', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (23, 'medium_image_size', '700:700', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (24, 'is_allow_comment', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (25, 'is_login_to_comment', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (26, 'is_auto_close_comment', '0', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (27, 'days_auto_close_comment', '15', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (28, 'is_break_comment', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (29, 'comment_page_displayed', 'last', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (30, 'comment_per_page', '3', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (31, 'save_changes', 'Save Changes', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (32, 'is_rewrite', 'yes', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (33, 'is_allow_post_like', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (34, 'is_allow_comment_like', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (35, 'alert_on_register', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (36, 'alert_on_comment', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (37, 'alert_on_comment_reply', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (38, 'alert_on_liked_post', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (39, 'alert_on_liked_comment', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (40, 'web_name', 'Arunna                                                  ', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (41, 'meta_description', '', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (42, 'meta_keywords', 'Arunna', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (43, 'meta_title', 'Arunna', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (44, 'status_viewed', '10', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (45, 'update', 'true', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (46, 'the_date_format', 'F j, Y', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (47, 'the_time_format', 'H:i', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (48, 'thumbnail_image_width', '300', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (49, 'thumbnail_image_height', '300', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (50, 'medium_image_width', '700', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (51, 'medium_image_height', '700', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (52, 'large_image_width', '1024', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (53, 'large_image_height', '1024', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (54, 'author', '{\"name\":\"Author\",\"privileges\":[\"dashboard\",\"status\",\"applications\",\"articles\",\"pages\",\"friends\",\"notifications\",\"people\"]}', 'user_privileges', 0);
INSERT INTO `lumonata_meta_data` VALUES (55, 'standard', '{\"name\":\"Standard\",\"privileges\":[\"dashboard\",\"status\",\"friends\",\"notifications\",\"people\"]}', 'user_privileges', 0);
INSERT INTO `lumonata_meta_data` VALUES (56, 'contributor', '{\"name\":\"Contributor\",\"privileges\":[\"dashboard\",\"status\",\"applications\",\"comments\",\"articles\",\"pages\",\"friends\",\"notifications\",\"people\"]}', 'user_privileges', 0);
INSERT INTO `lumonata_meta_data` VALUES (57, 'editor', '{\"name\":\"Editor\",\"privileges\":[\"dashboard\",\"status\",\"applications\",\"comments\",\"articles\",\"blogs\",\"pages\",\"categories\",\"tags\",\"friends\",\"notifications\",\"people\"]}', 'user_privileges', 0);
INSERT INTO `lumonata_meta_data` VALUES (58, 'administrator', '{\"name\":\"Administrator\",\"privileges\":[\"dashboard\",\"status\",\"global_settings\",\"menus\",\"applications\",\"plugins\",\"themes\",\"comments\",\"articles\",\"blogs\",\"media\",\"pages\",\"categories\",\"tags\",\"users\",\"friends\",\"notifications\",\"people\",\"metadata\"]}', 'user_privileges', 0);
INSERT INTO `lumonata_meta_data` VALUES (59, 'analytic_view_id', '', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (60, 'multi_language', '1', 'global_setting', 0);

-- ----------------------------
-- Table structure for lumonata_notifications
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_notifications`;
CREATE TABLE `lumonata_notifications`  (
  `lnotification_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lpost_id` bigint(20) NOT NULL,
  `lpost_owner` bigint(20) NOT NULL,
  `luser_id` bigint(20) NOT NULL,
  `laffected_user` bigint(20) NOT NULL,
  `laction_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `laction_date` date NOT NULL,
  `lstatus` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `lshare_to` bigint(20) NOT NULL,
  PRIMARY KEY (`lnotification_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for lumonata_rule_relationship
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_rule_relationship`;
CREATE TABLE `lumonata_rule_relationship`  (
  `lapp_id` bigint(20) NOT NULL,
  `lrule_id` bigint(20) NOT NULL,
  `lorder_id` bigint(20) NULL DEFAULT 1,
  PRIMARY KEY (`lapp_id`, `lrule_id`) USING BTREE,
  INDEX `taxonomy_id`(`lrule_id`) USING BTREE,
  CONSTRAINT `lumonata_rule_relationship_ibfk_1` FOREIGN KEY (`lrule_id`) REFERENCES `lumonata_rules` (`lrule_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for lumonata_rules
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_rules`;
CREATE TABLE `lumonata_rules`  (
  `lrule_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lparent` bigint(20) NULL DEFAULT 0,
  `lname` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lsef` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ldescription` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lrule` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lgroup` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lcount` bigint(20) NULL DEFAULT 0,
  `lorder` bigint(20) NULL DEFAULT 1,
  `lsubsite` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT 'arunna',
  PRIMARY KEY (`lrule_id`) USING BTREE,
  INDEX `rules_name`(`lname`) USING BTREE,
  INDEX `sef`(`lsef`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of lumonata_rules
-- ----------------------------
INSERT INTO `lumonata_rules` VALUES (1, 0, 'Uncategorized', 'uncategorized', '', 'category', 'default', 0, 14, 'arunna');

-- ----------------------------
-- Table structure for lumonata_users
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_users`;
CREATE TABLE `lumonata_users`  (
  `luser_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lusername` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ldisplay_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lpassword` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lemail` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lregistration_date` datetime(0) NULL DEFAULT NULL,
  `luser_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lactivation_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lavatar` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lsex` int(11) NULL DEFAULT 1 COMMENT '1=male,2=female',
  `lbirthday` date NULL DEFAULT NULL,
  `lstatus` int(11) NULL DEFAULT 0 COMMENT '0=pendding activation, 1=active,2=blocked',
  `ldlu` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`luser_id`) USING BTREE,
  INDEX `username`(`lusername`) USING BTREE,
  INDEX `display_name`(`ldisplay_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of lumonata_users
-- ----------------------------
INSERT INTO `lumonata_users` VALUES (1, 'admin', 'Administrator', 'fcea920f7412b5da7be0cf42b8c93759', 'dev@lumonata.com', '0000-00-00 00:00:00', 'administrator', '', 'admin-1.png|admin-2.png|admin-3.png', 1, '2011-03-19', 1, '2015-03-09 15:52:57');

SET FOREIGN_KEY_CHECKS = 1;
