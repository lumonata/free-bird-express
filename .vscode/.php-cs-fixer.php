<?php

$config = new PhpCsFixer\Config();

return $config->setRules( array(
    '@PSR12'                                  => true,
    'control_structure_continuation_position' => array(
        'position' => 'next_line',
    ),
    'curly_braces_position' => array(
        'control_structures_opening_brace' => 'next_line_unless_newline_at_signature_end',
    ),
    'array_syntax'         => array( 'syntax' => 'long' ),
    'function_declaration' => array(
        'closure_function_spacing' => 'none',
    ),
    'binary_operator_spaces' => array(
        'operators' => array(
            '=>' => 'align_single_space_minimal',
            '='  => 'align_single_space_minimal',
        ),
    ),
    'spaces_inside_parentheses' => array( 'space' => 'single' ),
) )->setLineEnding( "\n" );
