<!--
Author: WebThemez
Author URL: http://webthemez.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
Note: Please use our back link in your site (webthemez.com)
-->
<!DOCTYPE HTML>
<!--[if lt IE 7 ]> <html lang="en" class="ie ie6"> <![endif]--> 
<!--[if IE 7 ]>	<html lang="en" class="ie ie7"> <![endif]--> 
<!--[if IE 8 ]>	<html lang="en" class="ie ie8"> <![endif]--> 
<!--[if IE 9 ]>	<html lang="en" class="ie ie9"> <![endif]--> 
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<title>Freebird Express - Page under construction</title>
<meta name="description" content="Freebird Express Page under construction">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:regular,bold"> 
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body id="home">
<div id="Header">
<div class="wrapper">
	<h1>Freebird Express</h1>	
	</div>
</div>
<div id="Content" class="wrapper"> 
<h2 class="intro">Our website is under construction. If you have any questions please contact us at <a href="mailto:info@freebird-express.com">info@freebird-express.com</a></h2>
<span class="tempBy">All Rights Reserved - by freebird express</span>
</div>

<div id="overlay"></div>

<!--Scripts-->
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/Backstretch.js"></script>
<script type="text/javascript" src="js/jquery.countdown.js"></script>
<script type="text/javascript" src="js/global.js"></script>

</body>
</html>
